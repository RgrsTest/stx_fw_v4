<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new class(dirname(dirname(__DIR__))) extends Illuminate\Foundation\Application {

  protected $path;
  protected $configPath;
  protected $publicPath;
  protected $resourcesPath;
  protected $bootstrapPath;

  protected $kw2p_instancia;
  protected $kw2p_ambito;

  /**
   * Create a new Illuminate application instance.
   *
   * @param  string|null  $basePath
   * @return void
   */
  public function __construct($basePath = null)
  {
    $this->useBasePath($basePath);
    $this->usePath( dirname(__DIR__).DIRECTORY_SEPARATOR.'app' );
    $this->useConfigPath( dirname(__DIR__).DIRECTORY_SEPARATOR.'config' );
    $this->useStoragePath( dirname(__DIR__).DIRECTORY_SEPARATOR.'storage' );
    $this->useDatabasePath( dirname(__DIR__).DIRECTORY_SEPARATOR.'database' );
    $this->useResourcePath( dirname(__DIR__).DIRECTORY_SEPARATOR.'resources' );
    $this->useBootstrapPath( dirname(__DIR__).DIRECTORY_SEPARATOR.'bootstrap' );

    parent::__construct($basePath);
  }

  /**
   * Get the path to the bootstrap directory.
   *
   * @param  string  $path Optionally, a path to append to the bootstrap path
   * @return string
   */
  public function basePath($path = '')
  {
    return $this->anyPath('base', $path);
  }

  /**
   * Set the basepath directory.
   *
   * @param  string  $path
   * @return $this
   */
  public function useBasePath($basePath = '')
  {
    return $this->useAnyPath('base', rtrim($basePath, '\/'));
  }

  public function anyPath($anyName, $anyPath = '') {
    return ($this->{$anyName == 'app' ? 'path' : $anyName.'Path'} ?: $this->basePath.DIRECTORY_SEPARATOR.$anyName).($anyPath ? DIRECTORY_SEPARATOR.$anyPath : $anyPath);
  }

  public function useAnyPath($anyName, $anyPath = '') {
    $this->{$anyName == 'app' ? 'path' : $anyName.'Path'} = $anyPath;

    $this->instance($anyName == 'app' ? 'path' : 'path'.'.'.$anyName, $anyPath);

    return $this;
  }

  public function path($path = '')
  {
    return $this->anyPath('app', $path);
  }

  public function usePath($path = '')
  {
    return $this->useAnyPath('app', $path);
  }

  public function configPath($path = '')
  {
    return $this->anyPath('config', $path);
  }

  public function useConfigPath($configPath = '')
  {
    return $this->useAnyPath('config', $configPath);
  }

  public function publicPath($path = '')
  {
    return $this->anyPath('public', $path);
  }

  public function usePublicPath($publicPath = '')
  {
    return $this->useAnyPath('public', $publicPath);
  }

  public function storagePath($path = '')
  {
    return $this->anyPath('storage', $path);
  }

  public function useStoragePath($storagePath = '')
  {
    return $this->useAnyPath('storage', $storagePath);
  }

  public function databasePath($path = '')
  {
    return $this->anyPath('database', $path);
  }

  public function useDatabasePath($databasePath = '')
  {
    return $this->useAnyPath('database', $databasePath);
  }

  public function resourcePath($path = '')
  {
    return $this->anyPath('resources', $path);
  }

  public function useResourcePath($resourcePath = '')
  {
    return $this->useAnyPath('resources', $resourcePath);
  }

  public function bootstrapPath($path = '')
  {
    return $this->anyPath('bootstrap', $path);
  }

  public function useBootstrapPath($bootstrapPath = '')
  {
    return $this->useAnyPath('bootstrap', $bootstrapPath);
  }

  public function setKw2pInstancia($instancia)
  {
    $this->kw2p_instancia = $instancia;
  }

  public function kw2pInstancia()
  {
    return $this->kw2p_instancia;
  }

  public function setKw2pAmbito($ambito)
  {
    $this->kw2p_ambito = $ambito;
  }

  public function kw2pAmbito()
  {
    return $this->kw2p_ambito ?: $this->kw2p_instancia;
  }

};

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
  Illuminate\Contracts\Http\Kernel::class,
  Klayware\Http\Kernel::class
);

$app->singleton(
  Illuminate\Contracts\Console\Kernel::class,
  Klayware\Console\Kernel::class
);

$app->singleton(
  Illuminate\Contracts\Debug\ExceptionHandler::class,
  Klayware\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
