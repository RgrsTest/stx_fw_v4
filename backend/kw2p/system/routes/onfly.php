<?php

# Expresion regular para
$match = [];
if (preg_match('/^(?P<instancia>[a-z0-9_\-]+)(?:\:(?P<ambito>[a-z0-9_\-]+))?\/(?P<action>[^\/][a-z_\-\/]+[^\/]):(?P<metodo>[a-z_\-]+)$/', request()->path(), $match)) {
  $toMap = array_merge([$match['instancia']], ['actions'], explode('/', $match['action']));
  # Registramos provedor de instancia
  // $provider = [$match['instancia'], 'Providers', $match['instancia'].'ServiceProvider'];
  // $provider = implode('\\', array_map('studly_case', $provider));
  // App::register("\\{$provider}");
  #
  $action = implode('\\', array_map('studly_case', $toMap));
  $method = camel_case($match['metodo']);
  # Resolvemos ruta
  Route::any(($match['ambito'] ? '/{instancia}:{ambito}/' : '/{instancia}/')."{$match['action']}:{$match['metodo']}", "\\{$action}Action@{$method}");
  Route::fallback(function () {});
}
