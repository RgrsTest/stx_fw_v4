<?php

namespace Klayware\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Klayware\Exceptions\KlayException;
use Psr\Log\LoggerInterface;
use ReflectionException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
      //
      KlayException::class,
      \BadMethodCallException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
      'password',
      'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $e)
    {
      if ($this->shouldntReport($e)) {
        return;
      }

      if (method_exists($e, 'report')) {
        return $e->report();
      }

      try {
        $logger = $this->container->make(LoggerInterface::class);
      } catch (Exception $ex) {
        throw $e;
      }

      $logger->channel(kw2p_ambito())->error(
        $e->getMessage(),
        array_merge($this->context(), ['exception' => $e]
      ));
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      if ($exception instanceof \BadMethodCallException) {

        $traces = [];
        foreach ($exception->getTrace() as $k => $trace) {
          if (in_array($trace['function'], ['__call'])) {
            $traces[] = $k;
          }
          elseif (!empty($traces)) {
            if (in_array($trace['function'], ['__callStatic'])) {
             continue;
            }
            $traces[] = $k;
            break;
          }
        }

        $trace = $exception->getTrace()[$traces[1]];

        // Actions
        if ($trace['function'] == 'call_user_func_array') {
            $trace['class'] = get_class($trace['args'][0][0]);
            $trace['function'] = $trace['args'][0][1];
        }

        if ($exception->getTrace()[$traces[0]]['args'][0] == $trace['function']) {
          $code = 'metodo_no_encontrado';
        } else {
          $code =  $exception->getTrace()[$traces[0]]['args'][0] . '.metodo_no_encontrado';
        }

        $klay_exception = (new KlayException('Metodo no encontrado'))->code($code, $trace);

        return response()->json([
          'status' => 'fail',
          'message' => $klay_exception->getMessage()
        ] + $klay_exception->errorsAndOrCode, 404);
      }

      if ($exception instanceof ModelNotFoundException) {

        $model = last(explode('\\', $exception->getModel()));

        $functions = ['findOrFail','firstOrFail'];

        $traces = [];
        foreach ($exception->getTrace() as $k => $trace) {
          if (in_array($trace['function'], $functions) || in_array(array_get($trace, 'args.0'), $functions)) {
            $traces[] = $k;
          }
        }

        $ids = $exception->getIds() ? ':'. implode(',', $exception->getIds()) : '';

        $trace = $exception->getTrace()[last($traces)+1];

        $klay_exception = (new KlayException("No hay resultados de consulta [{$model}{$ids}]."))->code("{$model}:fail", $trace);

        return response()->json([
          'status' => 'fail',
          'message' => $klay_exception->getMessage()
        ] + $klay_exception->errorsAndOrCode, 404);
      }

      if ($exception instanceof ValidationException) {

        $traces = [];
        foreach ($exception->getTrace() as $k => $trace) {
          if ($trace['function'] == 'validate' || array_get($trace, 'args.0') == 'validate') {
            $traces[] = $k;
          }
        }

        $trace = $exception->getTrace()[last($traces)+1];

        $klay_exception = (new KlayException($exception->errors()))->code('argumentos_invalidos', $trace);

        return response()->json([
          'status' => 'fail',
          'message' => $klay_exception->getMessage()
        ] + $klay_exception->errorsAndOrCode, $exception->status);
      }

      if ($exception instanceof ReflectionException) {
        $action = preg_filter('/^.*\\\\([^\\\\]+) does not exist+$/', '$1', $exception->getMessage());
        if ($action) {
          return response()->json([
            'status' => 'fail',
            'message' => 'Action inexistente',
          ] + ['code' => 'action_inexistente'], 404);
        }
      }

      # ¿Es KlayException?
      if ($exception instanceof KlayException) {
        return response()->json([
          'status' => 'fail',
          'message' => $exception->getMessage()
        ] + $exception->errorsAndOrCode, $exception->status);
      }




      return $this->prepareJsonResponse($request, $exception);
    }

    /**
     * Convert the given exception to an array.
     *
     * @param  \Exception  $e
     * @return array
     */
    protected function convertExceptionToArray(Exception $e)
    {
      return config('app.debug') ? [
        'message' => $e->getMessage(),
        'exception' => get_class($e),
        'file' => $e->getFile(),
        'line' => $e->getLine(),
        'trace' => collect($e->getTrace())->map(function ($trace) {
          return \Illuminate\Support\Arr::except($trace, ['args']);
        })->all(),
      ] : [
        'status' => 'error',
        'message' => $this->isHttpException($e) ? $e->getMessage() : 'Server Error',
      ];
    }

  }
