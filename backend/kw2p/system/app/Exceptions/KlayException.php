<?php

namespace Klayware\Exceptions;

class KlayException extends \Exception
{
    /**
     * Mensaje de excepción
     * @var string
     */
    protected $message = '';

    /**
     * Codigo de respuesta HTTP
     * @var integer
     */
    protected $code = 400;

    /**
     * The status code to use for the response.
     *
     * @var int
     */
    public $status = 400;

    public $errorsAndOrCode = [];

    public function __construct($messageOrErrors, $codeOrErrors = [])
    {
        if (is_array($messageOrErrors)) {
            $this->message = 'Los datos dados no eran válidos.';
            $this->errorsAndOrCode = ['errors' => $messageOrErrors];
        } else {
            $this->message = $messageOrErrors;
        }

        if (is_array($codeOrErrors)) {
            if (!empty($codeOrErrors)) {
                $this->errorsAndOrCode = [
                    'errors' => $codeOrErrors,
                ];
            }
        } else {
            $this->code($codeOrErrors, head($this->getTrace()));
        }
    }

    /**
     * Set the HTTP status code to be used for the response.
     *
     * @param  int  $status
     * @return $this
     */
    public function status($status)
    {
        $this->status = $status;

        return $this;
    }

    public function code($code, array $trace)
    {
        $code = "{$trace['class']}.{$trace['function']}.{$code}";

        $code = str_replace('\\', '.', $code);

        $code = str_replace_last('Action', '', $code);

        $code = str_replace_last('Batch', '', $code);

        $code = str_replace_last('Step', '', $code);

        $code = str_replace_last('Query', '', $code);

        $code = str_replace('._', '.', snake_case($code));

        $this->errorsAndOrCode = $this->errorsAndOrCode + [
          'code' => $code,
        ];

        return $this;
    }

    function toArray($params = null) {
        return [
            'status' => 'fail',
            'message' => $this->message,
        ] + $this->errorsAndOrCode + ($params ? ['params' => $params] : []);
    }
}
