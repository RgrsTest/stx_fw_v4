<?php

namespace Klayware\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;

class Kw2pServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
    # Extendemos carga de traducciones
    $this->loadTranslationsFrom(app_path('resources/lang'), kw2p_ambito());
  }

  /**
  * Register any application services.
  *
  * @return void
  */
  public function register()
  {
    # lista de instancias
    $kw2p_instancias = [];

    $loader = require base_path() . '/vendor/autoload.php';

    # Obtenemos instancias y registramos namespace
    foreach (glob(base_path('apps/*')) as $app) {
      $kw2p_instancias[basename($app)] = $app;
      $loader->setPsr4(basename($app).'\\', $app . DIRECTORY_SEPARATOR . 'app');
      $loader->setPsr4(basename($app).'Seeder\\', $app . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'seeds');
    }

    # ¿artisan?
    if ($this->app->runningInConsole()) {

      # Extendemos configuración de conexiones
      foreach ($kw2p_instancias as $instancia => $path) {

        $this->mergeConfig([
          $instancia => [
            'driver' => 'single',
            'path' => $path.'/storage/logs/'.camel_case($instancia).'.log',
            'level' => 'debug',
          ]
        ], 'logging.channels');

        if ($database = realpath("{$path}/config/database.php")) {
          $this->mergeConfigFrom($database, 'database');
        }

        $this->registerEloquentFactoriesFrom("{$path}/database/factories");

      }

    } else {

      # Instancia y ambito
      $segment = app(\Illuminate\Http\Request::class)->segment(1);
      list($instancia, $ambito) = array_pad(array_map('studly_case', explode(':', $segment)), 2, null);

      # ¿Existe instancia en apps?
      if (array_key_exists($instancia, $kw2p_instancias)) {

        #
        $this->app->setKw2pInstancia($instancia);

        if ($ambito != null) {
          if (array_key_exists($ambito, $kw2p_instancias)) {
            $this->app->setKw2pAmbito($ambito);
          } else {
            throw (new \Klayware\Exceptions\KlayException('Ambito inexistente', 'ambito_inexistente'))->status(404);
          }
        }

        # Levantamos instancia
        $this->appSetup($kw2p_instancias[kw2p_instancia()], kw2p_instancia());

        # Si ambito existe, extendemos configuración
        if (kw2p_instancia() != kw2p_ambito()) {
          $this->appSetup($kw2p_instancias[kw2p_ambito()], kw2p_ambito());
        }
      }

      // $this->app->register(
      //     'Klay\Lingo\Providers\AppServiceProvider'
      // );

      $this->app->singleton('auth', function($app) {
        return new \Klayware\KlayAuthManager($app);
      });
    }

    // dump([
    //   'path' => $this->app->path(),
    //   'path.base' => $this->app->basePath(),
    //   'path.lang' => $this->app->langPath(),
    //   'path.config' => $this->app->configPath(),
    //   'path.public' => $this->app->publicPath(),
    //   'path.storage' => $this->app->storagePath(),
    //   'path.database' => $this->app->databasePath(),
    //   'path.resources' => $this->app->resourcePath(),
    //   'path.bootstrap' => $this->app->bootstrapPath(),
    // ]);

  }

  protected function appSetup($app_path, $namespace)
  {
    # Re-asignamos ubicacion de aplicación
    $this->app->usePath($app_path);
    $this->app->useStoragePath(app_path('storage'));

    # Extendemos carga de vistas
    $this->loadViewsFrom(app_path('resources/views'), $namespace);

    if ($this->app->environment(['produccion', 'sandbox'])) {
      $this->mergeConfig([
        'papertrail' => [
          'handler_with' => [
            'host' => 'logs3.papertrailapp.com',
            'port' => '40678',
            'ident' => $namespace
          ],
        ],
        $namespace => [
          'driver' => 'stack',
          'channels' => ['papertrail', $namespace . 'Server'],
        ],
        $namespace . 'Server' => [
          'driver' => 'single',
          'path' => $app_path.'/storage/logs/'. camel_case($namespace) . '.log',
          'level' => 'debug',
        ],
      ], 'logging.channels');
    } else {
      $this->mergeConfig([
        $namespace => [
          'driver' => 'single',
          'path' => $app_path.'/storage/logs/'. camel_case($namespace) . '.log',
          'level' => 'debug',
        ]
      ], 'logging.channels');
    }

    # Extendemos configuración de ambito/instancia
    foreach (glob(app_path('config/*.php')) as $config) {
      $this->mergeConfigFrom($config, basename($config, '.php'));
    }

    # Resolvemos conexion default
    config(['database.default' => $namespace]);
  }

  /**
   * Merge the given configuration with the existing configuration.
   *
   * @param  string  $path
   * @param  string  $key
   * @return void
   */
  protected function mergeConfigFrom($path, $key)
  {
    $config = $this->app['config']->get($key, []);

    $this->app['config']->set($key, array_merge_recursive_keyed($config, require $path));
  }

  protected function mergeConfig($array, $key)
  {
    $config = $this->app['config']->get($key, []);

    $this->app['config']->set($key, array_merge_recursive_keyed($config, $array));
  }

  /**
   * Register factories.
   *
   * @param  string  $path
   * @return void
   */
  protected function registerEloquentFactoriesFrom($path)
  {
    $this->app->make(EloquentFactory::class)->load($path);
  }
}
