<?php

namespace Klayware\Providers;

use Illuminate\Auth\SessionGuard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
    * The policy mappings for the application.
    *
    * @var array
    */
    protected $policies = [
        'Klayware\Model' => 'Klayware\Policies\ModelPolicy',
    ];

    /**
    * Register any authentication / authorization services.
    *
    * @return void
    */
    public function boot()
    {
        // $this->registerPolicies();

        Gate::before(function($usuario, $credencial, $recursos) {
            if (starts_with($credencial, ':action')) {
                return $this->actionGate($usuario->credenciales, $recursos[0]);
            }
            if (starts_with($credencial, ':model')) {
                return $this->modelGate($usuario->credenciales, $recursos[0]);
            }
            if (starts_with($credencial, '@action')) {
                return $this->resourceActionGate($usuario->credenciales, $recursos[0]);
            }
            if (starts_with($credencial, '@resource')) {
                return $this->resourceGate($usuario->credenciales, $recursos);
            }
            if (in_array($credencial, $usuario->credenciales)) {
                return true;
            }
        });
    }

    public function actionGate($credentials, Request $action)
    {
        if (preg_match('/[^\/]+$/', $action->path(), $match)) {
            list($action, $method) = explode(':', $match[0]);
            return in_array("action:{$action},{$method}", $credentials) || in_array("action:{$action}", $credentials);
        }
        return false;
    }

    public function modelGate($credenciales, Model $eloquent)
    {
        $modelo = snake_case((new \ReflectionClass($eloquent))->getShortName());
        $primary = $eloquent->getKey();
        return in_array("model:{$modelo},{$primary}", $credenciales) || in_array("model:{$modelo}", $credenciales);
    }

    public function resourceActionGate($credentials, Request $action)
    {
        if (preg_match('/[^\/]+$/', $action->path(), $match)) {
            $request = array_combine(['action', 'method'], explode(':', $match[0]));
            if ($this->resourceGate($credentials, ['action', $request])) {
                return true;
            }
        }
    }

    public function resourceGate($credentials, $resource)
    {
        foreach ($credentials as $credential) {
            if (starts_with($credential, "@{$resource[0]}")) {
                [, $constraints] = explode(':', $credential, 2);

                if ($constraints === '*') {
                    return true;
                }

                $constraints = explode('|', $constraints );

                $rules = array_reduce($constraints, function ($result, $constraint) {
                    [$key, $value] = array_pad(explode('=', $constraint, 2), 2, null);
                    if ($value !== '*') {
                        $result[$key] = 'in:' . $value;
                    }
                    return $result;
                }, []);

                $missings = array_map(function() {}, array_diff_key($rules, $resource[1]));

                $data = array_merge($resource[1], $missings);

                $validator = \Validator::make($data, $rules);

                if ($validator->passes()) {
                    return true;
                }
            }
        }
    }
}
