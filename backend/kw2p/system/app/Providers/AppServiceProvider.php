<?php

namespace Klayware\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        \Validator::extend('imageable', function ($attribute, $value, $params, $validator) {
            try {
                \Image::make($value);
                return true;
            } catch (\Exception $e) {
                return false;
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
