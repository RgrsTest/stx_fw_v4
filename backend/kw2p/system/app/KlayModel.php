<?php

namespace Klayware;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Log;

class KlayModel extends EloquentModel
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Get the model's relationships in array form.
     *
     * @return array
     */
    public function relationsToArray()
    {
        $attributes = parent::relationsToArray();
        return !empty($attributes) ? ['_relaciones' => $attributes] : [];
    }
  }
