<?php

namespace Klayware;

class RelaxedJS
{

  protected $template;

  protected $delete_templete;

  function __construct($template = null)
  {
    throw_unless($template, \Exception::class);
    $this->template = $template;
  }

  public function toPdf($to_path = '')
  {
    if (!file_exists($this->template)) {
      throw new \Exception('Template no encontrado.');
    }

    if (($to_path ?: false) && !file_exists(pathinfo($to_path, PATHINFO_DIRNAME))) {
      throw new \Exception('Directorio de destino no encontrado.');
    }

    $orig_path = pathinfo($to_path ?: $this->template, PATHINFO_DIRNAME);
    $file_name = pathinfo($to_path ?: $this->template, PATHINFO_FILENAME);
    $dest_file = $to_path ?: $orig_path . DIRECTORY_SEPARATOR . $file_name . '.pdf';

    # Convertimos en pdf
    shell_exec("/usr/bin/sudo /usr/local/bin/relaxed {$this->template} {$dest_file} --bo");

    #
    unlink( $orig_path . DIRECTORY_SEPARATOR . $file_name . '_temp.htm' );

    return true;
  }

  public function deleteTemplete($delete = true)
  {
    if ($this->template) {
      unlink($this->template);
      return true;
    }

    return false;
  }

}
