<?php

namespace Klayware;

use Illuminate\Auth\AuthManager;
use Lcobucci\JWT\Parser as JWTParser;

class KlayAuthManager extends AuthManager
{
  /**
   * Create a new Auth manager instance.
   *
   * @param  \Illuminate\Foundation\Application  $app
   * @return void
   */
  public function __construct($app)
  {
    parent::__construct($app);
  }

  public function attemptOrFail(array $credentials)
  {
    if ($token = $this->attempt($credentials)) {
      return $token;
    }
    throw (new \Klayware\Exceptions\KlayException('Credenciales de acceso invalidas.'));
  }

  public function getClaims()
  {
    return $this->manager()->getJWTProvider()->decode($this->getToken()->get());
  }

}
