<?php

namespace Klayware\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
      \Klayware\Http\Middleware\CheckForMaintenanceMode::class,
      \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
      \Klayware\Http\Middleware\TrimStrings::class,
      // \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
      \Klayware\Http\Middleware\ConvertEmptyStringsToNull::class,
      \Klayware\Http\Middleware\TrustProxies::class,
      \Klayware\Http\Middleware\Cors::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
      'web' => [
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
      ],

      'oldweb' => [
        \Klayware\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Klayware\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
      ],

      'api' => [
        'throttle:60,1',
        'bindings',
      ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
      'auth' => \Klayware\Http\Middleware\Authenticate::class,
      'auth.basic' => \Klayware\Http\Middleware\BasicAuth::class,
      // 'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
      'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
      'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
      'can' => \Illuminate\Auth\Middleware\Authorize::class,
      'guest' => \Klayware\Http\Middleware\RedirectIfAuthenticated::class,
      'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
      'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
      'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
      'jwt.verify' => \Klayware\Http\Middleware\JwtVerify::class,
      'cors' => \Klayware\Http\Middleware\Cors::class,
      'auth.gate' => \Klayware\Http\Middleware\AuthGate::class,
    ];
  }
