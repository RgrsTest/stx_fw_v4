<?php

namespace Klayware\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Factory as AuthFactory;

class BasicAuth
{
    /**
     * The guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @param  string|null  $field
     * @return mixed
     */
    public function handle($request, Closure $next, $field = null)
    {
        Auth::shouldUse('basic');

        try {
          Auth::onceBasic($field ?: 'correo');
        } catch (\Exception $e) {
          throw (new \Klayware\Exceptions\KlayException('Credenciales invalidas.', 'credenciales_invalidas'))->status(401);
        }

        return $next($request);
    }
}
