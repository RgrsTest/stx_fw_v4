<?php

namespace Klayware\Http\Middleware;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getMethod() == 'OPTIONS') {
            return response('OK', 200, [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'POST',
                'Access-Control-Allow-Headers' => 'Content-Type, Authorization, Observe, cache-control, x-requested-with',
            ]);
        }

        $response = $next($request);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Expose-Headers', 'Authorization');
        return $response;
    }
}
