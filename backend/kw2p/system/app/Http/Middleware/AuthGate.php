<?php

namespace Klayware\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthGate
{
  /**
   * The gate instance.
   *
   * @var \Illuminate\Contracts\Auth\Access\Gate
   */
  protected $gate;

  /**
   * Create a new middleware instance.
   *
   * @param  \Illuminate\Contracts\Auth\Factory  $auth
   * @return void
   */
  public function __construct(Gate $gate)
  {
    $this->gate = $gate;
  }

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if (!$this->gate->allows(':action', $request) && !$this->gate->allows('@action', $request)) {
      throw (new \Klayware\Exceptions\KlayException('Usuario no cuenta con las credenciales requeridas.', 'credenciales_insuficientes'))->status(401);
    }
    return $next($request);
  }
}
