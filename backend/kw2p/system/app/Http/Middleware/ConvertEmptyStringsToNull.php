<?php

namespace Klayware\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull as Base;

class ConvertEmptyStringsToNull extends Base
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/archiving:*'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     */
    public function handle($request, Closure $next, ...$attributes)
    {
        if ($this->inExceptArray($request)) {
            return $next($request);
        }

        return parent::handle($request, $next, $attributes);
    }

    /**
     * Determine if the request has a URI that should pass through ConvertEmptyStringsToNull.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

}
