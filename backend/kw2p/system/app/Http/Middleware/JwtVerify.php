<?php

namespace Klayware\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtVerify extends BaseMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = '', $loginWithEmail = null)
  {
    #
    $jwt = app('tymon.jwt');
    $token = $jwt->parser()->parseToken();

    if (!$token && $loginWithEmail != null) {

      Auth::shouldUse($guard);

      $user = auth()->getProvider()->retrieveByCredentials([
        'correo' => $loginWithEmail
      ]);

      throw_unless($user, (new \Klayware\Exceptions\KlayException('Token de autorizacion no encontrado.', 'token_no_encontrado'))->status(401));

      Auth::login($user);

      return $next($request);
    }

    # Excepcion si no existe token
    throw_unless($token, (new \Klayware\Exceptions\KlayException('Token de autorizacion no encontrado.', 'token_no_encontrado'))->status(401));

    # Obtenemos payload
    try {
      $payload = $jwt->manager()->getJWTProvider()->decode($token);
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      throw (new \Klayware\Exceptions\KlayException('Token invalido.', 'token_invalido'))->status(401);
    }

    $guard = $payload['grd'] ?? $guard;

    # Asignamos guardia de ser necesario
    Auth::shouldUse($guard);

    try {
      throw_unless(Auth::byId($payload['sub']), (new \Klayware\Exceptions\KlayException('Usuario inexistente.', 'usuario_inexistente'))->status(401));
    } catch (\InvalidArgumentException $e) {
      throw (new \Klayware\Exceptions\KlayException("Guardia [{$guard}] inexistente.", 'guardia_inexistente'));
    }

    try {
      $sesion_key = auth()->user()->getFieldNameSession();
    } catch (\Exception $e) {
      $sesion_key = 'usuario_id';
    }

    # Si sesion no existe, el token es invalido
    try {
      $sesion = Auth::user()->sesiones()->where([
        [$sesion_key, '=', $payload['sub']],
        ['sesion', '=', $payload['ssn']],
      ])->firstOrFail();
    } catch (\Illuminate\Database\QueryException $e) {
      throw (new \Klayware\Exceptions\KlayException('Llave de sesión inexistente.', 'llave_inexistente'));
    } catch (\Exception $e) {
      throw (new \Klayware\Exceptions\KlayException('Usuario no tiene sesion activa.', 'sesion_inactiva'))->status(401);
    }

    # ¿Token expirado?. Eliminamos sesion
    try {
      Auth::setToken($token)->checkOrFail();
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      $sesion->delete();
      throw (new \Klayware\Exceptions\KlayException('Token expirado.', 'token_expirado'))->status(401);
    } catch (\Exception $e) {
      throw (new \Klayware\Exceptions\KlayException('Token invalido.', 'token_invalido'))->status(401);
    }

    $response = $next($request);

    $sesion->update([
      'vigencia' => now()->addMinutes(auth()->factory()->getTTL())
    ]);

    # Respuesta con token renovado
    return $this->setAuthenticationHeader($response, Auth::refresh());
  }
}
