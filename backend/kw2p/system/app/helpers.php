<?php

if (! function_exists('array_merge_recursive_keyed')) {
  /**
   * Similar to array_merge_recursive but keyed-valued are always overwritten
   * @param  array  $array1
   * @param  array  $array2
   * @return array
   */
  function array_merge_recursive_keyed( $array1,  $array2)
  {
    if (!is_array($array1) || !is_array($array2)) { return $array2; }
    foreach ($array2 AS $k => $v) {
      $array1[$k] = array_merge_recursive_keyed(@$array1[$k], $v);
    }
    return $array1;
  }
}

if (! function_exists('is_indexed')) {
  /**
   * ¿Arreglo de llaves numericas?
   * @param  $array
   * @return boolean
   */
  function is_indexed($array = [])
  {
    return ctype_digit(implode('', array_keys($array ?? [])));
  }
}

if (! function_exists('nanoId')) {
  /**
   * Generamos Id unico, rapido y seguro
   * @param  integer $size
   * @return string
   */
  function nanoId($size = 21)
  {
    return (new \Hidehalo\Nanoid\Client)->generateId($size);
  }
}

if (! function_exists('kw2p_instancia')) {
  /**
   * Obtenemos nombre de la instancia (studly)
   *
   * @return string
   */
  function kw2p_instancia()
  {
    return app()->kw2pInstancia();
  }
}

if (! function_exists('kw2p_ambito')) {
  /**
   * Obtenemos nombre de la ambito (studly)
   *
   * @return string
   */
  function kw2p_ambito()
  {
    return app()->kw2pAmbito();
  }
}

if (! function_exists('instancia_path')) {
  /**
   * Get the path to the instancia application folder.
   *
   * @param  string  $path
   * @return string
   */
  function instancia_path($path = '')
  {
    return str_replace(kw2p_ambito(), kw2p_instancia(), app()->path($path));
  }
}

if (! function_exists('instancia_resource_path')) {
  /**
   * Get the path to the instancia resources folder.
   *
   * @param  string  $path
   * @return string
   */
  function instancia_resource_path($path = '')
  {
    return str_replace(kw2p_ambito(), kw2p_instancia(), ambito_resource_path($path));
  }
}

if (! function_exists('instancia_storage_path')) {
  /**
   * Get the path to the instancia storage folder.
   *
   * @param  string  $path
   * @return string
   */
  function instancia_storage_path($path = '')
  {
    return str_replace(kw2p_ambito(), kw2p_instancia(), app()->storagePath($path));
  }
}

if (! function_exists('ambito_resource_path')) {
  /**
   * Get the path to the ambito resources folder.
   *
   * @param  string  $path
   * @return string
   */
  function ambito_resource_path($path = '')
  {
    return app()->path('resources').($path ? DIRECTORY_SEPARATOR.$path : $path);
  }
}

/**
 * Like array_reduce() but over an iterable, and it returns a single value as
 * the result of calling $cb over the contents of iterable $itr.
 *
 * If $initial is not null, $initial is set to the value of the first element
 * of the iterable, and $cb is called with the first element as the carry value
 * and the second element of the array as the current value.
 */
function iterator_reduce(callable $function, iterable $iterable, $startValue = null) {
    $acc = $startValue;
    foreach ($iterable as $key => $value) {
        $acc = $function($acc, $value, $key);
    }
    return $acc;
}

