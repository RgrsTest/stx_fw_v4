<?php

namespace Klayware\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Symfony\Component\Finder\Finder;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class Kw2pBareboneCloneCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kw2p:clone {repositorio} {instancia} {--remplazo=Barebone}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clona y configura el espacio de nombre de la aplicación';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new key generator command.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $repository = $this->argument('repositorio');
        $instance = $this->argument('instancia');
        $path = $this->laravel->basePath('apps/' . $instance);

        if ($this->files->exists($path)) {
            echo "\n";
            $this->error(str_repeat(' ', 42));
            $this->error('  Instancia ya existe, imposible clonar.  ');
            $this->error(str_repeat(' ', 42));
            die();
        }

        exec("git clone git@gitlab.com:kw2p/{$repository}.git {$path}");

        exec("rm -rf {$path}/.git");

        $this->setApplicationNamespace($path);

        $this->info('¡Clonación de aplicación exitosa!');
    }

    /**
     * Set the namespace on the files in the app directory.
     *
     * @return void
     */
    protected function setApplicationNamespace($path)
    {
        $target = $this->option('remplazo');

        $files = Finder::create()->in($path)->name("/{$target}/i");

        foreach ($files as $file) {
            $this->replaceFile($file->getRealPath());
        }

        $files = Finder::create()->in($path)->contains("/{$target}/i")->name('*.php');

        foreach ($files as $file) {
            $this->replaceNamespace($file->getRealPath());
        }
    }

    /**
     * Replace the App namespace at the given path.
     *
     * @param  string  $path
     * @return void
     */
    protected function replaceNamespace($path)
    {
        $target = $this->option('remplazo');
        $instance = $this->argument('instancia');

        $search = [
            strtolower(kebab_case($target)),
            strtoupper($target),
            $target,
        ];

        $replace = [
            strtolower(kebab_case($instance)),
            strtoupper($instance),
            $instance,
        ];

        $this->replaceIn($path, $search, $replace);
    }

    /**
     * Replace the App file at the given path.
     *
     * @param  string  $path
     * @return void
     */
    protected function replaceFile($path)
    {
        $target = $this->option('remplazo');
        $instance = $this->argument('instancia');

        $search = [
            snake_case($target),
        ];

        $replace = [
            snake_case($instance),
        ];

        if ($this->files->exists($path)) {
            $this->files->move($path, str_replace($search, $replace, $path));
        }
    }

    /**
     * Replace the given string in the given file.
     *
     * @param  string  $path
     * @param  string|array  $search
     * @param  string|array  $replace
     * @return void
     */
    protected function replaceIn($path, $search, $replace)
    {
        if ($this->files->exists($path)) {
            $this->files->put($path, str_replace($search, $replace, $this->files->get($path)));
        }
    }
}
