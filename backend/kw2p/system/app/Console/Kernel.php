<?php

namespace Klayware\Console;

use ReflectionClass;
use Illuminate\Console\Application as Artisan;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Symfony\Component\Finder\Finder;
use Illuminate\Console\Command;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      $paths = [];

      foreach ((new Finder)->depth('== 2')->in(base_path('apps'))->directories()->name('Console') as $path) {
        $paths[] = $path->getRealPath();
      }

      foreach (($paths ? (new Finder)->in($paths)->files()->name('Schedule.php') : []) as $command) {
        $command = trim(str_replace(
          ['app/', '/', '.php'],
          ['', '\\', ''],
          str_after($command->getPathname(), base_path('apps'))
        ), '\\');
        try {
          (new $command)($schedule);
        } catch (\Throwable $th) {
          \Log::channel(str_before($command, '\Console'))->error($th->getMessage(), ['exception' => $th]);
        }
      }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
      $this->load(__DIR__.'/Commands');
      $this->loadAppsCommands();

        // require app_path('/../routes/console.php');
    }

    public function loadAppsCommands()
    {
      $paths = [];

      foreach ((new Finder)->depth('== 3')->in(base_path('apps'))->directories()->name('Commands') as $path) {
        $paths[] = $path->getRealPath();
      }

      foreach (($paths ? (new Finder)->in($paths)->files() : []) as $command) {
        $command = trim(str_replace(
          ['app/', '/', '.php'],
          ['', '\\', ''],
          str_after($command->getPathname(), base_path('apps'))
        ), '\\');
        try {
          if (is_subclass_of($command, Command::class) && !(new ReflectionClass($command))->isAbstract()) {
            Artisan::starting(function ($artisan) use ($command) {
              $artisan->resolve($command);
            });
          }
        } catch (\Throwable $th) {
          \Log::channel(str_before($command, '\Console'))->error($th->getMessage(), ['exception' => $th]);
        }
      }
    }
  }
