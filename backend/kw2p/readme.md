## Instrucciones KW2P

### Pre-requisitos

- PHP versión 7.3.29
  - `sudo apt-get update`
  - `sudo apt-get install php7.3`
  - `sudo apt-get install php7.3 php7.3-cli php7.3-common php7.3-json php7.3-opcache php7.3-sqlite3 php7.3-bcmath php7.3-gd php7.3-zip php7.3-curl php7.3-mbstring php7.3-dom php7.3-sqlite3`
- Composer version 1.10.17
  - Si instalan la versión más actual de composer correr `composer self-update 1.10.17`
- Sqlite3
  - `sudo apt update`
  - `sudo apt upgrade`
  - `sudo apt install sqlite3`

### Configuración

Estos son los pasos a seguir para completar la configuración del backend:

- clonar el repositorio en /opt/
- `cd /opt/{ app_name }/backend/kw2p`
- `composer install`
- ` cp .env.example .env`
- Crear base de datos en `/opt/{ app_name }/database/`
  - `cd /opt/{ app_name }/database`
  - `sqlite3 { database_name }.db`
  - ` sqlite>.databases`
  - `sqlite>.quit`
- Verificar que la ruta de la base de datos coincida con la ruta de la variable `SMARTBUS_DB_DATABASE` en el archivo `.env`.
- Correr los siguientes comandos en `/opt/{ app_name }/backend/kw2p`:
  - `php artisan migrate --path=apps/Smartbus/database/migrations/ --database=Smartbus`
  - `php artisan db:seed --database=Smartbus --class=\\SmartbusSeeder\\DatabaseSeeder`
- Para comprobar que todo este correcto ejecutar en `/opt/{ app_name }/backend/kw2p`
  - `php artisan smartbus:helloworld`

### Consultas en la base de datos

- Abrir la consola y ejecutar: -`sqlite3 /opt/{ app_name }/database/{ database_name }.db`
  - Una vez dentro ejecutar sqlite> `.header on` && sqlite> `.mode column`
  - Listo puedes ejecutar las consultas.
