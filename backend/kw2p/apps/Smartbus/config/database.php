<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Database Connections
  |--------------------------------------------------------------------------
  |
  | Here are each of the database connections setup for your application.
  | Of course, examples of configuring each database platform that is
  | supported by Laravel is shown below to make development simple.
  |
  |
  | All database work in Laravel is done through the PHP PDO facilities
  | so make sure you have the driver for your particular database of
  | choice installed on your machine before you begin development.
  |
  */

  'connections' => [

    'Smartbus' => [
      'driver' => 'sqlite',
      'host' => env('SMARTBUS_DB_HOST', '127.0.0.1'),
      'port' => env('SMARTBUS_DB_PORT', '3306'),
      'database' => env('SMARTBUS_DB_DATABASE', 'forge'),
      'username' => env('SMARTBUS_DB_USERNAME', 'forge'),
      'password' => env('SMARTBUS_DB_PASSWORD', ''),
      'unix_socket' => env('SMARTBUS_DB_SOCKET', ''),
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_unicode_ci',
      'prefix' => '',
      'strict' => true,
      'engine' => 'MyISAM',
    ],

  ],

];
