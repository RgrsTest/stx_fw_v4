<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Guards
  |--------------------------------------------------------------------------
  |
  | Next, you may define every authentication guard for your application.
  | Of course, a great default configuration has been defined for you
  | here which uses session storage and the Eloquent user provider.
  |
  | All authentication drivers have a user provider. This defines how the
  | users are actually retrieved out of your database or other storage
  | mechanisms used by this application to persist your user's data.
  |
  | Supported: "session", "token"
  |
  */

  'guards' => [
    'Smartbus' => [
      'driver' => 'jwt',
      'provider' => 'Smartbus',
    ],
  ],

  /*
  |--------------------------------------------------------------------------
  | User Providers
  |--------------------------------------------------------------------------
  |
  | All authentication drivers have a user provider. This defines how the
  | users are actually retrieved out of your database or other storage
  | mechanisms used by this application to persist your user's data.
  |
  | If you have multiple user tables or models you may configure multiple
  | sources which represent each model / table. These sources may then
  | be assigned to any extra authentication guards you have defined.
  |
  | Supported: "database", "eloquent"
  |
  */

  'providers' => [
    'Smartbus' => [
      'driver' => 'eloquent',
      'model' => Klay\Models\Usuario::class,
    ],
  ],

  /*
  |--------------------------------------------------------------------------
  | Authentication limits
  |--------------------------------------------------------------------------
  |
  | This defines the number of active sessions and the expulsion time when
  | the sessions are full and a new one is requested.
  |
  | at_the_same_time: number or null to disable limits
  | idle_time: number (hours) or null to disable kick out
  |
  */

  /*
  'limits' => [
    'at_the_same_time' => 2,
    'idle_time' => 60 * 5 # 5 hours
  ]
  */

  /*
  |--------------------------------------------------------------------------
  | Authentication is enable
  |--------------------------------------------------------------------------
  |
  */

  // 'disabled' => false,


];
