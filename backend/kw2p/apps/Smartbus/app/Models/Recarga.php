<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class Recarga extends KlayModel
{

  protected $table = 'recargas';

  protected $primaryKey = 'id';

  protected $fillable = [
    'fecha_hora',
    'uid',
    'id_producto',
    'perfil',
    'id_sam',
    'monto',
    'tipo_recarga',
    'saldo_inicial',
    'saldo_final',
    'consecutivo_sam',
    'consecutivo_aplicacion',
    'latitud',
    'longitud',
    'enviada'
  ];

}
