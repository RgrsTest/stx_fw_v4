<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class ListaLapR extends KlayModel
{

  protected $table = 'listas_lap_r';

  protected $primaryKey = 'id';

  protected $fillable = [
    'uid',
    'fecha_hora',
    'no_accion_aplicada',
    'monto_recarga',
    'id_producto',
    'id_sam',
    'id_lapr',
    'enviada'
    
  ];

}
