<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class ListaLam extends KlayModel
{

  protected $table = 'listas_lam';

  protected $primaryKey = 'id';

  protected $fillable = [
    'uid',
    'no_consecutivo',
    'accion',
    'aplicada',
    'fecha_registro',
    'enviada'
  ];

}
