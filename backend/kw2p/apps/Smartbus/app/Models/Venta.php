<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class Venta extends KlayModel
{

  protected $table = 'ventas';

  protected $primaryKey = 'id';

  protected $fillable = [
    'fecha_hora',
    'monto',
    'tipo',
    'latitud',
    'longitud',
    'enviada'
  ];

}
