<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class Corte extends KlayModel
{

  protected $table = 'cortes';

  protected $primaryKey = 'id';

  protected $fillable = [
    'fecha_inicio',
    'fecha_fin',
    'eventos_validaciones',
    'eventos_ventas',
    'eventos_bpd',
    'monto_validaciones',
    'monto_ventas'
  ];

}
