<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class ListaLapV extends KlayModel
{
  protected $table = 'listas_lap_v';

  protected $primaryKey = 'id';

  protected $fillable = [
    'uid',
    'no_accion_aplicada',
    'accion',
    'id_producto',
    'id_sam',
    'enviada'
  ];

}
