<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class Evento extends KlayModel
{

  protected $table = 'eventos';

  protected $primaryKey = 'id';

  protected $fillable = [
    'evento',
    'enviado'
  ];

}
