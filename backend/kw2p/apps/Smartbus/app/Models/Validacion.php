<?php
namespace Smartbus\Models;

use Klayware\KlayModel;

class Validacion extends KlayModel
{

  protected $table = 'validaciones';

  protected $primaryKey = 'id';

  protected $fillable = [
    'fecha_hora',
    'uid',
    'id_producto',
    'perfil',
    'id_sam',
    'tipo_debito',
    'monto',
    'saldo_inicial',
    'saldo_final',
    'consecutivo_sam',
    'consecutivo_aplicacion',
    'latitud',
    'longitud',
    'enviada'
  ];

}
