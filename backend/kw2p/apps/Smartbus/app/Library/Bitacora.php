<?php
namespace Smartbus\Library;

use Illuminate\Support\Facades\Log;

class Bitacora
{
  public function infoLog($canal, $mensaje)
  {
    Log::channel($canal)->info($mensaje);
  }

  public function errorLog($canal, $mensaje)
  {
    Log::channel($canal)->error($mensaje);
  }


}
