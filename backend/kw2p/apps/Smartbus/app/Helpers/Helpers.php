<?php

use GuzzleHttp\Client as HTTPClient;
use Klayware\Exceptions\KlayException;
use Smartbus\Library\Bitacora;

function obtener_contadores()
{
  $path = dirname(base_path(), 2).'/shared';

  $subidas = 0;
  $bajadas = 0;

  
  $archivo_contadores = $path.'/contador/pasajeros.json';

 
    
      if (file_exists($archivo_contadores)) {
        $contadores = json_decode(file_get_contents($archivo_contadores), true);
        $subidas = $contadores['subidas'];
        $bajadas = $contadores['bajadas'];
      }
    
  

  return [
    "subidas" => (double) $subidas,
    "bajadas" => (double) $bajadas
  ];

}


function obtener_coordenadas()
{
  $path = dirname(base_path(), 2).'/shared';

  $latitud = 0;
  $longitud = 0;

  $archivo_estado_gps = $path.'/gps/estado.json';
  $archivo_coordenadas = $path.'/gps/coordenadas.json';

  if (file_exists($archivo_estado_gps)) {
    $estado_gps = json_decode(file_get_contents($archivo_estado_gps));
    if ($estado_gps === 1) {
      if (file_exists($archivo_coordenadas)) {
        $coordenadas = json_decode(file_get_contents($archivo_coordenadas), true);
        $latitud = $coordenadas['lat'];
        $longitud = $coordenadas['long'];
      }
    }
  }

  return [
    "latitud" => (double) $latitud,
    "longitud" => (double) $longitud
  ];

}

function checar_directorios($directorios)
{

  $path = dirname(base_path(), 2).'/shared';

  foreach($directorios as $un_directorio)
  {
    $un_directorio_path = $path.'/'.$un_directorio;

    if (!file_exists($un_directorio_path)) {
      mkdir($un_directorio_path, 0775);
    }
  }
}
function obtenerTransbordos()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente Transbordos");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente Transbordos");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'transbordos'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}

function obtenerVersiones()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente Versiones");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente Versiones");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'versiones'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}
function obtenerPerfiles()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente PERFILES");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente PERFILES");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'perfiles'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}




function obtenerProductos()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente PRODUCTOS");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente PRODUCTOS");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'productos'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}




function obtenerListasLapr()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente LAPR");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente lapr");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'lapr'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}

function actualizarLapr($payload)
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente LAPR");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente lapr");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    
    
    $log->infoLog('Connectivity',json_encode($payload));
    
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:actualizar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode($payload)]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }
}




function obtenerListasLam()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente LAM");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente LAM");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'lam'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}



function actualizarLam($payload)
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente LAM");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente lam");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    
    
    $log->infoLog('Connectivity',json_encode($payload));
    
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:actualizar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode($payload)]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

    $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }
}




function obtenerListasLapv()
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente LAPV");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente lapv");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'lapv'])]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }


}

function actualizarLapv($payload)
{
    $log = new Bitacora();
    $log->infoLog("Connectivity", "Creando cliente LAPV");
    $path = dirname(base_path(), 2).'/shared';

    try {
    $log->infoLog("Connectivity", "Creando cliente lapv");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    
    
    $log->infoLog('Connectivity',json_encode($payload));
    
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:actualizar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode($payload)]);
  
    $log->infoLog("Connectivity", $request->getBody());
    
    $respuesta = json_decode($request->getBody(),true);
  
    $respuesta = $respuesta['data'];

   // $log->infoLog('Connectivity', $respuesta[1]['payload']['uid']);
  
    return $respuesta;
  
    }

   catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
    }
}









function obtenerMapping()
{
  $mapping_fake = [
  ];

  $response = [
    'status' => 'success',
    'data' => $mapping_fake
  ];

  return $response;
}

function obtenerOperacion()
{
  $operacion_fake = [];

  $response = [
    'status' => 'success',
    'data' => $operacion_fake
  ];

  return $response;
}

function autenticar()
{
  $log = new Bitacora();
  $log->infoLog("Connectivity", "Intentando autenticar");
  $path = dirname(base_path(), 2).'/shared';
  $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);

  try {
    $log->infoLog("Connectivity", "Creando cliente");
    $http = new HTTPClient();
    $log->infoLog("Connectivity", "Cliente creado");
    $request = $http->request('POST', $redParams['server'] . '/' . $redParams['instancia'] .'/usuarios:obtener_apikey', ['json' => ["correo" => $redParams['user'], "contrasena" => $redParams['password']]]);
    $log->infoLog("Connectivity", $request->getBody());

    
    $request = json_decode($request->getBody(), true);

    if ($request['status'] === 'success')
    {
      $apikey = $request['data']['apikey'];
      $request = $http->request('POST', $redParams['server'] . '/' . $redParams['instancia'] .'/sesion:iniciar', ['json' => ["correo" => $redParams['user'], "apikey" => $apikey]]);
      $request = json_decode($request->getBody(), true);
      if ($request['status'] === 'success'){
        $log->infoLog("Connectivity", "Se autentico correctamente");
        $jwt = $request['data']['jwt'];
        $redParams['jwt'] = $jwt;
        file_put_contents($path. '/config/red.json',json_encode($redParams));
        return true;
      }
    }
  }
  catch(\Exception $e){
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }
  return false;
}

function eliminarVersion($id){
  $path = dirname(base_path(), 2).'/shared';
  $log = new Bitacora();

  $http = new HTTPClient();
  
  $payload = [
                   'schema' => 'versiones',
                   'id' => $id       
              ];
              
  $log->infoLog('Connectivity', 'Obteniendo identidad');

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:eliminar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode($payload)]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $respuesta = json_decode($request->getBody(), true);

  return $respuesta;
  
}


function actualizarUnidad($payload){
  $path = dirname(base_path(), 2).'/shared';
  $log = new Bitacora();

  $http = new HTTPClient();

  $log->infoLog('Connectivity', 'Obteniendo identidad');

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:actualizar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode($payload)]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $respuesta = json_decode($request->getBody(), true);

  return $respuesta;
  
}

function informacion(){
  $path = dirname(base_path(), 2).'/shared';

  $log = new Bitacora();

  $http = new HTTPClient();

  $log->infoLog('Connectivity', 'Obteniendo identidad');

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:identidad',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['external_id' => $redParams['external_id']])]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  
  $respuesta = json_decode($request->getBody(),true);
  
  if ($respuesta['status'] === 'success'){
    $log->infoLog('Connectivity', $respuesta);

    if(file_exists($path . '/config/identidad.json')){
      $datos_previos = json_decode(file_get_contents($path . '/config/identidad.json'), true);
      $valores = array_keys($respuesta['data']);
      $log->infoLog('Connectivity', $valores);
      $log->infoLog('Connectivity', $datos_previos);
      
      foreach($valores as $valor){
        $datos_previos[$valor] = $respuesta['data'][$valor];
      }
      file_put_contents($path . '/config/identidad.json', json_encode($datos_previos)); 
    }
    else{
      file_put_contents($path . '/config/identidad.json', json_encode($respuesta['data']));
    }
  }

  if (!file_exists($path . '/config/rutas.json') || !file_exists($path . '/config/operadores.json')){
    obtenerCatalogos();
    file_put_contents($path . '/display/pantalla.json', json_encode('CAMBIAR_RUTA'));
  }

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:operacion',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']]]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $respuesta = json_decode($request->getBody(),true);
  $log->infoLog('Connectivity', $respuesta);

  file_put_contents($path . '/config/op.json', json_encode($respuesta));

}

function obtenerCatalogos(){
  $path = dirname(base_path(), 2).'/shared';

  $log = new Bitacora();

  $http = new HTTPClient();

  $log->infoLog('Connectivity', 'Obteniendo identidad');

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'rutas'])]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  
  $respuesta = json_decode($request->getBody(),true);
  
  if ($respuesta['status'] === 'success'){
    $log->infoLog('Connectivity', $respuesta);
    file_put_contents($path . '/config/rutas.json', json_encode($respuesta['data']));
  }

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/catalogos:consultar',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => json_encode(['schema' => 'operadores'])]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  
  $respuesta = json_decode($request->getBody(),true);
  
  if ($respuesta['status'] === 'success'){
    $log->infoLog('Connectivity', $respuesta);
    file_put_contents($path . '/config/operadores.json', json_encode($respuesta['data']));
  }

}


function enviarVentas($data)
{
  $path = dirname(base_path(), 2).'/shared';

  $log = new Bitacora();

  $ventas = $data->map(function($c) {
    return ['schema' =>'ventas', 'payload' => [ 'encabezado' => collect($c)->forget(['id', 'enviada', 'created_at', 'updated_at'])]];
  });

  $http = new HTTPClient();

  $log->infoLog('Connectivity', 'Enviando las ventas');

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/documentos:crear',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => $ventas]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  
  $respuesta = json_decode($request->getBody(),true);
  
  $respuesta = $respuesta['data'];

  $log->infoLog('Connectivity', $respuesta);
  
  return $respuesta;
}


function enviarValidaciones($data)
{

  $path = dirname(base_path(), 2).'/shared';

  $log = new Bitacora();

  $validaciones = $data->map(function($c) {
    return ['schema' => 'validaciones', 'payload' => ['encabezado' => collect($c)->forget(['id', 'enviada', 'created_at', 'updated_at'])]];
  });
  
  $http = new HTTPClient();

  $log->infoLog('Connectivity', 'Enviando Validaciones');

  try {
    $redParams = json_decode(file_get_contents($path . '/config/red.json'), true);
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/documentos:crear',  ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']], 'body' => $validaciones]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $respuesta = json_decode($request->getBody(),true);

  $log->infoLog('Connectivity', $respuesta);
  
  $respuesta = $respuesta['data'];

  return $respuesta;

}

function enviarEventos($data) {

  /* $path_identidad = dirname(base_path(), 2).'/shared/config/identidad.json';

  $dispositivo_id = 'XXXX';

  if (file_exists($path_identidad)) {
    $identidad = json_decode(file_get_contents($path_identidad));
    $dispositivo_id = isset($identidad->idDispositivo) ? $identidad->idDispositivo : 'XXXX';
  }

  $eventos['data'] = $data->map(function($c) {
    return collect($c)->forget(['id', 'enviado', 'updated_at']);
  });
  $eventos['dispositivo_id'] = $dispositivo_id;

  $http = new HTTPClient();

  try {
    $request = $http->request('POST','http://127.0.0.1:8000/smartbus/eventos:registrar', ['json' => $eventos]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $respuesta = json_decode($request->getBody(),true);

  return $respuesta; */

  return [
    'status' => 'success'
  ];
}

function enviarCoordenadas()
{

  $path = dirname(base_path(), 2).'/shared/config';

  $log = new Bitacora();

  if (!file_exists($path . '/identidad.json')){
    $log->infoLog('Connectivity', 'No identidad');
    return ['status' => 'error'];
  }
  
  $log->infoLog('Connectivity', ' Consiguiendo las coordenadas');
  
  $identidad = json_decode(file_get_contents($path . '/identidad.json'), true);

  $redParams = json_decode(file_get_contents($path . '/red.json'), true);

  $coordenadas = obtener_coordenadas();
  
  $pasajeros = obtener_contadores();

  $coordenadas = [
    'payload'=> [
      'id_vehiculo' => $identidad['idVehiculo'],
      'id_ruta' => $identidad['idRutaDB'],
      'id_operador' => $identidad['idOperador'],
      'fecha' => date("Y-m-d"),
      'hora' => date("H:i:s"),
      'id_dispositivo' => $identidad['idDispositivoDB'],
      'latitud' => $coordenadas['latitud'],
      'longitud' => $coordenadas['longitud'],
      'subidas' => $pasajeros['subidas'],
      'bajadas' => $pasajeros['bajadas']
    ]
  ];

  
   $log->infoLog('Connectivity', 'Enviando las coordenadas');
  
  $http = new HTTPClient();

  try {
    $request = $http->request('POST', $redParams['server'] .'/'. $redParams['instancia'] . '/tracking:create', ['headers' => ['Content-Type' => 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']],'body' => json_encode($coordenadas)]);
  } catch(\Exception $e) {
    
   $log->infoLog('Connectivity', $e->getMessage());
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $log->infoLog('Connectivity', $request->getStatusCode());

  $respuesta = json_decode($request->getBody(),true);

  $log->infoLog('Connectivity', $respuesta);

  return $respuesta;
}


function enviarRecargas($data)
{
  $path = dirname(base_path(), 2).'/shared/config';

  $log = new Bitacora();
  
  $redParams = json_decode(file_get_contents($path . '/red.json'), true);
  
  $recargas = $data->map(function($c) {

    return ['schema' => 'recargas', 'payload' => ['encabezado' => collect($c)->forget(['id', 'enviada', 'created_at', 'updated_at'])]];
  });
  
  $http = new HTTPClient();

  try {
    $request = $http->request('POST',$redParams['server'] .'/'. $redParams['instancia'] . '/documentos:crear', ['headers' => ['Content-Type'=> 'application/json', 'X-Requested-With' => 'XMLHttpRequest', 'Authorization' => 'Bearer '. $redParams['jwt']],'body' => $recargas]);
  } catch(\Exception $e) {
    throw (new KlayException($e->getMessage(), $e->getCode()));
  }

  $respuesta = json_decode($request->getBody(),true);

  $log->infoLog('Connectivity', $respuesta);

  return $respuesta['data'];
}
