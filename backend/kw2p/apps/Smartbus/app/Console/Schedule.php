<?php

namespace Smartbus\Console;

use Illuminate\Console\Scheduling\Schedule as BaseSchedule;

class Schedule
{
  public function __invoke(BaseSchedule $schedule)
  {
    # Lunes a las 12 hrs
    // $schedule->call(function () {
    //   config(['database.default' => 'Smartbus']);
    //   \Klay\Models\Sesion::depurar();
    // })->weekly()->mondays()->at('12:00')->runInBackground()->name('Smartbus Sesion::depurar');

    $schedule->command(\Smartbus\Console\Commands\Prueba::class)
      ->daily()
      ->withoutOverlapping()
      ->runInBackground();
  }
}
