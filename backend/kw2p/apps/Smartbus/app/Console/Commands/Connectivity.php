<?php

namespace Smartbus\Console\Commands;

use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;
use Smartbus\Models\Evento;
use Smartbus\Models\Recarga;
use Smartbus\Models\Validacion;
use Smartbus\Models\Venta;
use Smartbus\Models\ListaLapR;
use Smartbus\Models\ListaLam;
use Smartbus\Models\ListaLapV;


class Connectivity extends Command
{ 
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartbus:connectivity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de datos generados (validaciones,ventas, gps)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public $bitacora;
    public $path;

    public function __construct()
    {
      parent::__construct();
      config(['database.default' => 'Smartbus']);

      $this->bitacora = new Bitacora();
      $this->bitacora->infoLog('Connectivity','Construct');

      $this->path = dirname(base_path(), 2).'/shared';
      checar_directorios(['modem', 'gps']);
    }

    public function handle()
    {
      $this->bitacora->infoLog('Connectivity','Iniciando el proceso de Connectivity');
      $archivo_modem_estado = $this->path.'/modem/estado.json';
      $archivo_update_operador = $this->path.'/connect/operador.json';
      $archivo_update_ruta = $this->path.'/connect/ruta.json';
      $archivo_config_identidad = $this->path.'/config/identidad.json';
      $archivo_config_operacion = $this->path.'/config/operacion.json';
      $archivo_config_versiones = $this->path.'/config/versiones.json';
      $autenticado = false;

      while(true) {
        try {
          if (file_exists($archivo_modem_estado)) {
              
                
            if(!$autenticado){
              $autenticado = autenticar();
              usleep(100);
              continue;
            }
        
          sleep(30);

           // usleep(20000);
            informacion();
            $versiones = obtenerVersiones();
            $versionesfile =  json_decode(file_get_contents($archivo_config_versiones),true);
            //$this->bitacora->infoLog('Connectivity',  $versionesfile['LAM'] );
            
           
            
            $versionesTemp = [
                   'LAM' => 0,
                   'LAPR' => 0,
                   'LAPV' => 0,
                   'Transbordos' => 0,
                   'Productos' => 0,
                   'Perfiles' => 0
                ];
                
            foreach($versiones as $version) {
               
                $versionesTemp[$version['payload']['catalogo']] = $version['id'];
                
             }
          
            foreach($versionesTemp as $key => $value) {
               
                if($value > $versionesfile[$key]['version'])
                {
                  $versionesfile[$key]['version'] = $value;
                  $versionesfile[$key]['estado'] = 'actualizar';
                  
                  
                  }
                
             }
            
            
           
            $this->bitacora->infoLog('Connectivity',  $versionesfile );
            
            
            
            $this->bitacora->infoLog('Connectivity', "Transbordos Version Stage");
            if($versionesfile['Transbordos']['estado'] == 'actualizar'){
              
              
                $new_transbordos = obtenerTransbordos();
                $operacionfile =  json_decode(file_get_contents($archivo_config_operacion),true);
                
                foreach($new_transbordos as $transbordo) {
                  
                
                   
                    $operacionfile["matrizTransbordos"][$transbordo['payload']['rutaAnterior']][$transbordo['payload']['rutaActual']]= $transbordo['payload']['Especificaciones'];
                   
            
                  }
                
                
                file_put_contents($this->path . '/config/operacion.json', json_encode($operacionfile));
                $versionesfile['Transbordos']['estado'] = 'actualizado';
                eliminarVersion($versionesfile['Transbordos']['version']);
                     
            }
              
             
             
             
           
                 $this->bitacora->infoLog('Connectivity', "Productos Version Stage");
            
            if($versionesfile['Productos']['estado'] == 'actualizar'){
            
                $new_productos = obtenerProductos();
                $operacionfile =  json_decode(file_get_contents($archivo_config_operacion),true);
                
                foreach($new_productos as $producto) {
                  
                  foreach($operacionfile["productos"] as $key => $value) {
                   $this->bitacora->infoLog('Connectivity', $key);
                    
                   if($producto['payload']['id_producto'] == $key)
                   {
                      $this->bitacora->infoLog('Connectivity', "Updating Product");
                      $this->bitacora->infoLog('Connectivity', $producto['payload']['id_producto']);
                      
                      $operacionfile["productos"][$key]["productoNombre"] = $producto['payload']['producto_nombre'];
                      $operacionfile["productos"][$key]["productoClave"]  = $producto['payload']['clave'];
                      $operacionfile["productos"][$key]["recarga"]  = $producto['payload']['opciones']['recarga'];
                      $operacionfile["productos"][$key]["validacion"]  = $producto['payload']['opciones']['validacion'];
                      $operacionfile["productos"][$key]["contrato"] = $producto['payload']['opciones']['contrato'];
                      $operacionfile["productos"][$key]["servicio"] = $producto['payload']['opciones']['servicio'];       
                   }
                   else
                   {
                      $this->bitacora->infoLog('Connectivity', "Updating Product");
                      $this->bitacora->infoLog('Connectivity', $producto['payload']['id_producto']);
                      
                      $operacionfile["productos"][$key]["productoNombre"] = $producto['payload']['producto_nombre'];
                      $operacionfile["productos"][$key]["productoClave"]  = $producto['payload']['clave'];
                      $operacionfile["productos"][$key]["recarga"]  = $producto['payload']['opciones']['recarga'];
                      $operacionfile["productos"][$key]["validacion"]  = $producto['payload']['opciones']['validacion'];
                      $operacionfile["productos"][$key]["contrato"] = $producto['payload']['opciones']['contrato'];
                      $operacionfile["productos"][$key]["servicio"] = $producto['payload']['opciones']['servicio'];       
                   }
                  }
                }
                
                file_put_contents($this->path . '/config/operacion.json', json_encode($operacionfile));
                $versionesfile['Productos']['estado'] = 'actualizado';
                eliminarVersion($versionesfile['Productos']['version']);
               }
               
            $this->bitacora->infoLog('Connectivity', "Perfiles Version Stage");
               
            if($versionesfile['Perfiles']['estado'] == 'actualizar'){
               
                $new_perfiles = obtenerPerfiles();
                $operacionfile =  json_decode(file_get_contents($archivo_config_operacion),true);
                
                foreach($new_perfiles as $perfil) {
                  
                  foreach($operacionfile["perfiles"] as $key => $value) {
                     $this->bitacora->infoLog('Connectivity', "value");
                   $this->bitacora->infoLog('Connectivity', $value['codigoPerfil']);
                   
                   if($perfil['payload']['codigo_perfil'] == $value['codigoPerfil'])
                   {
                      $productosPerfil = array();
                      $this->bitacora->infoLog('Connectivity', "Updating Profile");
                      //$this->bitacora->infoLog('Connectivity', $perfil['payload']['id_producto']);
                      
                      $operacionfile["perfiles"][$key]["usuarioTipo"] = $perfil['payload']['usuario_tipo'];
                      $operacionfile["perfiles"][$key]["antiPassback"] = $perfil['payload']['anti_passback'];
                      $operacionfile["perfiles"][$key]["transbordo"] = $perfil['payload']['transbordo'];
                      $operacionfile["perfiles"][$key]["credito"] = $perfil['payload']['credito'];
                      
                      
                     
                      
                      foreach($perfil['payload']['productos'] as $key2 => $value2) {    
                        
                      $productosPerfil += [$value2['productoId'] => ["maximoValor" => $value2['maximoValor'],"tarifa"=> $value2['tarifa']]];              
                      }
                       $this->bitacora->infoLog('Connectivity',"productosPerfil!");
                     //  $this->bitacora->infoLog('Connectivity',json_encode($productosPerfil));
                       
                      $operacionfile["perfiles"][$key]["productos"] = $productosPerfil;
                      
                      $this->bitacora->infoLog('Connectivity',json_encode($operacionfile["perfiles"][$key]["productos"]));
                      
                      
                   }
                  
                  }
                  
              
                }
                 file_put_contents($this->path . '/config/operacion.json', json_encode($operacionfile));
                // file_put_contents($this->path . '/config/operacion.json', json_encode($operacionfile));
                $versionesfile['Perfiles']['estado'] = 'actualizado';
              eliminarVersion($versionesfile['Perfiles']['version']);
             } 
            
            
             
            
                /* Se envian recargas remotas LAPR pendientes */
            $this->bitacora->infoLog('Connectivity', "LAPR Stage");
            $lapr = ListaLapR::where('id_sam','!=', null)->first();
            if ($lapr) {
               $this->bitacora->infoLog('Connectivity', "Creando Payload LAPR");
                $payload = [
                   'schema' => 'lapr',
                   'id' => $lapr["enviada"],
                   'payload' => [
                   'id_sam' => $lapr["id_sam"]
                ]
              ];
             
              $this->bitacora->infoLog('Connectivity', "Actualizando LAPR");
              $resp_lapr = actualizarLapr($payload);
              $this->bitacora->infoLog('Connectivity', "LAPR Actualizado");
              $lapr->delete();
           
            }
            else
            {
              if($versionesfile['LAPR']['estado'] == 'actualizar'){
               
                   $this->bitacora->infoLog('Connectivity', "Buscando LAPR");
                   ListaLapR::truncate();
                   usleep(100);
                
                   $LAPR_List = obtenerListasLapr();
                
                
                  foreach($LAPR_List as $lapr) {
                
                  $this->bitacora->infoLog('Connectivity', $lapr['payload']['uid']);
                  
                  $lapr_exist = ListaLapR::where('uid', $lapr['payload']['uid'])->first();
                  
                  if($lapr_exist == null)
                  {
                  $lapr_action = new ListaLapR();
                  $lapr_action->uid = $lapr['payload']['uid'];
                  $lapr_action->id_producto = $lapr['payload']['id_producto'];
                  $lapr_action->fecha_hora = $lapr['created_at'];
                  $lapr_action->monto = $lapr['payload']['monto'];
                  $lapr_action->numero_accion_aplicada = $lapr['payload']['numero_accion_aplicada'];
                  
                  $lapr_action->enviada = $lapr['id'];
                  
                  
                  if($lapr['payload']['id_sam'] == null)
                  {
                    $lapr_action->save();
                  }
                  
               
                  }
                 }
                    $versionesfile['LAPR']['estado'] = 'actualizado';
                    eliminarVersion($versionesfile['LAPR']['version']);
                   
                 }
                  
            }
            
            
            
            
            /* Se envian acciones pendientes LAM */
              $this->bitacora->infoLog('Connectivity', "LAM Stage");
            $lam = ListaLam::where('aplicada','!=', null)->first();
            if ($lam) {
               $this->bitacora->infoLog('Connectivity', "Creando Payload LAM");
                 $payload = [
                   'schema' => 'lam',
                   'id' => $lam["enviada"],
                   'payload' => [
                   'accionAplicada' => $lam["aplicada"]
                ]
              ];
             
              $this->bitacora->infoLog('Connectivity', "Actualizando LAM");
              $resp_lapr = actualizarLam($payload);
              $this->bitacora->infoLog('Connectivity', "LAM Actualizado");
              $lam->delete();
           
            }
            else
            {
               if($versionesfile['LAM']['estado'] == 'actualizar'){
                 
                  $this->bitacora->infoLog('Connectivity', "Buscando LAM");
                  ListaLam::truncate();
                  usleep(100);
                
                  $LAM_List = obtenerListasLam();
                
                
                  foreach($LAM_List as $lam) {
                
                  $this->bitacora->infoLog('Connectivity', $lam['payload']['uid']);
                  
                  $lam_exist = ListaLam::where('uid', $lam['payload']['uid'])->first();
                  
                  if($lam_exist == null)
                  {
                  $lam_action = new ListaLam();
                  $lam_action->uid = $lam['payload']['uid'];
                  $lam_action->fecha_hora = $lam['created_at'];
                  $lam_action->no_consecutivo_accion = $lam['payload']['numeroAccion'];
                  $lam_action->accion = $lam['payload']['codigoAccion'];
                  
                  $lam_action->enviada = $lam['id'];
                  
                  
                  if($lam['payload']['accionAplicada'] == null)
                  {
                    $lam_action->aplicada = null;
                    $lam_action->save();
                    $this->bitacora->infoLog('Connectivity', "LAM Saved");
                  }
                  
               
                  }
                 }
                 
                 $versionesfile['LAM']['estado'] = 'actualizado';
                   eliminarVersion($versionesfile['LAM']['version']);
                }   
            }
            
            
            
                
                /* Se envian recargas remotas LAPV pendientes */
                    $this->bitacora->infoLog('Connectivity', "LAPV Stage");
            $lapv = ListaLapV::where('id_sam','!=', null)->first();
            if ($lapv) {
               $this->bitacora->infoLog('Connectivity', "Creando Payload LAPV");
                $payload = [
                   'schema' => 'lapv',
                   'id' => $lapv["enviada"],
                   'payload' => [
                   'id_sam' => $lapv["id_sam"]
                ]
              ];
             
              $this->bitacora->infoLog('Connectivity', "Actualizando LAPV");
              $resp_lapv = actualizarLapv($payload);
              $this->bitacora->infoLog('Connectivity', "LAPV Actualizado");
              $lapv->delete();
           
            }
            else
            {
               if($versionesfile['LAPV']['estado'] == 'actualizar'){
                  
                   $this->bitacora->infoLog('Connectivity', "Buscando LAPV");
                   ListaLapV::truncate();
                   usleep(100);
                
                   $LAPV_List = obtenerListasLapv();
                
                
                 foreach($LAPV_List as $lapv) {
                
                  $this->bitacora->infoLog('Connectivity', $lapv['payload']['uid']);
                  
                  $lapv_exist = ListaLapV::where('uid', $lapv['payload']['uid'])->first();
                  
                  if($lapv_exist == null)
                  {
                  $lapv_action = new ListaLapV();
                  $lapv_action->uid = $lapv['payload']['uid'];
                  $lapv_action->id_producto = $lapv['payload']['id_producto'];
                  
                  $lapv_action->no_accion_aplicada = $lapv['payload']['numeroSuspensionProducto'];
                  $lapv_action->enviada = $lapv['id'];
                  
                  $lapv_action->accion = 0;
                  
                  if($lapv['payload']['id_sam'] == null)
                  {
                    $lapv_action->save();
                  }
                  
               
                  }
                 }
               $versionesfile['LAPV']['estado'] = 'actualizado';
               eliminarVersion($versionesfile['LAPV']['version']);
             }
            }
            
            
              
              
              
            /* Se enviar las ventas pendientes */
                $this->bitacora->infoLog('Connectivity', "Ventas Stage");
            $ventas = Venta::where('enviada', 0)->get();
            if (count($ventas) > 0) {
              $resp_ventas = enviarVentas($ventas);
              $this->bitacora->infoLog('Connectivity', $resp_ventas);
              foreach($resp_ventas as $respu){
                if (!isset($respu['status']) && isset($respu['estado']) && $respu['estado'] === 'vigente') {
                  Venta::where('folio_transaccion', '=',$respu['payload']['encabezado']['folio_transaccion'])->update(['enviada' => 1]);
                  $this->bitacora->infoLog('Connectivity','Venta enviada con folio: ' .$respu['payload']['encabezado']['folio_transaccion']);
                }
              }
            }

            /* Se envian las validaciones pendietes */
                 $this->bitacora->infoLog('Connectivity', "Validaciones Stage");
            $validaciones = Validacion::where('enviada', 0)->get();
            if (count($validaciones) > 0) {
              $resp_ventas = enviarValidaciones($validaciones);
              foreach($resp_ventas as $respu){
                if (!isset($respu['status']) && isset($respu['estado']) && $respu['estado'] === 'vigente') {
                  Validacion::where('folio_transaccion', '=',$respu['payload']['encabezado']['folio_transaccion'])->update(['enviada' => 1]);
                  $this->bitacora->infoLog('Connectivity','Venta enviada con folio: ' .$respu['payload']['encabezado']['folio_transaccion']);
                }
              }
            }

            /* Se envian las recargas pendietes */
                 $this->bitacora->infoLog('Connectivity', "Recargas Stage");
            $recargas = Recarga::where('enviada', 0)->get();
            if (count($recargas) > 0) {
              $resp_recargas = enviarRecargas($recargas);
              foreach($resp_recargas as $respu){
                if (!isset($respu['status']) && isset($respu['estado']) && $respu['estado'] === 'vigente') {
                  Recarga::where('folio_transaccion', '=',$respu['payload']['encabezado']['folio_transaccion'])->update(['enviada' => 1]);
                  $this->bitacora->infoLog('Connectivity','Venta enviada con folio: ' .$respu['payload']['encabezado']['folio_transaccion']);
                }
              }
            }
            /* Se envia el cambio de operador */
                 $this->bitacora->infoLog('Connectivity', "Cambio Operador Stage");
            if (file_exists($archivo_update_operador)){
              $identidad = json_decode(file_get_contents($archivo_config_identidad), true);
              $operador = json_decode(file_get_contents($archivo_update_operador), true);
              $payload = [
                'schema' => 'vehiculos',
                'id' => $identidad['idVehiculo'],
                'payload' => [
                  'id_operador' => intval($operador)
                ]
              ];
              
              $response = actualizarUnidad($payload);
              
              if ($response['status']==='sucess'){
                unlink($archivo_update_operador);
              }
            }

            /* Se envia el cambio de ruta */
                 $this->bitacora->infoLog('Connectivity', "Cambio Ruta Stage");
            if (file_exists($archivo_update_ruta)){
              $identidad = json_decode(file_get_contents($archivo_config_identidad), true);
              $ruta = json_decode(file_get_contents($archivo_update_ruta), true);
              $payload = [
                'schema' => 'vehiculos',
                'id' => $identidad['idVehiculo'],
                'payload' => [
                  'id_operador' => intval($ruta)
                ]
              ];

              $response = actualizarUnidad($payload);
              
              if ($response['status']==='sucess'){
                unlink($archivo_update_ruta);
              }
            }

            /* Se envian los eventos pendientes */
                 $this->bitacora->infoLog('Connectivity', "Eventos Stage");
            try{
              $eventos = Evento::where('enviado', 0)->get();
              if (count($eventos) > 0) {
                $resp_eventos = enviarEventos($eventos);
                if ($resp_eventos['status'] === 'success') {
                  $this->bitacora->infoLog('Connectivity','Se enviaron las eventos correctamente');
                  Evento::whereIn('id',$eventos->pluck('id')->all())->update(['enviado' => 1]);
                }
              } else {
                $this->bitacora->infoLog('Connectivity', "Coordenadas Stage");
                $resp_coordenadas = enviarCoordenadas();
                if ($resp_coordenadas['status'] === 'success') {
                  $this->bitacora->infoLog('Connectivity','Se enviaron las coordenadas correctamente');
                }
              }
            }
            catch (\Exception $e) {
              $this->bitacora->errorLog('Connectivity', $e->getMessage());
            }
          }

          sleep(15);
        } catch(\Exception $e) {
          $this->bitacora->errorLog('Connectivity', $e->getMessage());
          sleep(2);
          $autenticado = false;
          continue;
        }
        
                    file_put_contents($this->path . '/config/versiones.json', json_encode($versionesfile));
                    
      }
      
    }
}
