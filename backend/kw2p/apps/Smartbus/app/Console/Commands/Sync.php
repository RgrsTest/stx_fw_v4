<?php

namespace Smartbus\Console\Commands;

use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;
use Smartbus\Models\ListaLam;
use Smartbus\Models\ListaLapR;

class Sync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartbus:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronización de listas lam y listas lap';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public $bitacora;
    public $path;

    public function __construct()
    {
      parent::__construct();

      config(['database.default' => 'Smartbus']);

      $this->bitacora = new Bitacora();

      $this->path = dirname(base_path(), 2).'/shared';

      checar_directorios(['modem', 'config']);
    }

    public function handle()
    {
      $this->bitacora->infoLog('Sync','Inicio el proceso de sync');
      $archivo_modem_estado = $this->path.'/modem/estado.json';

      while(true) {
        try {
          if (file_exists($archivo_modem_estado)) {
            $valor_modem_estado = json_decode(file_get_contents($archivo_modem_estado), true);

            if ($valor_modem_estado === 1) {
              /* Actualizamos las listas lam */
              $listaslam_temp = obtenerListasLam();
              if ($listaslam_temp['status'] === 'success') {
                foreach($listaslam_temp['data'] as $una_lista) {
                  ListaLam::updateOrcreate(['uid' => $una_lista['uid']],
                    [
                      'uid' => $una_lista['uid'],
                      'no_consecutivo' => $una_lista['no_consecutivo'],
                      'accion' => $una_lista['accion'],
                      'fecha_registro' => $una_lista['fecha_registro']
                    ]
                  );
                }
                $this->bitacora->infoLog('Sync','Se actualizaron las listas LAM');
              } else {
                $this->bitacora->errorLog('Sync','Ocurrio un error a la hora de actualizar las listas LAM');
              }

              /* Actualizamos las listas lapr */
              $listaslapr_temp = obtenerListasLapr();
              if ($listaslapr_temp['status'] === 'success') {
                foreach($listaslapr_temp['data'] as $una_lista) {
                  ListaLapR::create(
                    [
                      'uid' => $una_lista['uid'],
                      'fecha_hora' => $una_lista['fecha_hora'],
                      'no_accion_aplicada' => $una_lista['no_accion_aplicada'],
                      'monto_recarga' => $una_lista['monto_recarga'],
                      'id_producto' => $una_lista['id_producto']
                    ]
                  );
                }
                $this->bitacora->infoLog('Sync','Se actualizaron las listas LAPR');
              } else {
                $this->bitacora->errorLog('Sync','Ocurrio un error a la hora de actualizar las listas LAPR');
              }

              $mapping_temp = obtenerMapping();
              if ($mapping_temp['status'] === 'success') {
                file_put_contents($this->path . '/config'. '/mapping.json', json_encode($mapping_temp['data']));
                $this->bitacora->infoLog('Sync','Se actualizo el archivo config/mapping');
              } else {
                $this->bitacora->errorLog('Sync','Ocurrio un error a la hora de actualizar el archivo config/mapping');
              }

              $operacion_temp = obtenerOperacion();
              if ($operacion_temp['status'] === 'success') {
                file_put_contents($this->path . '/config'. '/operacion.json', json_encode($operacion_temp['data']));
                $this->bitacora->infoLog('Sync','Se actualizo el archivo config/operacion');
              } else {
                $this->bitacora->errorLog('Sync','Ocurrio un error a la hora de actualizar el archivo config/operacion');
              }
            }
            /* sleep(10); */
          }
          sleep(10);
        } catch(\Exception $e) {
          $this->bitacora->errorLog('Sync', $e->getMessage());
          usleep(100);
          continue;
        }
      }
    }
}
