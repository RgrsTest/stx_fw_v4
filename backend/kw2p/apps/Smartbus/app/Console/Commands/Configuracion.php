<?php

namespace Smartbus\Console\Commands;

use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;

class Configuracion extends Command
{
  protected $signature = 'smartbus:configuracion';
  protected $description = 'Proceso de configuracion de unidades y choferes';

  public $bitacora;
  public $path;

  public function __construct()
  {
    parent::__construct();

    config(['database.default' => 'Smartbus']);

    $this->bitacora = new Bitacora();

    $this->path = dirname(base_path(), 2).'/shared';

    checar_directorios(['display', 'validador', 'config', 'changer']);
  }

  public function handle()
  {
    $this->bitacora->infoLog('Configuracion','Inicio el proceso de configuracion');
    $archivo_display_pantalla = $this->path.'/display/pantalla.json';
    $archivo_display_tarjeta = $this->path.'/display/tarjeta.json';
    $archivo_validador_estado = $this->path.'/validador/estado.json';
    $archivo_validador_lectura = $this->path.'/validador/lectura.json';
    $archivo_validador_tarjeta = $this->path.'/validador/tarjeta.json';
    $archivo_config_identidad = $this->path.'/config/identidad.json';
    $archivo_changer_estado = $this->path.'/changer/estado.json';
    $archivo_maintainer_estado = $this->path.'/maintainer/status.json';
    $archivo_validador_monto = $this->path. '/validador/monto.json';
    $archivo_validador_fallidas = $this->path. '/validador/fallidas.json';
    $archivo_validador_recargas  = $this->path. '/validador/recargas.json';
    $archivo_changer_dinero = $this->path. '/changer/dinero.json';
    $archivo_display_ruta = $this->path. '/display/ruta.json';
    $archivo_config_operadores = $this->path. '/config/operadores.json';
    $archivo_update_operador = $this->path .'/connect/operador.json';
    $archivo_update_ruta = $this->path . '/connect/ruta.json';

    while (true) {
      try {
        if (file_exists($archivo_display_pantalla)) {

          if (file_exists($archivo_maintainer_estado)){
            copy($archivo_maintainer_estado, $this->path . '/display/estados.json');
          }

          $valor_pantalla = json_decode(file_get_contents($archivo_display_pantalla), true);

          if ($valor_pantalla === 'INGRESO_MENU') {
            /* Leer el archivo /validador/estado.json*/
            $validador_estado = json_decode(file_get_contents($archivo_validador_estado), true);
            if ($validador_estado !== 3) {

              /* Escribir 3 en el archivo /validador/estado.json */
              file_put_contents($archivo_validador_estado, json_encode(3));

              /* Escribir 0 en el archivo /changer/estado.json */
              if (file_exists($archivo_changer_estado)) {
                file_put_contents($archivo_changer_estado, json_encode(0));
              } else {
                file_put_contents($archivo_changer_estado, json_encode(0));
              }

              usleep(100);
              continue;
            } else {
              /* Revisar si existe el archivo /validador/lectura.json*/
              if (file_exists($archivo_validador_lectura)) {
                $validador_lectura = json_decode(file_get_contents($archivo_validador_lectura), true);

                /* Si el rol del usuario leído es igual a 1 */
                if ($validador_lectura['rol'] === 1) {

                  file_put_contents($archivo_display_pantalla, json_encode("DATOS_UNIDAD"));
                  file_put_contents($archivo_validador_estado, json_encode(0));
                  unlink($archivo_validador_lectura);

                  usleep(100);
                  continue;
                  /* Si el rol del usuario leído es igual a 2 */
                } elseif ($validador_lectura['rol'] === 2) {

                  file_put_contents($archivo_display_pantalla, json_encode("CONTABILIDAD"));
                  file_put_contents($archivo_validador_estado, json_encode(0));
                  unlink($archivo_validador_lectura);

                  usleep(100);
                  continue;
                  /* Si el rol del usuario leído es igual a 3 */
                } elseif ($validador_lectura['rol'] === 3) {

                  file_put_contents($archivo_display_pantalla, json_encode("MENU_CONFIGURACION"));
                  file_put_contents($archivo_validador_estado, json_encode(0));
                  unlink($archivo_validador_lectura);

                  usleep(100);
                  continue;
                  /* Si el rol es mayor a 3 */
                } elseif($validador_lectura['rol'] > 3 || $validador_lectura['rol'] === 0) {
                  $this->bitacora->infoLog('Configuracion', "rol mayor a tres: espera");
                  file_put_contents($archivo_display_pantalla, json_encode("PRINCIPAL"));
                  file_put_contents($archivo_validador_estado, json_encode(0));
                  unlink($archivo_validador_lectura);

                  usleep(100);
                  continue;
                }
              }

              usleep(100);
              continue;
            }
          }

          if ($valor_pantalla === 'CAMBIAR_CHOFER') {
            /* Leer el archivo /validador/estado.json*/
            $validador_estado = json_decode(file_get_contents($archivo_validador_estado), true);

            if ($validador_estado !== 3) {

              /* Escribir 3 en el archivo /validador/estado.json */
              file_put_contents($archivo_validador_estado, json_encode(3));

              usleep(100);
              continue;
            } else {
              $config_identidad = json_decode(file_get_contents($archivo_config_identidad), true);
              if (file_exists($archivo_validador_lectura)) {
                if (isset($config_identidad['idOperador'])){
              
                  $validador_lectura = json_decode(file_get_contents($archivo_validador_lectura), true);
  
                  /* Si el rol del usuario leído es igual a 1 */
                  if ($validador_lectura['rol'] === 1) {
                    /* Escribir en el archivo /config/identidad.json, en el valor  */
                    $operadores = json_decode(file_get_contents($archivo_config_operadores), true);
                    $oper = array_search($validador_lectura['id'], array_column($operadores, 'id'));
                    if($oper !== false ){
                      $config_identidad['chofer'] = $operadores[$oper]['payload']['label'];
                      $config_identidad['idOperador'] = $operadores[$oper]['id'];

                      file_put_contents($archivo_config_identidad, json_encode($config_identidad));
                      $this->bitacora->infoLog('Configuracion','Se actualizo el nombre del chofer');
  
                      /* Escribir 0 en el archivo /validador/estado.json */
                      file_put_contents($archivo_validador_estado, json_encode(0));
                      unlink($archivo_validador_lectura);
    
                      /* Escribir en el archivo /display/pantalla.json CONFIRMAR_CHOFER */
                      $this->bitacora->infoLog('Configuracion', " confirmar chofer pantalla");
                      file_put_contents($archivo_display_pantalla, json_encode("CONFIRMAR_CHOFER"));
                      file_put_contents($archivo_update_operador, json_encode($config_identidad['idOperador']));
                      shell_exec("systemctl restart validador");
                      shell_exec("systemctl restart printer");
                      usleep(100);
                      continue;
                    }
                    else{
                      unlink($archivo_validador_lectura);
                      $this->bitacora->infoLog('Configuracion', "chofer inexistente pantalla");
                      file_put_contents($archivo_display_pantalla, json_encode("CHOFER_INEXISTENTE"));
                      sleep(3);
                      file_put_contents($archivo_display_pantalla, json_encode("CAMBIAR_CHOFER"));
                      continue;
                    }
                  } else {
                    /* Escribir 0 en el archivo /validador/estado.json */
                    file_put_contents($archivo_validador_estado, json_encode(0));
                    unlink($archivo_validador_lectura);
                    $this->bitacora->infoLog('Configuracion', "chofer inexistente pantalla");
                    file_put_contents($archivo_display_pantalla, json_encode("CHOFER_INEXISTENTE"));
                    sleep(3);
                    /* Escribir en el archivo /display/pantalla.json DATOS_UNIDAD*/
                    $this->bitacora->infoLog('Configuracion', "datos unidad pantalla");
                    file_put_contents($archivo_display_pantalla, json_encode("DATOS_UNIDAD"));
                  }
                }
                else{
                  $validador_lectura = json_decode(file_get_contents($archivo_validador_lectura), true);
                  if ($validador_lectura['rol'] == 1){
                    $operadores = json_decode(file_get_contents($archivo_config_operadores), true);

                    $this->bitacora->infoLog('Configuracion', $operadores);

                    $oper = array_search($validador_lectura['id'], array_column($operadores, 'id'));
                    
                    if($oper !== false  ){
                      $config_identidad['chofer'] = $operadores[$oper]['payload']['label'];
                      $config_identidad['idOperador'] = $operadores[$oper]['id'];

                      file_put_contents($archivo_config_identidad, json_encode($config_identidad));
                      $this->bitacora->infoLog('Configuracion','Se actualizo el nombre del chofer');
  
                      /* Escribir 0 en el archivo /validador/estado.json */
                      file_put_contents($archivo_validador_estado, json_encode(0));
                      unlink($archivo_validador_lectura);
    
                      /* Escribir en el archivo /display/pantalla.json CONFIRMAR_CHOFER */
                      $this->bitacora->infoLog('Configuracion', "confirmar chofer pantalla");
                      file_put_contents($archivo_display_pantalla, json_encode("CONFIRMAR_CHOFER"));
                      file_put_contents($archivo_update_operador, json_encode($config_identidad['idOperador']));
                      shell_exec("systemctl restart validador");
                      shell_exec("systemctl restart printer");
                      usleep(100);
                      continue;
                    }
                    else {
                      unlink($archivo_validador_lectura);
                      $this->bitacora->infoLog('Configuracion', "chofer inexistente pantalla");
                      file_put_contents($archivo_display_pantalla, json_encode("CHOFER_INEXISTENTE"));
                      sleep(3);
                      file_put_contents($archivo_display_pantalla, json_encode("CAMBIAR_CHOFER"));
                      continue;
                    }
                  }
                  else {
                    unlink($archivo_validador_lectura);
                    $this->bitacora->infoLog('Configuracion', "chofer inexistente pantalla ");
                    file_put_contents($archivo_display_pantalla, json_encode("CHOFER_INEXISTENTE"));
                    sleep(3);
                    file_put_contents($archivo_display_pantalla, json_encode("CAMBIAR_CHOFER"));
                    continue;
                  }
                }
              }
            }
          }

          if ($valor_pantalla === 'CONFIRMAR_RUTA') {
            $this->bitacora->infoLog('Configuracion', 'Estamos confirmando ruta');

            if (file_exists($archivo_display_ruta) && filesize($archivo_display_ruta) > 0){
              $this->bitacora->infoLog('Configuracion', 'Existe archivo');
            
              $ruta = json_decode(file_get_contents($archivo_display_ruta), true);
              $datos = json_decode(file_get_contents($archivo_config_identidad), true);
              $datos["ruta"] = $ruta["payload"]["label"];
              $datos["idRuta_Estacion"] = $ruta["payload"]["external_id"];
              $datos["idRutaDB"] = $ruta["id"];
              file_put_contents($archivo_config_identidad, json_encode($datos));
              file_put_contents($archivo_update_ruta, json_encode($datos['idRutaDB']));
              unlink($archivo_display_ruta);
              $this->bitacora->infoLog('Configuracion', 'A este punto se supone que eliminamos el archivo');
              if (isset($datos['idOperador'])){
                shell_exec("systemctl restart validador");
                shell_exec("systemctl restart printer");
                $this->bitacora->infoLog('Configuracion', "datos unidad pantalla");
                file_put_contents($archivo_display_pantalla, json_encode("DATOS_UNIDAD"));
              }
              else {
                $this->bitacora->infoLog('Configuracion', 'Nos movemos al chofer');
                $this->bitacora->infoLog('Configuracion', "cambiar chofer pantalla");
                file_put_contents($archivo_display_pantalla, json_encode("CAMBIAR_CHOFER"));
              }
            }
            usleep(100);
            continue;
          }

          if ($valor_pantalla === 'PRINCIPAL') {
            
        

            /* Revisar si existe el archivo /validador/estado.json */
            if (!file_exists($archivo_validador_estado)) {  /* Si no existe */
              /* Crear el archivo = 1 */
              sleep(2);
              file_put_contents($archivo_validador_estado, json_encode(1));
            } else {

              /* Leer el archivo /validador/estado.json */
              $validador_estado = json_decode(file_get_contents($archivo_validador_estado), true);

              /* Si el valor es diferente de 1 */
              if ($validador_estado !== 1) {
                sleep(2);
                file_put_contents($archivo_validador_estado, json_encode(1));
              }
            }

            /* Revisar si existe el archivo /changer/estado.json */
            if (!file_exists($archivo_changer_estado)) { /* Si no existe */

              /* Crear el archivo = 1 */
              file_put_contents($archivo_changer_estado, json_encode(1));

            } else {

              /* Leer el archivo /changer/estado.json */
              $changer_estado = json_decode(file_get_contents($archivo_changer_estado), true);

              /* Si el valor es diferente de 1 */
              if ($changer_estado !== 1) {
                file_put_contents($archivo_changer_estado, json_encode(1));
              }
            }

            if (file_exists($archivo_display_tarjeta))
            {
              unlink($archivo_display_tarjeta);
            }
            if (file_exists($archivo_validador_tarjeta))
            {
              unlink($archivo_validador_tarjeta);
            }
            if (file_exists($archivo_validador_fallidas)){
              unlink($archivo_validador_fallidas);
            }
            if (file_exists($archivo_validador_recargas)){
              unlink($archivo_validador_recargas);
            }
            if (file_exists($archivo_validador_monto)){
              unlink($archivo_validador_monto);
            }

            usleep(100);
            continue;
          }

          if ($valor_pantalla === 'PRE_RECARGA' || $valor_pantalla === 'REALIZANDO_RECARGA' || $valor_pantalla === 'RECARGA' ) {

            /* Leer el archivo /validador/estado.json */
            $validador_estado = json_decode(file_get_contents($archivo_validador_estado), true);

            if ($validador_estado !== 2) {

              /* Escribir 2 en el archivo /validador/estado.json */
              file_put_contents($archivo_validador_estado, json_encode(2));
              file_put_contents($archivo_changer_estado, json_encode(0));

              usleep(100);
              continue;
            }
          }


          if ($valor_pantalla === "PUERTA_ABIERTA"){
            $validador_estado = json_decode(file_get_contents($archivo_validador_estado), true);
            if ($validador_estado !== 0) {

              /* Escribir 2 en el archivo /validador/estado.json */
              file_put_contents($archivo_validador_estado, json_encode(0));
              file_put_contents($archivo_changer_estado, json_encode(0));

              usleep(100);
              continue;
            }
          }

          if ($valor_pantalla === 'PUERTA_ABIERTA' || $valor_pantalla === 'FUERA_SERVICIO') {

            if (file_exists($archivo_maintainer_estado)){
              copy($archivo_maintainer_estado, $this->path . '/display/estados.json');
            }
          }

          usleep(100);
          continue;
        }

        usleep(100);
      } catch(\Exception $e) {
        $this->bitacora->errorLog('Configuracion', $e->getMessage());
        usleep(100);
        continue;
      }
    }
  }
}
