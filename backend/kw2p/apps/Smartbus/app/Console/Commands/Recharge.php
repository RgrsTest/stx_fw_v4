<?php

namespace Smartbus\Console\Commands;

use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;
use Smartbus\Models\Recarga;

class Recharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartbus:recharge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Proceso de recarga de tarjeta';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public $bitacora;

    public function __construct()
    {
      parent::__construct();

      config(['database.default' => 'Smartbus']);

      $this->bitacora = new Bitacora();

      $this->path = dirname(base_path(), 2).'/shared';
      checar_directorios(['validador', 'changer', 'display', 'sound', 'printer']);
    }

    public function handle()
    {
      $this->bitacora->infoLog('Recharge','Inicio el proceso de recarga');

      $archivo_validador_estado = $this->path.'/validador/estado.json';
      $archivo_validador_tarjeta = $this->path.'/validador/tarjeta.json';
      $archivo_changer_estado = $this->path.'/changer/estado.json';
      $archivo_changer_dinero = $this->path.'/changer/recargas.json';
    
      $archivo_validador_recargas = $this->path.'/validador/recargas.json';
      $archivo_validador_fallidas = $this->path.'/validador/fallidas.json';
      $archivo_validador_monto = $this->path.'/validador/monto.json';
      $archivo_display_tarjeta = $this->path.'/display/tarjeta.json';
      $archivo_changer_total_tubos = $this->path.'/changer/total_tubos.json';
      $archivo_printer_deuda = $this->path.'/printer/deuda.json';
      $archivo_config_identidad = $this->path.'/config/identidad.json';
      
      $archivo_validador_coin_return = $this->path.'/validador/coin_return.json';
      $archivo_validador_lastcoin = $this->path.'/validador/lastcoin.json';
      
      $archivo_validador_montoLapr = $this->path.'/validador/montoLapr.json';
    

      while(true) {
        try {
          /* Revisar si existe el archivo /validador/estado.json */
          if (file_exists($archivo_validador_estado)) {

            /* Revisar si el valor en el archivo es igual a 2 */
            $validador_estado = json_decode(file_get_contents($archivo_validador_estado), true);
            if ($validador_estado === 2) {

              if (file_exists($archivo_validador_tarjeta)) {
                $tarjeta = json_decode(file_get_contents($archivo_validador_tarjeta), true);
                if ($tarjeta['codigo'] == "TARJETA_OK")
                {
                  $this->bitacora->infoLog('Recharge','Se encontro tarjeta');
                    /* Copiar archivo /validador/tarjeta.json en /display/tarjeta.json */
                  copy($archivo_validador_tarjeta, $this->path.'/display/tarjeta.json');     
                   
                    
            
               

                  /* Escribir en el archivo /display/pantalla.json “RECARGA”, solo si dice PRE_RECARGA */
                  if (json_decode(file_get_contents($this->path . '/display/pantalla.json'), true) == "PRE_RECARGA"){
                    file_put_contents($this->path . '/display/pantalla.json', json_encode("RECARGA"));
                  }
                }
              }
              
              
              
              if(file_exists($archivo_validador_coin_return)) {
                
                if (json_decode(file_get_contents($archivo_validador_coin_return), true) != null)
                      {
                        file_put_contents($archivo_changer_estado, json_encode(0));
                        
                        $coinreturn = json_decode(file_get_contents($archivo_validador_coin_return), true);
                        
                        file_put_contents( $this->path.'/changer/pagar.json', $coinreturn);
                        
                        file_put_contents($archivo_changer_estado, json_encode(3));
                        
                        sleep(2);
                      }
                
                 //unlink($archivo_changer_dinero);
                
                 unlink($archivo_validador_coin_return);
               
                
              }
            
            
            
            
            

              /* Si existe el archivo validador recargas */
              if (file_exists($archivo_validador_recargas)) {
                $this->bitacora->infoLog('Recharge','Existe archivo de recarga');

                /* Escribir 0 en charger estado */
                file_put_contents($archivo_changer_estado, json_encode(0));
                file_put_contents($archivo_validador_estado, json_encode(0));

                /* Guardar evento en la  base de datos */
                $recargas = json_decode(file_get_contents($archivo_validador_recargas), true);
                $coordenadas = obtener_coordenadas();

                $identidad = json_decode(file_get_contents($archivo_config_identidad), true);

                foreach($recargas['eventos'] as $una_recarga) {
                  $validacion = new Recarga();
                  $validacion->fecha_hora = date('Y-m-d H:m:s');
                  $validacion->id_operador = $identidad['idOperador'];
                  $validacion->id_ruta = $identidad['idRutaDB'];
                  $validacion->folio_transaccion = $una_recarga['uid'] . $una_recarga['consecutivoAplicacion'];
                  $validacion->id_dispositivo = $identidad['idDispositivoDB'];
                  $validacion->id_vehiculo = $identidad['idVehiculo'];
                  $validacion->uid = $una_recarga['uid'];
                  $validacion->id_producto = $una_recarga['idProducto'];
                  $validacion->perfil = $una_recarga['perfil'];
                  $validacion->id_sam = $una_recarga['idSAM'];
                  $validacion->monto = $una_recarga['monto'];
                  $validacion->tipo_recarga = $una_recarga['tipoRecarga'];
                  $validacion->saldo_inicial = $una_recarga['saldoInicial'];
                  $validacion->saldo_final = $una_recarga['saldoFinal'];
                  $validacion->consecutivo_sam = $una_recarga['consecutivoSAM'];
                  $validacion->consecutivo_aplicacion = $una_recarga['consecutivoAplicacion'];
                  $validacion->latitud = $coordenadas['latitud'];
                  $validacion->longitud = $coordenadas['longitud'];
                  $validacion->save();
                  $this->bitacora->infoLog('Recharge','Se registro una recarga id:' . $validacion->id);
                }

                if (isset($recargas['cambio'])){
                  if($recargas['cambio']>0){
                    $total = json_decode(file_get_contents($archivo_changer_total_tubos), true);
                    if ($recargas['cambio']>$total){
                      file_put_contents($archivo_printer_deuda, json_encode($recargas['cambio']-$total));
                    }
                    $this->bitacora->infoLog('Recharge','Se tiene que dar cambio '. $recargas['cambio']);
                    file_put_contents( $this->path.'/changer/pagar.json', $recargas['cambio']);
                    file_put_contents($archivo_changer_estado, json_encode(3));
                  }
                }
                unlink($archivo_validador_tarjeta);
                file_put_contents($archivo_validador_estado, json_encode(0));
                file_put_contents($this->path . '/display/pantalla.json', json_encode("RECARGA_EXITOSA"));
                
                sleep(2);
                
                
                
                if (file_exists($archivo_validador_montoLapr)){
                   file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
                    unlink($archivo_validador_montoLapr);
                  
                }
             
                
                  /* Eliminar archivo tarjeta de display */
               // file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
               // unlink($archivo_display_tarjeta);
                copy($archivo_validador_recargas, $this->path . '/printer/recarga.json');

                /* Eliminar el archivo /validador/recargas.json */
                unlink($archivo_validador_recargas);
               
                usleep(100);
                continue;

              } 
              /* Si existe el archivo /validador/fallidas.json */
              if (file_exists($archivo_validador_fallidas)){
                $this->bitacora->infoLog('Recharge','Hay una recarga fallida');

                /* Escribir “RECARGA_FALLIDA” en el archivo /display/pantalla.json */
                file_put_contents($this->path . '/display/pantalla.json', json_encode("RECARGA_FALLIDA"));

                file_put_contents($archivo_changer_estado, json_encode(3));

                /* Copiar archivo /validador/faliidas.json a /changer/pagar.json (nombre diferente) */
                copy($archivo_validador_fallidas, $this->path.'/changer/pagar.json');

                /* Eliminar archivo tarjeta de display */
                //unlink($archivo_display_tarjeta);
                unlink($archivo_validador_tarjeta);

                unlink($archivo_validador_fallidas);

                sleep(2);
                /* Cambiar la vista a PRE_RECARGA */
                file_put_contents($this->path . '/display/pantalla.json', json_encode("PRE_RECARGA"));

                /* Inhabilitar el monedero */
                file_put_contents($archivo_changer_estado, json_encode(0));

                usleep(100);
                continue;
              }

              if (json_decode(file_get_contents($this->path . '/display/pantalla.json'), true) === "REALIZANDO_RECARGA"){
                file_put_contents($archivo_validador_estado, json_encode(2));
                if (file_exists($archivo_changer_dinero) && filesize($archivo_changer_dinero) > 0){
                  $this->bitacora->infoLog('Recharge', 'Moviendo archivo de monedero a validador');
                  copy($archivo_changer_dinero, $this->path . "/validador/monto.json");
                  $this->bitacora->infoLog('Recharge', 'Movido');
                  file_put_contents($archivo_changer_estado, json_encode(0));
                  unlink($archivo_changer_dinero);
                  unlink($this->path.'/display/monto.json');
                  
                }
                else{
                  // file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
                }
                usleep(100);
                continue;
              }

                /* Revisar si existe el archivo /validador/monto.json */
              if (file_exists($archivo_validador_monto)) {
                $this->bitacora->infoLog('Recharge','Hay validador monto');
                /* Escribir 0 en charger estado */
                if (file_exists($archivo_changer_estado)) {
                  file_put_contents($archivo_changer_estado, json_encode(0));
                } else {
                  file_put_contents($archivo_changer_estado, json_encode(0));
                }
              } else {
                if (file_exists($archivo_changer_dinero))
                {
                  $this->bitacora->infoLog('Recharge','Existe el archivo changer_dinero');
                  if (json_decode(file_get_contents($archivo_changer_dinero), true) != null)
                  {
                    $this->bitacora->infoLog('Recharge','Copiando el monto a display');
                    copy($archivo_changer_dinero, $this->path.'/display/monto.json');
                  }
                }
                file_put_contents($archivo_changer_estado, json_encode(2));
              }

            }
          }
          usleep(100);
        } catch(\Exception $e) {
          $this->bitacora->errorLog('Recharge', $e->getMessage());
          usleep(100);
          continue;
        }
      }
    }
}
