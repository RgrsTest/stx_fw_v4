<?php

namespace Smartbus\Console\Commands;

use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;

class HelloWorld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartbus:helloworld';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hola mundo, comando de prueba';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public $bitacora;

    public function __construct()
    {
      parent::__construct();

      config(['database.default' => 'Smartbus']);

      $this->bitacora = new Bitacora();
    }

    public function handle()
    {

      while(true) {
        $this->bitacora->infoLog('Smarbus','Hello world disparado desde library');
        echo "\n Hola mundo! \n";
        sleep(5);
      }

    }
}
