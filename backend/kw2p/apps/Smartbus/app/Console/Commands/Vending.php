<?php

namespace Smartbus\Console\Commands;

use Illuminate\Console\Command;
use Smartbus\Library\Bitacora;
use Smartbus\Models\Validacion;
use Smartbus\Models\Venta;

require(base_path('apps/Smartbus/app/Helpers/Helpers.php'));

class Vending extends Command
{

  protected $signature = 'smartbus:vending';

  protected $description = 'Proceso de cobro del monedero';

  public $bitacora;

  public $path;

  public $latitud = 0;
  public $longitud = 0;

  public function __construct()
  {
    parent::__construct();

    config(['database.default' => 'Smartbus']);

    $this->bitacora = new Bitacora();

    $this->path = dirname(base_path(), 2).'/shared';

    checar_directorios(['validador', 'changer', 'display', 'sound', 'printer']);
  }

  public function handle()
  {

    $this->bitacora->infoLog('Vending','Inicio el monedero');
    $archivo_eventos = $this->path.'/validador/eventos.json';
    $archivo_ventas = $this->path.'/changer/ventas.json';
    $archivo_dinero = $this->path.'/changer/dinero.json';
    $archivo_dinero_display = $this->path.'/display/dinero.json';
    $archivo_remanente = $this->path.'/changer/remanente.json';
    $archivo_config_identidad = $this->path.'/config/identidad.json';
    $audios = json_decode(file_get_contents(dirname(base_path(), 2).'/config/val_config.json'), true);

    while(true) {

      try {
        if (file_exists($archivo_eventos)) {

          file_put_contents($this->path . '/changer'. '/estado.json', json_encode(0));

          $eventos = json_decode(file_get_contents($archivo_eventos), true);
         
          copy($archivo_eventos, $this->path.'/display/validacion.json');

          if (count($eventos['eventos']) <= 0) {
            file_put_contents($this->path . '/sound/play.json', json_encode($audios["returnCodesCompra"][$eventos["codigo"]]["audio"]));
            file_put_contents($this->path . '/display/pantalla.json', json_encode("VALIDACION_ERROR"));
       
            echo "VALIDACION_ERROR \n";
            sleep(3);
            unlink($archivo_eventos);
            file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
            file_put_contents($this->path . '/changer/estado.json', json_encode(0));

            usleep(100);
            continue;
          } else {
            file_put_contents($this->path . '/sound/play.json', json_encode("ok"));
            file_put_contents($this->path . '/display/pantalla.json', json_encode("VALIDACION_OK"));
           
          }

          $identidad = json_decode(file_get_contents($archivo_config_identidad), true);

          $coordenadas = obtener_coordenadas();
          
          $pasajeros = obtener_contadores();

          foreach($eventos['eventos'] as $una_validacion) {

            $validacion = new Validacion();
            $validacion->fecha_hora = $una_validacion['fechaHora'];
            $validacion->id_operador = $identidad['idOperador'];
            $validacion->id_ruta = $identidad['idRutaDB'];
            $validacion->folio_transaccion = $una_validacion['uid'] . $una_validacion['consecutivoAplicacion'];
            $validacion->id_vehiculo = $identidad['idVehiculo'];
            $validacion->id_dispositivo = $identidad['idDispositivoDB'];
            $validacion->uid = $una_validacion['uid'];
            $validacion->id_producto = $una_validacion['idProducto'];
            $validacion->perfil = $una_validacion['perfil'];
            $validacion->id_sam = $una_validacion['idSAM'];
            $validacion->tipo_debito = $una_validacion['idDebito'];
            $validacion->monto = $una_validacion['monto'];
            $validacion->saldo_inicial = $una_validacion['saldoInicial'];
            $validacion->saldo_final = $una_validacion['saldoFinal'];
            $validacion->consecutivo_sam = $una_validacion['consecutivoSAM'];
            $validacion->consecutivo_aplicacion = $una_validacion['consecutivoAplicacion'];
            $validacion->latitud = $coordenadas['latitud'];
            $validacion->longitud = $coordenadas['longitud'];
            $validacion->subidas = $pasajeros['subidas'];
            $validacion->bajadas = $pasajeros['bajadas'];
            $validacion->save();

            $this->bitacora->infoLog('Vending','Se registro una validación id:' . $validacion->id);

          }
          copy($archivo_eventos, $this->path.'/printer/validacion.json');
          sleep(3);
          unlink($archivo_eventos);
          file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
          file_put_contents($this->path . '/changer/estado.json', json_encode(0));

          usleep(100);
          continue;
        }

        if (file_exists($archivo_ventas)) {

          file_put_contents($this->path . '/validador/estado.json', json_encode(0));

          $venta_temp = json_decode(file_get_contents($archivo_ventas), true);

          copy($archivo_ventas, $this->path.'/printer/venta.json');

          file_put_contents($this->path . '/display/pantalla.json', json_encode("VENTA"));
          file_put_contents($this->path . '/sound'. '/play.json', json_encode("ok"));

          $coordenadas = obtener_coordenadas();

          $identidad = json_decode(file_get_contents($archivo_config_identidad), true);

          $venta = new Venta();
          $venta->fecha_hora = $venta_temp['fechaHora'];
          $venta->id_operador = $identidad['idOperador'];
          $venta->id_ruta = $identidad['idRutaDB'];
          $venta->id_vehiculo = $identidad['idVehiculo'];
          $venta->id_dispositivo = $identidad['idDispositivoDB'];
          $venta->monto = $venta_temp['monto'];
          $venta->tipo = $venta_temp['tipo'];
          $venta->latitud = $coordenadas['latitud'];
          $venta->longitud = $coordenadas['longitud'];
          $venta->folio_transaccion = nanoId();
          $venta->save();

          $this->bitacora->infoLog('Vending','Se registro una venta id:' . $venta->id);
          
          sleep(3);

          unlink($archivo_ventas);
          file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
          file_put_contents($this->path . '/validador/estado.json', json_encode(0));

          usleep(100);
          continue;

        }

        if (file_exists($archivo_dinero)) {
          if (json_decode(file_get_contents($this->path . '/changer'. '/estado.json')) == 1)
          {
            copy($archivo_dinero, $this->path.'/display/dinero.json');
            file_put_contents($this->path . '/display/pantalla.json', json_encode("DINERO_INGRESADO"));
            sleep(1);
            continue;
          }
        } else {
          if (file_exists($archivo_dinero_display)) {
            file_put_contents($this->path . '/display/pantalla.json', json_encode("PRINCIPAL"));
            unlink($archivo_dinero_display);
          }
        }

        if (file_exists($archivo_remanente)) {

          $remanente_temp = json_decode(file_get_contents($archivo_remanente), true);

          $coordenadas = obtener_coordenadas();
          
          $pasajeros = obtener_contadores();

          $identidad = json_decode(file_get_contents($archivo_config_identidad), true);

          $venta = new Venta();
          $venta->fecha_hora = $remanente_temp['fechaHora'];
          $venta->id_operador = $identidad['idOperador'];
          $venta->id_ruta = $identidad['idRutaDB'];
          $venta->id_vehiculo = $identidad['idVehiculo'];
          $venta->id_dispositivo = $identidad['idDispositivoDB'];
          $venta->monto = $remanente_temp['monto'];
          $venta->tipo = $remanente_temp['tipo'];
          $venta->latitud = $coordenadas['latitud'];
          $venta->longitud = $coordenadas['longitud'];
          $venta->subidas = $pasajeros['subidas'];
          $venta->bajadas = $pasajeros['bajadas'];
          $venta->folio_transaccion = nanoId();
          $venta->save();

          $this->bitacora->infoLog('Vending','Se registro una venta remanente id:' . $venta->id);

          unlink($archivo_remanente);
        }
        usleep(100);
      } catch(\Exception $e) {
        $this->bitacora->errorLog('Vending', $e->getMessage());
        usleep(100);
        continue;
      }

    }
  }

}
