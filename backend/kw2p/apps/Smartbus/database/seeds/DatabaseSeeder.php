<?php

namespace SmartbusSeeder;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      \Klay\Models\Usuario::firstOrCreate([
        'correo' => 'desarrollo@stx.com.mx'],[
        'contrasena' => bcrypt('0000'),
        'apikey' => nanoId(),
        'roles' => [],
        'credenciales' => [
          '@action:*',
          '@schema:*',
          '@catalogo:*',
          '@flujo:*',
          '@documento:*',
        ]
      ]);

      $this->call([
        CortesSeeder::class,
       ]);
    }
  }
