<?php

namespace SmartbusSeeder;

use Illuminate\Database\Seeder;
use Smartbus\Models\Corte;

class CortesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      Corte::firstOrCreate([
        'fecha_inicio' => date('Y-m-d'),
        'fecha_fin' => date('Y-m-d')
      ]);
    }
  }
