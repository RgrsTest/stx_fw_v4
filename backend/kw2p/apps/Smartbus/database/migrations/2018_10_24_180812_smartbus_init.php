<?php

require base_path('apps/Klay/database/migrations/2018_12_12_000001_klay_init.php');

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SmartbusInit extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    (new KlayInit)->up();

    Schema::create('listas_lap_v', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('uid', 15);
      $table->integer('no_accion_aplicada');
      $table->integer('accion');
      $table->string('id_producto', 6);
      $table->string('id_sam', 15)->nullable();
      $table->integer('enviada')->default(0);
      $table->timestamps();
    });

    Schema::create('listas_lap_r', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('uid', 15);
      $table->dateTime('fecha_hora');
      $table->integer('numero_accion_aplicada');
      $table->integer('monto');
      $table->string('id_producto', 6);
      $table->string('id_sam', 15)->nullable();
      $table->integer('id_lapr');
      $table->integer('enviada')->default(0);
      $table->timestamps();
    });

    Schema::create('listas_lam', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('uid', 15);
      $table->dateTime('fecha_hora');
      $table->integer('no_consecutivo_accion');
      $table->integer('accion');
      $table->integer('aplicada')->nullable();
      $table->integer('enviada')->default(0);
      $table->timestamps();
    });
    
    Schema::create('contador_pasajeros', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->dateTime('fecha_hora');
      $table->integer('subidas');
      $table->integer('bajadas');
      $table->timestamps();
    });

    Schema::create('ventas', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->dateTime('fecha_hora');
      $table->integer('id_operador');
      $table->integer('id_ruta');
      $table->integer('id_vehiculo');
      $table->integer('id_dispositivo');
      $table->integer('monto');
      $table->string('tipo', 20);
      $table->double('latitud');
      $table->double('longitud');
      $table->double('subidas');
      $table->double('bajadas');
      $table->integer('enviada')->default(0);
      $table->string('folio_transaccion');
      $table->timestamps();
    });

    Schema::create('validaciones', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->dateTime('fecha_hora');
      $table->integer('id_operador');
      $table->integer('id_ruta');
      $table->string('folio_transaccion', 30);
      $table->integer('id_vehiculo');
      $table->integer('id_dispositivo');
      $table->string('uid', 15);
      $table->string('id_producto', 6);
      $table->string('perfil', 2);
      $table->string('id_sam', 15);
      $table->integer('tipo_debito');
      $table->integer('monto');
      $table->integer('saldo_inicial');
      $table->integer('saldo_final');
      $table->string('consecutivo_sam', 15);
      $table->string('consecutivo_aplicacion', 12);
      $table->double('latitud');
      $table->double('longitud');
      $table->double('subidas');
      $table->double('bajadas');
      $table->integer('enviada')->default(0);
      $table->timestamps();
    });

    Schema::create('eventos', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('evento', 255);
      $table->integer('enviado')->default(0);
      $table->timestamps();
    });

    Schema::create('cortes', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->dateTime('fecha_inicio');
      $table->dateTime('fecha_fin');
      $table->integer('eventos_validaciones')->nullable();
      $table->integer('eventos_ventas')->nullable();
      $table->integer('eventos_bpd')->nullable();
      $table->integer('monto_validaciones')->nullable();
      $table->integer('monto_ventas')->nullable();
      $table->timestamps();
    });

    Schema::create('recargas', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->dateTime('fecha_hora');
      $table->integer('id_operador');
      $table->integer('id_ruta');
      $table->string('folio_transaccion', 30);
      $table->integer('id_dispositivo');
      $table->integer('id_vehiculo');
      $table->string('uid', 15);
      $table->string('id_producto', 6);
      $table->string('perfil', 2);
      $table->string('id_sam', 15);
      $table->integer('monto');
      $table->string('tipo_recarga', 3);
      $table->integer('saldo_inicial');
      $table->integer('saldo_final');
      $table->string('consecutivo_sam', 15);
      $table->string('consecutivo_aplicacion', 12);
      $table->double('latitud');
      $table->double('longitud');
      $table->integer('enviada')->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    (new KlayInit)->down();

    Schema::dropIfExists('listas_lap_v');
    Schema::dropIfExists('listas_lap_r');
    Schema::dropIfExists('listas_lam');
    Schema::dropIfExists('ventas');
    Schema::dropIfExists('validaciones');
    Schema::dropIfExists('eventos');
    Schema::dropIfExists('cortes');
    Schema::dropIfExists('recargas');
  }
}
