<?php

namespace Klay\Actions;

use Archiving\SDK\Client;
use Archiving\SDK\Exception as SDKException;
use Archiving\SDK\Flujo;
use Illuminate\Support\Arr;
use Klayware\Actions\Action;
use Klayware\Exceptions\KlayException;

class FlujosAction extends Action
{
  /**
   * If request is a bulk operation
   *
   * @var boolean
   */
  protected $is_bulk;

  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
    $this->is_bulk = !empty(request()->all()) && !Arr::isAssoc(request()->all());
  }

  public function consultar() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'with' => 'filled|array',
      'with-storage' => 'filled|array',
      'filter' => 'filled|array',
      'filter.*' => 'filled|array',
      'filter.where.*.field' => 'required|string',
      'filter.where.*.operator' => 'required|string',
      'filter.where.*.value' => 'present|nullable',
      'filter.orwhere.*.field' => 'required|string',
      'filter.orwhere.*.operator' => 'required|string',
      'filter.orwhere.*.value' => 'required',
      'filter.orWhere.*.field' => 'required|string',
      'filter.orWhere.*.operator' => 'required|string',
      'filter.orWhere.*.value' => 'required',
      'filter.wherein.*.field' => 'required|string',
      'filter.wherein.*.value' => 'required|array',
      'filter.whereIn.*.field' => 'required|string',
      'filter.whereIn.*.value' => 'required|array',
      'filter.orWhereIn.*.field' => 'required|string',
      'filter.orWhereIn.*.value' => 'required|array',
      'filter.whereJoin.*.field' => 'required|string',
      'filter.whereJoin.*.operator' => 'required|string',
      'filter.whereJoin.*.value' => 'present|nullable',
      'filter.orWhereJoin.*.field' => 'required|string',
      'filter.orWhereJoin.*.operator' => 'required|string',
      'filter.orWhereJoin.*.value' => 'present|nullable',

      'filter.and.where.*.field' => 'required|string',
      'filter.and.where.*.operator' => 'required|string',
      'filter.and.where.*.value' => 'present|nullable',
      'filter.and.orWhere.*.field' => 'required|string',
      'filter.and.orWhere.*.operator' => 'required|string',
      'filter.and.orWhere.*.value' => 'required',
      'filter.and.whereIn.*.field' => 'required|string',
      'filter.and.whereIn.*.value' => 'required|array',
      'filter.and.orWhereIn.*.field' => 'required|string',
      'filter.and.orWhereIn.*.value' => 'required|array',
      'filter.and.whereJoin.*.field' => 'required|string',
      'filter.and.whereJoin.*.operator' => 'required|string',
      'filter.and.whereJoin.*.value' => 'present|nullable',
      'filter.and.orWhereJoin.*.field' => 'required|string',
      'filter.and.orWhereJoin.*.operator' => 'required|string',
      'filter.and.orWhereJoin.*.value' => 'present|nullable',

      'filter.or.where.*.field' => 'required|string',
      'filter.or.where.*.operator' => 'required|string',
      'filter.or.where.*.value' => 'present|nullable',
      'filter.or.orWhere.*.field' => 'required|string',
      'filter.or.orWhere.*.operator' => 'required|string',
      'filter.or.orWhere.*.value' => 'required',
      'filter.or.whereIn.*.field' => 'required|string',
      'filter.or.whereIn.*.value' => 'required|array',
      'filter.or.orWhereIn.*.field' => 'required|string',
      'filter.or.orWhereIn.*.value' => 'required|array',
      'filter.or.whereJoin.*.field' => 'required|string',
      'filter.or.whereJoin.*.operator' => 'required|string',
      'filter.or.whereJoin.*.value' => 'present|nullable',
      'filter.or.orWhereJoin.*.field' => 'required|string',
      'filter.or.orWhereJoin.*.operator' => 'required|string',
      'filter.or.orWhereJoin.*.value' => 'present|nullable',

      'order' => 'filled|array',
      'order.*.field' => 'required|string',
      'order.*.direction' => 'required|string',
      'orderByJoin' => 'filled|array',
      'orderByJoin.*.field' => 'required|string',
      'orderByJoin.*.direction' => 'required|string',
      'limit' => 'filled|integer',
      'page' => 'filled|integer'
    ])->validate();

    $cliente = new Client(config('services.archiving'));
    $paginate = null;

    try {
      if ($before = $this->resolveBeforeSchemaActionClass($params['schema'], __FUNCTION__)) {
        $before($params);
      }

      $flujos = Flujo::consultar($params['schema'], $cliente, $params, $paginate);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $flujos);
      }

    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    # Resolución
    if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
      $flujos = $resolution($params, $flujos)->getOriginalContent()['data'];
    }

    # Filtro flujos
    $recurso = ['method' => __FUNCTION__];
    $flujos = $flujos->filter(function ($flujo, $key) use (&$recurso) {
      $recurso = array_merge($recurso, $flujo->toArray());
      return auth()->user()->can('@resource', ['flujo', $recurso]);
    })->map(function ($flujo, $key) use (&$recurso) {
        # Filtro almacenamientos
        foreach (array_keys($flujo->payload) as $storage) {
          $recurso = array_merge($recurso, ['storage' => $storage]);
          if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
            $flujo->payload = array_except($flujo->payload, [$storage]);
          }
        }
        return $flujo;
    })->values();

    return response()->json([
      'status' => 'success',
      'data' => $flujos,
      'paginate' => $paginate
    ]);
  }

  public function crear() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'etapa' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $flujo = new Flujo($data['schema'], $cliente);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }
        $flujo->validarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
          $params[$index] = $flujo->crear($data['etapa'], $data['serie'] ?? '', $data['payload'] ?? null, $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function actualizar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }
        $flujo->validarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->actualizar($data['payload'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function limpiar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }
        $flujo->validarLimpiarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->limpiar($data['payload'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function remplazar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }
        $flujo->validarRemplazarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->remplazar($data['payload'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function avanzar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'etapa' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->avanzar($data['etapa'], $data['payload'] ?? null, $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function retroceder() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->retroceder($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function abrir() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->abrir($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function cerrar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->cerrar($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function cancelar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->cancelar($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function descancelar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->descancelar($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $flujo) {
      # Si no es instancia de flujo, array de excepción
      if (!$flujo instanceof Flujo) continue;
      $filtered_payload = [];
      foreach (array_keys($flujo->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $flujo->toArray();
        if (auth()->user()->can('@resource', ['flujo', $recurso])) {
          $filtered_payload[$storage] = $flujo->payload[$storage];
        }
      }
      $flujo->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Flujo) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function comentar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'comentario' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->comentar($data['comentario'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof \stdClass) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : (array) $params[0]
    ]);
  }

  public function descomentar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'comentario_id' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $flujo = Flujo::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $flujo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $flujo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $flujo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $flujo->descomentar($data['comentario_id'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && $params[0] !== true) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function comentarios() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'folio' => 'required|integer',
      'serie' => 'nullable|alpha',
    ])->validate();

    $recurso = ['method' => __FUNCTION__] + $params;
    if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
      return response()->json([
        'status' => 'fail',
        'message' => 'Usuario no cuenta con las credenciales requeridas.',
        'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes'
      ], 401);
    }

    $cliente = new Client(config('services.archiving'));

    try {
      $flujo = Flujo::cargar($params['schema'], $params['folio'], $params['serie'] ?? '', $cliente);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $flujo);
      }

      if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
        $before($data, $flujo);
      }

      if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
        $comentarios = $resolution($params, $flujo)->getOriginalContent()['data'];
      } else {
          $comentarios = $flujo->comentarios();
      }
    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $comentarios
    ]);
  }

  public function eventos() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'folio' => 'required|integer',
      'serie' => 'nullable|alpha',
    ])->validate();

    $recurso = ['method' => __FUNCTION__] + $params;
    if (!auth()->user()->can('@resource', ['flujo', $recurso])) {
      return response()->json([
        'status' => 'fail',
        'message' => 'Usuario no cuenta con las credenciales requeridas.',
        'code' => 'klay.actions.flujos.'.__FUNCTION__.'.credenciales_insuficientes'
      ], 401);
    }

    $cliente = new Client(config('services.archiving'));

    try {
      $flujo = Flujo::cargar($params['schema'], $params['folio'], $params['serie'] ?? '', $cliente);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $flujo);
      }

      if ($before = $this->resolveBeforeSchemaActionClass($params['schema'], __FUNCTION__)) {
        $before($params, $flujo);
      }

      if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
        $eventos = $resolution($params, $flujo)->getOriginalContent()['data'];
      } else {
          $eventos = $flujo->eventos();
      }
    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $eventos
    ]);
  }

  private function resolveSchemaActionClass($schema, $method)
  {
    $class =  kw2p_instancia() . '\\Actions\\' . studly_case("{$schema}Action");
    if (class_exists($class) && method_exists($class, $method) && $class != static::class && is_subclass_of($class, 'Klay\Actions\ResolutionAction')) {
      return (new \ReflectionMethod($class, $method))->getClosure(new $class);
    }
    return false;
  }

  private function resolveBeforeSchemaActionClass($schema, $method)
  {
    return $this->resolveSchemaActionClass($schema, "{$method}Before");
  }
}
