<?php

namespace Klay\Actions;

use Archiving\SDK\Catalogo;
use Archiving\SDK\Client;
use Archiving\SDK\Exception as SDKException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Klayware\Actions\Action;
use Klayware\Exceptions\KlayException;

class CatalogosAction extends Action
{
  /**
   * If request is a bulk operation
   *
   * @var boolean
   */
  protected $is_bulk;

  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
    $this->is_bulk = !empty(request()->all()) && !Arr::isAssoc(request()->all());
  }

  public function consultar() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'with' => 'filled|array',
      'with-storage' => 'filled|array',
      'with-trashed' => 'filled|boolean',
      'filter' => 'filled|array',
      'filter.*' => 'filled|array',
      'filter.where.*.field' => 'required|string',
      'filter.where.*.operator' => 'required|string',
      'filter.where.*.value' => 'present|nullable',
      'filter.orwhere.*.field' => 'required|string',
      'filter.orwhere.*.operator' => 'required|string',
      'filter.orwhere.*.value' => 'required',
      'filter.orWhere.*.field' => 'required|string',
      'filter.orWhere.*.operator' => 'required|string',
      'filter.orWhere.*.value' => 'required',
      'filter.wherein.*.field' => 'required|string',
      'filter.wherein.*.value' => 'required|array',
      'filter.whereIn.*.field' => 'required|string',
      'filter.whereIn.*.value' => 'required|array',
      'filter.orWhereIn.*.field' => 'required|string',
      'filter.orWhereIn.*.value' => 'required|array',
      'filter.whereJoin.*.field' => 'required|string',
      'filter.whereJoin.*.operator' => 'required|string',
      'filter.whereJoin.*.value' => 'present|nullable',
      'filter.orWhereJoin.*.field' => 'required|string',
      'filter.orWhereJoin.*.operator' => 'required|string',
      'filter.orWhereJoin.*.value' => 'present|nullable',

      'filter.and.where.*.field' => 'required|string',
      'filter.and.where.*.operator' => 'required|string',
      'filter.and.where.*.value' => 'present|nullable',
      'filter.and.orWhere.*.field' => 'required|string',
      'filter.and.orWhere.*.operator' => 'required|string',
      'filter.and.orWhere.*.value' => 'required',
      'filter.and.whereIn.*.field' => 'required|string',
      'filter.and.whereIn.*.value' => 'required|array',
      'filter.and.orWhereIn.*.field' => 'required|string',
      'filter.and.orWhereIn.*.value' => 'required|array',
      'filter.and.whereJoin.*.field' => 'required|string',
      'filter.and.whereJoin.*.operator' => 'required|string',
      'filter.and.whereJoin.*.value' => 'present|nullable',
      'filter.and.orWhereJoin.*.field' => 'required|string',
      'filter.and.orWhereJoin.*.operator' => 'required|string',
      'filter.and.orWhereJoin.*.value' => 'present|nullable',

      'filter.or.where.*.field' => 'required|string',
      'filter.or.where.*.operator' => 'required|string',
      'filter.or.where.*.value' => 'present|nullable',
      'filter.or.orWhere.*.field' => 'required|string',
      'filter.or.orWhere.*.operator' => 'required|string',
      'filter.or.orWhere.*.value' => 'required',
      'filter.or.whereIn.*.field' => 'required|string',
      'filter.or.whereIn.*.value' => 'required|array',
      'filter.or.orWhereIn.*.field' => 'required|string',
      'filter.or.orWhereIn.*.value' => 'required|array',
      'filter.or.whereJoin.*.field' => 'required|string',
      'filter.or.whereJoin.*.operator' => 'required|string',
      'filter.or.whereJoin.*.value' => 'present|nullable',
      'filter.or.orWhereJoin.*.field' => 'required|string',
      'filter.or.orWhereJoin.*.operator' => 'required|string',
      'filter.or.orWhereJoin.*.value' => 'present|nullable',

      'order' => 'filled|array',
      'order.*.field' => 'required|string',
      'order.*.direction' => 'required|string',
      'orderByJoin' => 'filled|array',
      'orderByJoin.*.field' => 'required|string',
      'orderByJoin.*.direction' => 'required|string',
      'limit' => 'filled|integer',
      'page' => 'filled|integer'
    ])->validate();

    $cliente = new Client(config('services.archiving'));
    $paginate = null;

    try {
      if ($before = $this->resolveBeforeSchemaActionClass($params['schema'], __FUNCTION__)) {
        $before($params);
      }

      $catalogos = Catalogo::consultar($params['schema'], $cliente, $params, $paginate);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $catalogos);
      }

    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    # Resolución
    if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
      $catalogos = $resolution($params, $catalogos)->getOriginalContent()['data'];

      if (request('collection.limit', null)) {
        $limit = request('collection.limit', 15);
        $page = request('collection.page', 1);
        $for_page = $catalogos->forPage($page, $limit);
        $paginate = new LengthAwarePaginator($for_page, $catalogos->count(), $limit, $page);
        $paginate = array_except($paginate->toArray(), ['data', 'first_page_url','next_page_url','path','prev_page_url','last_page_url']);
        $catalogos = $for_page;
      }
    }

    # Filtro catalogos
    $fn = __FUNCTION__;
    $catalogos = $catalogos->filter(function ($catalogo, $key) use ($fn) {
      $recurso = ['method' => $fn] + $catalogo->toArray();
      return auth()->user()->can('@resource', ['catalogo', $recurso]);
    })->values();

    return response()->json([
      'status' => 'success',
      'data' => $catalogos,
      'paginate' => $paginate
    ]);
  }

  public function actualizar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'id' => 'filled|integer',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['catalogo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.catalogos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        if (isset($data['id'])) {
          $catalogo = Catalogo::cargar($data['schema'], $data['id'], $cliente, [
            'with' => ['*'],
            'with-storage' => ['*']
          ]);
        } else {
          $catalogo = new Catalogo($data['schema'], $cliente);
        }

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $catalogo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $catalogo);
        }
        $catalogo->validarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $catalogo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $catalogo->actualizar($data['payload'], $data['id'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Catalogo) {
      return response()->json(array_except($params[0], ['scode']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function limpiar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'id' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['catalogo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.catalogos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        if (isset($data['id'])) {
          $catalogo = Catalogo::cargar($data['schema'], $data['id'], $cliente, [
            'with' => ['*'],
            'with-storage' => ['*']
          ]);
        } else {
          $catalogo = new Catalogo($data['schema'], $cliente);
        }

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $catalogo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $catalogo);
        }
        $catalogo->validarLimpiarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $catalogo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $catalogo->limpiar($data['payload'], $data['id'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Catalogo) {
      return response()->json(array_except($params[0], ['scode']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function remplazar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'id' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;

      if (!auth()->user()->can('@resource', ['catalogo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.catalogos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        if (isset($data['id'])) {
          $catalogo = Catalogo::cargar($data['schema'], $data['id'], $cliente, [
            'with' => ['*'],
            'with-storage' => ['*']
          ]);
        } else {
          $catalogo = new Catalogo($data['schema'], $cliente);
        }

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $catalogo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $catalogo);
        }
        $catalogo->validarRemplazarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $catalogo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $catalogo->remplazar($data['payload'], $data['id'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Catalogo) {
      return response()->json(array_except($params[0], ['scode']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function eliminar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'id' => 'required|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['catalogo', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.catalogos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $catalogo = Catalogo::cargar($data['schema'], $data['id'], $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $catalogo);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $catalogo);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $catalogo)->getOriginalContent()['data'];
        } else {
            $params[$index] = $catalogo->eliminar($data['id']);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && $params[0] !== true) {
      return response()->json(array_except($params[0], ['scode']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function validar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'id' => 'filled|integer',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      try {
        $catalogo = new Catalogo($data['schema'], $cliente);
        $params[$index] = $catalogo->validarSchema($data['payload']);
      } catch (SDKException $e) {
        $params[$index] = $e->toArray();
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && $params[0] !== true) {
      return response()->json(array_except($params[0], ['scode']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  protected function resolveSchemaActionClass($schema, $method)
  {
    $class =  kw2p_instancia() . '\\Actions\\' . studly_case("{$schema}Action");
    if (class_exists($class) && method_exists($class, $method) && $class != static::class && is_subclass_of($class, 'Klay\Actions\ResolutionAction')) {
      return (new \ReflectionMethod($class, $method))->getClosure(new $class);
    }
    return false;
  }

  protected function resolveBeforeSchemaActionClass($schema, $method)
  {
    return $this->resolveSchemaActionClass($schema, "{$method}Before");
  }
}
