<?php

namespace Klay\Actions;

use Archiving\SDK\Client;
use Archiving\SDK\Media;
use Klayware\Actions\Action;

class MediaAction extends Action
{
  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
  }

  public function cargar()
  {
    $params = \Validator::make(request()->all(), [
      'media' => 'required|file',
    ])->validate();

    $cliente = new Client(config('services.archiving'));

    $media = Media::cargar($params['media'], $cliente);

    return response()->json([
      'status' => 'success',
      'data' => $media
    ]);
  }

  public function obtener()
  {
    $params = \Validator::make(request()->all(), [
      'media' => 'required|string',
    ])->validate();

    $download = request()->has('download');

    $cliente = new Client(config('services.archiving'));

    $media = Media::obtener($params['media'], $cliente);

    if (!$media) {
      return response('', 404);
    }

    if ($download) {
      $disposition = str_replace('inline', 'attachment', $media->getHeader('Content-Disposition'));
    } else {
      $disposition = $media->getHeader('Content-Disposition');
    }

    return response((string) $media->getBody(), 200)->withHeaders([
      'Content-Type' => $media->getHeader('Content-Type'),
      'Content-Length' => $media->getHeader('Content-Length'),
      'Content-Disposition' => $disposition
    ]);
  }
}
