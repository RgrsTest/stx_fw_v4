<?php

namespace Klay\Actions;

use Archiving\SDK\Client;
use Archiving\SDK\Archivo;
use Klayware\Actions\Action;

class ArchivosAction extends Action
{
  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
  }

  public function obtener()
  {
    $params = \Validator::make(request()->all(), [
      'file' => 'required|string',
    ])->validate();

    $cliente = new Client(config('services.archiving'));

    $archivo = Archivo::obtener($params['file'], $cliente);

    if (!$archivo) {
      return response('', 404);
    }

    return response((string) $archivo->getBody(), 200)->withHeaders([
      'Content-Type' => $archivo->getHeader('Content-Type'),
      'Content-Length' => $archivo->getHeader('Content-Length'),
      'Content-Disposition' => $archivo->getHeader('Content-Disposition')
    ]);
  }
}
