<?php

namespace Klay\Actions;

use Archiving\SDK\Client;
use Archiving\SDK\Documento;
use Archiving\SDK\Exception as SDKException;
use Illuminate\Support\Arr;
use Klay\Actions\ConsultarAction;
use Klayware\Actions\Action;
use Klayware\Exceptions\KlayException;

class DocumentosAction extends Action
{
  /**
   * If request is a bulk operation
   *
   * @var boolean
   */
  protected $is_bulk;

  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
    $this->is_bulk = !empty(request()->all()) && !Arr::isAssoc(request()->all());
  }

  public function consultar() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'with' => 'filled|array',
      'with-storage' => 'filled|array',
      'filter' => 'filled|array',
      'filter.*' => 'filled|array',
      'filter.where.*.field' => 'required|string',
      'filter.where.*.operator' => 'required|string',
      'filter.where.*.value' => 'present|nullable',
      'filter.orwhere.*.field' => 'required|string',
      'filter.orwhere.*.operator' => 'required|string',
      'filter.orwhere.*.value' => 'required',
      'filter.orWhere.*.field' => 'required|string',
      'filter.orWhere.*.operator' => 'required|string',
      'filter.orWhere.*.value' => 'required',
      'filter.wherein.*.field' => 'required|string',
      'filter.wherein.*.value' => 'required|array',
      'filter.whereIn.*.field' => 'required|string',
      'filter.whereIn.*.value' => 'required|array',
      'filter.orWhereIn.*.field' => 'required|string',
      'filter.orWhereIn.*.value' => 'required|array',
      'filter.whereJoin.*.field' => 'required|string',
      'filter.whereJoin.*.operator' => 'required|string',
      'filter.whereJoin.*.value' => 'present|nullable',
      'filter.orWhereJoin.*.field' => 'required|string',
      'filter.orWhereJoin.*.operator' => 'required|string',
      'filter.orWhereJoin.*.value' => 'present|nullable',

      'filter.and.where.*.field' => 'required|string',
      'filter.and.where.*.operator' => 'required|string',
      'filter.and.where.*.value' => 'present|nullable',
      'filter.and.orWhere.*.field' => 'required|string',
      'filter.and.orWhere.*.operator' => 'required|string',
      'filter.and.orWhere.*.value' => 'required',
      'filter.and.whereIn.*.field' => 'required|string',
      'filter.and.whereIn.*.value' => 'required|array',
      'filter.and.orWhereIn.*.field' => 'required|string',
      'filter.and.orWhereIn.*.value' => 'required|array',
      'filter.and.whereJoin.*.field' => 'required|string',
      'filter.and.whereJoin.*.operator' => 'required|string',
      'filter.and.whereJoin.*.value' => 'present|nullable',
      'filter.and.orWhereJoin.*.field' => 'required|string',
      'filter.and.orWhereJoin.*.operator' => 'required|string',
      'filter.and.orWhereJoin.*.value' => 'present|nullable',

      'filter.or.where.*.field' => 'required|string',
      'filter.or.where.*.operator' => 'required|string',
      'filter.or.where.*.value' => 'present|nullable',
      'filter.or.orWhere.*.field' => 'required|string',
      'filter.or.orWhere.*.operator' => 'required|string',
      'filter.or.orWhere.*.value' => 'required',
      'filter.or.whereIn.*.field' => 'required|string',
      'filter.or.whereIn.*.value' => 'required|array',
      'filter.or.orWhereIn.*.field' => 'required|string',
      'filter.or.orWhereIn.*.value' => 'required|array',
      'filter.or.whereJoin.*.field' => 'required|string',
      'filter.or.whereJoin.*.operator' => 'required|string',
      'filter.or.whereJoin.*.value' => 'present|nullable',
      'filter.or.orWhereJoin.*.field' => 'required|string',
      'filter.or.orWhereJoin.*.operator' => 'required|string',
      'filter.or.orWhereJoin.*.value' => 'present|nullable',

      'order' => 'filled|array',
      'order.*.field' => 'required|string',
      'order.*.direction' => 'required|string',
      'orderByJoin' => 'filled|array',
      'orderByJoin.*.field' => 'required|string',
      'orderByJoin.*.direction' => 'required|string',
      'limit' => 'filled|integer',
      'page' => 'filled|integer'
    ])->validate();

    $cliente = new Client(config('services.archiving'));
    $paginate = null;

    try {
      if ($before = $this->resolveBeforeSchemaActionClass($params['schema'], __FUNCTION__)) {
        $before($params);
      }

      $documentos = Documento::consultar($params['schema'], $cliente, $params, $paginate);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $documentos);
      }

    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    # Resolución
    if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
      $documentos = $resolution($params, $documentos)->getOriginalContent()['data'];
    }

    # Filtro documentos
    $recurso = ['method' => __FUNCTION__];
    $documentos = $documentos->filter(function ($documento, $key) use (&$recurso) {
      $recurso = array_merge($recurso, $documento->toArray());
      return auth()->user()->can('@resource', ['documento', $recurso]);
    })->map(function ($documento, $key) use (&$recurso) {
        # Filtro almacenamientos
        foreach (array_keys($documento->payload) as $storage) {
          $recurso = array_merge($recurso, ['storage' => $storage]);
          if (!auth()->user()->can('@resource', ['documento', $recurso])) {
            $documento->payload = array_except($documento->payload, [$storage]);
          }
        }
        return $documento;
    })->values();

    return response()->json([
      'status' => 'success',
      'data' => $documentos,
      'paginate' => $paginate
    ]);
  }

  public function crear() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['documento', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $documento = new Documento($data['schema'], $cliente);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }
        $documento->validarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->crear($data['serie'] ?? '', $data['payload'] ?? null, $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Documento) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function actualizar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['documento', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }
        $documento->validarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->actualizar($data['payload'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $documento) {
      # Si no es instancia de documento, array de excepción
      if (!$documento instanceof Documento) continue;
      $filtered_payload = [];
      foreach (array_keys($documento->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $documento->toArray();
        if (auth()->user()->can('@resource', ['documento', $recurso])) {
          $filtered_payload[$storage] = $documento->payload[$storage];
        }
      }
      $documento->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Documento) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function limpiar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['documento', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }
        $documento->validarLimpiarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->limpiar($data['payload'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $documento) {
      # Si no es instancia de documento, array de excepción
      if (!$documento instanceof Documento) continue;
      $filtered_payload = [];
      foreach (array_keys($documento->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $documento->toArray();
        if (auth()->user()->can('@resource', ['documento', $recurso])) {
          $filtered_payload[$storage] = $documento->payload[$storage];
        }
      }
      $documento->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Documento) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function remplazar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {

      foreach (array_keys($data['payload'] ?? []) as $storage) {
        $recurso = ['method' => __FUNCTION__, 'storage' => $storage] + $data;
        if (!auth()->user()->can('@resource', ['documento', $recurso])) {
          $params[$index] = [
            'status' => 'fail',
            'message' => 'Usuario no cuenta con las credenciales requeridas.',
            'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
            'scode' => 401,
          ];
          continue 2;
        }
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }
        $documento->validarRemplazarSchema($data['payload']);

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->remplazar($data['payload'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $documento) {
      # Si no es instancia de documento, array de excepción
      if (!$documento instanceof Documento) continue;
      $filtered_payload = [];
      foreach (array_keys($documento->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $documento->toArray();
        if (auth()->user()->can('@resource', ['documento', $recurso])) {
          $filtered_payload[$storage] = $documento->payload[$storage];
        }
      }
      $documento->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Documento) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function cancelar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['documento', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->cancelar($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $documento) {
      # Si no es instancia de documento, array de excepción
      if (!$documento instanceof Documento) continue;
      $filtered_payload = [];
      foreach (array_keys($documento->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $documento->toArray();
        if (auth()->user()->can('@resource', ['documento', $recurso])) {
          $filtered_payload[$storage] = $documento->payload[$storage];
        }
      }
      $documento->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Documento) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function descancelar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['documento', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->descancelar($data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Filtramos almacenamientos a los que no se tiene acceso
    foreach ($params as $index => $documento) {
      # Si no es instancia de documento, array de excepción
      if (!$documento instanceof Documento) continue;
      $filtered_payload = [];
      foreach (array_keys($documento->payload) as $storage) {
        $recurso = ['storage' => $storage, 'method' => __FUNCTION__] + $documento->toArray();
        if (auth()->user()->can('@resource', ['documento', $recurso])) {
          $filtered_payload[$storage] = $documento->payload[$storage];
        }
      }
      $documento->payload = $filtered_payload;
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof Documento) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function comentar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'comentario' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['documento', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->comentar($data['comentario'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && !$params[0] instanceof \stdClass) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : (array) $params[0]
    ]);
  }

  public function descomentar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'comentario_id' => 'required|integer',
      ($this->is_bulk ? '*.' : '') . 'author' => 'filled|array',
      ($this->is_bulk ? '*.' : '') . 'author.scope' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|string',
      ($this->is_bulk ? '*.' : '') . 'author.id' => 'required_with:'. ($this->is_bulk ? '*.' : '') .'author|integer',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      $recurso = ['method' => __FUNCTION__] + $data;
      if (!auth()->user()->can('@resource', ['documento', $recurso])) {
        $params[$index] = [
          'status' => 'fail',
          'message' => 'Usuario no cuenta con las credenciales requeridas.',
          'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes',
          'scode' => 401,
        ];
        continue;
      }

      try {
        $documento = Documento::cargar($data['schema'], $data['folio'], $data['serie'] ?? '', $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ]);

        # Callback
        if (func_num_args() && func_get_arg(0) instanceof \Closure) {
          func_get_arg(0)($data, $documento);
        }

        if ($before = $this->resolveBeforeSchemaActionClass($data['schema'], __FUNCTION__)) {
          $before($data, $documento);
        }

        if ($resolution = $this->resolveSchemaActionClass($data['schema'], __FUNCTION__)) {
          $params[$index] = $resolution($data, $documento)->getOriginalContent()['data'];
        } else {
          $params[$index] = $documento->descomentar($data['comentario_id'], $data['author'] ?? null);
        }
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      } catch (KlayException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && $params[0] !== true) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  public function comentarios() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'folio' => 'required|integer',
      'serie' => 'nullable|alpha',
    ])->validate();

    $recurso = ['method' => __FUNCTION__] + $params;
    if (!auth()->user()->can('@resource', ['documento', $recurso])) {
      return response()->json([
        'status' => 'fail',
        'message' => 'Usuario no cuenta con las credenciales requeridas.',
        'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes'
      ], 401);
    }

    $cliente = new Client(config('services.archiving'));

    try {
      $documento = Documento::cargar($params['schema'], $params['folio'], $params['serie'] ?? '', $cliente);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $documento);
      }

      if ($before = $this->resolveBeforeSchemaActionClass($params['schema'], __FUNCTION__)) {
        $before($params, $documento);
      }

      if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
        $comentarios = $resolution($params, $documento)->getOriginalContent()['data'];
      } else {
          $comentarios = $documento->comentarios();
      }
    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $comentarios
    ]);
  }

  public function eventos() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
      'folio' => 'required|integer',
      'serie' => 'nullable|alpha',
    ])->validate();

    $recurso = ['method' => __FUNCTION__] + $params;
    if (!auth()->user()->can('@resource', ['documento', $recurso])) {
      return response()->json([
        'status' => 'fail',
        'message' => 'Usuario no cuenta con las credenciales requeridas.',
        'code' => 'klay.actions.documentos.'.__FUNCTION__.'.credenciales_insuficientes'
      ], 401);
    }

    $cliente = new Client(config('services.archiving'));

    try {
      $documento = Documento::cargar($params['schema'], $params['folio'], $params['serie'] ?? '', $cliente);

      # Callback
      if (func_num_args() && func_get_arg(0) instanceof \Closure) {
        func_get_arg(0)($params, $documento);
      }

      if ($before = $this->resolveBeforeSchemaActionClass($params['schema'], __FUNCTION__)) {
        $before($params, $documento);
      }

      if ($resolution = $this->resolveSchemaActionClass($params['schema'], __FUNCTION__)) {
        $eventos = $resolution($params, $documento)->getOriginalContent()['data'];
      } else {
          $eventos = $documento->eventos();
      }
    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $eventos
    ]);
  }

  public function consultarEventos() {
    $params = \Validator::make(request()->all(), [
      'with' => 'filled|array',
      'with-storage' => 'filled|array',
      'filter' => 'filled|array',
      'filter.*' => 'filled|array',
      'filter.where.*.field' => 'required|string',
      'filter.where.*.operator' => 'required|string',
      'filter.where.*.value' => 'present|nullable',
      'filter.orwhere.*.field' => 'required|string',
      'filter.orwhere.*.operator' => 'required|string',
      'filter.orwhere.*.value' => 'required',
      'filter.wherein.*.field' => 'required|string',
      'filter.wherein.*.value' => 'required|array',
      'order' => 'filled|array',
      'order.*.field' => 'required|string',
      'order.*.direction' => 'required|string',
      'limit' => 'filled|integer',
      'page' => 'filled|integer'
    ])->validate();

    $cliente = new Client(config('services.archiving'));
    $paginate = null;

    try {
      $documentos = Documento::consultarEventos($cliente, $params, $paginate);
    } catch (SDKException $e) {
      return response()->json($e->toArray(), 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $documentos,
      'paginate' => $paginate
    ]);
  }

  public function validar() {
    $params = \Validator::make(request()->all(), [
      ($this->is_bulk ? '*.' : '') . 'schema' => 'required|string',
      ($this->is_bulk ? '*.' : '') . 'folio' => 'filled|integer',
      ($this->is_bulk ? '*.' : '') . 'serie' => 'nullable|alpha',
      ($this->is_bulk ? '*.' : '') . 'payload' => 'required|array',
    ])->validate();

    $params = $this->is_bulk ? $params : [$params];

    $cliente = new Client(config('services.archiving'));

    foreach ($params as $index => $data) {
      try {
        $documento = new Documento($data['schema'], $cliente);
        $params[$index] = $documento->validarSchema($data['payload']);
      } catch (SDKException $e) {
        $params[$index] = $e->toArray(array_only($data, ['schema', 'folio', 'serie', 'payload']));
      }
    }

    # Retornamos excepción
    if (!$this->is_bulk && $params[0] !== true) {
      return response()->json(array_except($params[0], ['scode', 'params']), $params[0]['scode'] ?? 400);
    }

    return response()->json([
      'status' => 'success',
      'data' => $this->is_bulk ? $params : $params[0]
    ]);
  }

  private function resolveSchemaActionClass($schema, $method)
  {
    $class =  kw2p_instancia() . '\\Actions\\' . studly_case("{$schema}Action");
    if (class_exists($class) && method_exists($class, $method) && $class != static::class && is_subclass_of($class, 'Klay\Actions\ResolutionAction')) {
      return (new \ReflectionMethod($class, $method))->getClosure(new $class);
    }
    return false;
  }

  private function resolveBeforeSchemaActionClass($schema, $method)
  {
    return $this->resolveSchemaActionClass($schema, "{$method}Before");
  }
}
