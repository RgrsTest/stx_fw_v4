<?php

namespace Klay\Actions;

use Klayware\Actions\Action;

require_once('../apps/Klay/balancing.php');

class BalancesAction extends Action
{
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
  }

  public function consultar()
  {
    //Validacion de parametros
    $params = \Validator::make(request()->all(), [
      'tipo_balance' => 'required|in:inventario,financiero',
      'tabla' => 'required|in:movimientos,resumenes,saldos',
      'movimientos' => 'required|array'
    ])->validate();

    //Consulta
    $metodo = "balance_{$params['tipo_balance']}_{$params['tabla']}_consultar";

    try {
      $respuesta = $metodo(strtoupper(snake_case(kw2p_ambito())), $params['movimientos']);
    } catch(BalancingException $e) {
      throw (new KlayException($e->getMessage(), $e->getCode()))->status(400);
    }

    //Filtrar respuesta
    foreach($respuesta as $un_balance) {
      $movimientos_filtrados = [];
      foreach ($un_balance['RESOURCE'] as $un_movimiento) {
        if (auth()->user()->can('@resource', ['balance', $un_movimiento])) {
          $movimientos_filtrados[] = $un_movimiento;
        }
      }
      $respuesta_filtrada[] = ['RUTA_MODULO' => $un_balance['RUTA_MODULO'], 'RESOURCE' => $movimientos_filtrados];
    }

    return response()->json([
      'status' => 'success',
      'data' => $respuesta_filtrada
    ]);
  }
}