<?php

namespace Klay\Actions;

use Archiving\SDK\Client;
use Archiving\SDK\Exception as SDKException;
use Archiving\SDK\Schema;
use Klayware\Actions\Action;

class SchemasAction extends Action
{
  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify','auth.gate']);
  }

  public function consultar() {
    $params = \Validator::make(request()->all(), [
      'filter' => 'filled|array',
      'filter.*' => 'filled|array',
      'filter.where.*.field' => 'required|string',
      'filter.where.*.operator' => 'required|string',
      'filter.where.*.value' => 'present|nullable',
      'filter.orwhere.*.field' => 'required|string',
      'filter.orwhere.*.operator' => 'required|string',
      'filter.orwhere.*.value' => 'required',
      'filter.orWhere.*.field' => 'required|string',
      'filter.orWhere.*.operator' => 'required|string',
      'filter.orWhere.*.value' => 'required',
      'filter.wherein.*.field' => 'required|string',
      'filter.wherein.*.value' => 'required|array',
      'filter.whereIn.*.field' => 'required|string',
      'filter.whereIn.*.value' => 'required|array',
      'filter.orWhereIn.*.field' => 'required|string',
      'filter.orWhereIn.*.value' => 'required|array',
      'filter.whereJoin.*.field' => 'required|string',
      'filter.whereJoin.*.operator' => 'required|string',
      'filter.whereJoin.*.value' => 'present|nullable',
      'filter.orWhereJoin.*.field' => 'required|string',
      'filter.orWhereJoin.*.operator' => 'required|string',
      'filter.orWhereJoin.*.value' => 'present|nullable',

      'filter.and.where.*.field' => 'required|string',
      'filter.and.where.*.operator' => 'required|string',
      'filter.and.where.*.value' => 'present|nullable',
      'filter.and.orWhere.*.field' => 'required|string',
      'filter.and.orWhere.*.operator' => 'required|string',
      'filter.and.orWhere.*.value' => 'required',
      'filter.and.whereIn.*.field' => 'required|string',
      'filter.and.whereIn.*.value' => 'required|array',
      'filter.and.orWhereIn.*.field' => 'required|string',
      'filter.and.orWhereIn.*.value' => 'required|array',
      'filter.and.whereJoin.*.field' => 'required|string',
      'filter.and.whereJoin.*.operator' => 'required|string',
      'filter.and.whereJoin.*.value' => 'present|nullable',
      'filter.and.orWhereJoin.*.field' => 'required|string',
      'filter.and.orWhereJoin.*.operator' => 'required|string',
      'filter.and.orWhereJoin.*.value' => 'present|nullable',

      'filter.or.where.*.field' => 'required|string',
      'filter.or.where.*.operator' => 'required|string',
      'filter.or.where.*.value' => 'present|nullable',
      'filter.or.orWhere.*.field' => 'required|string',
      'filter.or.orWhere.*.operator' => 'required|string',
      'filter.or.orWhere.*.value' => 'required',
      'filter.or.whereIn.*.field' => 'required|string',
      'filter.or.whereIn.*.value' => 'required|array',
      'filter.or.orWhereIn.*.field' => 'required|string',
      'filter.or.orWhereIn.*.value' => 'required|array',
      'filter.or.whereJoin.*.field' => 'required|string',
      'filter.or.whereJoin.*.operator' => 'required|string',
      'filter.or.whereJoin.*.value' => 'present|nullable',
      'filter.or.orWhereJoin.*.field' => 'required|string',
      'filter.or.orWhereJoin.*.operator' => 'required|string',
      'filter.or.orWhereJoin.*.value' => 'present|nullable',

      'order' => 'filled|array',
      'order.*.field' => 'required|string',
      'order.*.direction' => 'required|string',
      'orderByJoin' => 'filled|array',
      'orderByJoin.*.field' => 'required|string',
      'orderByJoin.*.direction' => 'required|string',
      'limit' => 'filled|integer',
      'page' => 'filled|integer'
    ])->validate();

    $cliente = new Client(config('services.archiving'));
    $paginate = null;

    try {
      $schemas = Schema::consultar($cliente, $params, $paginate);
    } catch (SDKException $e) {
      return $e->toArray();
    }

    # Filtro flujos
    $fn = __FUNCTION__;
    $schemas = $schemas->filter(function ($schema, $key) use ($fn) {
      $recurso = ['method' => $fn] + $schema->toArray();
      return auth()->user()->can('@resource', ['schema', $recurso]);
    })->values();

    return response()->json([
      'status' => 'success',
      'data' => $schemas,
      'paginate' => $paginate
    ]);
  }

  public function obtener() {
    $params = \Validator::make(request()->all(), [
      'schema' => 'required|string',
    ])->validate();

    $recurso = ['method' => __FUNCTION__, 'name' => $params['schema']];
    if (!auth()->user()->can('@resource', ['schema', $recurso])) {
      return response()->json([]);
    }

    $cliente = new Client(config('services.archiving'));

    try {
      $schema = Schema::cargar($params['schema'], $cliente);
    } catch (SDKException $e) {
      $schema = null;
    }

    return response()->json(optional($schema)->schema);
  }
}
