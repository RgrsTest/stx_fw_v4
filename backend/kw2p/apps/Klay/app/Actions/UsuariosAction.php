<?php

namespace Klay\Actions;

use Illuminate\Database\Eloquent\Builder;
use Klay\Actions\ConsultarAction;
use Klay\Models\Usuario;
use Klay\Steps\UsuariosStep;

class UsuariosAction extends ConsultarAction
{
  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware(['jwt.verify'])->except([
      'obtenerApikey',
      'recuperarContrasena',
      'modificarContrasenaRecuperar',
    ]);
  }

  public function consultar()
  {
    list($class, $callback) = array_pad(func_get_args(), 2, null);
    return parent::consultar($class instanceof Builder ? $class : Usuario::query(), $callback);
  }

  public function obtenerApikey(Usuario $usuario)
  {
    # Validacion de params
    $params = request()->validate([
      'correo' => 'required|email',
      'contrasena' => 'required',
    ]);

    # Obtenemos usuario
    $usuario = $this->obtenerModeloUsuario($usuario)->where('correo', $params['correo'])->firstOrFail();

    $usuario->validarContrasena($params['contrasena']);

    return response()->json([
      'status' => 'success',
      'data' => [
        'apikey' => $usuario->apikey
      ]
    ]);
  }

  public function actualizar()
  {
    $params = request()->validate([
      'id' => 'sometimes|integer',
      'correo' => 'required|email|unique:'.kw2p_ambito().'.seg_usuarios,correo,'.request('id').',id,deleted_at,NULL',
      'roles' => 'sometimes|array',
      'credenciales' => 'sometimes|array',
    ]);

    $params = request('params_overload') ? request()->except(['params_overload']) : $params;

    $usuario = (new UsuariosStep)->actualizar($params, $this->obtenerModeloUsuario());

    return response()->json([
      'status' => 'success',
      'data' => [
        'usuario' => $usuario
      ]
    ]);
  }

  public function modificarContrasena(Usuario $usuario)
  {
    # Validacion de params
    $params = request()->validate([
      'usuario_id' => 'required|integer|exists:'.kw2p_ambito().'.seg_usuarios,id',
      'contrasena_anterior' => 'required',
      'contrasena_nueva' => 'required',
    ]);

    # Obtenemos usuario
    $usuario = $this->obtenerModeloUsuario($usuario)->findOrFail($params['usuario_id']);

    # Usuario solo accede a si mismo
    $usuario->validarUsuario($params['usuario_id']);

    # Verificamos contraseña anterior
    $usuario->validarContrasena($params['contrasena_anterior']);

    # Actualizamos contraseña
    $usuario->contrasena = \Hash::make($params['contrasena_nueva']);
    $usuario->save();

    return response()->json([
      'status' => 'success',
      'data' => [
        'usuario' => $usuario
      ]
    ]);
  }

  public function recuperarContrasena(Usuario $usuario, UsuariosStep $step)
  {
    # Validacion de params
    $params = request()->validate([
      'correo' => 'required|email|exists:'.kw2p_ambito().'.seg_usuarios',
    ]);

    # Obtenemos usuario
    $usuario = $this->obtenerModeloUsuario($usuario)->where('correo', $params['correo'])->firstOrFail();

    # Llamamos step
    $step->recuperarContrasena($usuario);

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }

  public function modificarContrasenaRecuperar(Usuario $usuario)
  {
    # Validacion de params
    $params = request()->validate([
      'correo' => 'required|email|exists:'.kw2p_ambito().'.seg_usuarios',
      'token_recuperar' => 'required',
      'contrasena_nueva' => 'required',
    ]);

    # Decodeamos token
    //$params['token_recuperar'] = base64_decode($params['token_recuperar']);

    $usuario = $this->obtenerModeloUsuario($usuario)->where('correo', $params['correo'])->firstOrfail();

    $usuario->validarTokenRecuperar($params['token_recuperar']);

    # Actualizamos contraseña e invalidamos token
    $usuario->update([
      'contrasena' => \Hash::make($params['contrasena_nueva']),
      'token_vigencia' => now(),
    ]);

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }

  public function eliminar()
  {
    $params = request()->validate([
      'usuario_id' => 'required|integer|exists:'.kw2p_ambito().'.seg_usuarios,id',
    ]);

    Usuario::findOrFail($params['usuario_id'])->delete();

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }

  private function obtenerModeloUsuario($usuario = null)
  {
    $modelo = $this->modeloUsuario();
    if (!$usuario || get_class($usuario) != get_class($modelo)) {
      return $modelo;
    }
    return $usuario;
  }

  protected function modeloUsuario()
  {
    return new Usuario;
  }

}
