<?php

namespace Klay\Actions;

use Klayware\Actions\Action;

class ConsultarAction extends Action
{
    public function consultar()
    {
        $params = \Validator::make(request()->all(), [
            'filter' => 'filled|array',
            'filter.*' => 'filled|array',
            'filter.where.*.field' => 'required|string',
            'filter.where.*.operator' => 'required|string',
            'filter.where.*.value' => 'present|nullable',
            'filter.orwhere.*.field' => 'required|string',
            'filter.orwhere.*.operator' => 'required|string',
            'filter.orwhere.*.value' => 'required',
            'filter.wherein.*.field' => 'required|string',
            'filter.wherein.*.value' => 'required|array',
            'filter.whereJoin.*.field' => 'required|string',
            'filter.whereJoin.*.operator' => 'required|string',
            'filter.whereJoin.*.value' => 'present|nullable',
            'order' => 'filled|array',
            'order.*.field' => 'required|string',
            'order.*.direction' => 'required|string',
            'orderByJoin' => 'filled|array',
            'orderByJoin.*.field' => 'required|string',
            'orderByJoin.*.direction' => 'required|string',
            'limit' => 'filled|integer',
            'page' => 'filled|integer'
        ])->validate();

        list($class, $callback) = array_pad(func_get_args(), 2, null);

        if (is_object($class)) {
          $model = $class->getModel();
          $query = $class;
        } else {
          $model = new $class;
          $query = $model->query();
        }

        $columns_names = $model->getConnection()->getSchemaBuilder()->getColumnListing($model->getTable());
        $eagerloads = array_keys($query->getEagerLoads());

        // Logica filtro
        foreach (array_get($params, 'filter.where', []) as $filtro)
        {
            if (in_array(str_before($filtro['field'], '->'), $columns_names)) {
              $collate = $filtro['collate'] ?? false;
              if ($collate) {
                $field = $query->getQuery()->getGrammar()->wrap($filtro['field']);
                $query->where(\DB::raw("{$field} collate {$collate}"), $filtro['operator'], $filtro['value'] ?? '');
              } else {
                $query->where($filtro['field'], $filtro['operator'], $filtro['value'] ?? '');
              }
            }
        }

        if(isset($params['filter']['orwhere'])) {
            $query->where(
                function ($queryOrWhere) use ($params, $columns_names) {
                    foreach (array_get($params, 'filter.orwhere', []) as $filtro) {
                        if (str_contains($filtro['field'], '.')) {
                            list($relacion, $columna) = explode('.', $filtro['field']);
                            try {
                                $queryOrWhere->orWhereHas(
                                    $relacion, function ($relation) use ($columna, $filtro) {
                                        $columns_names_where_has = $relation->getModel()->getConnection()->getSchemaBuilder()->getColumnListing($relation->getModel()->getTable());
                                        if (in_array($columna, $columns_names_where_has)) {
                                            $relation->where($columna, $filtro['operator'], $filtro['value']);
                                        }
                                    }
                                );
                            } catch(\Exception $e) {
                            }
                        } else {
                            if (in_array(str_before($filtro['field'], '->'), $columns_names)) {
                                $collate = $filtro['collate'] ?? false;
                                if ($collate) {
                                    $field = $queryOrWhere->getQuery()->getGrammar()->wrap($filtro['field']);
                                    $queryOrWhere->orWhere(\DB::raw("{$field} collate {$collate}"), $filtro['operator'], $filtro['value']);
                                } else {
                                    $queryOrWhere->orWhere($filtro['field'], $filtro['operator'], $filtro['value']);
                                }
                            }
                        }
                    }
                    return $queryOrWhere;
                }
            );
        }

        foreach (array_get($params, 'filter.wherein', []) as $filtro)
        {
            if (in_array(str_before($filtro['field'], '->'), $columns_names)) {
                $query->whereIn($filtro['field'], $filtro['value']);
            }
        }

        foreach (array_get($params, 'filter.whereJoin', []) as $filtro) {
            if (in_array(str_before($filtro['field'], '.'), $eagerloads)) {
                $collate = $filtro['collate'] ?? false;
                if ($collate) {
                    $field = \Closure::bind(function ($query) use ($filtro) {
                        return $query->performJoin($filtro['field']);
                    }, null, get_class($query))($query);
                    $field = $query->getQuery()->getGrammar()->wrap($field);
                    $query->where(\DB::raw("{$field} collate {$collate}"), $filtro['operator'], $filtro['value'] ?? '');
                } else {
                    $query->whereJoin($filtro['field'], $filtro['operator'], $filtro['value'] ?? '');
                }
            }
        }

        // Logica OrderBy
        foreach (($params['order'] ?? []) as $order)
        {
            if (in_array(str_before($order['field'], '->'), $columns_names)) {
                $query->orderBy($order['field'], $order['direction']);
            }
        }

        foreach (($params['orderByJoin'] ?? []) as $order) {
            if (in_array(str_before($order['field'], '.'), $eagerloads)) {
                $query->orderByJoin($order['field'], $order['direction']);
            }
        }

        if ($callback && is_callable($callback)) {
            $callback($query);
        }

        $paginate = null;
        if (request('limit', null)) {

            $paginate = $query->paginate($params['limit']);

            $resultados = $paginate->items();

            $paginate = array_except($paginate->toArray(), ['data']);

            unset($paginate['first_page_url'], $paginate['next_page_url'], $paginate['path'], $paginate['prev_page_url'], $paginate['last_page_url']);
        } else {
            $resultados = $query->get();
        }

        return response()->json(['status' => 'success', 'data' => $resultados, 'paginate' => $paginate]);
    }
}
