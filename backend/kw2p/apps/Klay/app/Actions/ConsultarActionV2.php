<?php

namespace Klay\Actions;

use Klayware\Actions\Action;

class ConsultarActionV2 extends Action
{
    private $columns = [];

    private $eagerloads = [];

    public function consultar()
    {
        $params = \Validator::make(request()->all(), [
            'filter' => 'filled|array',
            'filter.*' => 'filled|array',
            'filter.where.*.field' => 'required|string',
            'filter.where.*.operator' => 'required|string',
            'filter.where.*.value' => 'present|nullable',
            'filter.orwhere.*.field' => 'required|string',
            'filter.orwhere.*.operator' => 'required|string',
            'filter.orwhere.*.value' => 'required',
            'filter.orWhere.*.field' => 'required|string',
            'filter.orWhere.*.operator' => 'required|string',
            'filter.orWhere.*.value' => 'required',
            'filter.wherein.*.field' => 'required|string',
            'filter.wherein.*.value' => 'required|array',
            'filter.whereIn.*.field' => 'required|string',
            'filter.whereIn.*.value' => 'required|array',
            'filter.orWhereIn.*.field' => 'required|string',
            'filter.orWhereIn.*.value' => 'required|array',
            'filter.whereJoin.*.field' => 'required|string',
            'filter.whereJoin.*.operator' => 'required|string',
            'filter.whereJoin.*.value' => 'present|nullable',
            'filter.orWhereJoin.*.field' => 'required|string',
            'filter.orWhereJoin.*.operator' => 'required|string',
            'filter.orWhereJoin.*.value' => 'present|nullable',

            'filter.and.where.*.field' => 'required|string',
            'filter.and.where.*.operator' => 'required|string',
            'filter.and.where.*.value' => 'present|nullable',
            'filter.and.orWhere.*.field' => 'required|string',
            'filter.and.orWhere.*.operator' => 'required|string',
            'filter.and.orWhere.*.value' => 'required',
            'filter.and.whereIn.*.field' => 'required|string',
            'filter.and.whereIn.*.value' => 'required|array',
            'filter.and.orWhereIn.*.field' => 'required|string',
            'filter.and.orWhereIn.*.value' => 'required|array',
            'filter.and.whereJoin.*.field' => 'required|string',
            'filter.and.whereJoin.*.operator' => 'required|string',
            'filter.and.whereJoin.*.value' => 'present|nullable',
            'filter.and.orWhereJoin.*.field' => 'required|string',
            'filter.and.orWhereJoin.*.operator' => 'required|string',
            'filter.and.orWhereJoin.*.value' => 'present|nullable',

            'filter.or.where.*.field' => 'required|string',
            'filter.or.where.*.operator' => 'required|string',
            'filter.or.where.*.value' => 'present|nullable',
            'filter.or.orWhere.*.field' => 'required|string',
            'filter.or.orWhere.*.operator' => 'required|string',
            'filter.or.orWhere.*.value' => 'required',
            'filter.or.whereIn.*.field' => 'required|string',
            'filter.or.whereIn.*.value' => 'required|array',
            'filter.or.orWhereIn.*.field' => 'required|string',
            'filter.or.orWhereIn.*.value' => 'required|array',
            'filter.or.whereJoin.*.field' => 'required|string',
            'filter.or.whereJoin.*.operator' => 'required|string',
            'filter.or.whereJoin.*.value' => 'present|nullable',
            'filter.or.orWhereJoin.*.field' => 'required|string',
            'filter.or.orWhereJoin.*.operator' => 'required|string',
            'filter.or.orWhereJoin.*.value' => 'present|nullable',

            'order' => 'filled|array',
            'order.*.field' => 'required|string',
            'order.*.direction' => 'required|string',
            'orderByJoin' => 'filled|array',
            'orderByJoin.*.field' => 'required|string',
            'orderByJoin.*.direction' => 'required|string',
            'limit' => 'filled|integer',
            'page' => 'filled|integer'
        ])->validate();

        list($class, $callback) = array_pad(func_get_args(), 2, null);

        if (is_object($class)) {
          $model = $class->getModel();
          $query = $class;
        } else {
          $model = new $class;
          $query = $model->query();
        }

        $this->columns = $model->getConnection()->getSchemaBuilder()->getColumnListing($model->getTable());
        $this->eagerloads = array_keys($query->getEagerLoads());

        $this->setUpClauses($params['filter'] ?? [], $query);

        foreach ($params['order'] ?? [] as $order) {
            if (in_array(str_after(str_before($order['field'], '->'), '.'), $this->columns)) {
                $query->orderBy($order['field'], $order['direction']);
            }
        }

        foreach ($params['orderByJoin'] ?? [] as $order) {
            if (in_array(str_before($order['field'], '.'), $this->eagerloads)) {
                $query->orderByJoin($order['field'], $order['direction']);
            }
        }

        if ($callback && is_callable($callback)) {
            $callback($query);
        }

        $paginate = null;
        if (request('limit', null)) {

            $paginate = $query->paginate($params['limit']);

            $resultados = $paginate->items();

            $paginate = array_except($paginate->toArray(), ['data']);

            unset($paginate['first_page_url'], $paginate['next_page_url'], $paginate['path'], $paginate['prev_page_url'], $paginate['last_page_url']);
        } else {
            $resultados = $query->get();
        }

        return response()->json(['status' => 'success', 'data' => $resultados, 'paginate' => $paginate]);
    }

    private function where(array $conditions, $query = null, $boolean = 'and')
    {
        foreach ($conditions as $condition) {
            if (in_array(str_after(str_before($condition['field'], '->'), '.'), $this->columns)) {
                $collate = $condition['collate'] ?? false;
                if ($collate) {
                    $field = $query->getQuery()->getGrammar()->wrap($condition['field']);
                    $query->where(\DB::raw("{$field} collate {$collate}"), $condition['operator'], $condition['value'] ?? '', $boolean);
                } else {
                    $query->where($condition['field'], $condition['operator'], $condition['value'] ?? '', $boolean);
                }
            }
        }
    }

    private function orWhere(array $conditions, $query = null)
    {
        $this->where($conditions, $query, 'or');
    }

    private function whereIn(array $conditions, $query = null, $boolean = 'and')
    {
        foreach ($conditions as $condition) {
            if (in_array(str_after(str_before($condition['field'], '->'), '.'), $this->columns)) {
                $query->whereIn($condition['field'], $condition['value'], $boolean);
            }
        }
    }

    private function orWhereIn(array $conditions, $query = null)
    {
        $this->whereIn($conditions, $query, 'or');
    }

    private function whereJoin(array $conditions, $query = null, $boolean = 'and')
    {
        foreach ($conditions as $condition) {
            if (in_array(str_before($condition['field'], '.'), $this->eagerloads)) {
                $collate = $condition['collate'] ?? false;
                if ($collate) {
                    $field = \Closure::bind(function ($query) use ($condition) {
                        return ($query->baseBuilder ?? $query)->performJoin($condition['field']);
                    }, null, get_class($query))($query);
                    $field = $query->getQuery()->getGrammar()->wrap($field);
                    $query->where(\DB::raw("{$field} collate {$collate}"), $condition['operator'], $condition['value'] ?? '', $boolean);
                } else {
                    $query->whereJoin($condition['field'], $condition['operator'], $condition['value'] ?? '', $boolean);
                }
            }
        }
    }

    private function orWhereJoin(array $conditions, $query = null)
    {
        $this->whereJoin($conditions, $query, 'or');
    }

    private function and($clauses, $query = null, $boolean = 'and')
    {
        $query->where(function ($query) use ($clauses) {
            $this->setUpClauses($clauses, $query);
        }, null, null, $boolean);
    }

    private function or($clauses, $query = null)
    {
        $this->and($clauses, $query, 'or');
    }

    private function setUpClauses($clauses, $query)
    {
        foreach ($clauses as $clause => $conditions) {
            switch ($clause) {
                case 'where':
                    $this->where($conditions, $query);
                    break;

                case 'orwhere':
                case 'orWhere':
                    $this->orWhere($conditions, $query);
                    break;

                case 'wherein':
                case 'whereIn':
                    $this->whereIn($conditions, $query);
                    break;

                case 'orWhereIn':
                    $this->orWhereIn($conditions, $query);
                    break;

                case 'whereJoin':
                    $this->whereJoin($conditions, $query);
                    break;

                case 'orWhereJoin':
                    $this->orwhereJoin($conditions, $query);
                    break;

                case 'and':
                    $this->and($conditions, $query);
                    break;

                case 'or':
                    $this->or($conditions, $query);
                    break;

                default:
                    break;
            }
        }
    }

}
