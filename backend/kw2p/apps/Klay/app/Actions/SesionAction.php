<?php

namespace Klay\Actions;

use Klay\Models\Sesion;
use Klay\Models\Usuario;
use Klayware\Actions\Action;
use Klayware\Exceptions\KlayException;

class SesionAction extends ConsultarAction
{

  protected $usuario;

  /**
   * Create a new action.
   *
   * @return void
   */
  public function __construct(Usuario $usuario)
  {
    $this->middleware('jwt.verify')->except(['iniciar','depurar']);
    $this->usuario = $usuario;
  }

  public function consultar()
  {
    return parent::consultar(Sesion::query(), function ($query) {
      $query->with('usuario');
    });
  }

  public function iniciar()
  {
    # Validacion de params
    $params = request()->validate([
      'correo' => 'required|email',
      'apikey' => 'required',
    ]);

    if (config('auth.disabled')) {
      throw new KlayException('Imposible iniciar sesion, sesiones deshabilitadas.', 'sesiones_deshabilitadas');
    }

    # Obtenemos usuario
    $usuario = $this->usuario->where('correo', $params['correo'])->firstOrFail();

    $usuario->validarApikey($params['apikey']);

    $limits = config('auth.limits');
    if ($limits['at_the_same_time'] ?? null) {
      $count = Sesion::count();
      if ($count >= $limits['at_the_same_time']) {
        if ($limits['idle_time'] ?? null) {
          $sesion = Sesion::where('updated_at', '<', now()->subMinutes($limits['idle_time']) )->first();
          if ($sesion) {
            $sesion->delete();
          } else {
            throw new KlayException('Sesiones ocupadas, intentelo mas tarde o cierre sesión en otro equipo para acceder', 'sesiones_ocupadas');
          }
        } else {
          throw new KlayException('Sesiones ocupadas, cierre sesión en otro equipo para acceder', 'sesiones_ocupadas');
        }
      }
    }

    $sesion = Sesion::agregar($usuario);

    $sesion['usuario'] = $usuario;

    return response()->json([
      'status' => 'success',
      'data' => $sesion
    ]);
  }

  public function cerrar()
  {
    Sesion::cerrar();

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }

  public function depurar()
  {
    Sesion::depurar();

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }

  public function patear()
  {
    # Validacion de params
    $params = request()->validate([
      'id' => 'required|integer',
    ]);

    Sesion::findOrFail($params['id'])->delete();

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }

}
