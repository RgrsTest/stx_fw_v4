<?php

namespace Klay\Actions;

use Klay\Models\Rol;

class RolesAction extends ConsultarAction
{
    /**
     * Create a new action.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['jwt.verify']);
    }

    public function consultar()
    {
        return parent::consultar(Rol::class);
    }

  public function actualizar()
  {
    $params = request()->validate([
      'id' => 'sometimes|integer',
      'rol' => 'required|string',
      'credenciales' => 'sometimes|array',
    ]);

    $rol = Rol::updateOrCreate(['id' => $params['id'] ?? null], $params);

    return response()->json([
      'status' => 'success',
      'data' => [
        'rol' => $rol
      ]
    ]);
  }

  public function eliminar()
  {
    $params = request()->validate([
      'rol_id' => 'required|integer|exists:'.kw2p_ambito().'.seg_roles,id',
    ]);

    Rol::findOrFail($params['rol_id'])->delete();

    return response()->json([
      'status' => 'success',
      'data' => []
    ]);
  }
}
