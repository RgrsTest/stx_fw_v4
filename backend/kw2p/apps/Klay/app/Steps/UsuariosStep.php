<?php

namespace Klay\Steps;

use Klay\Models\Correo;
use Klay\Models\Usuario;

class UsuariosStep
{
  public function actualizar($params, Usuario $usuario = null)
  {
    if ($usuario == null) {
      $usuario = Usuario::updateOrCreate(['id' => $params['id'] ?? null], $params);
    } else {
      $usuario = $usuario->updateOrCreate(['id' => $params['id'] ?? null], $params);
    }

    if ($usuario->wasRecentlyCreated) {
      $usuario->update([
        'contrasena' => \Hash::make(nanoId()),
        'apikey' => nanoId(),
        'token_recuperar' => nanoId(),
        'token_vigencia' => now()->addHours(4),
      ]);

      # Enviamos correo de bienvenida
      Correo::correoBienvenida($usuario);
    }

    return $usuario;
  }

  public function recuperarContrasena($usuario)
  {
    # Asignamos token con caducidad de 4hrs
    $usuario->update([
      'token_recuperar' => nanoId(),
      'token_vigencia' => now()->addHours(4),
    ]);

    # Enviamos correo de recuperacion
    Correo::recuperarContrasena($usuario);
  }
}
