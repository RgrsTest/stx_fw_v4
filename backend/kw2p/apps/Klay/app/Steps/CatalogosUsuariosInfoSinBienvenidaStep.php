<?php

namespace Klay\Steps;

use Klay\Models\Correo;
use Klay\Models\Usuario;

class CatalogosUsuariosInfoSinBienvenidaStep
{
  public static function actualizar($data, $usuario)
  {
    $usuario = Usuario::withTrashed()->updateOrCreate([
      'id' => $usuario['_relaciones']['usuario_info']['payload']['usuario_id'] ?? null
    ], [
      'correo' => $data['payload']['correo_electronico']
    ]);

    if ($usuario->wasRecentlyCreated) {
      $usuario->update([
        'apikey' => nanoId(),
        'contrasena' => \Hash::make($data['extra']['contrasena'] ?? nanoId()),
        'token_recuperar' => nanoId(),
        'token_vigencia' => now()->addHours(4),
      ]);

      # Enviamos correo de bienvenida
      if(!isset($data['extra']['bienvenida_sin_correo'])){
        Correo::correoBienvenidaSinContrasena($usuario);
      }

    } else {

      if ($data['extra']['contrasena'] ?? null) {
        $usuario->update([
          'contrasena' => \Hash::make($data['extra']['contrasena']),
        ]);
      }
    }
    return $usuario;
  }
}
