<?php

namespace Klay\Steps;

use Archiving\SDK\Client;
use Archiving\SDK\Catalogo;

class CatalogosJerarquicosStep
{
  public static function actualizar($schema, $id, $payload)
  {
    //Se inicializa cliente de archiving
    $cliente = new Client(config('services.archiving'));

    //Se inicializa instancia de schema
    $documento = new Catalogo($schema, $cliente);

    //Se actualiza/inserta el registro
    $registro = clone $documento->actualizar($payload, $id);

    //Se calcula la ruta del registro
    if($payload["pertenece_id"] != 0) {
      $registro_padre = Catalogo::cargar($schema, $payload["pertenece_id"], $cliente);

      $payload_ruta =
        [
          'ruta' => $registro_padre->payload["ruta"] . $registro->id . '/',
          'ruta_nombre' => $registro_padre->payload["ruta_nombre"] . $registro->payload['nombre'] . '/',
          'raiz_id' => $registro_padre->payload['raiz_id']
        ];
    } else {
      $payload_ruta =
        [
          'ruta' => "/{$registro->id}/",
          'ruta_nombre' => "/{$registro->payload['nombre']}/",
          'raiz_id' => $registro->id
        ];
    }

    //Se actualiza la ruta del registro
    $registro_ruta = $documento->actualizar($payload_ruta, $registro->id);

    //Si el registro fue actualizado se actualiza la ruta de los hijos en caso de que haya cambiado su ruta
    if($id && (strcmp($registro->payload['ruta'], $payload_ruta['ruta']) != 0 || strcmp($registro->payload['ruta_nombre'], $payload_ruta['ruta_nombre']) != 0)) {
      $filtro =
      [
        'filter' =>
        [
          'where' =>
          [
            [
              'field' => 'payload->ruta',
              'operator' => 'like',
              'value' => "%{$registro->payload['ruta']}%"
            ],
            [
              'field' => 'id',
              'operator' => '!=',
              'value' => $registro->id
            ]
          ]
        ]
      ];

      //Se obtienen los registros hijos
      $registros_hijos = Catalogo::consultar($schema, $cliente, $filtro);

      //Se actualiza la ruta de los registros hijos
      foreach ($registros_hijos as $un_registro_hijo) {
        $payload_ruta_hijo =
          [
            'ruta' => str_replace($registro->payload['ruta'], $payload_ruta['ruta'], $un_registro_hijo->payload["ruta"]),
            'ruta_nombre' => str_replace($registro->payload['ruta_nombre'], $payload_ruta['ruta_nombre'], $un_registro_hijo->payload["ruta_nombre"]),
            'raiz_id' => $payload_ruta['raiz_id']
          ];

        $un_registro_hijo->actualizar($payload_ruta_hijo, $un_registro_hijo->id);
      }
    }

    return $registro_ruta;
  }

  public static function recalcularJerarquia($schema, $catalogo)
  {
    //Se inicializa cliente de archiving
    $cliente = new Client(config('services.archiving'));

    //Se asigna el valor inicial del payload
    $payload = [
      'ruta' => "{$catalogo->id}/",
      'ruta_nombre' => "{$catalogo->payload['nombre']}/",
      'raiz_id' => 0
    ];

    //Se itera toda la ascendencia del registro
    $pertenece_id = $catalogo->payload['pertenece_id'];
    while($pertenece_id){
      $padre = Catalogo::cargar($schema, $pertenece_id, $cliente);

      $payload = [
        'ruta' => "{$padre->id}/{$payload['ruta']}",
        'ruta_nombre' => "{$padre->payload['nombre']}/{$payload['ruta_nombre']}",
        'raiz_id' => 0
      ];
  
      $pertenece_id = $padre->payload['pertenece_id'];
    }

    //Se asigna el valor final del payload
    $payload = [
      'ruta' => "/{$payload['ruta']}",
      'ruta_nombre' => "/{$payload['ruta_nombre']}",
      'raiz_id' => $padre->id ?? $catalogo->id
    ];

    $catalogo->actualizar($payload, $catalogo->id);
  }
}
