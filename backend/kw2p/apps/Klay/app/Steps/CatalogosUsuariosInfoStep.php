<?php

namespace Klay\Steps;

use Klay\Models\Correo;
use Klay\Models\Usuario;

class CatalogosUsuariosInfoStep
{
  public static function actualizar($data, $usuario)
  {
    $usuario = Usuario::withTrashed()->updateOrCreate([
      'id' => $usuario['_relaciones']['usuario_info']['payload']['usuario_id'] ?? null
    ], [
      'correo' => $data['payload']['correo_electronico']
    ]);

    if ($usuario->wasRecentlyCreated) {
      $usuario->update([
        'contrasena' => \Hash::make($data['payload']['contrasena'] ?? nanoId()),
        'apikey' => nanoId(),
        'roles' => [],
        'token_recuperar' => nanoId(),
        'token_vigencia' => now()->addHours(4),
      ]);

      # Enviamos correo de bienvenida
      Correo::correoBienvenida($usuario);
    }

    return $usuario;
  }
}
