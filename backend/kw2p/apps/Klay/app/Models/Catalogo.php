<?php

namespace Klay\Models;

use Archiving\SDK\Client;
use Archiving\SDK\Catalogo as CatalogoBase;
use Archiving\SDK\hasQueryBuilder;

class Catalogo extends CatalogoBase
{
  use hasQueryBuilder;

  public function __construct($schema = null, Client $client = null, array $attributes = [])
  {
    parent::__construct($schema, new Client(config('services.archiving')), $attributes);
  }

  public function findOrFailWithStorage($id, $with_storage = []){
    return $this->withStorage($with_storage)->where('id', $id)->FirstOrFail();
  }
}