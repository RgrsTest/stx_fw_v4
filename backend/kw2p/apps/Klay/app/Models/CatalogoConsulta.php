<?php

namespace Klay\Models;

use Archiving\SDK\Models\CatalogWithStorages;
use Archiving\SDK\Models\Scheme;

class CatalogoConsulta
{
  protected $catalogo = null;

  public function __construct($schema = null)
  {
    $scheme = Scheme::where(['name' => $schema, 'type' => 'cat'])->firstOrFail();
    $this->catalogo = CatalogWithStorages::setScheme($scheme);
  }

  public function findOrFailWithStorage($id, $with_storage = []){
    return $this->withStorageRelations($with_storage)->where('id', $id)->firstOrFail();
  }

  public function withRootRelations($arguments)
  {
    $this->catalogo->withRootRelations($arguments);
    return $this;
  }

  public function withStorageRelations($arguments)
  {
    $this->catalogo->withStorageRelations($arguments);
    return $this;
  }

  public function __call($method, $arguments)
  {
    return $this->catalogo->withRelations()->$method(...$arguments);
  }

}
