<?php

namespace Klay\Models;

use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class Correo
{
  public static function correoBienvenida($usuario)
  {
    $ambito = kw2p_ambito().'::bienvenida';
    $instancia = kw2p_instancia().'::bienvenida';
    $view = View::exists($ambito) ? $ambito : $instancia;

    Mail::send($view, [
      'correo' => $usuario->correo,
      'token' => $usuario->token_recuperar,
    ], function (Message $message) use ($usuario) {
      $message->subject(config('app.name').' - Correo bienvenida');
      $message->from(config('mail.from.address'), config('mail.from.name'));
      $message->to($usuario->correo, $usuario->nombre);
    });
  }

  public static function correoBienvenidaSinContrasena($usuario)
  {
    $ambito = kw2p_ambito().'::bienvenida_sin_contrasena';
    $instancia = kw2p_instancia().'::bienvenida_sin_contrasena';
    $view = View::exists($ambito) ? $ambito : $instancia;

    Mail::send($view, [
      'usuario' => $usuario,
    ], function (Message $message) use ($usuario) {
      $message->subject(config('app.name').' - Correo bienvenida');
      $message->from(config('mail.from.address'), config('mail.from.name'));
      $message->to($usuario->correo, $usuario->nombre);
    });
  }

  public static function recuperarContrasena($usuario)
  {
    $ambito = kw2p_ambito().'::recuperar_contrasena';
    $instancia = kw2p_instancia().'::recuperar_contrasena';
    $view = View::exists($ambito) ? $ambito : $instancia;

    Mail::send($view, [
      'correo' => $usuario->correo,
      'token' => $usuario->token_recuperar,
    ], function (Message $message) use ($usuario) {
      $message->subject(config('app.name').' - Recuperar contraseña');
      $message->from(config('mail.from.address'), config('mail.from.name'));
      $message->to($usuario->correo, $usuario->nombre);
    });
  }

  public static function asignarContrasena($usuario)
  {
    $ambito = kw2p_ambito().'::asignar_contrasena';
    $instancia = kw2p_instancia().'::asignar_contrasena';
    $view = View::exists($ambito) ? $ambito : $instancia;

    Mail::send($view, [
      'correo' => $usuario->correo,
      'token' => $usuario->token_recuperar,
    ], function (Message $message) use ($usuario) {
      $message->subject(config('app.name').' - Asignar contraseña');
      $message->from(config('mail.from.address'), config('mail.from.name'));
      $message->to($usuario->correo, $usuario->nombre);
    });
  }

  public static function correoPersonalizado($usuario, $data)
  {
    Mail::send([], [
      'usuario' => $usuario 
    ], function (Message $message) use ($usuario, $data) {
      $message->subject(config('app.name').' - '.$data['titulo']);
      //$message->setBody($data['cuerpo_correo']);
      $message->setBody('<p>'.$data['cuerpo_correo'].'</p>', 'text/html');
      $message->from(config('mail.from.address'), config('mail.from.name'));
      $message->to($usuario->correo, $usuario->nombre);
    });
  }
}
