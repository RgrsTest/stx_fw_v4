<?php

namespace Klay\Models;

use Archiving\SDK\Models\DocumentWithStorages;
use Archiving\SDK\Models\Scheme;

class DocumentoConsulta
{
  protected $documento = null;

  public function __construct($schema = null)
  {
    $scheme = Scheme::where(['name' => $schema, 'type' => 'doc'])->firstOrFail();
    $this->documento = DocumentWithStorages::setScheme($scheme);
  }

  public function findOrFailWithStorage($folio, $serie = '', $with_storage = []){
    return $this->withStorageRelations($with_storage)->where('folio', $folio)->where('serie', $serie)->firstOrFail();
  }

  public function withRootRelations($arguments)
  {
    $this->documento->withRootRelations($arguments);
    return $this;
  }

  public function withStorageRelations($arguments)
  {
    $this->documento->withStorageRelations($arguments);
    return $this;
  }

  public function __call($method, $arguments)
  {
    return $this->documento->withRelations()->$method(...$arguments);
  }

}
