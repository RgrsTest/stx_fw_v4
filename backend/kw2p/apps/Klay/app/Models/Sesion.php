<?php

namespace Klay\Models;

use Klayware\KlayModel;

class Sesion extends KlayModel
{
  protected $table = 'seg_sesiones';

  protected $primaryKey = 'id';

  protected $fillable =
  [
    'usuario_id',
    'sesion',
    'vigencia',
  ];

  protected $hidden =
  [
    'sesion',
  ];

  public static function agregar($usuario)
  {
    $token = auth(kw2p_ambito())->fromUser($usuario);

    static::create
    (
      [
        'usuario_id' => $usuario->getKey(),
        'sesion' => session()->getId(),
        'vigencia' => now()->addMinutes(auth()->factory()->getTTL()),
      ]
    );

    return
    [
      'jwt' => $token,
      'payload' => auth(kw2p_ambito())->setToken($token)->getPayload()
    ];
  }

  public static function cerrar()
  {
    $payload = auth()->getPayload()->toArray();

    static::where
    (
      [
        ['usuario_id', '=', $payload['sub']],
        ['sesion', '=', $payload['ssn']],
      ]
    )->delete();
  }

  public static function depurar()
  {
    static::where('vigencia', '<', now())->delete();
  }

  public function usuario()
  {
    return $this->hasOne(Usuario::class, 'id', 'usuario_id');
  }
}
