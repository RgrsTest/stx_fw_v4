<?php

namespace Klay\Models;

use Klayware\KlayModel;

class Rol extends KlayModel
{
    protected $table = 'seg_roles';

    protected $primaryKey = 'id';

    protected $fillable = [
      'rol',
      'credenciales',
    ];

    public $timestamps = false;

    public function getCredencialesAttribute($credenciales)
    {
        return json_decode($credenciales, true);
    }

    public function setCredencialesAttribute($credenciales)
    {
        $this->attributes['credenciales'] = json_encode($credenciales);
    }
}
