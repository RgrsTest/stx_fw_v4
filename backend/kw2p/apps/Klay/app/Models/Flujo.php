<?php

namespace Klay\Models;

use Archiving\SDK\Client;
use Archiving\SDK\Flujo as FlujoBase;
use Archiving\SDK\hasQueryBuilder;

class Flujo extends FlujoBase
{
  use hasQueryBuilder;

  public function __construct($schema = null, Client $client = null, array $attributes = [])
  {
    parent::__construct($schema, new Client(config('services.archiving')), $attributes);
  }

  public function findOrFailWithStorage($folio, $serie = '', $with_storage = []){
    return $this->withStorage($with_storage)->where('folio', $folio)->where('serie', $serie)->FirstOrFail();
  }
}