<?php

namespace Klay\Models;

use Archiving\SDK\Catalogo;
use Archiving\SDK\Client;
use Archiving\SDK\Documento;
use Archiving\SDK\Exception as SDKException;
use Archiving\SDK\Flujo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RulerZ\Compiler\CompilationTarget;
use RulerZ\Compiler\Compiler;
use RulerZ\Context\ExecutionContext;
use RulerZ\Context\ObjectContext;
use RulerZ\Parser\Parser;
use RulerZ\RulerZ as RulerzBase;
use RulerZ\Target;

class RulerZ
{
  protected static $compiler;

  public function __construct()
  {
  }

  public function satisfies($scope, $rule, $parameters = [])
  {
    return $this->getRulerzInstance($rule)->satisfies($scope, $rule, $parameters);
  }

  public function filter($scope, $rule, $parameters = [])
  {
    return $this->getRulerzInstance($rule)->filter($scope, $rule, $parameters);
  }

  public function execute($scope, $rule, $parameters = [])
  {
    $closure = function($target, string $rule, array $parameters = [], array $executionContext = []) {
      $targetCompiler = $this->findTargetCompiler($target, CompilationTarget::MODE_SATISFIES);
      $compilationContext = $targetCompiler->createCompilationContext($target);
      $executor = $this->compiler->compile($rule, $targetCompiler, $compilationContext);

      $closure = function($target, array $operators, array $parameters, ExecutionContext $context) {
        $wrappedTarget = is_array($target) ? $target : new ObjectContext($target);
        return $this->execute($wrappedTarget, $operators, $parameters);
      };

      return $closure->call($executor, $target, $targetCompiler->getOperators()->getOperators(), $parameters, new ExecutionContext($executionContext));
    };

    return $closure->call($this->getRulerzInstance($rule), $scope, $rule, $parameters);
  }

  private function getCompiler()
  {
    if (null === static::$compiler) {
      static::$compiler = Compiler::create();
    }

    return static::$compiler;
  }

  private function getRulerzInstance($rule)
  {
    $operators = $this->resolveOperators($rule);
    return new RulerzBase($this->getCompiler(), [
      new Target\Native\Native($operators)
    ]);
  }

  private function resolveOperators($rule)
  {
    $rule = (new Parser)->parse($rule);

    $operators = [];

    foreach ($rule->getOperators() as $operator) {
      if ($operator->isFunction()) {
        $operators[] = $operator->getName();
      }
    }
    return $this->intersectOperators($operators);
  }

  private function intersectOperators($operators)
  {
    $intersected = [];
    foreach ($operators as $operator) {
      $closure = $this->getOperator($operator);
      if ($closure) {
        $intersected[$operator] = $closure;
      }
    }
    return $intersected;
  }

  private function getOperator($operator)
  {
    $studly_operator = 'get'.Str::studly($operator).'Operator';
    if (method_exists($this, $studly_operator)) {
      return $this->{$studly_operator}();
    }

    if (function_exists($operator)) {
      return $operator;
    }
  }

  private function getFilterOperator()
  {
    return function($scope, $rule, $parameters = []) {
      return iterator_to_array((new static)->filter($scope, $rule, $parameters));
    };
  }

  private function getIfOperator()
  {
    return function($if, $then, $else) {
      return $if ? $then : $else;
    };
  }

  private function getIssetOperator()
  {
    return function($scope, ...$params) {
      foreach ($params as $param) {
        if (!Arr::has($scope, $param)) {
          return false;
        }
      }
      return true;
    };
  }

  private function getFlujoOperator()
  {
    return function($schema, $serie, $folio, $rule = null, $parameters = []) {
      $cliente = new Client(config('services.archiving'));

      try {
        $scope = Flujo::cargar($schema, $folio, $serie, $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ])->toArray();
      } catch (SDKException $e) {
        return false;
      }
      if (!$rule) {
        return true;
      }
      return (new static)->satisfies($scope, $rule, $parameters);
    };
  }

  private function getDocumentoOperator()
  {
    return function($schema, $serie, $folio, $rule = null, $parameters = []) {
      $cliente = new Client(config('services.archiving'));

      try {
        $scope = Documento::cargar($schema, $folio, $serie, $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ])->toArray();
      } catch (SDKException $e) {
        return false;
      }
      if (!$rule) {
        return true;
      }
      return (new static)->satisfies($scope, $rule, $parameters);
    };
  }

  private function getCatalogoOperator()
  {
    return function($schema, $serie, $folio, $rule = null, $parameters = []) {
      $cliente = new Client(config('services.archiving'));

      try {
        $scope = Catalogo::cargar($schema, $folio, $serie, $cliente, [
          'with' => ['*'],
          'with-storage' => ['*']
        ])->toArray();
      } catch (SDKException $e) {
        return false;
      }
      if (!$rule) {
        return true;
      }
      return (new static)->satisfies($scope, $rule, $parameters);
    };
  }

  private function getStepOperator()
  {
    return function($step, $method, ...$params) {
      $class =  kw2p_ambito() . '\\Steps\\' . $step;
      if (class_exists($class) && method_exists($class, $method)) {
        return $class::{$method}(...$params);
      }
    };
  }

  private function getBatchOperator()
  {
    return function($batch, $method, ...$params) {
      $class =  kw2p_ambito() . '\\Batches\\' . $batch;
      if (class_exists($class) && method_exists($class, $method)) {
        return $class::{$method}(...$params);
      }
    };
  }

  private function getQueryOperator()
  {
    return function($query, $method, ...$params) {
      $class =  kw2p_ambito() . '\\Queries\\' . $query;
      if (class_exists($class) && method_exists($class, $method)) {
        return $class::{$method}(...$params);
      }
    };
  }

}
