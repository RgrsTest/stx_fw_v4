<?php

namespace Klay\Models;

if (class_exists('Laravel\Lumen\Application')) {
    class Authenticatable extends \Klayware\User {}
    interface JWTSubject {}
} else {
    class Authenticatable extends \Illuminate\Foundation\Auth\User {}
    interface JWTSubject extends \Tymon\JWTAuth\Contracts\JWTSubject {}
}

use Illuminate\Database\Eloquent\SoftDeletes;
use Klay\Traits\MemoryStorage;
use Klayware\Exceptions\KlayException;

class Usuario extends Authenticatable implements JWTSubject
{
  use SoftDeletes, MemoryStorage;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'seg_usuarios';

  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    // 'correo',
    // 'contrasena',
    // 'apikey',
    // 'roles',
    // 'credenciales',
    // 'token_recuperar',
    // 'token_vigencia',
  ];

  /**
   * The attributes that aren't mass assignable.
   *
   * @var array
   */
  protected $guarded = [];

  /**
   * The attributes that should be hidden for serialization.
   *
   * @var array
   */
  protected $hidden = [
    'contrasena',
    'apikey',
    'token_recuperar',
    'token_vigencia',
    'deleted_at'
  ];

  /**
   * The model's attributes.
   *
   * @var array
   */
  protected $attributes = [
    'contrasena' => '',
    'apikey' => '',
    'roles' => '[]',
    'credenciales' => '[]',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'roles' => 'array',
    'credenciales' => 'array',
  ];

  /**
   * Get the password for the user.
   *
   * @return string
   */
  public function getAuthPassword()
  {
    return auth()->getDefaultDriver() === 'basic' ? bcrypt($this->apikey) : $this->contrasena;
  }

  /**
   * Get the identifier that will be stored in the subject claim of the JWT.
   *
   * @return mixed
   */
  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims()
  {
    return [
      'ssn' => session()->getId(),
      'grd' => kw2p_ambito()
    ];
  }

  /**
   * Usuario tiene muchas sesiones
   *
   * @return hasMany
   */
  public function sesiones()
  {
    return $this->hasMany(Sesion::class, 'usuario_id', 'id');
  }

  public function getCredencialesAttribute($credenciales_de_usuario)
  {
    $credenciales = json_decode($credenciales_de_usuario, true);

    if (!empty($this->roles)) {
      foreach ($this->roles as $role) {
        $rol_credenciales = $this->getCredencialesPorRol($role);
        $credenciales = array_merge($credenciales, $rol_credenciales);
      }
    }

    return array_values(array_unique($credenciales));
  }

  public function getCredencialesPorRol($rol)
  {
    return $this->memoize([__METHOD__, $rol], function() use ($rol) {
      return Rol::where(config('auth.rol_identified_by', 'rol'), $rol)->first(['credenciales'])->credenciales ?? [];
    });
  }

  public function agregarCredencial($nuevas_credenciales)
  {
    $credenciales = $this->castAttribute('credenciales', $this->attributes['credenciales']);

    $this->setAttribute('credenciales', array_merge($credenciales, array_wrap($nuevas_credenciales)));

    return $this;
  }

  /**
   * Usuario pertenece a algun rol
   * @param  string/array $rolOrRoles
   * @return boolean
   */
  public function is($rolOrRoles)
  {
    return (bool) array_intersect($this->roles, array_wrap($rolOrRoles));
  }

  public function validarUsuario($usuario_id)
  {
    if ($this->getKey() != $usuario_id) {
      throw (new KlayException('El usuario es incorrecto.', 'usuario_incorrecto'));
    }
    return true;
  }

  public function validarContrasena($contrasena)
  {
    if (!\Hash::check($contrasena, $this->contrasena)) {
      throw (new KlayException('Contraseña invalida.', 'contrasena_invalida'));
    }
    return true;
  }

  public function validarApikey($apikey)
  {
    if ($this->apikey != $apikey) {
      throw (new KlayException('Apikey incorrecta.', 'apikey_incorrecta'))->status(401);
    }
    return true;
  }

  public function validarTokenRecuperar($token_recuperar)
  {
    if (($this->token_recuperar != $token_recuperar) || ($this->token_vigencia < now())) {
      throw (new KlayException('El token del usuario no es correcto', 'token_invalido'));
    }
    return true;
  }
}
