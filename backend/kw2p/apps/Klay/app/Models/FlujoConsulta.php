<?php

namespace Klay\Models;

use Archiving\SDK\Models\FlowWithStorages;
use Archiving\SDK\Models\Scheme;

class FlujoConsulta
{
  protected $flujo = null;

  public function __construct($schema = null)
  {
    $scheme = Scheme::where(['name' => $schema, 'type' => 'doc'])->firstOrFail();
    $this->flujo = FlowWithStorages::setScheme($scheme);
  }

  public function findOrFailWithStorage($folio, $serie = '', $with_storage = []){
    return $this->withStorageRelations($with_storage)->where('folio', $folio)->where('serie', $serie)->firstOrFail();
  }

  public function withRootRelations($arguments)
  {
    $this->flujo->withRootRelations($arguments);
    return $this;
  }

  public function withStorageRelations($arguments)
  {
    $this->flujo->withStorageRelations($arguments);
    return $this;
  }

  public function __call($method, $arguments)
  {
    return $this->flujo->withRelations()->$method(...$arguments);
  }

}
