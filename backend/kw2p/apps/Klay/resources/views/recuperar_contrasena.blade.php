<p>Alguien ha solicitado restablecer su contraseña.</p>
<p>Seleccione el siguiente enlace para poder modificarla.</p>
<p><a href="{{ config('app.url') }}/login/asignar_clave/{{ $correo }}/{{ $token }}">Restablecer contraseña</a></p>
<p>Si usted no ha solicitado este cambio, ignore este mensaje.</p>
<p>Su contraseña no cambiará hasta que ingrese al enlace anterior y genere una nueva contraseña</p>
