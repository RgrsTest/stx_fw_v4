<p>Bienvenido a {{ config('app.name') }}.</p>
<p>Seleccione el siguiente enlace para poder asignar su contraseña.</p>
<p><a href="{{ config('app.url') }}/login/asignar_clave/{{ $correo }}/{{ $token }}">Asignar contraseña</a></p>
