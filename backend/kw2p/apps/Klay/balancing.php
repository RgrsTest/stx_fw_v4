<?php
///////////////////////////////////////////////////////////////////////////////////////////
// Balance financiero
///////////////////////////////////////////////////////////////////////////////////////////

function balance_financiero_hacer_movimiento_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS_PORAPLICAR.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MONTO_INCORRECTO');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCORRECTOS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCOMPLETOS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_INCOMPLETOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACIONES_INCORRECTAS');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'NO_ACEPTA_RELACIONES');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACION_MONTO_INCORRECTO');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACIONES_MAYORES_AL_MOVIMIENTO');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_NEGATIVO');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPOS_IGUALES');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_MENOR_MOVIMIENTO_CREADO');
        case '402-20':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_MENOR_MOVIMIENTO_RELACIONAR');
        case '402-21':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MONTO_DISPONIBLE_INSUFICIENTE');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_deshacer_movimiento_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS_PORAPLICAR.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_aplicar_movimiento($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS_PORAPLICAR.APLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_hacer_movimiento($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MONTO_INCORRECTO');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCORRECTOS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCOMPLETOS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_INCOMPLETOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACIONES_INCORRECTAS');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'NO_ACEPTA_RELACIONES');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACION_MONTO_INCORRECTO');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACIONES_MAYORES_AL_MOVIMIENTO');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_NEGATIVO');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPOS_IGUALES');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_MENOR_MOVIMIENTO_CREADO');
        case '402-20':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_MENOR_MOVIMIENTO_RELACIONAR');
        case '402-21':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MONTO_DISPONIBLE_INSUFICIENTE');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'BALANCING:HACER_MOVIMIENTO.TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'BALANCING:HACER_MOVIMIENTO.ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_deshacer_movimiento($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_CON_RELACIONES');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_NEGATIVO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_desaplicar_movimiento($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_CON_RELACIONES');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_NEGATIVO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_hacer_relacion($e_INSTANCIA, $m_RELACIONES)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'RELACIONES' => $m_RELACIONES
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.HACER_RELACION', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MONTO_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'NO_ACEPTA_RELACIONES');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ABONO_INCORRECTO');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CARGO_INCORRECTO');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_ABONO_INCORRECTO');
        case '402-8':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_CARGO_INCORRECTO');
        case '402-9':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_MENOR_MOVIMIENTO_ABONO');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_MENOR_MOVIMIENTO_CARGO');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MONTO_CARGO_INSUFICIENTE');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_deshacer_relacion($e_INSTANCIA, $m_RELACIONES)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'RELACIONES' => $m_RELACIONES
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.DESHACER_RELACION', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'NO_ACEPTA_RELACIONES');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RELACION_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ABONO_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CARGO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_actualizar_movimientos($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.ACTUALIZAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CAMPOS_VACIOS');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_movimientos_consultar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.MOVIMIENTOS.CONSULTAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_BETWEEN_ATRIBUTO_VALUE_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_DIRECTION_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_ORDER_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_LIMIT_ATRIBUTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_resumenes_consultar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.RESUMENES.CONSULTAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_BETWEEN_ATRIBUTO_VALUE_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_DIRECTION_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_ORDER_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_LIMIT_ATRIBUTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_financiero_saldos_consultar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_FINANCIERO.SALDOS.CONSULTAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_BETWEEN_ATRIBUTO_VALUE_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_DIRECTION_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_ORDER_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_LIMIT_ATRIBUTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

///////////////////////////////////////////////////////////////////////////////////////////
// Balance de inventario (Entradas)
///////////////////////////////////////////////////////////////////////////////////////////


function balance_inventario_hacer_entrada_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.ENTRADAS_PORAPLICAR.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INCORRECTA');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_SERIE_INCORRECTA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_CANTIDAD_INCORRECTA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'VALOR_CANTIDAD_INCORRECTO');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCOMPLETOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCORRECTOS');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCOMPLETOS');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCORRECTOS');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCOMPLETOS');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LONGITUD_SERIE_INCORRECTA');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_DUPLICADA');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_DUPLICADA_PORAPLICAR');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LIMITES_SERIE_INCORRECTOS');
        case '402-20':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_DUPLICADOS');
        case '402-21':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_DUPLICADOS_PORAPLICAR');
        case '402-22':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_INCORRECTA');
        case '402-23':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_INCORRECTO');
        case '402-24':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_DISPONIBLE_INSUFICIENTE');
        case '402-25':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_NO_DISPONIBLE');
        case '402-26':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_PORAPLICAR');
        case '402-27':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LONGITUD_INCORRECTA');
        case '402-28':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGO_INCORRECTO');
        case '402-29':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGO_NO_DISPONIBLE');
        case '402-30':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_RANGO_PORAPLICAR');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_deshacer_entrada_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.ENTRADAS_PORAPLICAR.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_ENTRADA_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_INCORRECTA');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_aplicar_entrada($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.ENTRADAS_PORAPLICAR.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_ENTRADA_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_ENTRADA_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_hacer_entrada($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.ENTRADAS.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INCORRECTA');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_SERIE_INCORRECTA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_CANTIDAD_INCORRECTA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'VALOR_CANTIDAD_INCORRECTO');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCOMPLETOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCORRECTOS');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCOMPLETOS');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCORRECTOS');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCOMPLETOS');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LONGITUD_SERIE_INCORRECTA');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_DUPLICADA');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_DUPLICADA_PORAPLICAR');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LIMITES_SERIE_INCORRECTOS');
        case '402-20':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_DUPLICADOS');
        case '402-21':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_DUPLICADOS_PORAPLICAR');
        case '402-22':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_INCORRECTA');
        case '402-23':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_INCORRECTO');
        case '402-24':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_DISPONIBLE_INSUFICIENTE');
        case '402-25':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_NO_DISPONIBLE');
        case '402-26':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_PORAPLICAR');
        case '402-27':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LONGITUD_INCORRECTA');
        case '402-28':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGO_INCORRECTO');
        case '402-29':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGO_NO_DISPONIBLE');
        case '402-30':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_RANGO_PORAPLICAR');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_deshacer_entrada($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.ENTRADAS.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_ENTRADA_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_PORAPLICAR');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'BALANCING:DESHACER_ENTRADA.TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'BALANCING:DESHACER_ENTRADA.ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_desaplicar_entrada($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.ENTRADAS.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_ENTRADA_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_ENTRADA_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_SALDOS_INCOMPLETOS');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], '.ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

///////////////////////////////////////////////////////////////////////////////////////////
// Balance de inventario (Salidas)
///////////////////////////////////////////////////////////////////////////////////////////

function balance_inventario_hacer_salida_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALIDAS_PORAPLICAR.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INCORRECTA');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_INCORRECTA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_CANTIDAD_INCORRECTA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'VALOR_CANTIDAD_INCORRECTO');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCOMPLETOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_INCORRECTO');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_PROVENIENTE_INCORRECTA');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INSUFICIENTE');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_INCORRECTA');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_PORAPLICAR');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LIMITES_SERIE');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGO_PORAPLICAR');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_deshacer_salida_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALIDAS_PORAPLICAR.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_SALIDA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_SALIDA_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_aplicar_salida($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALIDAS_PORAPLICAR.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_SALIDA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_SALIDA_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_hacer_salida($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALIDAS.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INCORRECTA');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_INCORRECTA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_CONFIGURACION_INCORRECTA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'VALOR_CANTIDAD_INCORRECTO');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCOMPLETOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_INCORRECTO');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_PROVENIENTE_INCORRECTA');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INSUFICIENTE');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_INCORRECTA');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_EN_SALDO');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LIMITES_SERIE');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGO_PORAPLICAR');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_deshacer_salida($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALIDAS.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_SALIDA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_SALIDA_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_RELACIONADA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_RELACIONADA_PORAPLICAR');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_INCORRECTA');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_EN_SALDOS_PORAPLICAR');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_EN_SALDOS_PORAPLICAR');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_ANTERIOR_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_desaplicar_salida($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALIDAS.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_SALIDA_INCORRECTA');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_SALIDA_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_RELACIONADA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALIDA_RELACIONADA_PORAPLICA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_EN_SALDOS');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_EN_SALDOS_PORAPLICAR');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_EN_SALDOS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_EN_SALDOS_PORAPLICAR');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_ANTERIOR_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

///////////////////////////////////////////////////////////////////////////////////////////
// Balance de inventario (Traspasos)
///////////////////////////////////////////////////////////////////////////////////////////


function balance_inventario_hacer_traspaso_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.TRASPASOS_PORAPLICAR.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INCORRECTA');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_INCORRECTA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_CANTIDAD_INCORRECTA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'VALOR_CANTIDAD_INCORRECTO');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCORRECTOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCOMPLETOS');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCORRECTOS');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCOMPLETOS');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_INCORRECTO');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_PROVENIENTE_INCORRECTA');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INSUFICIENTE');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_INCORRECTA');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_PORAPLICAR');
        case '402-20':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LIMITES_SERIE');
        case '402-21':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        case '402-22':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_deshacer_traspaso_poraplicar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.TRASPASOS_PORAPLICAR.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TRASPASO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_TRASPASO_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_TRASPASO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_aplicar_traspaso($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.TRASPASOS_PORAPLICAR.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TRASPASO_PORAPLICAR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_TRASPASO_PORAPLICAR_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_TRASPASO_PORAPLICAR_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_hacer_traspaso($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.TRASPASOS.HACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_INCORRECTA');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'DECIMALES_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INCORRECTA');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_INCORRECTA');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CONFIGURACION_CANTIDAD_INCORRECTA');
        case '402-07':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'VALOR_CANTIDAD_INCORRECTO');
        case '402-08':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCORRECTAS');
        case '402-09':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FIRMAS_INCOMPLETAS');
        case '402-10':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CRITERIOS_INCORRECTOS');
        case '402-11':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCORRECTOS');
        case '402-12':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_MOVIMIENTO_INCOMPLETOS');
        case '402-13':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCORRECTOS');
        case '402-14':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ATRIBUTOS_SALDO_INCOMPLETOS');
        case '402-15':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SALDO_INCORRECTO');
        case '402-16':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'FECHA_PROVENIENTE_INCORRECTA');
        case '402-17':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CANTIDAD_INSUFICIENTE');
        case '402-18':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_INCORRECTA');
        case '402-19':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'SERIE_PORAPLICAR');
        case '402-20':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'LIMITES_SERIE');
        case '402-21':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        case '402-22':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RANGOS_INCORRECTOS');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_deshacer_traspaso($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.TRASPASOS.DESHACER', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TRASPASO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_TRASPASO_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_TRASPASO_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_PORAPLICAR');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_ANTERIOR_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_desaplicar_traspaso($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.TRASPASOS.DESAPLICAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'RUTA_MODULO_INCORRECTA');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TRASPASO_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'TIPO_TRASPASO_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ID_TRASPASO_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ENTRADA_PORAPLICAR');
        case '402-06':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_ANTERIOR_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_actualizar_movimientos($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.MOVIMIENTOS.ACTUALIZAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'MOVIMIENTO_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'CAMPOS_VACIOS');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_movimientos_consultar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.MOVIMIENTOS.CONSULTAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_BETWEEN_ATRIBUTO_VALUE_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_DIRECTION_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_ORDER_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_LIMIT_ATRIBUTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_resumenes_consultar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.RESUMENES.CONSULTAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_BETWEEN_ATRIBUTO_VALUE_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_DIRECTION_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_ORDER_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_LIMIT_ATRIBUTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function balance_inventario_saldos_consultar($e_INSTANCIA, $m_MOVIMIENTOS)
{
  $m_BODY = array
  (
    'INSTANCIA' => $e_INSTANCIA,
    'MOVIMIENTOS' => $m_MOVIMIENTOS
  );

  $m_RESPONSE = rest('POST', config('balancing.server') . 'index.php?ACTION=BALANCING.BE:C1.ACTION.BALANCE_INVENTARIO.SALDOS.CONSULTAR', ['Content-Type: application/json'], json_encode($m_BODY), 600, 600);

  switch($m_RESPONSE['STATUS'])
  {
    case '200':
      break;
    case '400':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_INTERNO');
    case '402':
      switch($m_RESPONSE['BODY']['error_code'])
      {
        case '402-01':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_BETWEEN_ATRIBUTO_VALUE_INCORRECTO');
        case '402-02':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_INCORRECTO');
        case '402-03':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_DIRECTION_INCORRECTO');
        case '402-04':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_ORDERBY_ATRIBUTO_ORDER_INCORRECTO');
        case '402-05':
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'OPERADOR_LIMIT_ATRIBUTO_INCORRECTO');
        default:
          throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
      }
    case '500':
      throw new BalancingException($m_RESPONSE['BODY']['error_message'], 'ERROR_EXTERNO');
    case '628':
      throw new BalancingException('Tiempo de espera agotado', 'TIMEOUT');
    default:
      throw new BalancingException("Error de conexion con el servidor ({$m_RESPONSE['STATUS']})", 'ERROR_EXTERNO');
  }

  return $m_RESPONSE['BODY']['RETURN'];
}

function rest($e_METHOD, $e_URL, $m_HEADERS, $m_BODY, $e_CONNECT_TIMEOUT, $e_RESPONSE_TIMEOUT)
{
  $r_CURL = curl_init($e_URL);

  curl_setopt($r_CURL, CURLOPT_CONNECTTIMEOUT, $e_CONNECT_TIMEOUT);
  curl_setopt($r_CURL, CURLOPT_TIMEOUT, $e_RESPONSE_TIMEOUT);
  curl_setopt($r_CURL, CURLOPT_HTTPHEADER, $m_HEADERS);
  curl_setopt($r_CURL, CURLOPT_CUSTOMREQUEST, $e_METHOD);
  curl_setopt($r_CURL, CURLOPT_POSTFIELDS, $m_BODY);
  curl_setopt($r_CURL, CURLOPT_RETURNTRANSFER, 1);

  if(($e_RESPONSE = curl_exec($r_CURL)) === false)
  {
    $m_RESPONSE = curl_error($r_CURL);

    $e_HTTP_CODE = 600 + curl_errno($r_CURL);
  }
  else
  {
    $m_RESPONSE = json_decode($e_RESPONSE, true);

    $e_HTTP_CODE = curl_getinfo($r_CURL, CURLINFO_HTTP_CODE);
  }

  return ['STATUS' => $e_HTTP_CODE, 'BODY' => $m_RESPONSE];
}

class BalancingException extends Exception
{
  public $code;

  public function __construct($message = '', $code = '', Throwable $previous = null)
  {
    parent::__construct($message, 0, $previous);

    $this->code = $code;
  }
}
?>
