<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KlayInit extends Migration
{
  public function allow($tables, $tableName)
  {
    return empty($tables) || in_array($tableName, $tables);
  }

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up(array $tables = [])
  {
    if ($this->allow($tables, 'seg_sesiones')) {
      Schema::create('seg_sesiones', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('usuario_id');
        $table->string('sesion', 40);
        $table->dateTime('vigencia');
        $table->timestamps();
      });
    }

    if ($this->allow($tables, 'seg_usuarios')) {
      Schema::create('seg_usuarios', function (Blueprint $table) {
        $table->increments('id');
        $table->string('correo', 100);
        $table->string('contrasena', 100);
        $table->string('apikey', 100);
        $table->json('roles');
        $table->json('credenciales');
        $table->string('token_recuperar', 100)->nullable();
        $table->dateTime('token_vigencia')->nullable();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    if ($this->allow($tables, 'seg_roles')) {
      Schema::create('seg_roles', function (Blueprint $table) {
        $table->increments('id');
        $table->string('rol');
        $table->json('credenciales');
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down(array $tables = [])
  {
    if ($this->allow($tables, 'seg_sesiones')) {
      Schema::dropIfExists('seg_sesiones');
    }

    if ($this->allow($tables, 'seg_usuarios')) {
      Schema::dropIfExists('seg_usuarios');
    }

    if ($this->allow($tables, 'seg_roles')) {
      Schema::dropIfExists('seg_roles');
    }
  }
}
