#!/bin/bash
echo "Activando servicios"
cp /opt/fw_alcancia/perifericos/services/*.service /etc/systemd/system/

systemctl daemon-reload

for serv in $(ls /opt/fw_alcancia/perifericos/services)
do
    echo "Instalando servicio $serv"
    systemctl enable $serv
done

echo "Configurando logrotate"

cp /opt/fw_alcancia/logs/logrotate/smartbus /etc/logrotate.d


logrotate /etc/logrotate.conf --verbose

echo "@sudo python3 /opt/fw_alcancia/perifericos/display/display.py start" >> /etc/xdg/lxsession/LXDE-pi/autostart

echo "@sudo /opt/fw_alcancia/scripts/screen.sh" >> /etc/xdg/lxsession/LXDE-pi/autostart
