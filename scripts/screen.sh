#!/bin/bash

sleep 5s
xrandr --newmode "1024x600_60.00"  48.96  1024 1064 1168 1312  600 601 604 622  -HSync +Vsync

sleep 10s
xrandr --addmode HDMI-1 "1024x600_60.00"
xrandr --output HDMI-1 --mode "1024x600_60.00"