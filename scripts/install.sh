#!/bin/bash

#Clonar el repositorio
#cd /opt
#git clone https://gitlab.com/RgrsTest/stx_fw_v4

apt-get update

apt-get upgrade

echo "Agregando el repositorio de php 7.3"
sudo apt -y install lsb-release apt-transport-https ca-certificates 
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

apt update

echo "Instalando PHP y SQLite"
apt-get install php7.3 php7.3-cli php7.3-common php7.3-json php7.3-opcache php7.3-sqlite3 php7.3-bcmath php7.3-gd php7.3-zip php7.3-curl php7.3-mbstring php7.3-dom php7.3-sqlite3 sqlite3


#Instalar composer

echo "Instalando Composer"
EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --install-dir=/bin --filename=composer


RESULT=$?
rm composer-setup.php
echo $RESULT
if [ $RESULT = 0 ]
    then
        echo "Configurando Backend"

        composer self-update 1.10.17

        #Configurar backend
        cd /opt/fw_alcancia/backend/kw2p
        composer install

        cp .env.example .env

        touch /opt/fw_alcancia/database/smartbus.db

        php artisan migrate --path=apps/Smartbus/database/migrations/ --database=Smartbus
        php artisan db:seed --database=Smartbus --class=\\SmartbusSeeder\\DatabaseSeeder

        #Configurar el Display
        echo "Configurando Display"

        echo "@xset s noblank" >> /etc/xdg/lxsession/LXDE-pi/autostart
        echo "@xset s off" >> /etc/xdg/lxsession/LXDE-pi/autostart
        echo "@xset -dpms" >> /etc/xdg/lxsession/LXDE-pi/autostart

        sed -i "1d" /etc/xdg/lxsession/LXDE-pi/autostart

        echo "max_usb_current:0=1" >> /boot/config.txt
        echo "hdmi_group:0=2" >> /boot/config.txt
        echo "hdmi_mode:0=87" >> /boot/config.txt
        echo "hdmi_cvt:0=1024 600 60 6 0 0 0" >> /boot/config.txt
        echo "max_usb_current:1=1" >> /boot/config.txt
        echo "hdmi_group:1=2" >> /boot/config.txt
        echo "hdmi_mode:1=87" >> /boot/config.txt
        echo "hdmi_cvt:1=1024 600 60 6 0 0 0" >> /boot/config.txt
        
        echo "hdmi_drive=1" >> /boot/config.txt

        
        sed -i 's/dtoverlay=vc4-fkms-v3d/#dtoverlay=vc4-fkms-v3d/g' /boot/config.txt

        apt-get install xserver-xorg-input-libinput

        mkdir /etc/X11/xorg.conf.d

        cp /usr/share/X11/xorg.conf.d/40-libinput.conf /etc/X11/xorg.conf.d/

        sed -i '/MatchIsTouchscreen "on"/a\Option "CalibrationMatrix" "0 -1 1 1 0 0 0 0 1"' /etc/X11/xorg.conf.d/40-libinput.conf

        #instalar librerias de python
        echo "Instalar Liberias de Python"

        apt install libmbim-utils python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qtwidgets libusb-dev python3-cffi

        pip3 install crcmod simpleaudio
        
        cd /opt/fw_alcancia/perifericos/changer/izmdb-1.0.0

        make install-all

        echo "Dando permisos a los scripts"

        chmod 755 /opt/fw_alcancia/scripts/screen.sh /opt/fw_alcancia/scripts/activate.sh
        
fi
echo "Finalizado"
exit $RESULT
