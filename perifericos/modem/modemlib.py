import subprocess
from bitacora import Bitacora
import json
import serial
import puerto
from time import sleep
import os

class Modem:
    """
    Clase para controlar el modem
    """
    #Constructor
    def __init__(self, path):
        """
        path: string Ruta del directorio raiz
        """
        self.trid = ""
        self.bitacora = Bitacora(path, "Modem")
        self.bitacora.escribir("Inicializando Modem....")
    
    def status(self):
        """
        Funcion para verificar el estado del subscriptor
        Retorna True si es posible verificarlo, False en caso contrario
        """       
        try:
            self.bitacora.escribir("Check subscriber query status")
            consulta = subprocess.check_output("sudo mbimcli -d  /dev/cdc-wdm0 --query-subscriber-ready-status --no-close ", shell=True)
            consulta = consulta.decode("latin-1")
            self.bitacora.escribir(consulta)
            caracter = consulta.find('TRID')
            self.trid = consulta[(caracter+7):(caracter+8)]
            return True
        except Exception as e:
            self.bitacora.escribir("Error al consultar el estado: " + str(e))
            return False

    #Funcion para registrar el suscriptor en la red
    def registration(self):
        """
        Funcion para registrar el suscriptor en la red
        Retorna True si es posible verificarlo, False en caso contrario
        """
        try:
            self.bitacora.escribir("Check registration state")
            comando = "sudo mbimcli -d /dev/cdc-wdm0 --query-registration-state --no-open="+ self.trid + " --no-close"
            consulta = subprocess.check_output(comando, shell=True)
            consulta = consulta.decode("latin-1")
            self.bitacora.escribir(consulta)
            caracter = consulta.find('TRID')
            self.trid = consulta[(caracter+7):(caracter+8)]
            return True
        except Exception as e:
            self.bitacora.escribir("Error al consultar el registro: " + str(e))
            return False

    #Funcion para atar al subscriptor a la interfaz de red
    def attach(self):
        """
        Funcion para atar al subscriptor a la interfaz de red
        Retorna True si es posible verificarlo, False en caso contrario
        """
        try:
            self.bitacora.escribir("Check attach packet")
            comando = "sudo mbimcli -d /dev/cdc-wdm0  --attach-packet-service --no-open=" + self.trid +" --no-close" 
            consulta = subprocess.check_output(comando, shell=True)
            consulta = consulta.decode("latin-1")
            self.bitacora.escribir(consulta)
            caracter = consulta.find('TRID')
            self.trid = consulta[(caracter+7):(caracter+8)]
            return True
        except Exception as e:
            self.bitacora.escribir("Error al intentar enlazar: " + str(e))
            return False

    #Funcion para conectar con el apn
    def connect(self):
        """
        Funcion para conectar con el apn
        Retorna True si es posible verificarlo, False en caso contrario
        """
        try:
            self.bitacora.escribir("Connecting...")
            comando = "sudo mbimcli -d /dev/cdc-wdm0  --connect=apn='internet.itelecel.com' --no-open="+ self.trid + " --no-close "
            consulta = subprocess.check_output(comando, shell=True)
            consulta = consulta.decode("latin-1")
            self.bitacora.escribir(consulta)
            caracter = consulta.find('TRID')
            self.trid = consulta[(caracter+7):(caracter+8)]
            if "Sucessfully connected" in consulta:
                return True
            else:
                return False
        except Exception as e:
            self.bitacora.escribir("Error al conectar: " + str(e))
            return False

    #Funcion para desconectar de la red
    def disconnect(self):
        """
        Funcion para desconectar de la red
        Retorna True si es posible verificarlo, False en caso contrario
        """
        try:
            self.bitacora.escribir("Disconnecting...")
            comando = "sudo mbimcli -d /dev/cdc-wdm0  --disconnect --no-open="+ self.trid
            consulta = subprocess.check_output(comando, shell=True)
            consulta = consulta.decode("latin-1")
            self.bitacora.escribir(consulta)
            caracter = consulta.find('TRID')
            self.trid = consulta[(caracter+7):(caracter+8)]
            if "Sucessfully disconnected" in consulta:
                return True
            else:
                return False
        except Exception as e:
            self.bitacora.escribir("Error al intentar desconectar: " + str(e))
            return True

    
    #Funcion para inicializar haciendo todos los pasos
    def inicializar(self):
        """
        Funcion para inicializar haciendo todos los pasos
        Retorna True si la conexion se establecio o False en caso contrario
        """
        try:
            if self.status():
                if self.registration(): 
                    if self.attach():
                        return self.connect()
            return False
        except Exception as e:
            self.bitacora.escribir("Error al inicializar conexion: " + str(e))
            return False

    #Funcion para verificar la conexion de a la red
    def checkConnection(self):
        """
        Funcion para verificar la conexion de a la red
        Retorna True si la conexion esta establecida o False en caso contrario
        """
        try:
            self.bitacora.escribir("Checking connection")
            consulta = subprocess.check_output("timeout 10 ping -c 4 -I wwan0 google.com", shell=True)
            consulta = consulta.decode('latin-1')
            self.bitacora.escribir(consulta)
            if "4 received" in consulta:
                return True
        except Exception as e:
            self.bitacora.escribir("Error al consultar el estatus: " + str(e))
            return False
        return False

    def initialize(self, path):
       
       
        """
        Funcion para inicializar la comunicacion con el periferico
        path: string Ruta del archivo de configuracion
        Retorna True si el puerto se ha abierto, False en caso contrario
        """
        try:
            
            if os.path.exists(path):
                archivo = json.load(open(path, 'r'))
                port = puerto.obtenerPuerto(archivo["port"])
                self.serial = serial.Serial(port, 115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1)
                self.bitacora.escribir("Puerto Abierto con exito")
                return True
            else:
                self.bitacora.escribir("Error al abrir el puerto: " + str(e))
                return False
            
            
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False
    
 
    def start(self, path):
        
        """
        Funcion para iniciar el proceso desde afuera
        path: string Ruta del archivo de configuracion
        Retorna True si fue exitoso, False en caso contrario
        """
        self.estado = self.initialize(path)
        return self.estado

    def QCRMCALL(self):
        """
        Funcion para asignar IP 
        """
        
        try:
            self.bitacora.escribir("ENABLE QCRMCALL")
            self.serial.write(b"AT$QCRMCALL=1,1\r")
            sleep(3)
            while self.serial.in_waiting > 0:
                response = self.serial.readline()
                print(response)
                self.bitacora.escribir(response)
                response = response.decode("latin-1")
                print(response)
                self.bitacora.escribir(response)
                
                if "V4" in response:
                    self.bitacora.escribir("QCRMCALL SUCCESS")
                    return True
                elif "NO CARRIER" in response:
                    self.bitacora.escribir("QCRMCALL FAIL")
                    return False
                
            return False
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False
          
    
    def DISABLE_QCRMCALL(self):
        """
        Funcion para asignar IP 
        """
        
        try:
            self.bitacora.escribir("DISABLE QCRMCALL")
            self.serial.write(b"AT$QCRMCALL=0,1\r")
            sleep(3)
            while self.serial.in_waiting > 0:
                response = self.serial.readline()
                print(response)
                self.bitacora.escribir(response)
                response = response.decode("latin-1")
                print(response)
                self.bitacora.escribir(response)
                
               
                
            return False
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False
           
               
    def start_udhcpc(self):
        
        try:
            self.bitacora.escribir("Start udhcpc")        
            udhcpcProcess = subprocess.check_output("sudo timeout 5 udhcpc -i wwan0", shell=True)
            udhcpcProcess = udhcpcProcess.decode('latin-1')
            self.bitacora.escribir(udhcpcProcess)
           
             
            self.bitacora.escribir("Succesful udhcpc") 
            return True
            
        
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False
            
    def remove_qmi(self):
        
        try:
            self.bitacora.escribir("Removing qmi_wwan")        
            udhcpcProcess = subprocess.check_output("sudo rmmod qmi_wwan", shell=True)
            udhcpcProcess = udhcpcProcess.decode('latin-1')
            self.bitacora.escribir(udhcpcProcess)
           
             
            self.bitacora.escribir("qmi removed") 
            return True
            
        
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False

    def install_simcom(self):
        
        try:
            self.bitacora.escribir("Installing simcom_wwan")        
            udhcpcProcess = subprocess.check_output("sudo insmod /opt/fw_alcancia/simcom_wwan.ko", shell=True)
            udhcpcProcess = udhcpcProcess.decode('latin-1')
            self.bitacora.escribir(udhcpcProcess)
           
             
            self.bitacora.escribir("simcom installed") 
            return True
            
        
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False
          
