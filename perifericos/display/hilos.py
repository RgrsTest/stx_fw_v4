from genericpath import exists
import json
import datetime
import os
import time
from signals import Senials
from PySide2 import QtCore, QtWidgets, QtGui
from bitacora import Bitacora


class Reloj(QtCore.QThread):
    """
    Clase para crear el hilo que controla el reloj de la interfaz
    """
    fechaActual = "0000-00-00 00:00:00"
    def __init__(self, parent= None):
        """
        parent: QObject Objeto al que pertenece la instancia
        """
        QtCore.QThread.__init__(self, parent)
        self.signal = Senials()
        self.signal.fecha.connect(parent.actualizarTiempo)
        self.bitacora = Bitacora(parent.path, "Display_hilos")
    
    def run(self):
        """
        Funcion para ejecutar el hilo
        """
        while 1:
            fecha = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S")
            if fecha != self.fechaActual:
                self.fechaActual = fecha
                self.signal.fecha.emit(self.fechaActual)
                time.sleep(0.5)


class Estados(QtCore.QThread):
    """
    Clase para crear el hilo que controla los estados
    """

    def __init__(self, parent = None):
        """
        parent: QObject Objeto al que pertenece la instancia
        """
        QtCore.QThread.__init__(self,parent)
        self.estados = parent.path + "/shared/display/estados.json"
        self.signal = Senials()
        self.signal.estados.connect(parent.actualizarEstado)
        self.bitacora = Bitacora(parent.path, "Display_hilos")

    def run(self):
        """
        Funcion para ejecutar el hilo
        """
        while 1:
            try:
                if os.path.exists(self.estados):
                    estados = json.load(open(self.estados, 'r'))
                    self.signal.estados.emit(estados)
                    time.sleep(2)
            except Exception as e:
                self.bitacora.escribir("Hilo de arriba")
                self.bitacora.escribir(str(e))
                time.sleep(0.2)


class Manejador(QtCore.QThread):
    """
    Clase para crear el hilo que controla los cambios de pantalla
    """
    
    def __init__(self, parent = None):
        """
        parent: QObject Objeto al que pertenece la instancia
        """
        QtCore.QThread.__init__(self, parent)
        self.pantalla = parent.path + "/shared/display/pantalla.json"
        self.identidad = parent.documentoIdentidad
        self.signal = Senials()
        self.signal.pantalla.connect(parent.actualizarPantalla)
        self.signal.dineroIngreso.connect(parent.actualizarDinero)
        self.signal.recagaTarjeta.connect(parent.actualizarmontoRecarga)
        self.bitacora = Bitacora(parent.path, "Display_hilos")

    def run(self):
        """
        Funcion para ejecutar el hilo
        """
        if os.path.exists(self.identidad):
            ident = json.load(open(self.identidad, 'r'))
            if ident['activo']:
                if ident.get('idRutaDB') and ident.get('idOperador'):
                    json.dump("PRINCIPAL", open(self.pantalla, "w+"))
                    actual= "PRINCIPAL"
                    
                else:
                    json.dump("CAMBIAR_RUTA", open(self.pantalla, "w+"))
                    actual= "CAMBIAR_RUTA"
            else:
                json.dump("ESPERA", open(self.pantalla, 'w+'))
                actual= "ESPERA"
        else:
            self.bitacora.escribir('Espera')
            json.dump("ESPERA", open(self.pantalla, 'w+'))
            actual= "ESPERA"
        while 1:
            try:
                if os.path.exists(self.pantalla):
                    pantalla = json.load(open(self.pantalla,'r'))
                    self.bitacora.escribir("Pantalla ")
                    self.bitacora.escribir(str(pantalla))
                    if actual != pantalla:
                        self.bitacora.escribir("Emit Pantalla ")
                        self.signal.pantalla.emit(pantalla, actual)
                        actual = pantalla
                    elif actual == "DINERO_INGRESADO":
                        self.signal.dineroIngreso.emit()
                    elif actual == "RECARGA":
                        self.bitacora.escribir("Recarga emit ")
                        self.signal.recagaTarjeta.emit()
                    
                time.sleep(0.2)
            except Exception as e:
                self.bitacora.escribir("Hilo de abajo")
                self.bitacora.escribir(str(e))
                time.sleep(0.2)
