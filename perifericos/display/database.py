import sqlite3
import datetime
from bitacora import Bitacora

class Resumen:
    """
    La base de datos para el resumen de contabilidad

    """
    def __init__(self, path, url):
        """
        path: string Ruta del directorio raiz
        url: string Ruta donde se encuentra el archivo de base de datos
        """
        self.conection = sqlite3.connect(url)
        self.bitacora = Bitacora(path, "Database")

    def hacerSelect(self, query, multiple = False):
        """
        Funcion para obtener los valores de un select obteniendo un 0 en caso de que el valor sea None
        query: string Query que sera ejecutado en la base de datos
        multiple: boolean Bandera que indica si se espera un resultado de muchos registros o solo de uno, por default es False que indica un solo registro
        Retorna una lista con los valores obtenidos
        """
        try:
            cursor = self.conection.cursor()
            self.bitacora.escribir(query)
            cursor.execute(query)
            resultado = cursor.fetchall()
            if multiple:
                for x in resultado:
                    valores = [0 if i == None else i for i in x]
            else:
                valores = [0 if i == None else i for i in resultado[0]]
            self.bitacora.escribir(valores)
            return valores
        except:
            return [0,0]
                

    def obtenerResumen(self):
        """
        Funcion para obtener los valores de la base de datos
        Retorna un diccionario con los montos totales
        """
        try:
            query = "SELECT fecha_fin FROM cortes ORDER BY id DESC LIMIT 1"
            result = self.hacerSelect(query)
            fecha = result[0]
            
            query = "SELECT COUNT(id), SUM(monto) FROM validaciones WHERE fecha_hora > '" + fecha + "' AND (id_producto = '4331' OR id_producto = '4d31')"
            result= self.hacerSelect(query)
            retorno = {"validaciones": result}
            self.bitacora.escribir(retorno)
            
            query = "SELECT COUNT(id) FROM validaciones WHERE fecha_hora > '" + fecha + "' AND (id_producto = '4231' OR id_producto = '4232')"
            result = self.hacerSelect(query)
            retorno["bpds"] = result
            self.bitacora.escribir(retorno)
            
            query = "SELECT COUNT(id), SUM(monto) FROM ventas WHERE fecha_hora > '" + fecha + "' AND tipo = 'VENTA'"
            result = self.hacerSelect(query)
            retorno["ventas"] = result
            self.bitacora.escribir(retorno)
            
            query = "SELECT COUNT(id), SUM(monto) FROM ventas WHERE fecha_hora > '" + fecha + "' AND tipo = 'VENTA_INCOMPLETA'"
            result = self.hacerSelect(query)
            retorno["remanente"] = result
            self.bitacora.escribir(retorno)
            
            query = "SELECT COUNT(id), SUM(monto) FROM recargas WHERE fecha_hora > '" + fecha + "'" 
            result = self.hacerSelect(query)
            retorno["recargas"] = result
            
            self.bitacora.escribir(retorno)
            retorno["fecha_inicio"] = fecha
            self.bitacora.escribir(retorno)
            return retorno
        except Exception as e:
            self.bitacora.escribir("Error al consultar la base de datos: " + str(e))
            return {"error": "Error al consultar la base de datos"}

    def hacerCorte(self, valores):
        """
        Funcion para cerrar el corte con los valores dados
        valores: dict Diccionario con los valores totales que seran guardados
        """
        try:
            cursor = self.conection.cursor()
            query = f"INSERT INTO cortes (fecha_inicio, fecha_fin, eventos_validaciones, eventos_ventas, eventos_bpd, monto_validaciones, monto_ventas) VALUES ('{valores['fecha_inicio']}', '{datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d %H:%M:%S')}', {valores['validaciones'][0]}, {valores['ventas'][0]}, {valores['bpds'][0]}, {valores['validaciones'][1]}, {valores['ventas'][1]})"
            self.bitacora.escribir(query)
            cursor.execute(query)
            self.conection.commit()
        except Exception as e:
            self.bitacora.escribir("Error al escribir en la base de datos: " + str(e))
    
