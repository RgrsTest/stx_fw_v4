import os
from PySide2 import QtCore, QtWidgets, QtGui
from hilos import Reloj, Estados
import json

class BarraEstado(QtWidgets.QHBoxLayout):
    """
    Clase que crea un objeto de tipo QLayout para pintar la barra de estado
    """
    def __init__(self, path):
        """
        path: string Ruta donde se encuentra el directorio raiz
        """
        QtWidgets.QHBoxLayout.__init__(self)
        self.pantalla = path + "/shared/display/pantalla.json"
        self.setSpacing(0)
        self.path = path
        self.crearWidgets()
        reloj = Reloj(self)
        reloj.start()
        status = Estados(self)
        status.start()
    
    def crearWidgets(self):
        """
        Funcion para crear los widgets que van dentro de la barra
        """
        self.fechaHora()
        self.estados()

    def fechaHora(self):
        """
        Funcion que crea el widget de fecha y hora
        """
        self.timeInfoWdidget = QtWidgets.QWidget()
        self.timeInfoLayout = QtWidgets.QVBoxLayout()
        self.hora = QtWidgets.QLabel("00:00:00", alignment=QtCore.Qt.AlignCenter)
        self.fecha = QtWidgets.QLabel("0000-00-00", alignment=QtCore.Qt.AlignCenter)
        sans = QtGui.QFont("Open Sans", 20, QtGui.QFont.Bold)
        
        self.hora.setFont(sans)
        self.fecha.setFont(sans)

        self.timeInfoWdidget.setMinimumHeight(100)
        self.timeInfoWdidget.setMinimumWidth(200)
        self.timeInfoWdidget.setMaximumHeight(100)
        self.timeInfoWdidget.setMaximumWidth(200)
        
        self.timeInfoLayout.addWidget(self.hora)
        self.timeInfoLayout.addWidget(self.fecha)
        self.timeInfoLayout.setSpacing(0)


        self.timeInfoWdidget.setLayout(self.timeInfoLayout)
        self.timeInfoWdidget.setAutoFillBackground(True)
        self.timeInfoWdidget.setStyleSheet("background: #00838f; color: #fff;")
        self.addWidget(self.timeInfoWdidget)

    def estados(self):
        """
        Funcion que crea el widget de estados
        """
        #Crear widget y Layout
        self.estadoLayout = QtWidgets.QHBoxLayout()
        self.estadoWidget = QtWidgets.QWidget()

        #Crear QLabels para las imagenes
        self.gps = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
        self.changer = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
        self.modem = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
        self.validador = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
        self.printer = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
        
        #Crear Pixmaps
        self.gpsPixmaps = {
            0 : QtGui.QPixmap(self.path + "/media/img/gps/0.png"),
            1 : QtGui.QPixmap(self.path + "/media/img/gps/1.png"),
            2 : QtGui.QPixmap(self.path + "/media/img/gps/2.png")
        }
        self.changerPixmaps = {
            0 : QtGui.QPixmap(self.path + "/media/img/changer/0.png"),
            1 : QtGui.QPixmap(self.path + "/media/img/changer/1.png")
        }
        self.modemPixmaps = {
            0 : QtGui.QPixmap(self.path + "/media/img/modem/0.png"),
            1 : QtGui.QPixmap(self.path + "/media/img/modem/1.png"),
            2 : QtGui.QPixmap(self.path + "/media/img/modem/2.png")
        }
        self.validadorPixmaps = {
            0 : QtGui.QPixmap(self.path + "/media/img/validador/0.png"),
            1 : QtGui.QPixmap(self.path + "/media/img/validador/1.png")
        }
        self.printerPixmaps = {
            0 : QtGui.QPixmap(self.path + "/media/img/printer/0.png"),
            1 : QtGui.QPixmap(self.path + "/media/img/printer/1.png")
        }
        #Asignar los pixmaps a los labels
        self.gps.setPixmap(self.gpsPixmaps[1])
        self.changer.setPixmap(self.changerPixmaps[1])
        self.modem.setPixmap(self.modemPixmaps[1])
        self.validador.setPixmap(self.validadorPixmaps[1])
        self.printer.setPixmap(self.printerPixmaps[1])
        
        #Dando tamaño al layout
        self.estadoWidget.setMinimumWidth(350)
        self.estadoWidget.setMinimumHeight(100)
        self.estadoWidget.setMaximumWidth(350)
        self.estadoWidget.setMaximumHeight(100)

        #Agregando widgets al layout
        self.estadoLayout.addWidget(self.gps)
        self.estadoLayout.addWidget(self.changer)
        self.estadoLayout.addWidget(self.modem)
        self.estadoLayout.addWidget(self.validador)
        self.estadoLayout.addWidget(self.printer)
        self.estadoLayout.setSpacing(0)
        
        #Setear Layout al widget principal
        self.estadoWidget.setLayout(self.estadoLayout)
        self.estadoWidget.setAutoFillBackground(True)
        self.estadoWidget.setStyleSheet("background: #00838f")
        self.addWidget(self.estadoWidget)

        
        
    
    @QtCore.Slot(str)
    def actualizarTiempo(self, hora):
        """
        Slot para cambiar el tiempo
        hora: string Fecha y hora de la forma 'YYYY-MM-DD HH:mm:ss'
        """
        listaHora = hora.split(' ')
        self.hora.setText(listaHora[1])
        self.fecha.setText(listaHora[0])

    @QtCore.Slot(dict)
    def actualizarEstado(self, estados):
        """
        Slot para cambiar el status de cada dispositivo, tambien verifica si el equipo puede seguir operando
        estados: dict Diccionario con cada estado de cada dispositivo
        """
        try:
            self.gps.setPixmap(self.gpsPixmaps[estados["gps"]])
            self.changer.setPixmap(self.changerPixmaps[estados["changer"]])
            self.modem.setPixmap(self.modemPixmaps[estados["modem"]])
            self.printer.setPixmap(self.printerPixmaps[estados["printer"]])
            self.validador.setPixmap(self.validadorPixmaps[estados["validador"]])
            panta = json.load(open(self.pantalla, 'r'))
            if os.path.exists(self.path + '/shared/config/identidad.json'):
                if estados["sensor"] != 0:
                    json.dump("PUERTA_ABIERTA", open(self.pantalla, 'w+'))
                elif estados["changer"] != 1 or estados["validador"] != 1:
                    json.dump("FUERA_SERVICIO", open(self.pantalla, 'w+'))
                elif panta == "FUERA_SERVICIO" or  panta == "PUERTA_ABIERTA":
                    json.dump("PRINCIPAL", open(self.pantalla, 'w+'))
        except Exception as e:
            print(str(e)) 