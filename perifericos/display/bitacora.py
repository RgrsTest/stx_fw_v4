import os
import datetime

class Bitacora():
    def __init__(self, path, name):
        """
        Modulo de bitacorizacion de procesos
        name: string Nombre del archivo donde se guardaran las bitacoras
        """
        self.archivo = os.path.abspath(path + '/logs/'  + name + ".log")
    
    def escribir(self, mensaje):
        """
        Funcion para escribir un mensaje en el archivo de bitacora
        mensaje: Object Mensaje que sera escrito, puede ser un string o Numero
        """
        escritura = open(self.archivo, 'a+')
        fecha = datetime.datetime.now()
        men = fecha.strftime("%Y-%m-%d %H:%M:%S.%f")+ " "+ str(mensaje) +" \n"
        escritura.write(men)
        escritura.close()