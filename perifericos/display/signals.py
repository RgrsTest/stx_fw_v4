from PySide2 import QtCore

class Senials(QtCore.QObject):
    """
    Clase que contiene los signals que se emitiran por cada uno de los hilos
    """
    fecha = QtCore.Signal(str)
    estados = QtCore.Signal(dict)
    pantalla = QtCore.Signal(str, str)
    dineroIngreso = QtCore.Signal()
    recagaTarjeta = QtCore.Signal()
    