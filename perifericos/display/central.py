import datetime
import json
import os
from hilos import Manejador
from database import Resumen
from PySide2 import QtCore, QtWidgets, QtGui

import time, threading


class Central(QtWidgets.QVBoxLayout):
    """
    Clase que crea el Layout central
    """
    #Declaracion de widget de cada ventana
    principal = None
    venta = None
    ventaError = None
    dinero = None
    servicio = None
    puerta = None
    ingreso = None
    menu = None
    dispositivo = None
    unidad = None
    contabilidad = None
    estado = None
    chofer = None
    confirmarChofer = None
    ruta = None
    confirmarRuta = None

    colorFondo = "background: #fff"
    colorSuccess = "background: #303f9f; color: #fff;"
    colorCancel = "background: #8f0c00; color: #fff;"

    #Fuente
    sans = QtGui.QFont("Open Sans", 20, QtGui.QFont.Bold)

    def __init__(self, path, parent):
        """
        path: string Ruta del directorio raiz
        parent: QWidget Objeto de Qt al que pertenece 
        """
        #Inicializar la herencia
        QtWidgets.QVBoxLayout.__init__(self)
        self.parent = parent
        self.bitacora = parent.bitacora
        #Timer para salir a la pantalla principal
        self.timer =  QtCore.QTimer(self)
        self.timer.timeout.connect(lambda : self.llamarVentana("PRINCIPAL"))
        
        self.timerRecarga = threading.Timer(6,lambda : self.llamarVentana("REALIZANDO_RECARGA"))
       

        #Documentos de funcioanmiento
        self.documentoValidacion = path + "/shared/display/validacion.json"
        self.documentoDinero = path + "/shared/display/dinero.json"
        self.documentoConfig = path + "/config/changer_config.json"
        self.documentoIdentidad = path + "/shared/config/identidad.json"
        self.documentoPantalla = path + "/shared/display/pantalla.json"
        self.documentoTarjeta = path + "/shared/display/tarjeta.json"
        self.documentoRecarga = path + "/shared/display/recarga.json"
        self.documentoConfigVal = path + "/config/val_config.json"
        self.documentoAcceso = path + "/shared/display/lectura.json"
        self.documentoOperacion = path + "/shared/config/operacion.json"
        self.documentoMontoRecarga = path + "/shared/display/monto.json"
        self.documentoRutas = path + "/shared/config/rutas.json"
        self.documentoRutaSelect = path + "/shared/display/ruta.json"
        
        self.valorRecarga = 0
        
        #Declaraion de Funciones para pintar cada ventana
        self.ventanas = {
            "PRINCIPAL": self.pintarPrincipal,
            "VENTA": self.pintarVenta,
            "VALIDACION_OK": self.pintarValidacionOk,
            "VALIDACION_ERROR": self.pintarVentaError,
            "DINERO_INGRESADO": self.pintarDinero,
            "FUERA_SERVICIO": self.pintarServicio,
            "PUERTA_ABIERTA": self.pintarPuerta,
            "INGRESO_MENU": self.pintarIngreso,
            "MENU_CONFIGURACION": self.pintarMenu,
            "DATOS_DISPOSITIVO": self.pintarDispositivo,
            "DATOS_UNIDAD": self.pintarUnidad,
            "CONTABILIDAD": self.pintarContabilidad,
            "CAMBIAR_CHOFER": self.pintarChofer,
            "CONFIRMAR_CHOFER": self.pintarConfirmarChofer,
            "CAMBIAR_RUTA": self.pintarRuta,
            "CONFIRMAR_RUTA": self.pintarConfirmarRuta,
            "PRE_RECARGA": self.pintarPrerecarga,
            "RECARGA": self.pintarRecarga,
            "RECARGA_EXITOSA": self.pintarRecargaExitosa,
            "RECARGA_FALLIDA": self.pintarRecargaFallida,
            "REALIZANDO_RECARGA": self.pintarRealizandoRecarga,
            "ESPERA": self.pintarEspera,
            "CHOFER_INEXISTENTE": self.pintarChoferInexistente
        }

        #Guardamos los punteros a cada widget dentro de un diccionario pare generalizar una funcion de borrado
        self.eliminacion= {
            "PRINCIPAL": None,
            "VENTA": None,
            "VALIDACION_OK": None,
            "VALIDACION_ERROR": None,
            "DINERO_INGRESADO": None,
            "FUERA_SERVICIO": None,
            "PUERTA_ABIERTA": None,
            "INGRESO_MENU": None,
            "MENU_CONFIGURACION": None,
            "DATOS_DISPOSITIVO": None,
            "DATOS_UNIDAD": None,
            "CONTABILIDAD": None,
            "CAMBIAR_CHOFER": None,
            "CONFIRMAR_CHOFER": None,
            "CAMBIAR_RUTA": None,
            "CONFIRMAR_RUTA": None,
            "PRE_RECARGA": None,
            "RECARGA": None,
            "RECARGA_EXITOSA": None,
            "RECARGA_FALLIDA": None,
            "REALIZANDO_RECARGA": None,
            "ESPERA": None,
            "CHOFER_INEXISTENTE": None
        }
        
        #Guardamos el parametro del path
        self.path = path

        #Mandamos a llamar la pantalla principal
        if os.path.exists(self.documentoIdentidad):
            ident = json.load(open(self.documentoIdentidad, 'r'))
            if ident['activo']:
                if ident.get('idRutaDB') and ident.get('idOperador'):
                    
                    self.pintarPrincipal()
                    #self.pintarRuta()
                else:
                    
                    self.pintarRuta()
            else:
                self.pintarEspera()
        else:
            self.pintarEspera()
        
        #Iniciamos el proceso que estara buscando archivos y actualizando
        self.manejador = Manejador(self)
        self.manejador.start()

    #Funciones para Pintar cada una de las ventanas
    def pintarRealizandoRecarga(self):
        """
        Funcion para pintar la ventana de espera cuando se esta recargando
        """
        
       
        
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/wait.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("Recargando ....", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["REALIZANDO_RECARGA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["REALIZANDO_RECARGA"].setLayout(lay)
        self.eliminacion["REALIZANDO_RECARGA"].setAutoFillBackground(True)
        self.eliminacion["REALIZANDO_RECARGA"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["REALIZANDO_RECARGA"])
        
        self.timerRecarga.cancel()
        
       
        

    def pintarRecargaFallida(self):
        """
        Funcion para pintar la pantalla de recarga fallida
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/cancel.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("¡Recarga Fallida!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["RECARGA_FALLIDA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["RECARGA_FALLIDA"].setLayout(lay)
        self.eliminacion["RECARGA_FALLIDA"].setAutoFillBackground(True)
        self.eliminacion["RECARGA_FALLIDA"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["RECARGA_FALLIDA"])
        
        self.timer.start(2000)


    def pintarRecargaExitosa(self):
        """
        Funcion para pintar la pantalla de recarga exitosa
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/checked.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("¡Recarga Exitosa!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["RECARGA_EXITOSA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["RECARGA_EXITOSA"].setLayout(lay)
        self.eliminacion["RECARGA_EXITOSA"].setAutoFillBackground(True)
        self.eliminacion["RECARGA_EXITOSA"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["RECARGA_EXITOSA"])
        
        self.timer.start(2000)
        """
        self.timer.stop()
        self.timer.timeout.connect(lambda : self.llamarVentana("PRINCIPAL"))
        self.timer.start(5000)
        """

    def pintarPrerecarga(self):
        """
        Funcion para pintar la pantalla donde se inicia el proceso de recarga
        """
        lay = QtWidgets.QVBoxLayout()
        self.imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        self.imagen.setPixmap(QtGui.QPixmap(self.path + "/media/img/acerca-tarjeta.png"))
        lay.addWidget(self.imagen)
        self.cancelar = QtWidgets.QPushButton("Cancelar", font= self.sans)
        self.cancelar.setMinimumHeight(100)
        self.cancelar.setStyleSheet(self.colorCancel)
        self.cancelar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        self.cancelar.installEventFilter(self.parent)
        self.cancelar.clicked.connect(lambda: self.llamarVentana("PRINCIPAL"))
        lay.addWidget(self.cancelar)
        
        self.eliminacion["PRE_RECARGA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["PRE_RECARGA"].setLayout(lay)
        self.eliminacion["PRE_RECARGA"].setAutoFillBackground(True)
        self.eliminacion["PRE_RECARGA"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["PRE_RECARGA"])
        
       
       
        self.timer.start(7000)
        

    def pintarRecarga(self):
        """
        Funcion para pintar la pantalla donde se muestran los datos de recarga
        """
        try:
            
            self.valorRecarga = 0
            tar = json.load(open(self.documentoTarjeta, 'r'))
            os.remove(self.documentoTarjeta)
        
            lay = QtWidgets.QGridLayout()
            self.recargaTarjeta = QtWidgets.QLabel(tar["tarjeta"], alignment= QtCore.Qt.AlignCenter, font= self.sans)
            self.recargaSaldo = QtWidgets.QLabel(f"${(tar['saldo']/100):.2f}", alignment= QtCore.Qt.AlignCenter, font= self.sans)
            self.montoRecarga = QtWidgets.QLabel("$0.00",alignment= QtCore.Qt.AlignCenter, font= self.sans )
            lay.addWidget(QtWidgets.QLabel("Recarga de Saldo", alignment= QtCore.Qt.AlignCenter, font= self.sans), 0, 0, 1, 2)
            lay.addWidget(QtWidgets.QLabel("Tarjeta:", alignment= QtCore.Qt.AlignCenter, font= self.sans), 1, 0, 1, 2)
            lay.addWidget(self.recargaTarjeta, 2, 0, 1, 2)
            lay.addWidget(QtWidgets.QLabel("Saldo Actual:", alignment= QtCore.Qt.AlignCenter, font= self.sans), 3, 0)
            lay.addWidget(self.recargaSaldo, 3, 1)
            lay.addWidget(QtWidgets.QLabel("Monto Recarga:", alignment= QtCore.Qt.AlignCenter, font= self.sans), 4, 0)
            lay.addWidget(self.montoRecarga, 4, 1)
            lay.addWidget(QtWidgets.QLabel("Ingrese la cantidad que", alignment= QtCore.Qt.AlignCenter, font= self.sans), 5, 0, 1, 2)
            lay.addWidget(QtWidgets.QLabel("desea recargar", alignment= QtCore.Qt.AlignCenter, font= self.sans), 6, 0, 1, 2)
            

            self.abonar = QtWidgets.QPushButton("Recargar", font= self.sans)
            self.abonar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            self.abonar.installEventFilter(self.parent)
            self.abonar.clicked.connect(lambda: self.llamarVentana("REALIZANDO_RECARGA"))
            self.abonar.setMinimumHeight(100)
            self.abonar.setStyleSheet(self.colorSuccess)
            self.abonar.setEnabled(False)
            
            
            self.salir = QtWidgets.QPushButton("Salir", font= self.sans)
            self.salir.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            self.salir.installEventFilter(self.parent)
            self.salir.clicked.connect(lambda: self.llamarVentana("PRINCIPAL"))
            self.salir.setMinimumHeight(100)
            self.salir.setStyleSheet(self.colorCancel)
            

            lay.addWidget(self.abonar, 7, 0, 1, 2)
            lay.addWidget(self.salir, 8,0,1,2)
            
            self.eliminacion["RECARGA"] = QtWidgets.QWidget()
            lay.setSpacing(0)
            
            self.eliminacion["RECARGA"].setLayout(lay)
            self.eliminacion["RECARGA"].setAutoFillBackground(True)
            self.eliminacion["RECARGA"].setStyleSheet(self.colorFondo)
            self.addWidget(self.eliminacion["RECARGA"])
            
        
            
            
            
            
            
           
            self.timer.start(7000)
            
            
                
            
            
            
     
        except Exception as e:
            self.bitacora.escribir(e)
            self.pintarRecarga()
        
    def pintarConfirmarRuta(self):
        """
        Funcion para pintar la pantalla que confirma el camnbio de la ruta
        """
        lay = QtWidgets.QVBoxLayout()
        leyenda = QtWidgets.QLabel("Se actualizo la ruta!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        
        lay.addWidget(leyenda)
        
        self.eliminacion["CONFIRMAR_RUTA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["CONFIRMAR_RUTA"].setAutoFillBackground(True)
        self.eliminacion["CONFIRMAR_RUTA"].setStyleSheet(self.colorFondo)
        self.eliminacion["CONFIRMAR_RUTA"].setLayout(lay)
        self.addWidget(self.eliminacion["CONFIRMAR_RUTA"])

    def pintarRuta(self, noPrimera = True):
        """
        Funcion para pintar la pantalla donde se selecciona la ruta
        noPrimera: boolean Indica si es la primera vez que se va a setear la ruta
        """
        try:
            lay = QtWidgets.QVBoxLayout()
            self.rutas = json.load(open(self.documentoRutas))
            texto = QtWidgets.QLabel("Selecciona la ruta actual", alignment= QtCore.Qt.AlignCenter, font= self.sans)
            lay.addWidget(texto)
            wid = QtWidgets.QWidget()
            capa = QtWidgets.QVBoxLayout()
            self.grupo = QtWidgets.QButtonGroup()
            scroll = QtWidgets.QScrollArea()
            scroll.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            scroll.installEventFilter(self.parent)
            self.bitacora.escribir(self.rutas)
            for i in self.rutas:
                buton = QtWidgets.QRadioButton(i["payload"]["label"], font = self.sans)
                buton.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
                buton.installEventFilter(self.parent)
                buton.setMinimumHeight(100)
                self.grupo.addButton(buton, self.rutas.index(i))
                capa.addWidget(buton)
            wid.setLayout(capa)
            wid.setMinimumHeight(400)
            scroll.setWidget(wid)
            scroll.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            scroll.installEventFilter(self.parent)
            scroll.grabGesture(QtCore.Qt.SwipeGesture)
            lay.addWidget(scroll)
            aceptar = QtWidgets.QPushButton("Aceptar", font= self.sans)
            aceptar.setStyleSheet(self.colorSuccess)
            aceptar.setMinimumHeight(100)
            aceptar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            aceptar.installEventFilter(self.parent)
            aceptar.clicked.connect(self.valorSeleccionado)
            
            cancelar = QtWidgets.QPushButton("Cancelar", font= self.sans)
            cancelar.setMinimumHeight(100)
            cancelar.setStyleSheet(self.colorCancel)
            cancelar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            cancelar.installEventFilter(self.parent)
            cancelar.clicked.connect(lambda: self.llamarVentana("MENU_CONFIGURACION"))
            
            lay.addWidget(aceptar)
            lay.addWidget(cancelar)
            
            self.eliminacion["CAMBIAR_RUTA"] = QtWidgets.QWidget()
            lay.setSpacing(0)
            self.eliminacion["CAMBIAR_RUTA"].setAutoFillBackground(True)
            self.eliminacion["CAMBIAR_RUTA"].setStyleSheet(self.colorFondo)
            self.eliminacion["CAMBIAR_RUTA"].setLayout(lay)
            self.addWidget(self.eliminacion["CAMBIAR_RUTA"])
            ident = json.load(open(self.documentoIdentidad, 'r'))
            if ident.get('idRutaDB') and ident.get('idOperador'):
                self.timer.start(30000)
        except Exception as e:
            self.bitacora.escribir("Error pintando Rutas: " + str(e))


    def pintarConfirmarChofer(self):
        """
        Funcion para pintar la pantalla donde se confirma que se actualizo el chofer
        """
        lay = QtWidgets.QVBoxLayout()
        leyenda = QtWidgets.QLabel("Se actualizo el chofer!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(leyenda)

        self.eliminacion["CONFIRMAR_CHOFER"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["CONFIRMAR_CHOFER"].setAutoFillBackground(True)
        self.eliminacion["CONFIRMAR_CHOFER"].setStyleSheet(self.colorFondo)
        self.eliminacion["CONFIRMAR_CHOFER"].setLayout(lay)
        self.addWidget(self.eliminacion["CONFIRMAR_CHOFER"])
        self.timer.start(3000)

    def pintarChofer(self):
        """
        Funcion para pintar la pantalla de cambio de chofer
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/acerca-tarjeta.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        texto = QtWidgets.QLabel("Acerca para cambiar chofer", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)
        
        self.eliminacion["CAMBIAR_CHOFER"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["CAMBIAR_CHOFER"].setAutoFillBackground(True)
        self.eliminacion["CAMBIAR_CHOFER"].setStyleSheet(self.colorFondo)
        self.eliminacion["CAMBIAR_CHOFER"].setLayout(lay)
        self.addWidget(self.eliminacion["CAMBIAR_CHOFER"])
        ident = json.load(open(self.documentoIdentidad, 'r'))
        if ident.get('idOperador'):
            self.timer.start(10000)


    def pintarContabilidad(self):
        """
        Funcion para pintar la pantalla donde se muestran los datos recolectados
        """
        try:
            resumen = Resumen(self.path, self.path + '/database/smartbus.db')
            valores = resumen.obtenerResumen()
            lay = QtWidgets.QGridLayout()
            lay.addWidget(QtWidgets.QLabel(f"Datos del {valores['fecha_inicio']}", alignment= QtCore.Qt.AlignCenter, font= self.sans), 0, 0, 1, 2)
            lay.addWidget(QtWidgets.QLabel(f" al {datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d %H:%M:%S')}", alignment= QtCore.Qt.AlignCenter, font= self.sans), 1, 0, 1, 2)
            lay.addWidget(QtWidgets.QLabel(f"Boletos Vendidos:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 2, 0)
            lay.addWidget(QtWidgets.QLabel(f"{valores['ventas'][0]}", alignment= QtCore.Qt.AlignRight, font= self.sans), 2, 1)
            lay.addWidget(QtWidgets.QLabel(f"Validaciones: ", alignment= QtCore.Qt.AlignLeft, font= self.sans), 3 ,0)
            lay.addWidget(QtWidgets.QLabel(f"{valores['validaciones'][0]}", alignment= QtCore.Qt.AlignRight, font= self.sans), 3 ,1)
            lay.addWidget(QtWidgets.QLabel(f"BPD:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 4, 0)
            lay.addWidget(QtWidgets.QLabel(f"{valores['bpds'][0]}", alignment= QtCore.Qt.AlignRight, font= self.sans), 4, 1)
            lay.addWidget(QtWidgets.QLabel(f"Recargas:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 5, 0)
            lay.addWidget(QtWidgets.QLabel(f"{valores['recargas'][0]}", alignment= QtCore.Qt.AlignRight, font= self.sans), 5, 1)
            lay.addWidget(QtWidgets.QLabel(f"Ventas Efectivo:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 6, 0)
            lay.addWidget(QtWidgets.QLabel(f"${(valores['ventas'][1]/100):.2f}", alignment= QtCore.Qt.AlignRight, font= self.sans), 6, 1)
            lay.addWidget(QtWidgets.QLabel(f"Dinero Remanente:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 7, 0)
            lay.addWidget(QtWidgets.QLabel(f"${(valores['remanente'][1]/100):.2f}", alignment= QtCore.Qt.AlignRight, font= self.sans), 7, 1)
            lay.addWidget(QtWidgets.QLabel(f"Dinero Validaciones:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 8, 0)
            lay.addWidget(QtWidgets.QLabel(f"${(valores['validaciones'][1]/100):.2f}", alignment= QtCore.Qt.AlignRight, font= self.sans), 8, 1)
            lay.addWidget(QtWidgets.QLabel(f"Monto de recargas:", alignment= QtCore.Qt.AlignLeft, font= self.sans), 9, 0)
            lay.addWidget(QtWidgets.QLabel(f"${(valores['recargas'][1]/100):.2f}", alignment= QtCore.Qt.AlignRight, font= self.sans), 9, 1)
            corte = QtWidgets.QPushButton("Hacer Corte", font = self.sans)
            cancel = QtWidgets.QPushButton("Salir", font = self.sans)

            corte.setMinimumHeight(100)
            corte.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            corte.installEventFilter(self.parent)
            corte.setStyleSheet(self.colorSuccess)
            corte.clicked.connect(lambda: self.hacerCorte(valores))
            lay.addWidget(corte, 10, 0, 1, 2)

            cancel.setMinimumHeight(100)
            cancel.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            cancel.installEventFilter(self.parent)
            cancel.setStyleSheet(self.colorCancel)
            cancel.clicked.connect(lambda: self.llamarVentana("PRINCIPAL"))
            lay.addWidget(cancel, 11, 0, 1, 2)

            self.eliminacion["CONTABILIDAD"] = QtWidgets.QWidget()
            self.eliminacion["CONTABILIDAD"].setLayout(lay)
            self.eliminacion["CONTABILIDAD"].setAutoFillBackground(True)
            self.eliminacion["CONTABILIDAD"].setStyleSheet(self.colorFondo)
            self.addWidget(self.eliminacion["CONTABILIDAD"])
            self.timer.start(30000)
        except Exception as e:
            self.bitacora.escribir("Error pintando contabilidad: " + str(e))


    def pintarUnidad(self):
        """
        Funcion para pintar la pantalla que muestra la informacion de la unidad
        """
        try:
            datos = json.load(open(self.documentoIdentidad, 'r'))
        except Exception as e:
            datos = {
                "idTransporte": "",
                "chofer": "",
                "ruta": ""
            }
        
        lay = QtWidgets.QVBoxLayout()
        unidad = QtWidgets.QLabel(f"Unidad: {datos['idTransporte']}", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        chofer = QtWidgets.QLabel(f"Conductor: {datos['chofer']}", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        ruta = QtWidgets.QLabel(f"Ruta: {datos['ruta']}", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        
        lay.addWidget(unidad)
        lay.addWidget(chofer)
        lay.addWidget(ruta)

        cambiarChofer = QtWidgets.QPushButton("Cambiar Chofer", font= self.sans)
        cambiarChofer.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        cambiarChofer.installEventFilter(self.parent)
        cambiarChofer.clicked.connect(lambda: self.llamarVentana("CAMBIAR_CHOFER"))
        cambiarChofer.setMinimumHeight(100)
        cambiarChofer.setStyleSheet(self.colorSuccess)
        lay.addWidget(cambiarChofer)

        cambiarRuta = QtWidgets.QPushButton("Cambiar Ruta", font= self.sans)
        cambiarRuta.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        cambiarRuta.installEventFilter(self.parent)
        cambiarRuta.clicked.connect(lambda: self.llamarVentana("CAMBIAR_RUTA"))
        cambiarRuta.setMinimumHeight(100)
        cambiarRuta.setStyleSheet(self.colorSuccess)
        lay.addWidget(cambiarRuta)

        boton = QtWidgets.QPushButton("Salir", font= self.sans)
        boton.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        boton.installEventFilter(self.parent)
        boton.clicked.connect(lambda: self.llamarVentana("PRINCIPAL"))
        boton.setMinimumHeight(100)
        boton.setStyleSheet(self.colorCancel)
        lay.addWidget(boton)

        self.eliminacion["DATOS_UNIDAD"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["DATOS_UNIDAD"].setLayout(lay)
        self.eliminacion["DATOS_UNIDAD"].setAutoFillBackground(True)
        self.eliminacion["DATOS_UNIDAD"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["DATOS_UNIDAD"])
        self.timer.start(10000)

    def pintarDispositivo(self):
        """
        Funcion para pintar la pantalla que muestra la informacion del dispositivo
        """
        lines = 3
        lay = QtWidgets.QGridLayout()
        identidad = json.load(open(self.documentoIdentidad))
        lay.addWidget(QtWidgets.QLabel("Version:", alignment= QtCore.Qt.AlignCenter, font= self.sans), 0, 0)
        lay.addWidget(QtWidgets.QLabel("v1.0", alignment= QtCore.Qt.AlignCenter, font= self.sans), 0, 1)
        lay.addWidget(QtWidgets.QLabel("Id de SAM:", alignment= QtCore.Qt.AlignCenter, font= self.sans), 1, 0)
        lay.addWidget(QtWidgets.QLabel(identidad["idSAM"], alignment= QtCore.Qt.AlignCenter, font= self.sans), 1,1)
        lay.addWidget(QtWidgets.QLabel("Contadores", alignment= QtCore.Qt.AlignCenter, font= self.sans), 2, 0, 1, 2)
        for i in identidad['contadores']:
            lay.addWidget(QtWidgets.QLabel(f"Llave {i['llave']} :", alignment= QtCore.Qt.AlignCenter, font= self.sans), lines, 0)
            lay.addWidget(QtWidgets.QLabel(i["contador"], alignment= QtCore.Qt.AlignCenter, font= self.sans), lines,1)
            lines = lines + 1
        actualizar = QtWidgets.QPushButton("Buscar Actualizaciones", font= self.sans)
        cancelar = QtWidgets.QPushButton("Cancelar", font= self.sans)

        actualizar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        actualizar.installEventFilter(self.parent)
        actualizar.setMinimumHeight(100)
        actualizar.setStyleSheet(self.colorSuccess)
        
        cancelar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        cancelar.installEventFilter(self.parent)
        cancelar.setMinimumHeight(100)
        cancelar.setStyleSheet(self.colorCancel)
        cancelar.clicked.connect(lambda: self.llamarVentana("MENU_CONFIGURACION"))

        lay.addWidget(actualizar, lines, 0, 1, 2)
        lay.addWidget(cancelar, lines + 1, 0, 1, 2)

        actualizar.setMinimumHeight(100)
        actualizar.setStyleSheet(self.colorSuccess)
        
        self.eliminacion["DATOS_DISPOSITIVO"] = QtWidgets.QWidget()
        self.eliminacion["DATOS_DISPOSITIVO"].setLayout(lay)
        self.eliminacion["DATOS_DISPOSITIVO"].setAutoFillBackground(True)
        self.eliminacion["DATOS_DISPOSITIVO"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["DATOS_DISPOSITIVO"])
        self.timer.start(10000)
        

    def pintarMenu(self):
        """
        Funcion para pintar la pantalla del menu de datos
        """
        lay = QtWidgets.QVBoxLayout()
        unidad = QtWidgets.QPushButton("Datos de Unidad", font= self.sans)
        dispositivo = QtWidgets.QPushButton("Datos de Dispositivo", font= self.sans)
        contabilidad = QtWidgets.QPushButton("Contabilidad", font= self.sans)
        estado = QtWidgets.QPushButton("Estado del Dispositivo", font= self.sans)

        unidad.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        dispositivo.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        contabilidad.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        estado.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        
        unidad.installEventFilter(self.parent)
        dispositivo.installEventFilter(self.parent)
        contabilidad.installEventFilter(self.parent)
        estado.installEventFilter(self.parent)

        unidad.clicked.connect(lambda: self.llamarVentana("DATOS_UNIDAD"))
        dispositivo.clicked.connect(lambda: self.llamarVentana("DATOS_DISPOSITIVO"))
        contabilidad.clicked.connect(lambda: self.llamarVentana("CONTABILIDAD"))
        
        lay.addWidget(unidad)
        lay.addWidget(dispositivo)
        lay.addWidget(contabilidad)
        #lay.addWidget(estado)

        unidad.setMinimumHeight(100)
        dispositivo.setMinimumHeight(100)
        contabilidad.setMinimumHeight(100)
        estado.setMinimumHeight(100)

        unidad.setStyleSheet(self.colorSuccess)
        dispositivo.setStyleSheet(self.colorSuccess)
        contabilidad.setStyleSheet(self.colorSuccess)
        estado.setStyleSheet(self.colorSuccess)

        self.eliminacion["MENU_CONFIGURACION"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["MENU_CONFIGURACION"].setAutoFillBackground(True)
        self.eliminacion["MENU_CONFIGURACION"].setStyleSheet(self.colorFondo)
        self.eliminacion["MENU_CONFIGURACION"].setLayout(lay)
        self.addWidget(self.eliminacion["MENU_CONFIGURACION"])
        self.timer.start(10000)

    def pintarIngreso(self):
        """
        Funcion para pintar el ingreso a las configuraciones
        """
        lay = QtWidgets.QVBoxLayout()
        
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/acerca-tarjeta.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        
        lay.addWidget(imagen)
        
        boton = QtWidgets.QPushButton("Cancelar", font= self.sans)
        boton.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        boton.installEventFilter(self.parent)
        boton.clicked.connect(lambda: self.llamarVentana("PRINCIPAL"))

        boton.setMinimumHeight(100)
        boton.setStyleSheet(self.colorCancel)
        
        lay.addWidget(boton)
        self.eliminacion["INGRESO_MENU"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["INGRESO_MENU"].setAutoFillBackground(True)
        self.eliminacion["INGRESO_MENU"].setStyleSheet(self.colorFondo)
        self.eliminacion["INGRESO_MENU"].setLayout(lay)
        self.addWidget(self.eliminacion["INGRESO_MENU"])
        self.timer.start(10000)

    def pintarPuerta(self):
        """
        Funcion para pintar la alerta de puerta abierta
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/warning.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("¡Puerta Abierta!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["PUERTA_ABIERTA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["PUERTA_ABIERTA"].setAutoFillBackground(True)
        self.eliminacion["PUERTA_ABIERTA"].setStyleSheet(self.colorFondo)
        self.eliminacion["PUERTA_ABIERTA"].setLayout(lay)
        self.addWidget(self.eliminacion["PUERTA_ABIERTA"])

    def pintarChoferInexistente(self):
        """
        Funcion para pintar alerta de chofer inexistente
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/cancel.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("Chofer no existe!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["CHOFER_INEXISTENTE"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["CHOFER_INEXISTENTE"].setAutoFillBackground(True)
        self.eliminacion["CHOFER_INEXISTENTE"].setStyleSheet(self.colorFondo)
        self.eliminacion["CHOFER_INEXISTENTE"].setLayout(lay)
        self.addWidget(self.eliminacion["CHOFER_INEXISTENTE"])

    
    def pintarEspera(self):
        """
        Funcion para pintar la alerta de bloqueo
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/wait.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("Configurando...", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["ESPERA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["ESPERA"].setAutoFillBackground(True)
        self.eliminacion["ESPERA"].setStyleSheet(self.colorFondo)
        self.eliminacion["ESPERA"].setLayout(lay)
        self.addWidget(self.eliminacion["ESPERA"])
        
   
        self.timer.start(2000)

    def pintarServicio(self):
        """
        Funcion para pintar la alerta de fuera de servicio
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/warning.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("¡Fuera de Servicio!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)

        self.eliminacion["FUERA_SERVICIO"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["FUERA_SERVICIO"].setAutoFillBackground(True)
        self.eliminacion["FUERA_SERVICIO"].setStyleSheet(self.colorFondo)
        self.eliminacion["FUERA_SERVICIO"].setLayout(lay)
        self.addWidget(self.eliminacion["FUERA_SERVICIO"])


    def pintarDinero(self):
        """
        Funcion para pintar el ingreso de dinero en la alcancia
        """
        self.eliminacion["DINERO_INGRESADO"]
        try:
            dinero = json.load(open(self.documentoDinero, 'r'))
            tarifa = json.load(open(self.documentoConfig, 'r'))
            cantidades = {
            "ingresado": dinero,
            "tarifa": tarifa["tarifa"]
            }
        except Exception as e:
            cantidades = {
            "ingresado": 0,
            "tarifa": 0
            }
        ingresado = cantidades["ingresado"] /100
        restante = (cantidades["tarifa"] - cantidades["ingresado"]) / 100
        self.dineroIngresado = QtWidgets.QLabel("$" + str(ingresado), alignment= QtCore.Qt.AlignCenter, font= self.sans)
        self.dineroRestante = QtWidgets.QLabel("$" + str(restante), alignment= QtCore.Qt.AlignCenter, font= self.sans)
        
        lay = QtWidgets.QGridLayout()
        lay.addWidget(QtWidgets.QLabel("Dinero Ingresado:", alignment= QtCore.Qt.AlignCenter, font= self.sans),0,0)
        lay.addWidget(self.dineroIngresado, 0,1)
        lay.addWidget(QtWidgets.QLabel("Dinero Faltante:", alignment= QtCore.Qt.AlignCenter, font= self.sans),1,0)
        lay.addWidget(self.dineroRestante)
        lay.addWidget(QtWidgets.QLabel("Ingrese Monedas", alignment= QtCore.Qt.AlignCenter, font= self.sans),2,0, 1, 2)
        
        espacing = QtWidgets.QLabel()
        espacing.setMinimumHeight(300)
        lay.addWidget(espacing, 3, 0, 1, 2)
        
        self.eliminacion["DINERO_INGRESADO"] = QtWidgets.QWidget()
        capa = QtWidgets.QVBoxLayout()
        capa.addLayout(lay)
        capa.setSpacing(0)
        self.eliminacion["DINERO_INGRESADO"].setLayout(capa)
        self.eliminacion["DINERO_INGRESADO"].setAutoFillBackground(True)
        self.eliminacion["DINERO_INGRESADO"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["DINERO_INGRESADO"])

    def pintarVentaError(self):
        
        """
        Funcion para pintar un error en la validacion de la tarjeta
        """
      
        
        try:
            
            if os.path.exists(self.documentoConfigVal):
                errores = json.load(open(self.documentoConfigVal, 'r'))
                errores = errores["returnCodesCompra"]
                #self.bitacora.escribir(errores)
            if os.path.exists(self.documentoValidacion):
                validacion =  json.load(open(self.documentoValidacion, 'r'))
                #self.bitacora.escribir(validacion)
                if validacion["codigo"] == "CUSTOM_ERROR":
                    error = validacion["leyenda"]
                else:
                    error = errores[validacion["codigo"]]["leyenda"]
                self.bitacora.escribir(error)
                #os.remove(self.documentoValidacion)
        except Exception as e:
            self.bitacora.escribir(e)
            error = "ERROR DESCONOCIDO"
            os.remove(self.documentoValidacion)
        
        
        try:        
            lay = QtWidgets.QVBoxLayout()
            self.bitacora.escribir(error)
            pixmapImg = QtGui.QPixmap(self.path + "/media/img/cancel.png")
            imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
            imagen.setPixmap(pixmapImg)
            self.venta = QtWidgets.QWidget()
            texto = QtWidgets.QLabel(str(error), alignment= QtCore.Qt.AlignCenter, font= self.sans)
            lay.addWidget(imagen)
            lay.addWidget(texto) 
        
            self.eliminacion["VALIDACION_ERROR"] = QtWidgets.QWidget()
            lay.setSpacing(0)
            self.eliminacion["VALIDACION_ERROR"].setLayout(lay)
            self.eliminacion["VALIDACION_ERROR"].setAutoFillBackground(True)
            self.eliminacion["VALIDACION_ERROR"].setStyleSheet(self.colorFondo)
            self.addWidget(self.eliminacion["VALIDACION_ERROR"])
        except Excepion as e:
            self.bitacora.escribir(e)
        
       
        
       
      
    def pintarValidacionOk(self):
        """
        Funcion para pintar la validacion exitosa de la tarjeta
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/checked.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("¡Gracias por su Pago!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)
        
        
       
        
        
        
        
        
        leyendaTarjeta = "Prueba"
        

        if os.path.exists(self.documentoValidacion):
            validacion =  json.load(open(self.documentoValidacion, 'r'))
            
            try:
         
               leyendaTarjeta = validacion["leyenda"]
               
            except Exception as e:
               self.bitacora.escribir(e)
               
            card_mensaje = QtWidgets.QLabel(leyendaTarjeta, alignment= QtCore.Qt.AlignCenter, font= self.sans)
            lay.addWidget(card_mensaje)
            
            os.remove(self.documentoValidacion)            
            operacion = json.load(open(self.documentoOperacion, 'r'))
            if "BPD" in operacion["productos"][validacion["eventos"][-1]["idProducto"]]["productoNombre"]:
                saldo = validacion["eventos"][-1]["saldoFinal"]
                lay.addWidget(QtWidgets.QLabel(f"Saldo: {saldo} Pasajes", alignment= QtCore.Qt.AlignCenter, font= self.sans))
            else:
                saldo = (validacion["eventos"][-1]["saldoFinal"])/100
                lay.addWidget(QtWidgets.QLabel(f"Saldo: ${saldo:.2f}", alignment= QtCore.Qt.AlignCenter, font= self.sans))
        
        self.eliminacion["VALIDACION_OK"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["VALIDACION_OK"].setLayout(lay)
        self.eliminacion["VALIDACION_OK"].setAutoFillBackground(True)
        self.eliminacion["VALIDACION_OK"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["VALIDACION_OK"])


    def pintarPrincipal(self):
        """
        Funcion para pintar la pantalla principal
        """
        
        try:
            
            self.timer.stop()
            self.timer.timeout.connect(lambda : self.llamarVentana("PRINCIPAL"))
            
            
            
            
            identidad = json.load(open(self.documentoIdentidad, "r"))
            tarifa = json.load(open(self.documentoConfig, "r"))
            lay = QtWidgets.QGridLayout()
            imag = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
            self.eliminacion["PRINCIPAL"] = QtWidgets.QWidget()
            pixmapImg = QtGui.QPixmap(self.path + "/media/img/ingrese-monedas.png")
            imag.setPixmap(pixmapImg)
            imag.setMinimumHeight(700)
            self.recargar = QtWidgets.QPushButton("Recargar",  font=self.sans)
            self.recargar.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
            self.recargar.installEventFilter(self.parent)
            lay.addWidget(QtWidgets.QLabel(identidad["idTransporte"], alignment=QtCore.Qt.AlignCenter, font=self.sans),0,0)
            lay.addWidget(QtWidgets.QLabel("Ruta: " + identidad["ruta"], alignment=QtCore.Qt.AlignCenter, font= self.sans), 0, 1)
            lay.addWidget(QtWidgets.QLabel("¡Bienvenido a Bordo!", alignment=QtCore.Qt.AlignCenter, font= self.sans), 1, 0, 1, 2)
            lay.addWidget(QtWidgets.QLabel(f"Tarifa: ${(tarifa['tarifa']/100):.2f}", alignment=QtCore.Qt.AlignCenter, font= self.sans), 2, 0, 1, 2)
            lay.addWidget(imag,4,0,2,2)
            
            self.recargar.clicked.connect(lambda: self.llamarVentana("PRE_RECARGA"))
            self.recargar.setMinimumHeight(100)
            self.recargar.setStyleSheet(self.colorSuccess)
            lay.addWidget(self.recargar, 5, 0, 1, 2)
            lay.setSpacing(0)
            self.eliminacion["PRINCIPAL"].setLayout(lay)
            self.eliminacion["PRINCIPAL"].setAutoFillBackground(True)
            self.eliminacion["PRINCIPAL"].setStyleSheet(self.colorFondo)
            self.addWidget(self.eliminacion["PRINCIPAL"])
        except Exception as e:
            self.bitacora.escribir(e)
            json.dump("ESPERA", open(self.documentoPantalla, 'w+'))

    def pintarVenta(self):
        """
        Funcion para pintar una venta en efectivo
        """
        lay = QtWidgets.QVBoxLayout()
        pixmapImg = QtGui.QPixmap(self.path + "/media/img/checked.png")
        imagen = QtWidgets.QLabel(alignment= QtCore.Qt.AlignCenter)
        imagen.setPixmap(pixmapImg)
        self.venta = QtWidgets.QWidget()
        texto = QtWidgets.QLabel("¡Gracias por su Pago!", alignment= QtCore.Qt.AlignCenter, font= self.sans)
        lay.addWidget(imagen)
        lay.addWidget(texto)
        
        self.eliminacion["VENTA"] = QtWidgets.QWidget()
        lay.setSpacing(0)
        self.eliminacion["VENTA"].setLayout(lay)
        self.eliminacion["VENTA"].setAutoFillBackground(True)
        self.eliminacion["VENTA"].setStyleSheet(self.colorFondo)
        self.addWidget(self.eliminacion["VENTA"])


    def borrar(self, puntero):
        """
        Funcion para borrar la pantalla dada
        puntero: string Nombre de la pantalla a borrar
        """
        puntero.hide()
        try:
            self.timer.stop()
        except Exception as e:
            self.bitacora.escribir("Error al intentar borrar la pantalla")
            self.bitacora.escribir(e)
    
    @QtCore.Slot(dict)
    def hacerCorte(self, valores):
        """
        Funcion para grabar un registro de los datos actuales de ventas
        valores: dict Diccionario con los valores que seran grabados
        """
        try:
            resumen = Resumen(self.path, self.path + '/database/smartbus.db')
            resumen.hacerCorte(valores)
            self.borrar(self.eliminacion["CONTABILIDAD"])
            self.pintarContabilidad()
        except Exception as e:
            self.bitacora.escribir("Error haciendo el corte: " + str(e))
    
    @QtCore.Slot(str)
    def llamarVentana(self, ventana):
        """
        Funcion para escribir en el archivo de pantalla la ventana que se desea pintar
        ventana: string Nombre de la ventana a pintar
        """
        json.dump(ventana, open(self.documentoPantalla, 'w+'))
        

    @QtCore.Slot()
    def valorSeleccionado(self, noPrimera = True):
        """
        Funcion para obtener y guardar el valor de la ruta que se selecciono
        """
        try:
            index = self.grupo.checkedId()
            with open(self.documentoRutaSelect, 'w+') as file:
                json.dump(self.rutas[index], file)
            
                json.dump("CONFIRMAR_RUTA", open(self.documentoPantalla, 'w+'))
        except Exception as e:
            self.bitacora.escribir("Error: " + str(e))
            

    @QtCore.Slot(str, str)
    def actualizarPantalla(self, ventana, anterior):
        """
        Funcion para cambiar de pantalla
        ventana: string Nombre de la nueva ventana
        anterior: string Nombre de la ventana anterior
        """
        self.bitacora.escribir(str(ventana) +"\t" + str(anterior))
        self.borrar(self.eliminacion[anterior])
        self.ventanas[ventana]()

    @QtCore.Slot()
    def actualizarDinero(self):
        """
        Funcion para incrementar el monto de dinero ingresado en la alcancia
        """
        try:
            dinero = json.load(open(self.documentoDinero, 'r'))
            tarifa = json.load(open(self.documentoConfig, 'r'))
            cantidades = {
            "ingresado": dinero,
            "tarifa": tarifa["tarifa"]
            }
        except Exception as e:
            cantidades = {
            "ingresado": 0,
            "tarifa": 0
            }
        ingresado = cantidades["ingresado"] /100
        restante = (cantidades["tarifa"] - cantidades["ingresado"]) / 100
        
        self.dineroIngresado.setText("$" + str(ingresado))
        self.dineroRestante.setText("$" + str(restante))

    @QtCore.Slot()
    def actualizarTarjetaRecarga(self):
        """
        Funcion para obtener la informacion de la tarjeta que sera recargada
        """
        if os.path.exist(self.documentoTarjeta):
            try:
                tar = json.load(open(self.documentoTarjeta, 'r'))
                os.remove(self.documentoTarjeta)
            except Exception as e:
                tar = {"tarjeta": "", "saldo": 0}
                self.bitacora.escribir(e)

            self.recargaTarjeta.setText(tar["tarjeta"])
            self.recargaSaldo.setText(f"${(tar['saldo']/100):.2f}")
            

    @QtCore.Slot()
    def actualizarmontoRecarga(self):
        """
        Funcion para incrementar el valor del monto ingresado para la recarga
        """
        self.bitacora.escribir("Iniciando actualizacion")
        if os.path.exists(self.documentoMontoRecarga) and os.stat(self.documentoMontoRecarga).st_size > 0:
            self.bitacora.escribir("Se encontro el archivo")
            try:
                monto = json.load(open(self.documentoMontoRecarga))
                os.remove(self.documentoMontoRecarga)
                self.montoRecarga.setText(f"${(monto/100):.2f}")
                if monto != self.valorRecarga:
                    self.timer.start(20000)
                    self.valorRecarga = monto
                    
                    
                    
                    if not self.abonar.isEnabled():
                        self.timerRecarga = threading.Timer(6,lambda : self.llamarVentana("REALIZANDO_RECARGA"))
                        self.timerRecarga.start()
                        
                        self.abonar.setEnabled(True)
                    
                    else:
                        self.timerRecarga.cancel()
                        self.timerRecarga = threading.Timer(6,lambda : self.llamarVentana("REALIZANDO_RECARGA"))
                        self.timerRecarga.start()
                        
                        
                        

                    
                    
                    
            except Exception as e:
                self.bitacora.escribir(e)
