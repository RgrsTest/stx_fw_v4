import time
import json
from bitacora import Bitacora
from barra import BarraEstado
from central import Central
from PySide2 import QtCore, QtWidgets, QtGui


class Interfaz(QtWidgets.QWidget):
    """
    Clase donde se crea el widget que contendra toda la interfaz
    """
    point = None
    def __init__(self, path):
        """
        path: string Ruta del directorio raiz
        """
        super().__init__()
        #try:
        self.path = path
        self.documentoPantalla = path + "/shared/display/pantalla.json"
        self.bitacora = Bitacora(path,"Display")

        self.setCursor(QtCore.Qt.BlankCursor)

        self.layout = QtWidgets.QGridLayout()
        self.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, True)
        self.installEventFilter(self)
        self.grabGesture(QtCore.Qt.SwipeGesture)
            
            
        self.layout.addLayout(BarraEstado(path),0,0)
        self.central = Central(path, parent=self)
        self.layout.addLayout(self.central,3,0)
        self.setLayout(self.layout)
        self.setAutoFillBackground(True)
        self.setStyleSheet("background: #919fa1")
    #except Exception as e:
        #self.bitacora.escribir("Error: " + str(e))

    
    def eventFilter(self, obj, evt):
        """
        Funcion para filtrar los eventos de la interfaz
        obj: QObject Objeto al que se le aplica el evento
        evt: QEvent Evento que fue disparado
        """
        if obj.inherits("QScrollArea"):
            self.bitacora.escribir(obj)
            self.bitacora.escribir(evt.type())
        if evt.type() == QtCore.QEvent.TouchBegin:
            if obj.inherits("QScrollArea"):
                self.bitacora.escribir(evt.touchPoints()[0].pos())
                if len(evt.touchPoints()) == 1:
                    self.point = evt.touchPoints()[0].pos().y()
            return True
        elif evt.type() == QtCore.QEvent.TouchUpdate:
            if obj.inherits("QScrollArea"):
                return QtCore.Qevent(QtCore.QEvent.Wheel)
            if len(evt.touchPoints()) == 3:
                    json.dump("INGRESO_MENU", open(self.documentoPantalla, "w+"))
                    return False
        elif evt.type() == QtCore.QEvent.TouchEnd:
            if  obj.inherits("QPushButton"):
                obj.click()
            elif obj.inherits("QRadioButton"):
                obj.click()
            elif obj.inherits("QScrollArea"):
                self.bitacora.escribir(self.point)
                if self.point != None:
                    try:
                        recorrido = self.point - evt.touchPoints()[0].pos().y()
                        obj.verticalScrollBar().setValue(obj.verticalScrollBar().value() + int(recorrido))
                        self.point = None
                    except Exception as e:
                        self.bitacora.escribir("Error al hacer el eveto: " + str(e))


        #return QtCore.QObject.eventFilter(self,obj,evt)