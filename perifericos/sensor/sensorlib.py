import RPi.GPIO as GPIO
from bitacora import Bitacora
import json


class Sensor:
    """
    Clase que controla los sensores
    """
    #Constructor
    def __init__(self,path):
        """
        path: string Ruta del directorio raiz
        """
        self.bitacora = Bitacora(path,"Sensor")
        self.bitacora.escribir("Inicializando Sensores....")
    
    def start(self, path):
        """
        Funcion para inicializar los sensores
        path: string Ruta del archivo de configuracion
        """
        try:
            #Igualar la variable local pines al diccionario que se envia de parametro
            sensores = json.load(open(path, 'r'))
            self.pines = sensores["sensores"] 
            #Setear el modo de los GPIO
            GPIO.setmode(GPIO.BCM)
            #Setear todos los pines al pull up
            for i in self.pines:
                GPIO.setup(i['pin'], GPIO.IN, pull_up_down=GPIO.PUD_UP)
        except Exception as e:
            self.bitacora.escribir(str(e))

        
    def checkAll(self):
        """
        Funcion para checar todos los sensores
        Retorna 0 si ninguno ha sido activado, de lo contrario retorna el numero del sensor activado
        """
        try:
            for i in self.pines :
                salida = GPIO.input(i["pin"])
                if salida:
                    return i["numero"]
            return 0
        except Exception as e:
            self.bitacora.escribir(str(e))
            return 0