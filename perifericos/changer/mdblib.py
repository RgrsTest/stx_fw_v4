import json
import izmdb

from bitacora import Bitacora

class Changer:
    def __init__(self, path):
        self.bitacora = Bitacora(path, "Changer")

    
    def start(self, path):
        """ 
        Funcion para preparar el monedero
        path: string Ruta donde se encuentra el script ejecutandose
        """
        
        valores = json.load(open(path, 'r'))
        self.coin_scaling_factor = 50
        self.coin_types={0}
        self.cash_total_inserted = 0.0
        self.bitacora.escribir(str(int(valores["vendorId"],16)) + "\t"+ str(int(valores["productId"],16)))
        self.init_communication(int(valores["vendorId"],16), int(valores["productId"],16))       
        self.init_coin_changer()
        return  self.get_setup_coin()

    def init_communication(self,VID, PID):
        '''
        Abre la comunicacion entre el monedero y el sistema
        VID: Vendor ID
        PID: Product ID
        '''
        print("Iniciando la comunicacion")
        try:
            self.handle = izmdb.open(VID, PID)
        except Exception as e:
            print("Error abriendo la comunicacion: " + str(e))

    def convertir(self, data):
        """
        Funcion para convertir los datos en el buffer en una lista de strings representando el valor en hexadecimal
        data: cdata Buffer de datos que sera convertido
        """
        try:
            result = []
            if len(data) > 0:
                result = ["{:02x}".format(i) for i in data]
            return result
        except Exception as e:
            self.bitacora.escribir("Error al convertir los datos: " + str(e))
            return []

    def sendCommand(self, cmd):
        """
        Funcion para enviar, recibir y convertir los comandos
        Retorna una lista con la respuesta del comando
        cmd: bytes El comando a enviar
        """
        try:
            izmdb.send(self.handle, cmd)
            databytes =izmdb.receive(self.handle, 4096)
            convertido = self.convertir(databytes)
            self.bitacora.escribir(convertido)
            return convertido
        except Exception as e:
            return []

    #function init coin changer
    def init_coin_changer(self):
        '''
        Inicializa la comunicacion con el monedero
        Retorna True si fue exitosa o False si ocurrio un error
        '''
        try:
            databytes = bytes([0x00])
            response = self.sendCommand(databytes)
            print(response)
            if len(response) > 0:
                if response[0] == '40':
                    return True
                
            return False
        except Exception as e:
            print("Error de comunicacion: " + str(e))
            return False
            

    #function setup coin changer
    def get_setup_coin(self):
        '''
        Obtiene e imprime la informacion del monedero
        '''
        try:
            #Preparamos el paquete
            databytes = bytes([0x01, 0x12, 0x09])
            
            #Enviamos y recibimos
            response = self.sendCommand(databytes)
            self.bitacora.escribir("Obteniendo informacion del monedero:")
            if len(response)>1:
                self.bitacora.escribir("Tipo de Monedero: " + str(response[2]))
                self.bitacora.escribir("Codigo de Moneda: " + str(response[3:5]))
                self.bitacora.escribir("Factor de escala: " + str(response[5]))
                self.coin_scaling_factor = int(response[5],16)
                self.bitacora.escribir("Decimales: " + str(response[6]))
                self.bitacora.escribir("Tipos de Monedas a rutear: " + str(response[7:9]))
                self.bitacora.escribir("Tipo de credito de monedas: " + str(response[9:]))
                self.coin_types = [int(i,16)*self.coin_scaling_factor for i in response[9:]]
                self.bitacora.escribir(str(self.coin_types))
                return True
            return False
        except Exception as e:
            self.bitacora.escribir("Error recibiendo comando: " + str(e))
            return False


    def tubes_coin(self):
        '''
        Obtiene el estado de los tubos, regresa el total de dinero ingresado
        ''' 
        try:
            databytes = bytes([0x01, 0x12, 0x0A])
            response = self.sendCommand(databytes)
            if len(response) > 0:
                self.bitacora.escribir("Tubos llenos: " + str(response[2:4]))
                tubos = response[4:]
                self.bitacora.escribir(tubos)
                total = 0
                for i in range(len(tubos)):
                    total += int(tubos[i],16) * self.coin_types[i]
                self.bitacora.escribir(total)
                return total
        except Exception as e:
            self.bitacora.escribir("Error obteniendo el comando: " + str(e))
        
    
    #function poll coin
    def poll_coin(self):
        '''
        Obtiene eventos del monedero
        Retorna el evento ocurrido
        '''
        try:
            databytes = bytes([0x01, 0x12, 0x0B])
            response = self.sendCommand(databytes)
            if len(response)> 0:
                if response[0] == '41':
                    return 0
                elif response[0] == '21':
                    res = int(response[2],16) & 0x30
                    if res == 0x00:
                        coin = int(response[2],16) & 0x0F
                        return  self.coin_types[coin]
                    elif res == 0x10:
                        coin = int(response[2],16) & 0x0F
                        return self.coin_types[coin]
            else:
                raise NameError("Changer no responde")
            return 0
        except NameError:
            self.bitacora.escribir("Reiniciemos todo, el changer fallo")
            raise NameError("Changer no responde")
        except Exception as e:
            self.bitacora.escribir("Error obteniendo el comando: " + str(e))
            return 0

    #function for payout
    def pay_out_coin(self, amount):
        '''
        Dispensar una cantidad de dinero expresada en centavos 
        El limite es 12,750, es decir $127.50
        amount: int Cantidad de dinero 
        '''
        try:
            if amount < 12750:
                if amount%50 == 0:
                    pagar = int(amount/50)
                    databytes = bytes([0x01, 0x12, 0x0F, 0x02, pagar])
                    response = self.sendCommand(databytes)
                    print(response)
                    if len(response) > 0:
                        if response[0] == '41':
                            return True
            return False
        except Exception as e:
            self.bitacora.escribir("Error enviando el comando: " + str(e))
    
    #function coin accepted
    def coin_type(self, coins, dispense = 0):
        '''
        Activar o desactivar la aceptacion de monedas, asi como dispensar monedas
        coins: int  1 para activar 0 para desactivar
        dispene: int 1 para activar, 0 para desactiva, por default es 0
        '''
        try:
            databytes = bytes([0x01, 0x12, 0x0C])
            if coins:
                databytes= databytes + bytes([0xFF, 0xFF])
            else:
                databytes = databytes + bytes([0x00, 0x00])
            
            if dispense:
                databytes = databytes + bytes([0xFF, 0xFF])
            else:
                databytes = databytes + bytes([0x00, 0x00])

            response = self.sendCommand(databytes)
            if len(response) >0:
                if response[0] == '41':
                    return True
            
            return False
        except Exception as e:
            self.bitacora.escribir("Error enviando el comando: " + str(e))
            return False

    #function Dispense coin
    def coin_dispense(self, coinType, coinNumber):
        '''
        Dispensar monedas
        coinType: int Tipo de moneda a dispensar, el valor puede ser de 0 a 15
        coinNumber: int Cantidad de monedas a dispensar, el valor puede ser de 0 a 15
        '''
        try:
            #Verificamos que ambos valores sean menor a 16
            if coinType < 16 and coinNumber < 16:
                
                #Convertimos el valor del numero de monedas en los ultimos bits
                tipo = coinNumber << 4
                
                #Sumamos los valores para obtener el byte completo (8 bits)
                total = coinType + tipo

                #Preparamos el comando
                databytes = bytes([0x01, 0x12, 0x0F, 0x0D, total])
                
                #Enviamos y recibimos los datos
                response = self.sendCommand(databytes)
                
                #Tratamos los datos
                if len(response) > 0:
                    if response[0] == '41':
                        return True
            
            return False
        except Exception as e:
            self.bitacora.escribir("Error enviando el comando: " + str(e))
            return False


    #function id coin changer
    def coin_id(self):
        '''
        Identificar el producto 
        '''
        try:
            databytes = bytes([0x01, 0x12, 0x0F, 0x00])
            response = self.sendCommand(databytes)
            if len(response) > 0:
                if response[0] == '21':
                    cadena = "".join(chr(int(i,16)) for i in response[2:31])
                    self.bitacora.escribir("Producto: " + str(cadena))
        except Exception as e:
            self.bitacora.escribir("Error enviando el comando: " + str(e))