/*
 * simple-mdbtest.c
 *
 * Programa de ejemplo para demostrar el uso de la API de
 * la biblioteca izmdb.
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (c) 2021 Felipe Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files ("the Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * -----------------------------------------------------------------------------
 *
 * El proceso para enviar comandos y recibir la respuesta del
 * dispositivo es simple:
 *
 * 1) Inicializar la comunicacion con el dispositivo. Solo
 *    es necesario hacerlo una vez al inicio del programa.
 *
 *   izmdb_open()
 *
 * 2) Preparar el comando y argumentos como un arreglo de bytes
 *    y enviarlo al dispositivo.
 *
 *   izmdb_send()
 *
 * 3) Leer la respuesta del dispositivo.
 *
 *   izmdb_receive()
 *
 * 4) Finalizar la comunicacion con el dispositivo antes de
 *    terminar el programa.
 *
 *   izmdb_close()
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <izmdb.h>


/* Vendor ID y Product ID de la alcancia */
#define CF_VID 0x0BED
#define CF_PID 0x0400



void print_bytes (unsigned char *data, int size) {
/*
 * Despliega para consumo humano el contenido de
 * un arreglo de bytes DATA de tama~o SIZE.
 */

  printf ("bytes[%d]: [ ", size);
  for (int i = 0; i < size; i++) {
     printf ("%02x ", data[i]);
  }
  printf ("]\n");
}


int main (void) {

  int retval;
  int databytes;
  unsigned char *pkdata;
  izmdb_handle *handle;
  struct izmdb_packet packet;

  /* Creamos un buffer de trabajo para construir ahi el comando */
  pkdata = malloc(4096);
  if (pkdata == NULL) {
     printf ("Error en malloc de pkdata\n");
     exit(1);
  }

  /*****************************************************************/
  /*          1) Abrir la comunicacion con el dispositivo          */
  /*****************************************************************/

  printf ("Buscando dispositivo USB: [%04x:%04x]\n", CF_VID, CF_PID);
  retval = izmdb_open(CF_VID, CF_PID, &handle);
  if (retval != IZMDB_OK) {
     printf ("Error en izmdb_open: %d\n", retval);
     exit(1);
  }
  printf ("Dispositivo encontrado!\n");

  /********** Preparamos el comando MDB ***********/
  printf ("Preparando el comando MDB:\n   MDB_TRX Expansion: Identification\n");
  pkdata[0] = 0x01; /* MDB_TRX */
  pkdata[1] = 0x12; /* CMD_ID */
  pkdata[2] = 0x0F; /* MDB Expansion */
  pkdata[3] = 0x00; /* MDB Expansion: Identification */



  /*****************************************************************/
  /*            2) Enviamos el comando al dispositivo              */
  /*****************************************************************/
  
  printf ("Enviando datos al dispositivo...\n");
  print_bytes(pkdata, 4);
  if (izmdb_send(handle, pkdata, 4) != IZMDB_OK) {
     printf ("Error en izmdb_send\n");
     exit(1);
  }

  /* Ya no necesitamos este buffer */
  free (pkdata);

  /******************************************************************/
  /*         3) Recibimos la respuesta desde el dispositivo         */
  /******************************************************************/

  /* Al regresar de izmdb_receive() 'pkdata' apuntara a un buffer
   * con los datos contenidos en la respuesta del dipositivo
   * y 'databytes' apuntara al tama~o de ese buffer.
   */

  printf ("Recibiendo respuesta:\n");
  databytes = 4096; /* El tama~o del buffer pkdata */
  if ((retval = izmdb_receive(handle, &pkdata, &databytes)) != IZMDB_OK) {
     printf ("Error en izmdb_receive: %d\n", retval);
     exit(1);
  }

  print_bytes(pkdata, databytes);

  printf ("Info recibida: ");
  for (int i = 0; i < databytes; i ++) {
     printf ("%c", pkdata[i]);
  }
  printf ("\n");



  /******************************************************************/
  /*         4) Cerramos la comunicacion con el dispositivo         */
  /******************************************************************/

  retval = izmdb_close(handle);

  if (retval != IZMDB_OK) {
     printf ("Error en izmdb_close: %d\n", retval);
     exit(1);
  }

}
