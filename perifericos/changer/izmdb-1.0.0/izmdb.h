/*
 * izmdb.h
 *
 * Prototipos y definiciones para la biblioteca izmdb.
 *
 *
 * Copyright (c) 2021 Felipe Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files ("the Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <SiUSBXp.h>

#ifndef IZMDB_H
#define IZMDB_H

#ifdef DEBUG
#define IZ_DEBUG(args...) printf ("izmdb: "); printf (args)
#else
#define IZ_DEBUG(format, ...)
#endif

#define IZMDB_ERR -1
#define IZMDB_OK  1
#define IZMDB_NOMEM -2
#define IZMDB_INVALID_PACKET -3
#define IZMDB_DEV_NOT_FOUND -4
#define IZMDB_READ_ERROR -5
#define IZMDB_INVALID_PARAM -6
#define IZMDB_INVALID_PACKET_FROM_DEVICE -7
#define IZMDB_CANNOT_OPEN_DEV -8
#define IZMDB_WRITE_ERROR -9

#define IZMDB_ALLOC 64

/*
 * Un paquete MDB:
 *
 * |----------------------- stream ------------------------|
 * | encabezado (0xC1) |   carga util  | terminador (0xC2) |
 * |------------------------ size -------------------------|
 *
 */

struct izmdb_packet {
  unsigned char *stream;
  int  size;
};

typedef struct SI_Private izmdb_handle;

int izmdb_pack (unsigned char *pkdata, int datasize, struct izmdb_packet *packet);
int izmdb_unpack (struct izmdb_packet *packet, unsigned char **pkdata, int *datasize);
int izmdb_open (int vendor_id, int product_id, struct SI_Private **handle);
int izmdb_close (struct SI_Private *handle);
int izmdb_send_packet (struct SI_Private *handle, struct izmdb_packet *packet);
int izmdb_receive_packet (struct SI_Private *handle, struct izmdb_packet *packet);
int izmdb_send (izmdb_handle *handle, unsigned char *data, int datasize);
int izmdb_receive (izmdb_handle *handle, unsigned char **databuf, int *datasize);

#endif /* IZMDB_H */
