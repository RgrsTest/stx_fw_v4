#!/usr/bin/python3
#
# Demostracion de uso de la API del paquete 'izmdb' en Python
#
# Copyright (c) 2021 Felipe Sanchez
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files ("the Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom
# the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
###############################################################################
#
# El proceso para enviar comandos y recibir la respuesta del
# dispositivo es simple:
#
# 1) Inicializar la comunicacion con el dispositivo. Solo
#    es necesario hacerlo una vez al inicio del programa.
#
#   izmdb.open()
#
# 2) Preparar el comando y argumentos como un arreglo de bytes
#    y enviarlo al dispositivo.
#
#   izmdb.send()
#
# 3) Leer la respuesta del dispositivo.
#
#   izmdb.receive()
#
# 4) Finalizar la comunicacion con el dispositivo antes de
#    terminar el programa.
#
#   izmdb.close()


import izmdb

# Vendor ID y Product ID de la alcancia */
CF_VID = 0x0BED
CF_PID = 0x0400


###########################################################
#
# Funciones de utilidad
#

def print_bytes (bytes):
  #
  # Desplegar el contenido de un arreglo de bytes
  #
  l = len(bytes)
  print ("bytes[" + str(l) + "]: [ ", end="")
  for i in range(0, l):
     print ("{:02x}".format(bytes[i]), end="")
     print (" ", end="")
  print ("]")


###########################################################
#
# Abrir la comunicacion con el dispositivo
#

print ("Buscando dispositivo USB: [" + "{:04x}".format(CF_VID) + ":" + "{:04x}".format(CF_PID) + "]")

handle = izmdb.open(CF_VID, CF_PID)

print ("Dispositivo encontrado!")


###########################################################
#
# Enviamos el paquete al dispositivo
#



############################################
print("Enviando RESET")
databytes = bytes([0x00])
izmdb.send (handle, databytes)


print("Recibiendo Respuesta")
databytes = izmdb.receive(handle, 4096)

print_bytes(databytes)




########## Preparamos el comando MDB ############
print ("Preparando el comando MDB:\n   MDB_TRX Expansion: Identification")


# 0x01 : MDB_TRX
# 0x12 : CMD_ID
# 0x0F : MDB Expansion
# 0x00 : MDB Expansion: Identification
databytes = bytes([0x01, 0x12, 0x0F, 0x00])

print ("Enviando paquete")
izmdb.send (handle, databytes)


###########################################################
#
# Recibimos la respuesta desde el dispositivo
#


print ("Recibiendo respuesta")
databytes = izmdb.receive(handle, 4096)

print_bytes(databytes)

print ("Info recibida: ", end="")
for i in range(0,len(databytes) ):
   print (chr(databytes[i]), end="")

print ()

###########################################################
#
# Cerramos la comunicacion con el dispositivo
#

izmdb.close(handle)

