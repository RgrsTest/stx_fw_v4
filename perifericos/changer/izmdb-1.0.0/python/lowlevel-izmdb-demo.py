#!/usr/bin/python3
#
# Demostracion de uso de funciones izmdb de bajo nivel con CFFI
#
# Copyright (c) 2021 Felipe Sanchez
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files ("the Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom
# the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


from _izmdb import ffi, lib
import os


# Constantes de izmdb
# FIXME: Deberian estar definidas en un paquete de python?
IZMDB_OK = 1

# Vendor ID y Product ID de la alcancia */
CF_VID = 0x0BED
CF_PID = 0x0400


########### Desplegar el contenido de un paquete ##########

def print_packet (packet):
  print ("packet[" + str(packet.size) + "]: [ ", end="")
  for i in range(0, packet.size):
     print ("{:02x}".format(packet.stream[i]), end="")
     print (" ", end="")
  print ("]")

############ Estructuras de C para izmdb ###########

# Como en Python no tenemos el concepto de los operadores & y *
# de C entonces  comenzamos con un apuntador a apuntador:

handle_p = ffi.new ("izmdb_handle **")

# ... y luego el izmdb_handle sera el primer elemento de ese apuntador,
# tal como en C:
#
#       handle            ==       handle_p[0]
# struct izmdb handle *       struct izmdb_handle ** [0]
#
#
# En general, *p  == p[0]
#
# 

packet = ffi.new ("struct izmdb_packet *")

############ Abrir la comunicacion con el dispositivo ##########

print ("Buscando dispositivo USB: [" + "{:04x}".format(CF_VID) + ":" + "{:04x}".format(CF_PID) + "]")

retval = lib.izmdb_open(CF_VID, CF_PID, handle_p)

# FIXME: Aqui probablemente querriamos usar una excepcion
if retval != IZMDB_OK:
   print ("Error en izmdb_open: " + str(retval))
   os._exit(1)

# Si izmdb_open regreso sin error entonces handle_p[0] es
# un apuntador a izmdb_handle, asi que &handle == handle_p[0].
handle = handle_p[0]

print ("Dispositivo encontrado!")

########## Preparamos el comando MDB ############
print ("Preparando el comando MDB:\n   MDB_TRX Expansion: Identification")

# El arreglo de 4 bytes para enviar el comando
#
# 0x01 : MDB_TRX
# 0x12 : CMD_ID
# 0x0F : MDB Expansion
# 0x00 : MDB Expansion: Identification

pkdata = bytes([0x01, 0x12, 0x0F, 0x00])


########### Empaquetamos el comando MDB ###########
if lib.izmdb_pack(pkdata, 4, packet) != IZMDB_OK:
   print ("Error en izmdb_pack")
   os._exit(1)

print_packet(packet)

########### Enviamos el paquete al dispositivo ###########

print ("Enviando paquete")

#retval = lib.izmdb_send(handle_p[0], packet)
retval = lib.izmdb_send_packet(handle, packet)
if retval != IZMDB_OK:
   print ("Error en izmdb_send" + str(retval))
   os._exit(1)

########### Recibimos la respuesta desde el dispositivo  ############

# Vamos a reutilizar el paquete asi que lo preparamos:

# Creamos un nuevo buffer en para el paquete... */
pkstream = ffi.new ("unsigned char[4096]")

packet.stream = pkstream
packet.size   = 4096

# ... y recibimos el paquete de respuesta
print ("Recibiendo respuesta")
retval = lib.izmdb_receive_packet(handle, packet)
if retval != IZMDB_OK:
   print ("Error en izmdb_receive: " + str(retval))
   os._exit(1)

print_packet(packet)

################### Desempaquetamos la informacion ###################
databytes = ffi.new("int *")
pkdata    = ffi.new("unsigned char *")
pkdata_p  = ffi.new("unsigned char **", pkdata)

retval = lib.izmdb_unpack(packet, ffi.cast("unsigned char **", pkdata_p), databytes)
if retval != IZMDB_OK:
   print ("Error en izmdb_unpack: " + str(retval))
   os._exit(1)

pkdata = pkdata_p[0]

# databytes[0] es lo mismo que *databytes en C
# (O para el caso, que databytes[0] en C):
#
# https://cffi.readthedocs.io/en/latest/using.html#working-with-pointers-structures-and-arrays
#
# y mismo caso para pkdata[0]

print ("Info recibida: ", end="")
for i in range(0, databytes[0]):
   print (chr(pkdata[i]), end="")

print ()

################## Cerramos la comunicacion con el dispositivo ##################
retval = lib.izmdb_close(handle)

if retval != IZMDB_OK:
   print ("Error en izmdb_close: " + str(retval))
   os._exit(1)
