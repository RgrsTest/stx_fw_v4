#
# pyizmdb_build.py
#
# Construir el modulo de python para enlazar con izmdb
#
# Copyright (c) 2021 Felipe Sanchez
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files ("the Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom
# the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

from cffi import FFI
ffibuilder = FFI()

#struct usb_dev_handle;
#typedef struct usb_dev_handle usb_dev_handle;
# Definiciones de los objectos de C necesarios
# para usar la biblioteca izmdb
ffibuilder.cdef("""
struct izmdb_packet {
  unsigned char *stream;
  int  size;
};

struct usb_dev_handle;
typedef struct usb_dev_handle usb_dev_handle;

struct SI_Private {
	int magic;
	usb_dev_handle  *udev;
	int interface;
	int ep_out;
	int ep_in;
	int bufsize;
	unsigned char buffer[4096];
};

typedef struct SI_Private izmdb_handle;

int izmdb_pack (unsigned char *pkdata, int datasize, struct izmdb_packet *packet);
int izmdb_unpack (struct izmdb_packet *packet, unsigned char **pkdata, int *datasize);
int izmdb_open (int vendor_id, int product_id, struct SI_Private **handle);
int izmdb_close (struct SI_Private *handle);
int izmdb_send_packet (struct SI_Private *handle, struct izmdb_packet *packet);
int izmdb_receive_packet (struct SI_Private *handle, struct izmdb_packet *packet);
int izmdb_send (izmdb_handle *handle, unsigned char *data, int datasize);
int izmdb_receive (izmdb_handle *handle, unsigned char **databuf, int *datasize);

int SI_GetNumDevices(int *NumDevices);
int SI_GetProductString(int DeviceNum, char * DeviceString, int Flags);
int SI_Open(int DeviceNum, struct SI_Private ** pHandle);
int SI_Close(struct SI_Private * Handle);
int SI_Read(struct SI_Private * Handle, unsigned char * Buffer, int BytesToRead, int * BytesReturned, void * o);
int SI_Write(struct SI_Private * Handle, unsigned char * Buffer, int BytesToWrite, int * BytesWritten, void * o);
int SI_ResetDevice(struct SI_Private * Handle);
int SI_DeviceIOControl(struct SI_Private * Handle, int IoControlCode, char * InBuffer, int BytesToRead, char * OutBuffer, int BytesToWrite);
int SI_FlushBuffers(struct SI_Private * Handle, char FlushTransmit, char FlushReceive);
int SI_SetTimeouts(int ReadTimeout, int WriteTimeout);
int SI_GetTimeouts(int *ReadTimeout, int *WriteTimeout);
int SI_CheckRXQueue(struct SI_Private * Handle, int *NumBytesInQueue, int *QueueStatus);

""")


# Nombre del nuevo modulo y codigo de encabezados
# exportados desde izmdb
ffibuilder.set_source("_izmdb",
"""
     #include "../izmdb.h"
""",
     libraries=['izmdb', 'SiUSBXp', 'usb'])   # library name, for the linker

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)

