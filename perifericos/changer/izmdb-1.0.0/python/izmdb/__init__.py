#
# Inicializacion de modulo izmdb
#
# Copyright (c) 2021 Felipe Sanchez
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files ("the Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom
# the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


from _izmdb import ffi, lib


###########################################################
#
# Constantes de izmdb
#
# FIXME: Deberian estar definidas en un archivo separado?
#

IZMDB_OK     =  1
IZMDB_ERR    = -1
IZMDB_NOMEM  = -2
IZMDB_INVALID_PACKET = -3
IZMDB_DEV_NOT_FOUND  = -4
IZMDB_READ_ERROR     = -5
IZMDB_INVALID_PARAM  = -6
IZMDB_INVALID_PACKET_FROM_DEVICE = -7
IZMDB_CANNOT_OPEN_DEV = -8
IZMDB_WRITE_ERROR     = -9



def error_decode (errnum):

   #
   # Regresa una cadena que representa el numero de error ERRNUM
   #

   if errnum == IZMDB_OK:
      return 'ALL OK'
   elif errnum == IZMDB_ERR:
      return 'GENERAL ERROR'
   elif errnum == IZMDB_NOMEM:
      return 'NOT ENOUGH MEMORY'
   elif errnum == IZMDB_INVALID_PACKET:
      return 'INVALID PACKET'
   elif errnum == IZMDB_DEV_NOT_FOUND:
      return 'DEVICE NOT FOUND'
   elif errnum == IZMDB_READ_ERROR:
      return 'READ ERROR'
   elif errnum == IZMDB_INVALID_PARAM:
      return 'INVALID PARAMETER'
   elif errnum == IZMDB_INVALID_PACKET_FROM_DEVICE:
      return 'INVALID PACKET FROM DEVICE'
   elif errnum == IZMDB_CANNOT_OPEN_DEV:
      return 'CANNOT OPEN DEVICE'
   elif errnum == IZMDB_WRITE_ERROR:
      return 'WRITE ERROR'
   else:
      return 'UNKNOWN ERROR'




###########################################################
#
# Estructuras de C para izmdb
#

# Como en Python no tenemos el concepto de los operadores & y *
# de C entonces  comenzamos con un apuntador a apuntador:

# handle_p = ffi.new ("izmdb_handle **")

# ... y luego el izmdb_handle sera el primer elemento de ese apuntador,
# tal como en C:
#
#       handle            ==       handle_p[0]
# struct izmdb handle *   ==  struct izmdb_handle ** [0]
#
#
# En general, *p  == p[0]
#
# 

packet = ffi.new ("struct izmdb_packet *")

def open(vid, pid):

   #
   # Buscar el dispositivo con VendorID VID y ProductID PID
   # y establecer comunicacion con el.
   #
   # Regresa un handle enlazado con ese dispositivo.
   #

   handle_p = ffi.new ("izmdb_handle **")
   retval = lib.izmdb_open(vid, pid, handle_p)

   if retval != IZMDB_OK:
      raise Exception('FFI izmdb_open() failed with error ({})',format(error_decode(retval)))

   return handle_p[0]


def close(handle):
   #
   # Cerrar la comunicacion con el dispositivo manejado por HANDLE
   #
   # En caso de exito devuelve IZMDB_OK.
   #

   retval = lib.izmdb_close(handle)

   if retval != IZMDB_OK:
      raise Exception('FFI izmdb_close() failed with error ({})',format(error_decode(retval)))

   return IZMDB_OK


def send(handle, bytes):
   #
   # Crea un paquete MDB codificando el contenido del arreglo BYTES
   # y lo envia al dispositivo manejado por HANDLE.
   #

   retval = lib.izmdb_send(handle, bytes, len(bytes))
   if retval != IZMDB_OK:
      raise Exception('FFI izmdb_send() failed with error ({})',format(error_decode(retval)))

   return IZMDB_OK


def receive(handle, datasize):
   #
   # Recibe el siguiente paquete MDB disponible en el dispositivo
   # manejado por HANDLE y devuelve su contenido descodificado
   # como un arreglo de bytes.
   #

   pkdata      = ffi.new ("unsigned char []", datasize)
   pkdata_p    = ffi.new ("unsigned char **", pkdata) 
   datasize_p  = ffi.new ("int *", datasize)

   retval = lib.izmdb_receive(handle, pkdata_p, datasize_p)
   if retval != IZMDB_OK:
      raise Exception('FFI izmdb_receive() failed with error ({})',format(error_decode(retval)))

   return pkdata_p[0][0:datasize_p[0]];

