/*
 * lowlevel-izmdb-demo.c
 *
 * Programa de ejemplo para demostrar el uso de la API de
 * bajo nivel de la biblioteca izmdb.
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (c) 2021 Felipe Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files ("the Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <SiUSBXp.h>
#include <izmdb.h>

/* Vendor ID y Product ID de la alcancia */
#define CF_VID 0x0BED
#define CF_PID 0x0400



void print_packet (struct izmdb_packet *packet) {
  printf ("packet[%d]: [ ", packet->size);
  for (int i = 0; i < packet->size; i++) {
     printf ("%02x ", (unsigned char) packet->stream[i]);
  }
  printf ("]\n");
}


int main (void) {

  int retval;
  int databytes;
  izmdb_handle *handle;
  struct izmdb_packet packet;
  unsigned char *pkdata;

  /********** Abrir la comunicacion con el dispositivo **********/
  printf ("Buscando dispositivo USB: [%04x:%04x]\n", CF_VID, CF_PID);
  retval = izmdb_open(CF_VID, CF_PID, &handle);
  if (retval != IZMDB_OK) {
     printf ("Error en izmdb_open: %d\n", retval);
     exit(1);
  }
  printf ("Dispositivo encontrado!\n");

  /********** Preparamos el comando MDB ***********/
  pkdata = malloc(16 * sizeof(unsigned char));
  if (pkdata == NULL) {
     printf ("Error en malloc de pkdata\n");
     exit(1);
  }

  printf ("Preparando el comando MDB:\n   MDB_TRX Expansion: Identification\n");
  pkdata[0] = 0x01; /* MDB_TRX */
  pkdata[1] = 0x12; /* CMD_ID */
  pkdata[2] = 0x0F; /* MDB Expansion */
  pkdata[3] = 0x00; /* MDB Expansion: Identification */

  /********** Empaquetamos el comando MDB ***********/
  if (izmdb_pack(pkdata, 4, &packet) != IZMDB_OK) {
     printf ("Error en izmdb_pack\n");
     exit(1);
  }
  print_packet(&packet);

  free(pkdata);

  /********** Enviamos el paquete al dispositivo  ***********/
  printf ("Enviando paquete\n");
  if (izmdb_send_packet(handle, &packet) != IZMDB_OK) {
     printf ("Error en izmdb_send\n");
     exit(1);
  }

  /********** Recibimos la respuesta desde el dispositivo  ***********/

  /* Vamos a reutilizar el paquete asi que lo preparamos: */

  /* Creamos un nuevo buffer en el paquete... */
  free (packet.stream);
  packet.stream = malloc (4096);
  packet.size   = 4096;

  /* ... y recibimos el paquete de respuesta */
  printf ("Recibiendo respuesta\n");
  if ((retval = izmdb_receive_packet(handle, &packet)) != IZMDB_OK) {
     printf ("Error en izmdb_receive: %d\n", retval);
     exit(1);
  }

  print_packet(&packet);

  /****************** Desempaquetamos la informacion *****************/
  if (retval = izmdb_unpack(&packet, &pkdata, &databytes) != IZMDB_OK) {
     printf ("Error en izmdb_unpack: %d\n", retval);
     exit(1);
  }

  printf ("Info recibida: ");
  for (int i = 0; i < databytes; i ++) {
     printf ("%c", pkdata[i]);
  }
  printf ("\n");

  /***************** Cerramos la comunicacion con el dispositivo *****************/
  retval = izmdb_close(handle);

  if (retval != IZMDB_OK) {
     printf ("Error en izmdb_close: %d\n", retval);
     exit(1);
  }

}
