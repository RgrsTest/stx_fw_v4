/*
 * Copyright (c) 2021 Felipe Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files ("the Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <usb.h>

#ifndef SIUSBXP_H
#define SIUSBXP_H

#define BUF_SIZE 4096

struct SI_Private {
	int magic;
	usb_dev_handle *udev;
	int interface;
	int ep_out;
	int ep_in;
	int bufsize;
	unsigned char buffer[BUF_SIZE];
};


int SI_GetNumDevices(int *NumDevices);
int SI_GetProductString(int DeviceNum, char * DeviceString, int Flags);
int SI_Open(int DeviceNum, struct SI_Private ** pHandle);
int SI_Close(struct SI_Private * Handle);
int SI_Read(struct SI_Private * Handle, unsigned char * Buffer, int BytesToRead, int * BytesReturned, void * o);
int SI_Write(struct SI_Private * Handle, unsigned char * Buffer, int BytesToWrite, int * BytesWritten, void * o);
int SI_ResetDevice(struct SI_Private * Handle);
int SI_DeviceIOControl(struct SI_Private * Handle, int IoControlCode, char * InBuffer, int BytesToRead, char * OutBuffer, int BytesToWrite);
int SI_FlushBuffers(struct SI_Private * Handle, char FlushTransmit, char FlushReceive);
int SI_SetTimeouts(int ReadTimeout, int WriteTimeout);
int SI_GetTimeouts(int *ReadTimeout, int *WriteTimeout);
int SI_CheckRXQueue(struct SI_Private * Handle, int *NumBytesInQueue, int *QueueStatus);

/*Vendor ID / Product ID*/
#define		SI_USB_VID                      0x10c4
#define         SI_USB_PID                      0x8149

/*Return codes*/
#define		SI_SUCCESS                      0x00
#define		SI_DEVICE_NOT_FOUND             0xFF
#define		SI_INVALID_HANDLE               0x01
#define		SI_READ_ERROR                   0x02
#define		SI_RX_QUEUE_NOT_READY           0x03
#define		SI_WRITE_ERROR                  0x04
#define		SI_RESET_ERROR                  0x05
#define		SI_INVALID_PARAMETER            0x06
#define		SI_INVALID_REQUEST_LENGTH       0x07
#define		SI_DEVICE_IO_FAILED             0x08
#define		SI_INVALID_BAUDRATE             0x09
#define		SI_FUNCTION_NOT_SUPPORTED       0x0a
#define		SI_GLOBAL_DATA_ERROR            0x0b
#define		SI_SYSTEM_ERROR_CODE            0x0c
#define		SI_READ_TIMED_OUT               0x0d
#define		SI_WRITE_TIMED_OUT              0x0e
#define		SI_IO_PENDING                   0x0f

/*GetProductString() function flags*/
#define		SI_RETURN_SERIAL_NUMBER         0x00
#define		SI_RETURN_DESCRIPTION           0x01
#define		SI_RETURN_LINK_NAME             0x02
#define		SI_RETURN_VID                   0x03
#define		SI_RETURN_PID                   0x04

/*RX Queue status flags*/
#define		SI_RX_NO_OVERRUN                0x00
#define		SI_RX_EMPTY                     0x00
#define		SI_RX_OVERRUN                   0x01
#define		SI_RX_READY                     0x02

/*Buffer size limits*/
#define		SI_MAX_DEVICE_STRLEN            256
#define		SI_MAX_READ_SIZE                4096*16
#define		SI_MAX_WRITE_SIZE               4096

#ifdef DEBUG
#define DBG(args...) printf (args)
#else
#define DBG(format, ...)
#endif
#define ERR(args...) printf (args)

#define MAGIC 12939485



#endif /* SIUSBXP_H */
