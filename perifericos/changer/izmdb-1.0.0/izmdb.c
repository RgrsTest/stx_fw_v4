/*
 * izmdb.c
 *
 * Esta biblioteca implementa funciones envio y recepcion de paquetes MDB
 * utilizando la API SiUSBXp.
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (c) 2021 Felipe Sanchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files ("the Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <stdio.h>
#include <izmdb.h>


int izmdb_pack (unsigned char *pkdata, int datasize, struct izmdb_packet *packet) {
/*
 * Empaquetar con Simple Coin Protocol la secuencia de comando MDB
 * (Comando + argumentos) de longitud DATASIZE a la que apunta PKDATA.
 * 
 * La secuencia de comando va empaquetada asi:
 *
 * | encabezado | comando + datos | terminador |
 *     0xC1         carga util         0xC2
 *
 * La carga util se codifica de manera que si esta incluye los bytes
 * 0xC0, 0xC1 y 0xC2 estos son escapados con un byte 0xC0 y
 * codificados en la siguiente forma:
 *
 * 0xC0 --> 0xC0 0x00
 * 0xC1 --> 0xC0 0x01
 * 0xC2 --> 0xC0 0x02
 *
 * Todos los demas bytes son copiados tal cual al paquete.
 *
 * Si el empaquetamiento es exitoso devuelve IZMDB_OK y PACKET apunta
 * al paquete codificado.
 * 
 */

   int data_idx = 0;
   int packet_mem_size;

   /* Requerimos datos y un paquete */
   if (pkdata == NULL || packet == NULL) {
      return IZMDB_INVALID_PARAM;
   }

   /* Preparamos el area de datos del paquete */
   packet->size = 0;
   packet->stream = malloc (IZMDB_ALLOC);

   if (packet->stream == NULL) {
      return IZMDB_NOMEM;
   }

   packet_mem_size = IZMDB_ALLOC;

   packet->stream[data_idx++] = (unsigned char) 0xC1; /* Encabezado del paquete MDB */

   /*
    * Copiamos los datos al stream del paquete escapando
    * los caracteres 0xC0, 0xC1 y 0xC2.
    */

   for (int i = 0; i < datasize; i++) {
      switch (pkdata[i]) {
         case 0xC0:
            packet->stream[data_idx++] = 0xC0;
            packet->stream[data_idx++] = 0x00;
            break;
         case 0xC1:
            packet->stream[data_idx++] = 0xC0;
            packet->stream[data_idx++] = 0x01;
            break;
         case 0xC2:
            packet->stream[data_idx++] = 0xC0;
            packet->stream[data_idx++] = 0x02;
            break;
         default:
            packet->stream[data_idx++] = pkdata[i];
      }
      /* Siempre queremos tener al menos 2 bytes libres */
      /* al final del espacio reservado para el paquete */
      if ( (packet_mem_size - data_idx) < 2 ) {
         packet->stream = realloc (packet->stream, packet_mem_size + IZMDB_ALLOC);
         if (packet->stream == NULL) {
            return IZMDB_NOMEM;
         }
         packet_mem_size += IZMDB_ALLOC;
      }
   }

   packet->stream[data_idx++] = (unsigned char) 0xC2; /* Terminador del paquete MDB */
   packet->size = data_idx;

   return IZMDB_OK;
}


int izmdb_unpack (struct izmdb_packet *packet, unsigned char **pkdata, int *datasize) {
 /*
 * Desempaquetar los datos contenidos en el paquete al que apunta PACKET
 *
 * Esta es la operacion inversa de izmdb_pack().
 *
 * Si el desempacamiento es exitoso devuelve IZMDB_OK, PKDATA apunta a
 * los datos descodificados y DATASIZE apunta a un entero que representa
 * la cantidad de bytes descodificados.
 */

   int data_idx = 0;


   /******* Verificaciones de sanidad *******/

   /* Los apuntadores recibidos son validos? */
   if ( (pkdata == NULL) || (packet == NULL) ) {
      return IZMDB_INVALID_PARAM;
   }


   /* Verificaciones del paquete */

   /* El paquete tiene un tama~o valido? */
   if (packet->size < 2) {
      return IZMDB_INVALID_PACKET;
   }

   /* El paquete tiene datos? */
   if (packet->stream == NULL) {
      return IZMDB_INVALID_PACKET;
   }

   /* El paquete tiene encabezado y terminador? */
   if (packet->stream[0] != 0xC1 || packet->stream[packet->size - 1] != 0xC2) {
      return IZMDB_INVALID_PACKET;
   }

   /* Lo primero es conseguir memoria para poner los datos descodificados */

   /*
    * Necesariamente seran menos que el tama~o del paquete asi que
    * ese tama~o sera suficiente. Por lo menos es igual al tama~o del
    * paquete menos el encabezado y terminador.
    */

   *pkdata = malloc (packet->size);

   if (*pkdata == NULL) {
      return IZMDB_NOMEM;
   }

   for (int i = 1; i < (packet->size - 1); i++) {
      if (packet->stream[i] == 0xC0) {
         /* Es un byte con escape, veamos que es lo que escapamos */
         switch (packet->stream[++i]) {
            case 0x00:
            case 0x01:
            case 0x02:
               (*pkdata)[data_idx++] = packet->stream[i] | 0xC0; /* Conservamos el nibble menos significativo */
               break;
            default:
               /* Cualquier otro valor despues de un 0xC0 es un error */
               return IZMDB_INVALID_PACKET_FROM_DEVICE;
         }
      } else {
         /* Byte normal sin escape, se copia tal cual */
         /* *(*pkdata + data_idx++) = packet->stream[i]; */
         (*pkdata)[data_idx++] = packet->stream[i];
      }
   }

   *datasize = data_idx;

   return IZMDB_OK;
}


int izmdb_open (int vendor_id, int product_id, izmdb_handle **handle) {
 /*
  * Busca el primer dispositivo USB con identificacion VENDOR_ID:PRODUCT_ID
  * y si lo encuentra, devuelve IZMDB_OK y HANDLE apunta a un handle para
  * el dispositivo, utilizable por izmdb_send_packet() e izmdb_receive_packet().
  *
  */

   int numdev;
   int retval;
   int cur_vid;
   int cur_pid;
   int dev_id;
   char desc[4096];

   /* Identificar cuantos dispositivos USB tenemos */
   SI_GetNumDevices(&numdev);
   if (!numdev) {
      return IZMDB_DEV_NOT_FOUND;
   }

   IZ_DEBUG ("NumDevices: %d\n", numdev);

   /* Busquemos el que nos pidieron */

   IZ_DEBUG ("Buscando [%04x:%04x]\n", vendor_id, product_id);

   dev_id = -1;
   for (int i = 0; i < numdev; i++) {

      cur_vid = SI_GetProductString (i, desc, SI_RETURN_VID);
      cur_pid = SI_GetProductString (i, desc, SI_RETURN_PID);

      IZ_DEBUG ("Dispositivo %d [%04x:%04x]\n", i, cur_vid, cur_pid);

      if (cur_vid == vendor_id && cur_pid == product_id) {
         IZ_DEBUG ("Encontre %04x:%04x!!\n", vendor_id, product_id);

         dev_id = i;
         break;
      }

   }

   if ( dev_id < 0 ) {
      IZ_DEBUG ("No encontre el dispositivo USB %04x:%04x :(\n", vendor_id, product_id);
      return IZMDB_DEV_NOT_FOUND;
   }

   IZ_DEBUG ("Abriendo el dispositivo con id %d\n", dev_id);
   retval = SI_Open(dev_id, handle);
   if ( retval != SI_SUCCESS ) {
      IZ_DEBUG ("Error en SI_Open(): %d\n", retval);
      return IZMDB_CANNOT_OPEN_DEV;
   }

   return IZMDB_OK;

}

int izmdb_close (izmdb_handle *handle) {
 /*
  * Cierra la comunicacion con el dispositivo USB referido por HANDLE.
  *
  */

   int retval;

   IZ_DEBUG ("Cerrando el dispositivo\n");

   if ( (retval = SI_Close(handle)) != SI_SUCCESS ) {
      IZ_DEBUG ("Error en SI_Close(): %d\n", retval);
      return IZMDB_ERR;
   }

   return IZMDB_OK;
}

int izmdb_send_packet (izmdb_handle *handle, struct izmdb_packet *packet) {
 /*
  * Envia el paquete MDB apuntado por PACKET al dispositivo USB
  * manejado por HANDLE.
  *
  */

   int byte_count;
   int retval;

   /* Los apuntadores recibidos son validos? */
   if ( (handle == NULL) || (packet == NULL) ) {
      return IZMDB_INVALID_PARAM;
   }

   if ((retval = SI_Write (handle, packet->stream, packet->size, &byte_count, NULL)) != SI_SUCCESS ) {
      IZ_DEBUG ("Error en SI_Write(): %d\n", retval);
      return IZMDB_WRITE_ERROR;
   }

   return IZMDB_OK;
}

int izmdb_receive_packet (izmdb_handle *handle, struct izmdb_packet *packet) {
 /*
  * Recibe un paquete MDB y lo almacena en el paquete apuntado por PACKET
  * desde el dispositivo USB manejado por HANDLE.
  *
  * Se espera que el campo 'stream' de PACKET apunta a un area reservada de
  * memoria de tama~o suficiente y el campo 'size' indica el tama~o de la misma.
  */

   unsigned char b;
   int byte_count;
   int data_idx;
   int term_encontrado;
   int retval;

   /* Los apuntadores recibidos son validos? */
   if ( (handle == NULL) || (packet == NULL) ) {
      return IZMDB_INVALID_PARAM;
   }

   /* Leamos un byte a la vez hasta que encontremos el inicio de un paquete */
   while ( b != 0xC1 ) {
      if ((retval = SI_Read (handle, &b, 1, &byte_count, NULL)) != SI_SUCCESS) {
         IZ_DEBUG ("SI_Read C1 error: %d\n", retval);
         return IZMDB_READ_ERROR;
      }
   }
   packet->stream[0] = b;

   /* Leemos un byte a la vez hasta que encontremos el terminador */
   term_encontrado = 0;
   for (data_idx = 1; data_idx < packet->size; data_idx++) {
      if ((retval = SI_Read (handle, &packet->stream[data_idx], 1, &byte_count, NULL)) != SI_SUCCESS) {
         IZ_DEBUG ("SI_Read C2 error: %d\n", retval);
         return IZMDB_READ_ERROR;
      }
      /* Solo queremos leer hasta el terminador */
      if (packet->stream[data_idx] == 0xC2) {
         term_encontrado = 1;
         break;
      }
   }

   /* Encontramos el terminador? */
   if (!term_encontrado) {
      /* Si no lo tenemos quiere decir que agotamos el espacio en el
       * paquete que nos dieron y no nos llego el final del paquete
       * que estamos leyendo.
       */
      return IZMDB_NOMEM;
   }

   /* Ajustamos el tama~o del paquete con lo que realmente se leyo */
   packet->size = data_idx + 1;

   return IZMDB_OK;
}

int izmdb_send (izmdb_handle *handle, unsigned char *data, int datasize) {
 /*
  * Envia al dispositivo manejado por HANDLE la cantidad de bytes DATASIZE
  * apuntados por DATA.
  *
  */

  struct izmdb_packet packet;
  int retval;

  IZ_DEBUG ("izmdb_send (handle: %p, data: %s, datasize: %d)\n", handle, data, datasize);

  if ( (retval = izmdb_pack(data, datasize, &packet)) != IZMDB_OK) {
     IZ_DEBUG ("izmdb_send: izmdb_pack() returned error (%d)\n", retval);
     return retval;
  }

  if ( (retval = izmdb_send_packet(handle, &packet)) != IZMDB_OK) {
     IZ_DEBUG ("izmdb_send: izmdb_send_packet() returned error (%d)\n", retval);
     return retval;
  }

  return IZMDB_OK;

}

int izmdb_receive (izmdb_handle *handle, unsigned char **databuf, int *datasize) {
 /*
  * Recibe en el buffer DATABUF de tama~o apuntado por DATASIZE
  * los datos contenidos en el siguiente paquete de comunicación disponible
  * en el dispositivo manejado por HANDLE.
  *
  * Regresa en el entero apuntado DATASIZE la cantidad de datos recibidos.
  *
  */

  struct izmdb_packet packet;
  int retval;

  /* Construimos el paquete. La cantidad maxima de datos que
   * tendremos que almacenar es el doble de la cantidad de datos
   * que esperamos recibir: si TODOS estuvieran escapados con
   * 0xCn 0x0n entonces esperariamos X bytes que al ser codificados
   * en la transmision serian 2X bytes. Mas 2 bytes para iniciador
   * y terminador de paquete.
   *
   * Desperdiciamos probalemente el 90% de los bytes direccionados? Si.
   * Nos importa? No. Son unos cuantos decenas o cientos de bytes. Este
   * comentario ya es mas grande que eso.
   */

  IZ_DEBUG ("izmdb_receive (handle: %p, databuf: %p, datasize: %p)\n", handle, databuf, datasize);

  packet.size   = (*datasize * 2) + 2;
  packet.stream = malloc(packet.size);

  if (packet.stream == NULL) {
     IZ_DEBUG ("izmdb_receive: malloc() failed\n", retval);
     return IZMDB_NOMEM;
  }

  if ((retval = izmdb_receive_packet(handle, &packet)) != IZMDB_OK) {
     IZ_DEBUG ("izmdb_receive: izmdb_receive_packet() returned error (%d)\n", retval);
     return retval;
  }

  if ( (retval = izmdb_unpack(&packet, databuf, datasize)) != IZMDB_OK) {
     IZ_DEBUG ("izmdb_receive: izmdb_unpack() returned error (%d)\n", retval);
     return retval;
  }

  return IZMDB_OK;

}
