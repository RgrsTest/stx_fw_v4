#! /usr/bin/python3

import atexit
import datetime
import os
import signal
import sys
import time
import json
from bitacora import Bitacora
from mdblib import Changer


class Daemon(object):
    """ Linux Daemon boilerplate. """
    
    def __init__(self, pid_file, path):
        self.stdout = path + '/logs/Changer.log'
        self.stderr = path + '/logs/Changer_err.log'
        self.path = path
        valores = json.load(open(self.path + "/config/changer_config.json"))
        self.estado = self.path + '/shared/changer/estado.json'
        self.compras = self.path + '/shared/changer/ventas.json'
        self.dinero = self.path +'/shared/changer/dinero.json'
        self.recarga = self.path + '/shared/changer/recargas.json'
        self.remanente = self.path  + '/shared/changer/remanente.json'
        self.pay = self.path + '/shared/changer/pagar.json'
        self.totalTubes = self.path + '/shared/changer/total_tubos.json'
        self.ventana = valores["ventanaTiempo"]
        self.tarifa = valores["tarifa"]
        
        self.pid_file = pid_file
        self.bitacora = Bitacora(path, 'Changer')
        self.changer = Changer(path)
        
        self.new_coin = self.path + '/shared/changer/newcoin.json'

    def del_pid(self):
        """ Delete the pid file. """
        os.remove(self.pid_file)

    def daemonize(self):
        """ There shined a shiny daemon, In the middle, Of the road... """
        # fork 1 to spin off the child that will spawn the deamon.
        if os.fork():
            sys.exit()

        # This is the child.
        # 1. cd to root for a guarenteed working dir.
        # 2. clear the session id to clear the controlling TTY.
        # 3. set the umask so we have access to all files created by the daemon.
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # fork 2 ensures we can't get a controlling ttd.
        if os.fork():
            sys.exit()

        # This is a child that can't ever have a controlling TTY.
        # Now we shut down stdin and point stdout/stderr at log files.

        # stdin
        with open('/dev/null', 'r') as dev_null:
            os.dup2(dev_null.fileno(), sys.stdin.fileno())

        # stderr - do this before stdout so that errors about setting stdout write to the log file.
        #
        # Exceptions raised after this point will be written to the log file.
        sys.stderr.flush()
        with open(self.stderr, 'a+b', 0) as stderr:
            os.dup2(stderr.fileno(), sys.stderr.fileno())

        # stdout
        #
        # Print statements after this step will not work. Use sys.stdout
        # instead.
        sys.stdout.flush()
        with open(self.stdout, 'a+b', 0) as stdout:
            os.dup2(stdout.fileno(), sys.stdout.fileno())

        # Write pid file
        # Before file creation, make sure we'll delete the pid file on exit!
        atexit.register(self.del_pid)
        pid = str(os.getpid())
        with open(self.pid_file, 'w+') as pid_file:
            pid_file.write('{0}'.format(pid))

    def get_pid_by_file(self):
        """ Return the pid read from the pid file. """
        try:
            with open(self.pid_file, 'r') as pid_file:
                pid = int(pid_file.read().strip())
            return pid
        except IOError:
            return


    def start(self):
        """
        Inicia el demonio
        """
        
        print("Instancia inicializada")
        if self.changer.start(self.path + "/config/changer_config.json"):     
            self.run()
        else:
            print("Error al inicializar la instancia")
            sys.exit(1)
        
        
    def stop(self):
        """ Stop the daemon. """
        print ("Stopping...")
        pid = self.get_pid_by_file()
        if not pid:
            print ("PID file {0} doesn't exist. Is the daemon not running?".format(self.pid_file))
            return

        # Time to kill.
        try:
            while 1:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            if 'No such process' in err.strerror and os.path.exists(self.pid_file):
                os.remove(self.pid_file)
            else:
                print (err)
                sys.exit(1)

    def restart(self):
        """ Restart the deamon. """
        self.stop()
        self.start()
    
    
    def run(self):
        """ Run main process """
        #Establecer la ventana de tiempo
        ventanaTiempo = datetime.datetime.now()
        self.bitacora.escribir("Inicializando")

        #Abrimos el ciclo que se ejecutara mientras este activo
        while True:
            try:
                self.bitacora.escribir("Validando ventana")

                #Si existe el archivo dinero entonces verificamos que no haya pasado el tiempo para depositar otra moneda
                if os.path.exists(self.dinero):
                    ventana = datetime.datetime.now() - ventanaTiempo
                    if ventana.seconds > self.ventana:
                        #Si se excede el tiempo entonces si crear un archivo remanente
                        dineiro = json.load(open(self.dinero, 'r'))
                        valores = {
                            "monto": dineiro,
                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d %H:%M:%S"),
                            "tipo": "VENTA_INCOMPLETA"
                        }
                        with open(self.remanente, 'a+') as texto:
                            json.dump(valores, texto)
                        os.remove(self.dinero)
                        ventanaTiempo = datetime.datetime.now()
                
                #Igualamos el valor de estado a 0, en caso de que no exista el archivo de estado
                estado = 0
                
                self.bitacora.escribir("Verificando si existe el archivo estado")
                if os.path.exists(self.estado):
                    #Si existe el archivo de estado, tomar el valor de estado
                    with open(self.estado, 'r') as estado_file:
                        estado = json.load(estado_file)
                        
                self.bitacora.escribir("El estado actual es: " + str(estado))
                if estado == 1:
                    #Si estado es igual a 1, activar el monedero para reciba monedas
                    #Activamos la recoleccion de monedas
                    self.changer.coin_type(1,0)
                    #Obtenemos las monedas ingresadas
                    ingresado = self.changer.poll_coin()
                    self.bitacora.escribir("Dinero Ingresado: " + str(ingresado))
                    totalIngresado = 0
                    #Verificamos si existe un archivo dinero para sumarlo con el ingresado
                    if os.path.exists(self.dinero):
                        with open(self.dinero, 'r') as dinero_file:
                            totalIngresado = json.load(dinero_file)

                    totalIngresado = totalIngresado + ingresado

                    self.bitacora.escribir("Total: " + str(totalIngresado))
                    if totalIngresado >= self.tarifa:
                        #Si el total ingresado es igual o mayor a la tarifa, entonces crear un archivo de venta
                        fecha = datetime.datetime.now()
                        valores = {
                            "monto": self.tarifa,
                            "fechaHora" : fecha.strftime("%Y-%m-%d %H:%M:%S"),
                            "tipo": "VENTA"
                        }
                        with open(self.compras, 'a+') as texto:
                            json.dump(valores,texto)
                        
                        restante = totalIngresado - self.tarifa
                        if restante > 0:
                            #Si existe dinero restante, sobreescribir el monto de dinero
                            with open(self.dinero, 'w+') as texto:
                                json.dump(restante,texto)
                            
                            ventanaTiempo = datetime.datetime.now()
                        else:
                            #Sino, borrar el archivo de dinero
                            os.path.exists(self.dinero)
                            os.remove(self.dinero)
                            ventanaTiempo = None
                    else:
                        #Si el monto ingresado no supera el valor de la tarifa entonces escribirlo en archivo dinero
                        if ingresado!= 0:
                            with open(self.dinero, 'w+') as texto:
                                    json.dump(totalIngresado, texto)
                            ventanaTiempo = datetime.datetime.now()
                
                elif estado == 2:
                    #Si el estado del validador es 2, activar la funcion para acumular dinero para recargar
                    if os.path.exists(self.recarga):
                        #Si existe un archivo de recarga, tomar el monto e igualar el total a ese monto
                        with open(self.recarga) as archivo:
                            total = json.load(archivo)
                            self.bitacora.escribir("Acumulado")
                            self.bitacora.escribir(total)
                    else:
                        #Sino, igualar el total a 0
                        total = 0
                    #Activar la funcion para recolectar monedas
                    self.changer.coin_type(1,0)
                    #Obtener las monedas ingresadas
                    ingresado = self.changer.poll_coin()
                    self.bitacora.escribir("Dinero Ingresado: " + str(ingresado))
                    if ingresado > 0:
                        #Si se ingreso algo, sumarlo y guardarlo en el archivo de recarga
                        total += ingresado
                        with open(self.recarga, 'w+') as archivo:
                            json.dump(total,archivo)
                            
                            
                        with open(self.new_coin, 'w+') as coindata:
                            json.dump(ingresado,coindata)
                    
                    #Obtener el monto total en los tubos para saber si se cuenta con dinero en caso de tener que regresar el dinero
                    tubesCoin = self.changer.tubes_coin()
                    with open(self.totalTubes, 'w+') as archivo:
                        json.dump(tubesCoin, archivo)
                        
                    """
                    if os.path.exists(self.pay):
                        #Si existe el archivo, leerlo y regresar el dinero que corresponde
                        self.bitacora.escribir("Pay Exist")
                        #self.changer.coin_type(0,0)
                        
                        total -= cantidad
                        
                        with open(self.recarga, 'w+') as archivo:
                            json.dump(total,archivo)
                        
                        
                        with open(self.pay, 'r') as pagar:
                            cantidad = json.load(pagar)
                            self.changer.pay_out_coin(cantidad)
                            self.bitacora.escribir("Regresar")
                            self.bitacora.escribir(cantidad)
                        
                        
                        
                        
                            
                        os.remove(self.pay)
                        self.bitacora.escribir("Se ha regresado el dinero")
                        self.bitacora.escribir(total)
                    """  

                elif estado == 3:
                    #Si el estado es igual a 3, activar las funciones para devolver el dinero
                    if os.path.exists(self.pay):
                        #Si existe el archivo, leerlo y regresar el dinero que corresponde
                        with open(self.pay, 'r') as pagar:
                            cantidad = json.load(pagar)
                            self.changer.pay_out_coin(cantidad)
                        
                        os.remove(self.pay)
                        self.bitacora.escribir("Se ha regresado el dinero")
                else:
                    #Si el valor de estado es igual a 0, entonces denegar la entrada de monedas
                    self.changer.coin_type(0,0)
            except NameError:
                #En caso de que se encuentre un Error de nombre, se reiniciara el proceso, ya que este error se provoca a proposito en caso de no recibir respuesta del monedero
                self.bitacora.escribir("Reiniciando")
                sys.exit(1)
            except Exception as e:
                self.bitacora.escribir("Error ejecutando el monedero:" + str(e))

            time.sleep(0.1)
            

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print ("Usage: {0} start|stop|restart".format(sys.argv[0]))
        sys.exit(2)
    
    filePath = os.path.abspath(os.path.realpath(__file__))
    filePath = filePath.split('/')
    camino = ""
    for i in filePath:
        if i != 'perifericos':
            camino += i + "/"
        else:
            break
    
    camino = camino[:len(camino)-1]
    
    daemon = Daemon('/tmp/changer_daemon.pid', camino)
    if 'start' == sys.argv[1]:
        daemon.start()
    elif 'stop' == sys.argv[1]:
        daemon.stop()
    elif 'restart' == sys.argv[1]:
        daemon.restart()    
    
    else:
        print ("Unknown command '{0}'".format(sys.argv[1]))
        sys.exit(2)