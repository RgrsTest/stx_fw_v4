from bitacora import Bitacora

class DataStructure:
    """
    Clase para convertir los datos enviados den hexadecimales a todo tipo de datos
    """
    def __init__(self, path):
        self.bitacora = Bitacora(path, "Validador")
    ##############################################################################################################################
    #Funciones para Hacer Calculos ##############################  Inicio  #######################################################
    ##############################################################################################################################

    def calcularFechas(self,valor, tam = 2):
        """
        Funcion para calcular fechas
        Retorna un string con el formato "YYYY-MM-DD HH:mm:ss" o "YYYY-MM-DD"
        valor: int Un entero base 10 que sera convertido a fecha
        tam: int Numero de bytes ingresados, si se ingresan 2 solo se retornara "YYYY-MM-DD", si es 4 se retornara "YYYY-MM-DD HH:mm:ss"
        """
        #self.bitacora.escribir("Calculando Fechas:")
        
        binary = bin(valor)
        fecha =""
        if tam == 2:
            anio = valor >> 9
            mes = valor >> 5
            mes = mes & 0x0F
            dia = valor & 0x1F
            fecha = str(anio+1990) + "-" + str(mes).zfill(2) + "-" + str(dia).zfill(2)
        elif tam == 4:
            anio = valor >> 25
            mes = valor >> 21
            mes = mes & 0x0F
            dia = valor >> 16
            dia = dia & 0x1F
            hora = valor >> 11
            hora = hora & 0x1F
            minu = valor >> 5
            minu = minu & 0x3F
            seg = valor & 0x1F
            fecha = str(anio+1990) + "-" + str(mes).zfill(2) + "-" + str(dia).zfill(2) + " " + str(hora).zfill(2) + ":" + str(minu).zfill(2) + ":" + str((seg*2)).zfill(2)
        #self.bitacora.escribir(fecha)
        return fecha

    def convertirFecha(self, fecha, hora = False):
        """
        Convierte una fecha a hexadecimal
        Retornma un string con un numero binario de longitud de 16 o 32 bits (010101.....)
        fecha: string La fecha que sera convertida, en formato "YYYY-MM-DD"
        hora: string La hora que sera convertida, en formato "HH:MM:SS" si es False se ignora
        """
        #self.bitacora.escribir("Convirtiendo Fechas:")
        if hora:
            horalist = hora.split(":")
            horabin= f"{int(horalist[0]):05b}"
            minutobin= f"{int(horalist[1]):06b}"
            segundobin= f"{int(int(horalist[2])/2):05b}"
            fechalist = fecha.split("-")
            aniobin = f"{(int(fechalist[0])-1990):07b}"
            mesbin = f"{int(fechalist[1]):04b}"
            diabin = f"{int(fechalist[2]):05b}"
            fechahoraBin = aniobin + mesbin+ diabin + horabin + minutobin + segundobin
            result = f"{int(fechahoraBin,2):08x}"
            
            return result
        else:
            fechalist = fecha.split("-")
            aniobin = f"{(int(fechalist[0])-1990):07b}"
            mesbin = f"{int(fechalist[1]):04b}"
            diabin = f"{int(fechalist[2]):05b}"
            fechahoraBin = aniobin + mesbin+ diabin
            result = f"{int(fechahoraBin,2):04x}"
            #self.bitacora.escribir(result)
            return result

    ##############################################################################################################################
    #Funciones para Hacer Calculos ##############################  Fin  ##########################################################
    ##############################################################################################################################

    def readEmisionFile(self,datos):
        """
        Funcion para transformar datos al ficheco Emision_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Emision")
        if datos:
            contenido = {
                "pais" : int(datos[0]+datos[1],16),
                "serialMedioPago" : datos[2]+datos[3]+datos[4]+datos[5],
                "fechaFinValidezMedioPago" : self.calcularFechas(int(datos[6]+datos[7],16),2),
                "idRed" : datos[8] + datos[9] + datos[10],
                "idPropietario" : datos[11] + datos[12],
                "idRedEmisor" : datos[13] + datos[14] + datos[15],
                "idDistribuidor" : datos[16] + datos[17],
                "idSAM" : datos[18] + datos[19] + datos[20] + datos[21] + datos[22] + datos[23] + datos[24],
                "idAlgSeguridad" : datos[25] + datos[26],
                "idVersionLlaves" : datos[27]
            }
            return contenido
        #self.bitacora.escribir("No se recibieron Datos")
        return False
        
    def readEntornoFile(self, datos):
        """
        Funcion para transformar datos al ficheco Entorno_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Entorno")
        if datos:
            contenido = {
                "versionAplicacion" : datos[0],
                "idRed" : datos[1] + datos[2] + datos[3],
                "fechaFinValidezAplicacion" : self.calcularFechas(int(datos[4]+datos[5],16),2)
            }
            return contenido
        #self.bitacora.escribir("No se recibieron datos")
        return False
            

    def readEstadoAplicacion(self, datos):
        """
        Funcion para transformar datos al ficheco EstadoAplicacion_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Estado Aplicacion")
        if datos:
            contenido = {
                
                "estadoAplicacion" : int(datos[0],16),
                "consecutivoAplicacion" : int(datos[1] + datos[2] + datos[3],16),
                "numeroAccionAplicada" : int(datos[4],16)
            }
            return contenido
        #self.bitacora.escribir("No se recibieron datos")
        return False


    def readPerfil(self, datos):
        """
        Funcion para transformar datos al ficheco Perfil_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Perfil")
        if datos:
            contenido = {
                "fechaNacimientoUsuario" : self.calcularFechas(int(datos[0] + datos[1] + datos[2] + datos[3], 16),4),
                "codigoPerfil" : int(datos[4],16),
                "fechaFinPerfil" : self.calcularFechas(int(datos[5] + datos[6], 16),2),
                "nombreUsuario" : bytes.fromhex("".join(datos[7:46])).decode('utf8'),
                "credencialUsuario" : bytes.fromhex("".join(datos[47:71])).decode('utf8'),
            }
            return contenido
        #self.bitacora.escribir("No se recibieron datos")
        return False

    def readFuncionario(self, datos):
        """
        Funcion para transformar datos al ficheco Funcionario_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Funcionario")
        if datos:
            contenido = {
                "datosPerfilFuncionario" : "".join(datos)
            }
            return contenido
        #self.bitacora.escribir("No se recibieron datos")
        return False

    def readListaProductos(self, datos):
        """
        Funcion para transformar datos al ficheco Productos_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Productos")
        #Este archivo es una lista y se tiene que iterar en los datos
        self.bitacora.escribir(len(datos))
        if datos:
            i = 0
            productos = []
            while i < len(datos)-9:
                contenido = {
                    "idProducto" : datos[i] + datos[i+1],
                    "refContrato" : datos[i+2],
                    "refServicio" : datos[i+3],
                    "refValor" : datos[i+4],
                    "prioridadProducto": int(datos[i+5],16)
                }
                productos.append(contenido)
                i = i+6
                
                
            return productos
        #self.bitacora.escribir("No se recibieron datos")
        return False

    def readContrato(self, datos):
        """
        Funcion para transformar datos al ficheco Contrato_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Contrato")
        if datos:
            contenido = {
                "idRedProducto" : datos[0] + datos[1] + datos[2],
                "idDistribuidorProducto" : datos[3] + datos[4],
                "idProducto" : datos[5] + datos[6],
                "serialProducto" : datos[7] + datos[8] + datos[9] + datos[10],
                "precioProducto" : int(datos[11] + datos[12],16),
                "unidadValorProducto" : int(datos[13]),
                "minimoValor" : int.from_bytes(bytes.fromhex(datos[14] + datos[15] + datos[16] + datos[17]), byteorder="big", signed=True),
                "maximoValor" : int.from_bytes(bytes.fromhex(datos[18] + datos[19] + datos[20] + datos[21]), byteorder="big", signed=True),
                "numeroReactivacionProducto" : int(datos[22] + datos[23],16),
                "numeroAccionAplicadaProducto" : int(datos[24] + datos[25],16),
                "fechaDistribucion" : self.calcularFechas(int(datos[26] + datos[27] + datos[28] + datos[29],16),4),
                "idSAMDistribucion" : datos[30] + datos[31] + datos[32] + datos[33] + datos[34] + datos[35] + datos[36],
                "idDispositivoDistribucion": datos[37] + datos[38],
                "inicioValidezProducto" : self.calcularFechas(int(datos[39] + datos[40] + datos[41] + datos[42], 16),4),
                "finValidezProducto" : self.calcularFechas(int(datos[43] + datos[44] + datos[45] + datos[46],16),4),
                "inicioValidezDia" : int(datos[47] + datos[48],16),
                "finValidezDia" : int(datos[49] + datos[50],16),
                "diasRestringidos" : int(datos[51],16),
                "maxViajesSemana" :  datos[52] + datos[53] + datos[54] + datos[55],
                "tiempoPassback" : datos[56] + datos[57],
                "passbackPermitidos" : datos[58],
                "tiempoTransbordo" : int(datos[59] + datos[60],16),
                "transbordosPermitidos": datos[1]
            }
            return contenido
        #self.bitacora.escribir("No se recibieron datos")    
        return False


    def readServicio(self,datos):
        """
        Funcion para transformar datos al ficheco Servicio_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Servicio")
        if datos:
            contenido = {
                "estadoProducto" : int(datos[0],16),
                "numeroSemanaAnio" : int(datos[1],16),
                "numeroViajesSemana" : int(datos[2] + datos[3] + datos[4] + datos[5],16),
                "numeroActualUsos" : int(datos[6] + datos[7],16),
                "fechaUltimoDebito" : self.calcularFechas(int(datos[8] + datos[9] + datos[10] + datos[11],16),4),
                "idEntidadUltimoDebito" : datos[12] + datos[13],
                "idRutaEstacionUltimoDebito" : datos[14] + datos[15],
                "idDispositivoUltimoDebito" : datos[16] + datos[17]
            }
            return contenido
        #self.bitacora.escribir("No se recibieron datos")
        return False
        
    def readValue(self, datos):
        """
        Funcion para transformar datos al ficheco Valor_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Valor")
        if datos:
            return int.from_bytes(bytes.fromhex(datos), byteorder="big", signed=True)

        #self.bitacora.escribir("No se recibieron datos")
        return False

    def readRecords(self, datos):
        """
        Funcion para transformar datos al ficheco Eventos_EF
        Retorna un diccionario con todas las propiedades del fichero o False en caso de error
        datos: list Una lista con todos los bytes represantados en hexadecimal (00,00)
        """
        #self.bitacora.escribir("Archivo Eventos")
        #Este es un archivo ciclico, en la respuesta dice cuantos registros se obtuvieron
        if datos:
            #Se toma la cantidad de registros
            cantRegistros = int(datos[0] + datos[1], 16)
            resultado = []
            byteNum = 2

            #Se itera en los datos
            while cantRegistros > 0:
                registro = {
                    "idProducto" : datos[byteNum] + datos[byteNum + 1],
                    "punteroProducto" : datos[byteNum + 2],
                    "entidad" : datos[byteNum + 3] + datos[byteNum + 4],
                    "fechaHoraEvento" : self.calcularFechas(int(datos[byteNum + 5] + datos[byteNum + 6] + datos[byteNum + 7] + datos[byteNum + 8],16),4),
                    "tipoEvento" : int(datos[byteNum + 9],16),
                    "montoEvento" : int(datos[byteNum + 10] + datos[byteNum + 11],16),
                    "consecutivoEvento" :int(datos[byteNum + 12] + datos[byteNum + 13] + datos[byteNum + 14],16),
                    "idSAM" : datos[byteNum + 15] + datos[byteNum + 16] + datos[byteNum + 17] + datos[byteNum + 18] + datos[byteNum + 19] + datos[byteNum + 20] + datos[byteNum + 21],
                    "consecutivoSAM" : datos[byteNum + 22] + datos[byteNum + 23] + datos[byteNum + 24] + datos[byteNum + 25] + datos[byteNum + 26] + datos[byteNum + 27] + datos[byteNum + 28] + datos[byteNum + 29],
                    "idDispositivo" : datos[byteNum + 30] + datos[byteNum + 31],
                    "idUbicacion" : datos[byteNum + 32] + datos[byteNum + 33],
                    "tipoTransporte" : int(datos[byteNum + 34],16),
                    "idRuta_Estacion" : datos[byteNum + 35] + datos[byteNum + 36],
                    "numeroTransbordos" : datos[byteNum + 37],
                    "limiteTransbordo" : self.calcularFechas(int(datos[byteNum + 38] + datos[byteNum + 39] + datos[byteNum + 40] + datos[byteNum + 41],16),4),
                    "numeroPassback" : datos[byteNum + 42],
                    "motivoDevolucion" : datos[byteNum + 43],
                    "idTipoDispositivo" : int(datos[byteNum + 44] + datos[byteNum + 45],16)
                }
                byteNum += 64
                cantRegistros -= 1
                resultado.append(registro)
            return resultado
        #self.bitacora.escribir("No se recibieron datos")    
        return False

    def convertirEvento(self,datos):
        """
        Funcion para convertir los datos en una cadena de bytes que sera escrita en el archivo Eventos_EF
        Retorna un string de hexadecimales que seran escritos (000000...)
        datos: dict Un diccionario cuyas propiedades seran convertidas a hexadecimal
        """
        #self.bitacora.escribir("Convertir evento")
        escritura = [
            #IdProducto
            datos["idProducto"],  #2
            #Puntero Producto
            datos["refContrato"], #1
            #Identidad
            datos["entidad"], #2
            #FechaHoraEvento
            self.convertirFecha(datos["fechaHoraEvento"].strftime("%Y-%m-%d"), datos["fechaHoraEvento"].strftime("%H:%M:%S")), #4
            #TipoEvento
            str(f"{datos['tipoEvento']:02x}"), # 1
            #MontoEvento
            str(f"{datos['montoEvento']:04x}"), # 2
            #ConsecutivoEvento
            str(f"{datos['consecutivoEvento']:06x}"), # 3
            #IdSAM
            datos["idSAM"], #7
            #ConsecutivoSAM
            datos["consecutivoSAM"], #8
            #IdDispositivo
            datos["idDispositivo"], #2
            #IdUbicacion
            datos["idUbicacion"], # 2
            #TipoTransporte
            datos["tipoTransporte"], # 1
            #IdRuta_Estacion
            datos["idRuta_Estacion"], # 2
            #Numero Transbordos
            str(f"{datos['numeroTransbordos']:02x}"), #1
            #LimiteTransbordos
            self.convertirFecha(datos["limiteTransbordo"].strftime("%Y-%m-%d"), datos["limiteTransbordo"].strftime("%H:%M:%S")), # 4
            #NumeroPassbacks
            str(f"{datos['numeroPassbacks']:02x}"), #1
            #MotivoDevolucion
            str(f"{datos['motivoDevolucion']:02x}"), #1
            #IdTipoDispositivo
            datos["idTipoDispositivo"] # 2
        ]
        #RFU
        escritura.append("00"*18)
        result = "".join(escritura)
        #self.bitacora.escribir("Datos convertidos")
        #self.bitacora.escribir(result)
        return result
    

    def convertirServicio(self, producto, datosLocales, fecha):
        """
        Funcion para convertir los datos en una cadena de bytes que sera escrita en el archivo Servicio_EF
        Retorna un string de hexadecimales que seran escritos (000000...)
        producto: dict Producto del que sera escrito el servicio
        datosLocales: dict Datos locales del aparato
        fecha: Object(datetime) objeto del que se tomaran los valores para convertir
        """
        #self.bitacora.escribir("Convirtiendo Servicio")
        if int(fecha.strftime("%U")) != producto["servicio"]["numeroViajesSemana"] :
            viajesSemana = 1
        else:
            viajesSemana = producto["servicio"]["numeroViajesSemana"] + 1
        datos = [
            #Estado del Servicio
            str(f"{producto['servicio']['estadoProducto']:02x}"),
            #Numero de Semana
            str(f"{int(fecha.strftime('%U')):02x}"),
            #Viajes realizados en la semana
            str(f"{viajesSemana:08x}"),
            #Numero de usos
            str(f"{(producto['servicio']['numeroActualUsos'] + 1):04x}"),
            #Fecha del ultimo debito
            str(self.convertirFecha(fecha.strftime("%Y-%m-%d"), fecha.strftime("%H:%M:%S"))),
            #EntidadUltimoDebito
            datosLocales["entidad"],
            #ID de ruta o estacsion
            datosLocales["idRuta_Estacion"],
            #ID de dispositivo
            datosLocales["idDispositivo"]
        ]

        datos.append("00"*5)
        result = "".join(datos)
        #self.bitacora.escribir("Datos convertidos")
        #self.bitacora.escribir(result) 
        return result

    