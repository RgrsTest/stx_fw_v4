import subprocess

def obtenerPuerto(interfaz):
    """
    Funcion para obtener el puerto en el que se ha conectado el dispositivo
    Retorna la ruta completa al archivo del dispositivo
    interfaz: string Valor donde se conecto el dispositivo en el HUB
    """
    resultado = subprocess.check_output("ls /dev/serial/by-id/*FTD* | head -n 1", shell=True)
    resultado = resultado.decode('latin1')
    resultado = resultado.strip()
    cortes = resultado.split('/')
    puerto = cortes[-1].split('-')
    salid = "/dev/serial/by-id/" + puerto[0] + "-" + puerto[1] + "-" + interfaz + "-" +puerto[3]
    return salid