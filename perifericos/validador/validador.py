import atexit
from datetime import datetime
import os
import signal
import sys
import time
import json

from validadorlib import Validador
from bitacora import Bitacora


class Daemon(object):
    """ Linux Daemon boilerplate. """
    
    def __init__(self, pid_file, path):
        self.stdout = path  + '/logs/Validador.log'
        self.stderr = path + '/logs/Validador_err.log'
        self.pid_file = pid_file
        self.datos = {}
        self.path = path
        self.eventos = path + "/shared/validador/eventos.json"
        self.estado = path + "/shared/validador/estado.json"
        self.recargas = path + "/shared/validador/recargas.json"
        self.monto = path + "/shared/validador/monto.json"
        self.tarjeta = path + "/shared/validador/tarjeta.json"
        self.fallidas = path + "/shared/validador/fallidas.json"
        self.informacion = path + "/shared/validador/lectura.json"
        self.bitacora = Bitacora(path,"Validador")
        self.eventlog = Bitacora(path, "Validador_evt")
        
        
        self.coin = path + '/shared/changer/newcoin.json'
        
        self.changer_accum = path + '/shared/changer/recargas.json'
        
        self.operacion = path + "/shared/config/operacion.json"
        
        self.coin_return = path + "/shared/validador/coin_return.json"
        
        self.display_accum = path + '/shared/display/monto.json'
        
        self.montoLapr = path + "/shared/validador/montoLapr.json"
        

    def del_pid(self):
        """ Delete the pid file. """
        os.remove(self.pid_file)

    def daemonize(self):
        """ There shined a shiny daemon, In the middle, Of the road... """
        # fork 1 to spin off the child that will spawn the deamon.
        if os.fork():
            sys.exit()

        # This is the child.
        # 1. cd to root for a guarenteed working dir.
        # 2. clear the session id to clear the controlling TTY.
        # 3. set the umask so we have access to all files created by the daemon.
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # fork 2 ensures we can't get a controlling ttd.
        if os.fork():
            sys.exit()

        # This is a child that can't ever have a controlling TTY.
        # Now we shut down stdin and point stdout/stderr at log files.

        # stdin
        with open('/dev/null', 'r') as dev_null:
            os.dup2(dev_null.fileno(), sys.stdin.fileno())

        # stderr - do this before stdout so that errors about setting stdout write to the log file.
        #
        # Exceptions raised after this point will be written to the log file.
        sys.stderr.flush()
        with open(self.stderr, 'a+b', 0) as stderr:
            os.dup2(stderr.fileno(), sys.stderr.fileno())

        # stdout
        #
        # Print statements after this step will not work. Use sys.stdout
        # instead.
        sys.stdout.flush()
        with open(self.stdout, 'a+b', 0) as stdout:
            os.dup2(stdout.fileno(), sys.stdout.fileno())

        # Write pid file
        # Before file creation, make sure we'll delete the pid file on exit!
        atexit.register(self.del_pid)
        pid = str(os.getpid())
        with open(self.pid_file, 'w+') as pid_file:
            pid_file.write('{0}'.format(pid))

    def get_pid_by_file(self):
        """ Return the pid read from the pid file. """
        try:
            with open(self.pid_file, 'r') as pid_file:
                pid = int(pid_file.read().strip())
            return pid
        except IOError:
            return


    def start(self):
        """
        Incia el demonio
        """    
        print("Instancia Validador inicializada")
        self.validador = Validador()
        if self.validador.inicializar(self.path):
            self.run()
        else:
            print("Error al inicializar la instancia")
            sys.exit(1)
        
    def stop(self):
        """ Stop the daemon. """
        print ("Stopping...")
        pid = self.get_pid_by_file()
        if not pid:
            print ("PID file {0} doesn't exist. Is the daemon not running?".format(self.pid_file))
            return

        
        # A matar el proceso
        try:
            while 1:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            if 'No such process' in err.strerror and os.path.exists(self.pid_file):
                os.remove(self.pid_file)

            else:
                print (err)
                sys.exit(1)
    
    
    def restart(self):
        """ Restart the deamon. """
        self.stop()
        self.start()

    def run(self):
        """ Intenta realizar compras """
        #Inicializamos el stado en cero para que no haga nada hasta que el backend de la orden
        estado = 0
        
        
        
        #Bucle principal
        while True:
            #Si existe el archivo de estado entonces que lea el estado
            if os.path.exists(self.estado):
                with open(self.estado, 'r') as estado_file:
                    estado = json.load(estado_file)

            #Si estado es 1 entonces intenta hacer validaciones
            if estado == 1:
                #Verifica que el archivo de validaciones no exista, si existe entonces no hace nada
                if not os.path.exists(self.eventos):
                    self.bitacora.escribir("Iniciando Proceso")
                    respuesta = self.validador.compra()
                    if respuesta["codigo"] != "NO_TARJETA":
                        self.bitacora.escribir("Escribiendo archivo")
                        self.eventlog.escribir("Respuesta de validacion:")
                        self.eventlog.escribir(respuesta)
                        json.dump(respuesta, open(self.eventos, 'w+'))
                    
            #Si el estado es 2, entonces hace la funcion para leer la tarjeta de informacion
            elif estado == 2:
                if not os.path.exists(self.tarjeta):
                    self.bitacora.escribir("Buscando Tarjeta")
                    tarjeta = self.validador.buscarTarjeta()
                    if tarjeta["codigo"] != "NO_TARJETA" and tarjeta["codigo"] != "TARJETA_ERROR":
                        with open(self.tarjeta, 'w+') as tarjeta_file:
                            self.eventlog.escribir("Respuesta de busqueda de tarjeta:")
                            self.eventlog.escribir(tarjeta)
                            json.dump(tarjeta, tarjeta_file)
                #Funcion para tarjetas que escribiran informacion
                if os.path.exists(self.tarjeta) and not os.path.exists(self.eventos) and not os.path.exists(self.fallidas):
                    
                    #self.bitacora.escribir("Iniciando Recarga")
                    self.bitacora.escribir("No existe eventos")
                    
                    #Revisa archivo de monedas para rechazar el ingreso cuando se calcula que llegara al limite de la recarga
                    
                    if os.path.exists(self.coin):
                        
                         self.bitacora.escribir("Hay monedas")
                         coin_recarga = 0
                        
                         with open(self.coin, 'r') as coin_file:
                              self.bitacora.escribir("Coins")
                              coin_recarga = json.load(coin_file)
                              self.bitacora.escribir(coin_recarga)
                              
                         with open(self.changer_accum, 'r') as acum:
                              self.bitacora.escribir("Acumulado")
                              acumulado = json.load(acum)
                              self.bitacora.escribir(acumulado)     
                             
                         
                         proximo_saldo = 0
                         max_saldo  = 25000
                       
                         if os.path.exists(self.tarjeta):
                            
                             with open(self.operacion,'r') as operacion_file:
                                  operacion_info = json.load(operacion_file)
                                 
                             max_saldo  = operacion_info["perfiles"][0]["productos"]["4d31"]["maximoValor"]
                             self.bitacora.escribir(max_saldo)
                            
                         if os.path.exists(self.tarjeta):
                            
                             with open(self.tarjeta, 'r') as tarjeta_file:                                
                                  card_info = json.load(tarjeta_file)
                                 
                             self.bitacora.escribir(card_info["saldo"])
                             proximo_saldo = card_info["saldo"] + acumulado
                             
                             self.bitacora.escribir("Saldo Proximo")
                             self.bitacora.escribir(proximo_saldo)
                            

                    
                        
                        
                         if proximo_saldo > max_saldo:
                             self.bitacora.escribir("Maximo Saldo Detectado")
                              
                            
                             with open(self.coin_return, 'w+') as amount:                           
                                 json.dump(coin_recarga, amount)
                                 
                             
                             acumulado -= coin_recarga
                         
                             with open(self.display_accum, 'w+') as damount:                           
                                 json.dump(acumulado, damount)
                                 
                             with open(self.changer_accum, 'w+') as camount:                           
                                 json.dump(acumulado, camount)
                            
                         os.remove(self.coin)
                      
                    
                    
                    if os.path.exists(self.monto):
                        self.bitacora.escribir("Hay monto de recarga")
                        with open(self.monto, 'r') as monto_file:
                            recarga = json.load(monto_file)
                        
                        resultado = self.validador.recargar(recarga)
                        self.eventlog.escribir(str(resultado))
                        if resultado["codigo"] in ["RECARGA_MONEDERO", "RECARGA_CREDITO", "RECARGA_MIXTA"]:
                            with open(self.recargas, 'w+') as evento:
                                json.dump(resultado, evento)
                            
                            os.remove(self.monto)
                           
                            self.bitacora.escribir("Escribiendo archivos")
                        else:
                            with open(self.fallidas, 'w+') as fail:
                                json.dump(recarga, fail)
                            self.bitacora.escribir("Error en la recarga, borrando tarjeta")
                            os.remove(self.monto)
                            
                            
                    """
                    RECARGA LAPR
                    """
                            
                    if os.path.exists(self.montoLapr):
                        self.bitacora.escribir("Hay monto de recarga LAPR")
                        with open(self.montoLapr, 'r') as montoLapr_file:
                            recargaLapr = json.load(montoLapr_file)
                        
                        resultado = self.validador.recargar(recargaLapr)
                        self.eventlog.escribir(str(resultado))
                        if resultado["codigo"] in ["RECARGA_MONEDERO", "RECARGA_CREDITO", "RECARGA_MIXTA"]:
                            with open(self.recargas, 'w+') as evento:
                                json.dump(resultado, evento)
                            
                            #os.remove(self.montoLapr)
                           
                            self.bitacora.escribir("Escribiendo archivos")
                        else:
                            with open(self.fallidas, 'w+') as fail:
                                json.dump(recargaLapr, fail)
                            self.bitacora.escribir("Error en la recarga, borrando tarjeta")
                            #os.remove(self.montoLapr)
                            
                          
                            
            elif estado == 3:
                self.bitacora.escribir("Iniciando Lectura de Mifare")
                if not os.path.exists(self.informacion):
                    tarjeta = self.validador.leerAccesos()
                    if tarjeta:
                        json.dump(tarjeta, open(self.informacion, 'w+'))    
                time.sleep(0.1)
            #Detener el proceso
            time.sleep(0.1)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print ("Usage: {0} start|stop|restart".format(sys.argv[0]))
        sys.exit(2)

    filePath = os.path.abspath(os.path.realpath(__file__))
    filePath = filePath.split('/')
    camino = ""
    for i in filePath:
        if i != 'perifericos':
            camino += i + "/"
        else:
            break
    
    camino = camino[:len(camino)-1]

    daemon = Daemon('/tmp/val_demon.pid', camino)
    if 'start' == sys.argv[1]:
        daemon.start()
    elif 'stop' == sys.argv[1]:
        daemon.stop()
    elif 'restart' == sys.argv[1]:
        daemon.restart()
    else:
        print ("Unknown command '{0}'".format(sys.argv[1]))
        sys.exit(2)