import sqlite3
from bitacora import Bitacora

class Lista:
    """
    La base de datos para las listas consta de tres tablas, una por cada lista
    La tabla de acciones lam:
    | id | uid | fecha_hora | no_consecutivo | accion | aplicada | created_at | updated_at

    La tabla de acciones LAP_R
    | id | uid | fecha_hora | no_accion_aplicada | monto_recarga | id_producto | id_sam | created_at | updated_at

    La tabla de acciones LAP_V
    | id | uid | fecha_hora | no_accion_aplicada | accion | id_producto | id_sam | created_at | updated_at

    """
    def __init__(self, path, url):
        self.conection = sqlite3.connect(url)
        self.bitacora = Bitacora(path, "Database")

    
    def consultarLAM(self, card):
        """
        Funcion para consultar las acciones sobre el medio de pago
        card: string El identificador de la tarjeta (UID)
        actual: int Estado actual del contador de acciones
        Retorna una lista con los todos los registros, cada registro esta representado por una tupla [(id, no_consecutivo, accion)]
        """
        try:
            cursor = self.conection.cursor()
            query = "SELECT id, no_consecutivo_accion, accion FROM listas_lam WHERE uid = '" + card + "' ORDER BY id ASC"
            self.bitacora.escribir(query)
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            self.bitacora.escribir("Erro al consultar la base de datos: " + str(e))
            return []
    
    def consultarLAP_R(self, card):
        """
        Funcion para solictar las recargas remotas de producto
        card: string El identificador de la tarjeta (UID)
        producto: string Identifcicador del producto
        actual: int Estado actual del contador de acciones 
        Retorna una lista con los todos los registros, cada registro esta representado por una tupla [(id, no_accion_aplicada, monto_recarga)]
        """
        try:
            cursor = self.conection.cursor()
            query = "SELECT id, numero_accion_aplicada, monto, id_producto FROM listas_lap_r WHERE uid = '" + card  + "' ORDER BY numero_accion_aplicada ASC" 
            #query = "SELECT * FROM listas_lap_r "
            self.bitacora.escribir(query)
            cursor.execute(query)
            result = cursor.fetchall()
            
            return result
        except Exception as e:
            self.bitacora.escribir("Erro al consultar la base de datos: " + str(e))
            return []
    
    def deleteAll_LAP_R(self):
        try:
            cursor = self.conection.cursor()
            #query = "SELECT id, no_accion_aplicada, monto_recarga FROM listas_lap_r WHERE uid = '" + card + "' AND id_producto = '" + producto + "' AND numero_accion_aplicada > "+ str(actual) + " ORDER BY no_accion_aplicada ASC"
            query = "DELETE FROM listas_lap_r "
            self.bitacora.escribir(query)
            cursor.execute(query)
            result = cursor.fetchall()
            
            return result
        except Exception as e:
            self.bitacora.escribir("Erro al consultar la base de datos: " + str(e))
            return []
    
    def consultarLAP_V(self, card):
        """
        Funcion para solictar las acciones de validacion del producto
        card: string El identificador de la tarjeta (UID)
        producto: string Identifcicador del producto
        actual: int Estado actual del contador de acciones 
        Retorna una lista con los todos los registros, cada registro esta representado por una tupla [(id, no_accion_aplicada, accion)]
        """
        try:
            cursor = self.conection.cursor()
            query ="SELECT id, id_producto, no_accion_aplicada FROM listas_lap_v WHERE uid = '" + card + "' ORDER BY no_accion_aplicada ASC"
            self.bitacora.escribir(query)
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            self.bitacora.escribir("Erro al consultar la base de datos: " + str(e))
            return []

    def confirmarLAM(self, id):
        """
        Funcion para verificar que una accion lam fue implementada
        id: int Id de la accion que fue implementada
        Retorna True si el query se ejecuto con exito, False en caso contrario
        """
        try:
            cursor = self.conection.cursor()
            query ="UPDATE listas_lam SET aplicada = 1 WHERE id = " + str(id)
            self.bitacora.escribir(query)
            cursor.execute(query)
            self.conection.commit()
            return True
        except Exception as e:
            self.bitacora.escribir("Erro al consultar la base de datos: " + str(e))
            return False

    def confirmarLAP_R(self, id, idsam):
        """
        Funcion para verificar que una recarga remota fue realizada en un producto
        id: int Id de la recarga que fue aplicada
        idsam: string Id de SAM
        Retorna True si el query se ejecuto con exito, False en caso contrario
        """
        try:
            cursor = self.conection.cursor()
            query = "UPDATE listas_lap_r SET id_sam = '" + idsam + "' WHERE id = " + str(id)
            self.bitacora.escribir(query)
            cursor.execute(query)
            self.conection.commit()
            return True
        except Exception as e:
            self.bitacora.escribir("Error al consultar la base de datos: " + str(e))
            return False
    
    def confirmarLAP_V(self, id, idsam):
        """
        Funcion para verificar que una accion fue realizada en un producto
        id: int Id de la accion que fue aplicada
        idsam: string Id de SAM
        Retorna True si el query se ejecuto con exito, False en caso contrario
        """
        try:
            cursor = self.conection.cursor()
            query ="UPDATE listas_lap_v SET id_sam = '" + idsam + "' WHERE id = " + str(id)
            self.bitacora.escribir(query)
            cursor.execute(query)
            self.conection.commit()
            return True
        except Exception as e:
            self.bitacora.escribir("Erro al consultar la base de datos: " + str(e))
            return False
        
        
    
