from time import sleep
from coupler import Coupler
from dataux import DataStructure
from bitacora import Bitacora
from database import Lista
import datetime
import json
import puerto

class Validador:
    """
    Clase que controla los procesos que se hacen con el validador
    """
    #Declaracion de Variables
    tiposEvento = None
    dispositivos = None
    transportes = None
    llaves = None
    app = None
    archivosInformativos = None
    distribuidorAplicacion = None
    versionSeguridad = None
    versionAplicacion = None
    redes = None
    informacionLocal = None
    perfiles = None
    productos = None
    returnCodesCompra = None
    returnCodesRecarga = None
    idSam = None
    consecutivoSam = None
    matrizTransbordos = None
    mifareKey = None

    #Instanciamos la bitacora, la clase acoplador y la estructura de datos
    bitacora = None
    coupler = None
    estructura = None

    #Inicializamos la tarjeta como un diccionario vacio
    card = {}
    

    def inicializar(self, path):
        """ Funcion para inicializar si las configuraciones han cambiado
            Retorna True si la inicializacion fue exitosa, False en caso contrario
            path: string La ruta de la carpeta raiz
        """
    
        datos = json.load(open(path + "/config/val_config.json", 'r'))
        mapping = json.load(open(path + "/shared/config/mapping.json", 'r'))
        operacion = json.load(open(path + "/shared/config/operacion.json", 'r'))
        identidad = json.load(open(path + "/shared/config/identidad.json", 'r'))
        self.app = bytes.fromhex(mapping["app"])
        self.archivosInformativos = mapping["files"]
        self.distribuidorAplicacion = mapping["distribuidorAplicacion"]
        self.versionSeguridad = mapping["versionSeguridad"]
        self.versionAplicacion = mapping["versionAplicacion"]
        self.redes = mapping["redes"]
        self.informacionLocal = identidad
        self.perfiles = operacion["perfiles"]
        self.productos = operacion["productos"]
        self.llaves = mapping["keys"]
        self.padding = mapping["padding"]
        self.returnCodesRecarga = datos["returnCodesRecarga"]
        self.returnCodesCompra = datos["returnCodesCompra"]
        self.matrizTransbordos = operacion["matrizTransbordos"]
        self.mifareKey = datos["mifareKey"]
        
        self.montoLapr = path + "/shared/validador/montoLapr.json"
        self.estado = path + "/shared/validador/estado.json"
        self.documentoPantalla = path + "/shared/display/pantalla.json" 
        
        #Instanciamos la bitacora, la clase acoplador y la estructura de datos
        self.bitacora = Bitacora(path,"Validador")
        inter = puerto.obtenerPuerto(datos["port"])
        self.coupler = Coupler(path,inter)
        self.estructura = DataStructure(path)
        self.database = Lista(path,path + datos["database"])

        #Inicializamos la tarjeta como un diccionario vacio
        self.card = {}
        return self.start(path)
        
    def start(self, path):
        """
        Funcion para inicializar el periferico
        Retorna True si la inicializacion fue exitosa, False en caso contrario
        path: string Ruta del directorio raiz
        """
        self.bitacora.escribir("Inicializando Validador...")
        #Inicializamos el acoplador
        self.coupler.inicializar()
        
        #Ingresamos la configuracion para que acepte tarjetas tipo ISO 4443A
        self.coupler.enterHuntParams()
        
        #Reseteamos la SAM para que comience a funcionar
        if self.coupler.resetSAM():
            #Obtenemos el ID del SAM
            self.idSam = self.coupler.SAMGetVersion()
            if self.idSam:
                self.informacionLocal["idSAM"] = self.idSam
                self.informacionLocal["contadores"] = self.obtenerContadores()
                
                json.dump(self.informacionLocal, open(path + "/shared/config/identidad.json", 'w+'))
                self.bitacora.escribir("Id SAM: " + self.idSam)
                
                #Se enciende la antena por si esta apagada
                self.coupler.switchOnAntenna()
                return True

        return False


    def obtenerContadores(self):
        """
        Funcion para obtener los contadores de las llaves del SAM
        Retorna una lista con diccionarios de cada llave
        """
        contadores = []
        for i in self.llaves.keys():
            contador = self.coupler.SAMGetKucEntry(bytes.fromhex(self.llaves[i]['refKuc']))
            contadores.append({'llave': i, 'contador': contador})
            
        return contadores

    def hayTarjeta(self):
        """
        Funcion para determinar si hay una tarjeta en el acoplador
        Retorna una lista con el UID de la tarjeta o None si no se encuentra una tarjeta
        """
        response = self.coupler.thereIsCard()
        if response:
            self.bitacora.escribir("Tarjeta detectada:")
            self.bitacora.escribir(response)
            return response

    def ordenarProductos(self,productos, orden= 1):
        """
        Funcion para ordenar y validar los productos 
        Basados en la propiedad "prioridad" del diccionario "producto", se ordenan de mayor a menor o viceversa y se eliminan los productos que son vacios
        productos   list    Lista con los productos a ordenar, cada producto es un diccionario
        orden:  int Si es 1 indica que seran ordenados de menor a mayor prioridad, si es 0 de mayor prioridad a menor
        """
        #Declaramos funciones locales para determinar si es ascendente o descendente
        def ordenarAsc(e):
            return e["prioridadProducto"]

        def ordenarDesc(e):
            return -1*(e["prioridadProducto"])

        try:
            self.bitacora.escribir("Ordenando Productos")
            self.bitacora.escribir(len(productos))
            productosValidos = []
            for i in productos:
                if i["idProducto"] != "0000":
                    productosValidos.append(i)
            if orden:
                productosValidos.sort(key=ordenarAsc)
            else:
                productosValidos.sort(key=ordenarDesc)
            return productosValidos
        except Exception as e:
            self.bitacora.escribir("Error al realizar la accion" + str(e) )
            return []

    def calcularMonto(self,monto):
        """
        Funcion para calcular el monto que sera procesado, retornado en hexadecimal de 4 bytes
        monto:  int Cantidad que sera convertida
        """
        self.bitacora.escribir(f"{monto:08x}")
        return f"{monto:08x}"
    
    ##########################################################################################
    ######Funciones de transformacion y consulta de datos ####### Inicio #####################
    ##########################################################################################

    def leerUsuario(self, timeout=200):
        """
        Funcion para leer los datos del archivo Usuario_EF
        Retorna un diccionario con los datos datos del archivo Usuario_EF
        timeout: int Tiempo de espera para la operacion en milisegundos, por default es 200
        """
        return self.estructura.readPerfil(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["usuario"]["id"]),timeout))

    def funcionario(self):
        """
        Funcion para leer los datos del archivo Funcionario_EF
        Retorna un diccionario con los datos del archivo Funcionario_EF
        """
        return self.estructura.readFuncionario(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["funcionario"]["id"])))


    def obtenerArchivosProducto(self, producto, timeout=200):
        """
        Funcion para obtener los archivos relacionados con el producto que se pasa como parametro
        Retorna un diccionario con todos los archivos del producto
        producto: dict Diccionario que contiene las referencias a los archivos
        timeout: int Tiempo de espera para la operacion en milisegundos, por default es 200
        """
        #Obtenemos todos los archivos relacionados con el producto, asi como sus referencias
        archivos = {
            "idProducto" : producto["idProducto"],
            "refContrato" : producto["refContrato"],
            "contrato" : self.estructura.readContrato(self.coupler.readFileData(bytes.fromhex(producto["refContrato"]),timeout)),
            "refServicio" : producto["refServicio"],
            "servicio" : self.estructura.readServicio(self.coupler.readFileData(bytes.fromhex(producto["refServicio"]),timeout)),
            "refValor" : producto["refValor"],
            "valor" : self.estructura.readValue(self.coupler.readValueData(bytes.fromhex(producto["refValor"]),timeout))
        }
        return archivos


    def obtenerProductos(self, orden= 1, timeout=200):
        """
        Funcion par leer los productos y todo lo que conlleva
        Retorna una lista con diccionarios de cada producto
        orden:  int Indica el orden de prioridad de los productos, si es uno los productos se ordenan descendente de acuerdo a su prioridad, si es 0 es ascendente
        timeout: int Tiempo de espera para la operacion en milisegundos, por default es 200
        """
        #Leemos el archivo Productos_EF y ordenamos
        productos = self.estructura.readListaProductos(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["listaProductos"]["id"]),timeout))
        productos = self.ordenarProductos(productos, orden=orden)
        
        return productos

    def isTransbordo(self, perfil, producto):
        """
        Funcion para determinar si una validacion es transbordo
        Retorna un entero con la tarifa a cobrar en caso de que haya un transbordo valido, en caso contrario retorna False
        perfil: int Perfil del usuario a evaluar
        producto: dict Evento a evaluar
        """
        try:
            if perfil["transbordo"]:
                transbordar = self.matrizTransbordos[producto["servicio"]["idEntidadUltimoDebito"]][producto["servicio"]["idRutaEstacionUltimoDebito"]]
                if datetime.datetime.fromisoformat(producto["servicio"]["fechaUltimoDebito"]) + datetime.timedelta(minutes=transbordar["tiempo"]) < datetime.datetime.now():
                    self.bitacora.escribir("Existe un transbordo")
                    return transbordar["tarifa"]
            return perfil["tarifa"]
        except Exception as e:
            return False

    ##########################################################################################
    ######Funciones de transformacion y consulta de datos ####### Fin ########################
    ##########################################################################################

    ##########################################################################################
    ######Funciones de escritura y autenticacion ############ Inicio #########################
    ##########################################################################################

    def authenticar(self, key, samKey, timeout=600):
        """
        Funcion para autenticar
        Si la autenticacion es exitosa, retorna True, retorna False en caso contrario
        key: string Valor en hexadecimal de la llave en el PICC(00)
        samKey: string  Valor en hexadecimla de la llave en el SAM (00)
        timeout: int Tiempo de espera para realizar la operacion en milisegundos, por default son 500
        """
        #self.bitacora.escribir("Autenticamos con " + key + " y con " + samKey )
        if self.coupler.authenticateEV1(bytes.fromhex("".join(self.card["id"])), bytes.fromhex(key),bytes.fromhex(samKey),bytes.fromhex(self.padding), timeout):
            return True
        self.bitacora.escribir("Error al autenticar")
        return False


    def validarEmision(self, emision):
        """
        Funcion para validar el archivo Emision_EF
        Retorna True en caso de que los valores sean correctos, False en caso contrario
        emision: dict Diccionario de los valores de emision
        """
        #Valida la fecha de validez
        
        try:
        
            if datetime.date.fromisoformat(emision["fechaFinValidezMedioPago"]) > datetime.date.today():
                #Valida el distribuidor
                if emision["idDistribuidor"] in self.distribuidorAplicacion:
                    #Valida la SAM de Emision
                    if emision["idAlgSeguridad"] in self.versionSeguridad:
                        return True
                    else:
                        self.bitacora.escribir("Error: Version Seguridad")
                else:
                    self.bitacora.escribir("Error: Distribuidor Aplicacion")
            else:
                self.bitacora.escribir("Medio Pago Vencido")
        
        except Exception as e:
            self.bitacora.escribir("Medio Pago Vencido")
            return False
            
        
         
        self.bitacora.escribir("Emision invalido")
        return False


    def validarEntorno(self,entorno):
        """
        Funcion para validar el archivo Entorno_EF
        Retorna True en caso de que los valores sean correcto, False en caso contrario
        entorno: dict Dccionario de los valores de entorno
        """
        try: 
            
            #Validar la version de aplicacion
            self.bitacora.escribir("version Aplicacion: " +  entorno["versionAplicacion"])            
            if entorno["versionAplicacion"] in self.versionAplicacion:
                #Validar la red de operacion
                self.bitacora.escribir("id Red: " +  entorno["idRed"])  
                if entorno["idRed"] in self.redes:
                    #Validar la fecha de validez
                    self.bitacora.escribir("fechaFinValidezAplicacion: " + entorno["fechaFinValidezAplicacion"])  
                    if datetime.date.fromisoformat(entorno["fechaFinValidezAplicacion"]) > datetime.date.today():
                        return True
        except Exception as e:
            self.bitacora.escribir("Entorno invalido") 
            return False
            
        self.bitacora.escribir("Entorno invalido")            
        return False

    def escribirArchivo(self, fileId, fromByte, data, timeout = 200):
        """
        Funcion para escribir en un archivo
        Retorna True si la operacion fue exitosa, False en caso contrario
        fileId: string Valor en hexadecimal del archivo a escribir (00)
        fromByte: string Valor en hexadecimal representando el byte desde donde se comenzara a escribir (00)
        data: string Valor en hexadecimal de los datos que seran escritos (0000....)
        timeout: int Tiempo de espera para realizar la operacion en milisegundos, por default es 200
        """
        if self.coupler.writeData(bytes.fromhex(fileId),bytes.fromhex(fromByte),bytes.fromhex(data), timeout):
            return True
        self.bitacora.escribir("Error escribiendo el archivo")
        return False




    def verificarAccionesLAM(self):
        """
        Verifica y aplica las acciones LAM correspondientes
        Retorna True si se aplicaron acciones, False en caso contrario
        card: dict Diccionario que contiene todas las caracteristicas de la tarjeta que se esta evaluando
        """
        try:
            self.bitacora.escribir("Verificando lista LAM")
            #Buscamos los items que tengan el mismo numero de serie que la tarjeta y los ordenamos
            self.bitacora.escribir(self.card["id"])
            buscado = self.database.consultarLAM("".join(self.card["id"]))
            self.bitacora.escribir(buscado)
            #Si la lista contiene al menos un elemento aplicar la primera accion
            if len(buscado) > 0 :
                #Se convierten los datos para ser escritos
                numeroAccion = f"{buscado[0][1]:02x}"
                consecutivo = f"{self.card['estado']['consecutivoAplicacion']:06x}"
                accion = f"{buscado[0][2]:02x}"
                #Escribir en el archivo
                self.authenticar(self.archivosInformativos["estadoAplicacion"]["llavePICC"], self.archivosInformativos["estadoAplicacion"]["llaveSAM"])
                if self.escribirArchivo(self.archivosInformativos["estadoAplicacion"]["id"], "0000",  accion + consecutivo + numeroAccion):
                    self.commit()
                    self.database.confirmarLAM(buscado[0][0])
                    return True
            return False
        except Exception as e:
            self.bitacora.escribir("Error al aplicar la accion: " + str(e))
            return False


    def incrementarEstadoAplicacion(self,tarjeta, timeout=200):
        """
        Funcion para incrementar contado del estado de la aplicacion
        Si la escritura es correcta, retorna el valor
        tarjeta: dict Diccionario de la tarjeta actual
        timeout: int Tiempo de espera para la operacion en milisegundos, por default es 200
        """
        valor = tarjeta["estado"]["consecutivoAplicacion"] + 1
        escritura = f"{valor:06x}"
        if self.escribirArchivo(self.archivosInformativos["estadoAplicacion"]["id"],"0001", escritura, timeout):
            self.card['estado']["consecutivoAplicacion"] = valor
    
    def validacion(self, timeout=200):
        """
        Funcion para validar una tarjeta
        Retorna string que indica si la operacion fue exitosa o no
        timeout: int Tiempo de espera para la operacion en milisegundos, por default es 200
        """
        #Se validan todos los archivos de la tarjeta
        self.bitacora.escribir("Validando")
        if self.card["id"]: 
            if self.coupler.selectApp(self.app):
                self.card["emision"] = self.estructura.readEmisionFile(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["emision"]["id"]),timeout))
                self.bitacora.escribir(str(self.card["emision"]))
                if self.validarEmision(self.card["emision"]):
                    self.card["entorno"] = self.estructura.readEntornoFile(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["entorno"]["id"]),timeout))
                    if self.validarEntorno(self.card["entorno"]):
                        #Se aplican las acciones LAM
                        self.card["estado"] = self.estructura.readEstadoAplicacion(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["estadoAplicacion"]["id"]),timeout))
                        self.bitacora.escribir("Validar acciones LAM")
                        self.bitacora.escribir(self.card["estado"])
                        
                        if self.verificarAccionesLAM():
                            self.card["estado"] = self.estructura.readEstadoAplicacion(self.coupler.readFileData(bytes.fromhex(self.archivosInformativos["estadoAplicacion"]["id"]),timeout))
                        
                        if self.card["estado"]["estadoAplicacion"] == 1:
                            return "OK"
                        else:
                            self.bitacora.escribir("Estado aplicacion Invalido")
                            return "TARJETA_BLOQUEADA"
                    else:
                        self.bitacora.escribir("Entorno Invalido")
                        return "ENTORNO_INVALIDO"
                else:
                    self.bitacora.escribir("Emision Invalida")
                    return "EMISION_INVALIDA"
            else:
                self.bitacora.escribir("App Invalida")
                return "APP_INVALIDA"
        else:
            self.bitacora.escribir("No hay tarjeta")
        return "NO_TARJETA"


    def verificarAccionesLAP_V(self,productos):
        """
        Funcion para aplicar las acciones LAP_V
        Retorna True si se aplicaron cambios, False en caso contrario
        producto: dict Diccionario con los valores del producto
        """
        
        try:
            self.bitacora.escribir("Revisando listasl LAP_V")
            
            #Verificamos que existe la tarjeta en la lista
            buscado = self.database.consultarLAP_V("".join(self.card["id"]))
            self.bitacora.escribir(self.card["id"])
            self.bitacora.escribir(buscado)
            if buscado:
            
            
                for x in buscado:
                    for i in productos:
                            
                            self.bitacora.escribir(i["idProducto"])
                            self.bitacora.escribir(x[1])
                            
                            if x[1] == i["idProducto"]:
                                
                                
                                
                                producto = self.obtenerArchivosProducto(i,400);
                            
                        
                                producto = {
                                     "idProducto" : producto["idProducto"],
                                     "refContrato": producto["refContrato"],
                                     "refServicio": producto["refServicio"],
                                     "contrato"   : self.estructura.readContrato(self.coupler.readFileData(bytes.fromhex(producto["refContrato"]))),
                                     "refValor"   : producto["refValor"],
                                     "valor"      : self.estructura.readValue(self.coupler.readValueData(bytes.fromhex(producto["refValor"])))
                                     
                                     
                                    }
                                
                                self.bitacora.escribir("refContrato: " + producto["refContrato"])
                                self.bitacora.escribir("refServicio: " + producto["refServicio"])
                                reactivacion = x[2]#f"{x[0][2]:04x}"
                                self.bitacora.escribir(reactivacion)
                                self.bitacora.escribir(producto["contrato"]["numeroReactivacionProducto"])
                                
                                
                                if reactivacion > producto["contrato"]["numeroReactivacionProducto"]:
                                    if self.authenticar("02", "03",700): 
                                        self.bitacora.escribir("Authenticated")
                                      
                                        if self.escribirArchivo(producto["refServicio"],"0000","02"):
                                            if self.commit():
                                                self.bitacora.escribir("Serv Written")
                                                self.database.confirmarLAP_V(x[0], self.idSam)
                                                return True
                                        
                                       
                                
            return False
        except Exception as e:
            self.bitacora.escribir("Error al aplicar la accion LAP_V: " + str(e))
            return False

    def verificarAccionesLAP_R(self,productos):
        """
        Funcion para aplicar las acciones LAP_R
        Retorna True si se aplicaron cambios, False en caso contrario
        producto: dict Diccionario con las propiedades del producto
        """
        
        try:
            self.bitacora.escribir("Revisando listas LAP_R")
            retornar = []
             
            self.bitacora.escribir("Obteniendo busqueda")
            #Verificamos que existe la tarjeta en la lista
            buscado = self.database.consultarLAP_R("".join(self.card["id"]))
            
            self.bitacora.escribir(buscado)
            
            
            
            if buscado:
            
            
                for x in buscado:
                    
                    
                    for i in productos:
                        
                        self.bitacora.escribir(i["idProducto"])
                        self.bitacora.escribir(x[3])
                        
                        if x[3] == i["idProducto"]:
                            
                            producto = self.obtenerArchivosProducto(i,400);
                        
                    
                            producto = {
                                 "idProducto" : producto["idProducto"],
                                 "refContrato": producto["refContrato"],
                                 "contrato"   : self.estructura.readContrato(self.coupler.readFileData(bytes.fromhex(producto["refContrato"]))),
                                 "refValor"   : producto["refValor"],
                                 "valor"      : self.estructura.readValue(self.coupler.readValueData(bytes.fromhex(producto["refValor"])))
                                 
                                }
                            
                            
                  
                            if x[1] - producto["contrato"]["numeroAccionAplicadaProducto"] == 1:
                                self.bitacora.escribir("No. Action validated")
                                self.bitacora.escribir(x[3])
                                return x, producto
                    
                    
                    return False,[]
            else:
                return False,[]
                    
        except Exception as e:
            self.bitacora.escribir("Error aplicando la accion LAP_R: " + str(e))
            return False

    
    def verificarAccionesLAP_R_Monedero(self, monedero, credito, perfil):
        """
        Funcion para aplicar las acciones LAP_R
        Retorna True si se aplicaron cambios, False en caso contrario
        monedero: dict Diccionario del producto Monedero
        credito: dict Diccionario del producto Credito
        perfil: dict Diccionario con las propiedades del perfil
        """
        
        try:
            self.bitacora.escribir("Revisando listas LAP_R de Monedero")
            retornar = False
            
            #Verificamos que existe la tarjeta en la lista
            buscado = self.database.consultarLAP_R("".join(self.card["id"]), monedero["idProducto"], monedero["contrato"]["numeroAccionAplicadaProducto"])
            self.bitacora.escribir(buscado)
            for x in buscado:
                #Obtenemos los productos en cada iteracion por si se han aplicado cambios
                producto = {
                    "idProducto" : monedero["idProducto"],
                    "refContrato" : monedero["refContrato"],
                    "contrato" : self.estructura.readContrato(self.coupler.readFileData(bytes.fromhex(monedero["refContrato"]))),
                    "refValor" : monedero["refValor"],
                    "valor" : self.estructura.readValue(self.coupler.readValueData(bytes.fromhex(monedero["refValor"])))
                }
                credito["valor"] = self.estructura.readValue(self.coupler.readValueData(bytes.fromhex(credito["refValor"])))
                
                #Validamos que la accion sea la siguiente
                
                        #respuesta = self.recargar(x[2])
                
                if x[1] - producto["contrato"]["numeroAccionAplicadaProducto"] == 1:
                    
                    
                        with open(self.monto, 'w+') as amount:
                                json.dump(x[2], amount)
                    
                        json.dump("REALIZANDO_RECARGA", open(self.documentoPantalla, 'w+'))
                        
                        retornar = True        
                        return True
                
                
                        """
                        
                                
                        with open(self.estado, 'w+') as status:
                                json.dump(2, status)
                        retornar = True        
                        return True
                        #respuesta = self.recargar(x[2])
                        """
                        """
                        #Acreditamos el monto del producto
                        #Si el credito tiene saldo primero acreditamos al credito
                        if credito["valor"] < 0:
                            restante = credito["valor"] + x[2]
                            if restante > 0:
                                if self.acreditar((x[2]-restante), credito, self.productos[credito["idProducto"]]["recarga"]["llavePICC"], self.productos[credito["idProducto"]]["recarga"]["llaveSAM"]) != None:
                                    #Validamos que no exceda el limite la recarga
                                    self.commit()
                                    if restante + producto["valor"] <= perfil["productos"][producto["idProducto"]]["maximoValor"]:
                                        if self.acreditar(restante, producto, self.productos[producto["idProducto"]]["recarga"]["llavePICC"], self.productos[producto["idProducto"]]["recarga"]["llaveSAM"]) != None:
                                            self.commit()
                                            numeroAccion = f"{x[1]:04x}"
                                            self.authenticar(self.productos[producto["idProducto"]]["contrato"]["llavePICC"], self.productos[producto["idProducto"]]["contrato"]["llaveSAM"],700)
                                            if self.escribirArchivo(producto["refContrato"], "0018", numeroAccion, 300):
                                                self.commit()
                                                self.bitacora.escribir("Guardando en base de datos")
                                                self.database.confirmarLAP_R(x[0], self.idSam)
                                                self.bitacora.escribir("Guardado")
                                                retornar = True
                            else:
                                if self.acreditar(x[2], credito, self.productos[credito["idProducto"]]["recarga"]["llavePICC"], self.productos[credito["idProducto"]]["recarga"]["llaveSAM"]) != None:
                                    numeroAccion = f"{x[1]:04x}"
                                    self.commit()
                                    self.authenticar(self.productos[producto["idProducto"]]["contrato"]["llavePICC"], self.productos[producto["idProducto"]]["contrato"]["llaveSAM"],700)
                                    if self.escribirArchivo(producto["refContrato"], "0018", numeroAccion):
                                        self.commit()
                                        self.bitacora.escribir("Guardando en base de datos")
                                        self.database.confirmarLAP_R(x[0], self.idSam)
                                        self.bitacora.escribir("Guardado")
                                        retornar = True
                        elif x[2] + producto["valor"] <= perfil["productos"][producto["idProducto"]]["maximoValor"]:            
                            if self.acreditar(x[2],producto, self.productos[producto["idProducto"]]["recarga"]["llavePICC"], self.productos[producto["idProducto"]]["recarga"]["llaveSAM"]) != None:
                                #Reescribimos los archivo
                                self.commit()
                                numeroAccion = f"{x[1]:04x}"
                                self.bitacora.escribir("Escribiendo Archivo")
                                self.bitacora.escribir(numeroAccion)
                                self.authenticar(self.productos[producto["idProducto"]]["contrato"]["llavePICC"], self.productos[producto["idProducto"]]["contrato"]["llaveSAM"],700)
                                if self.escribirArchivo(producto["refContrato"], "0018", numeroAccion):
                                    self.commit()
                                    self.bitacora.escribir("Guardando en base de datos")
                                    self.database.confirmarLAP_R(x[0], self.idSam)
                                    self.bitacora.escribir("Guardado")
                                    retornar = True
                        """
            return retornar
        except Exception as e:
            self.bitacora.escribir("Error aplicando la accion LAP_R: " + str(e))
            return False

    def validarProducto(self, producto, validacion = True):
        """
        Funcion para validar el producto
        Retorna True si el producto es valido, False en caso contrario
        producto: dict Diccionario del producrto a ser evaluado
        validacion: bool True si la accion que se esta efectuando es una validacion, False si es una recarga 
        """
        if validacion :

            #El producto es valido?
            self.bitacora.escribir("Validando estado de producto")
            self.bitacora.escribir(self.productos[producto["idProducto"]]["productoNombre"])
            self.bitacora.escribir(producto["servicio"])
            self.bitacora.escribir(producto["contrato"])
            
                        
            if producto["servicio"]["estadoProducto"] == 1:
                #La fecha actual esta dentro del periodo de validez y esta definida?
                self.bitacora.escribir("Validando la fecha de validez")
                if producto["contrato"]["inicioValidezProducto"] != "1990-00-00 00:00:00" and producto["contrato"]["finValidezProducto"] != "1990-00-00 00:00:00":
                    if datetime.datetime.fromisoformat(producto["contrato"]["inicioValidezProducto"]) < datetime.datetime.now() and datetime.datetime.fromisoformat(producto["contrato"]["finValidezProducto"]) > datetime.datetime.now():
                        #La hora esta dentro de las horas activas del producto?
                        self.bitacora.escribir("Validando Horas activas")
                        if producto["contrato"]["inicioValidezDia"] != 0 and producto["contrato"]["finValidezDia"] != 0 :
                            minutodia =  (datetime.datetime.now().hour)*60 + datetime.datetime.now().minute
                            if producto["contrato"]["inicioValidezDia"] < minutodia and producto["contrato"]["finValidezDia"] > minutodia:
                                return True
                            else:
                                self.bitacora.escribir("Producto Invalido")
                                return False
                        else:
                            return True
                else:
                 return True
            else:
                return False 
        else :
            if producto["servicio"]["estadoProducto"] == 1 or producto["servicio"]["estadoProducto"] == 2:
                if producto["contrato"]["inicioValidezProducto"] != "1990-00-00 00:00:00" and producto["contrato"]["finValidezProducto"] != "1990-00-00 00:00:00":
                    if datetime.datetime.fromisoformat(producto["contrato"]["inicioValidezProducto"]) < datetime.datetime.now() and datetime.datetime.fromisoformat(producto["contrato"]["finValidezProducto"]):
                        return True
        self.bitacora.escribir("Producto Invalido")
        return False
    
    def debitar(self, monto, producto, llavePICC, llaveSAM):
        """
        Funcion para debitar en la tarjeta
        Retorna la cantidad debitada
        monto: int Cantidad que sera debitada en el archivo
        producto: dict Diccionario del producto al que se le debitara
        llavePICC: string Valor en hexadecimal de la llave de PICC (00)
        llaveSAM: string Valor en hexadecimal de la llave del SAM (00)
        """
        #Se autentica
        if self.authenticar(llavePICC, llaveSAM):
            #Se debita
            if self.coupler.debitValue(bytes.fromhex(producto["refValor"]), bytes.fromhex(self.calcularMonto(monto))):
                return monto
        self.bitacora.escribir("Error al debitar")
        raise NameError("Error al debitar")
        
    def commit(self, timeout=500):
        """
        Funcion para realizar el commit en la tarjeta
        Retorna True si el commit fue realizado con exito, en caso contrario lanza un error
        timeout: int Tiempo de espera para la operacion, por default es 500
        """
        if self.coupler.commit(timeout):
            return True
        else:
            raise NameError("Error haciendo commit")

    #Funcion para acreditar
    def acreditar(self, monto, producto, llavePICC, llaveSAM):
        """
        Funcion para debitar en la tarjeta
        Retorna la cantidad debitada
        monto: int Cantidad que sera debitada en el archivo
        producto: dict Diccionario del producto al que se le debitara
        llavePICC: string Valor en hexadecimal de la llave de PICC (00)
        llaveSAM: string Valor en hexadecimal de la llave del SAM (00)
        """
        self.bitacora.escribir(producto)
        self.bitacora.escribir(monto)
        try:
            #Se autentica
            if self.authenticar(llavePICC, llaveSAM):
                #Se acredita
                self.bitacora.escribir("Authenticated")
                self.bitacora.escribir(producto["refValor"])
                if self.coupler.creditValue(bytes.fromhex(producto["refValor"]), bytes.fromhex(self.calcularMonto(monto))):
                    return monto
            self.bitacora.escribir("Error al acreditar")
                
        except Exception as e:
            
            self.bitacora.escribir("Error al acreditar")
        
    
    def obtenerConsecutivo(self, llaveSam):
        """
        Funcion para obtener el valor del contador del SAM
        llaveSam: string Valor hexadecimal de la llave en el SAM (00)
        """
        self.bitacora.escribir("Obteniendo el valor consecutivo de la llave " + llaveSam)
        prepare = llaveSam + self.llaves[llaveSam]["posicion"] + self.llaves[llaveSam]["version"] + self.llaves[llaveSam]["refKuc"]
        consecutivo = self.coupler.SAMGetKucEntry(bytes.fromhex(self.llaves[llaveSam]["refKuc"]))
        if consecutivo :
            self.consecutivoSam = prepare + consecutivo
        else:
            self.consecutivoSam = prepare + "00000000"


    def escribirServicio(self, producto, datosLocales):
        """
        Funcion para escribir en el archivo Servicio_EF de acuerdo al producto dado
        producto: dict Diccionario del producto que sera evaluado
        datosLocales: dict Diccionario de la informacion local del dispositivo
        """
        #self.bitacora.escribir("Intentando escribir servicio")
        #Se convierte la informacion con ayuda de la clase de estructura de datos
        conversion = self.estructura.convertirServicio(producto, datosLocales, datetime.datetime.now())
        
        #Se escribe en la tarjeta
        self.escribirArchivo(producto["refServicio"], "0000", conversion, 400)
        

    def escribirEventos(self, datos, locales, tarifa, tipoEvento, timeout=300):
        """
        Funcion para escribir un nuevo evento
        datos: dict Diccionario con los datos del evento
        locales: dict Diccionario con la informacion local del aparato
        tarifa: int Cantidad debitada o acreditada en el evento
        tipoEvento: int Codigo del evento
        timeout: int Tiempo de espera para la operacion en milisegundos, por default es 300
        """
        #self.bitacora.escribir("Intentando escribir evento")
        #Se unen los dos diccionarios y se definen bien los datos del evento
        final = {
            "fechaHoraEvento" : datetime.datetime.now(),
            "tipoEvento" : tipoEvento,
            "montoEvento" : tarifa,
            "consecutivoEvento": self.card["estado"]["consecutivoAplicacion"],
            "idUbicacion" : "0000",
            "numeroTransbordos" : 0,
            "motivoDevolucion" : 0,
            "limiteTransbordo" : datetime.datetime.now() + datetime.timedelta(minutes=datos["contrato"]["tiempoTransbordo"]),
            "numeroPassbacks" : 0,
            "idSAM" : self.idSam,
            "consecutivoSAM" : self.consecutivoSam
        }
        final.update(datos),
        final.update(locales)
        
        #Se convierte la informacion
        convertido = self.estructura.convertirEvento(final)

        self.coupler.writeRecord(bytes.fromhex(self.archivosInformativos["eventos"]["id"]),bytes.fromhex("0000"),bytes.fromhex("0040"), bytes.fromhex(convertido),timeout)
        

    def leerTodosEventos(self,archivo, tam):
        """
        Funcion para leer todos los eventos de la tarjeta
        Retorna una lista con el total de eventos escritos en la tarjeta
        archivo: string Valor en hexadecimal del archivo de eventos (00)
        tam: int Cantidad de bytes de cada registro
        """
        #Se lee evento por evento para evitar errores
        evento = True
        total = []
        cont = 0
        while evento: 
            evento = self.leerEventos(archivo, cont, 1, tam)
            print(evento)
            cont += 1
            if evento:
                total.append(evento[0])
        return total

    def leerEventos(self, fileId, fromRecord, toRecord, sizeRecord):
        """
        Funcion para leer una cantidad definida de eventos
        Si la operacion es exitosa, retorna el evento soliciado en forma de diccionario, False en caso contrario
        fileId: string Valor hexadecimal del archivo que sera leido (00)
        fromRecord: in Valor hexadecimal del registro desde donde se empezara a leer
        toRecord: int Cantidad de registros a leer
        sizeRecord: int Tamaño del registro
        """
        return self.estructura.readRecords(self.coupler.readRecordData(bytes.fromhex(fileId), bytes.fromhex(f"{fromRecord:04x}"), bytes.fromhex(f"{toRecord:04x}"),bytes.fromhex(f"{sizeRecord:04x}")))     
    
     


    def validarReglas(self, producto, perfil):
        """
        Funcion para validar las reglas de operacion
        Retorna un diccionario con los diferentes casos que puede haber
        producto: dict Diccionario del producto con sus archivos
        perfil: dict Diccionario con el perfil actual de la tarjeta y sus caracteristicas
        """
        try:
            
            
            self.bitacora.escribir(str(producto))
            self.bitacora.escribir(str(perfil))
            
            self.bitacora.escribir("Validando Passback")
            #self.bitacora.escribir(datetime.datetime.fromisoformat(producto["servicio"]["fechaUltimoDebito"]) + datetime.timedelta(seconds = perfil["antiPassback"]))
            
            
                
            if perfil["antiPassback"] > 0:
                       
                    try:
                        if producto["servicio"]["fechaUltimoDebito"] != "1990-00-00 00:00:00":
                            if (datetime.datetime.fromisoformat(producto["servicio"]["fechaUltimoDebito"]) + datetime.timedelta(seconds = perfil["antiPassback"])) > datetime.datetime.now():
                                self.bitacora.escribir("PASSBACK!")
                                return { "error": "PASSBACK"}
                        
                    except Exception as e:
                        self.bitacora.escribir("Error comparing passback")
                        
                            
           
               
                
                
                
            
                
                
            self.bitacora.escribir("Validando Monto maximo")
            self.bitacora.escribir(perfil["productos"])
            
            if producto["valor"] <= perfil["productos"][producto["idProducto"]]["maximoValor"]:
                self.bitacora.escribir("Validando Transbordo")
                if perfil["transbordo"]:
                    try:
                        self.bitacora.escribir(producto["servicio"]["idRutaEstacionUltimoDebito"])
                        self.bitacora.escribir(self.informacionLocal["idRuta_Estacion"])
                        
                        transbordar = self.matrizTransbordos[producto["servicio"]["idRutaEstacionUltimoDebito"]][self.informacionLocal["idRuta_Estacion"]]      
                        if producto["servicio"]["fechaUltimoDebito"] != "1990-00-00 00:00:00" :                 
                            if datetime.datetime.fromisoformat(producto["servicio"]["fechaUltimoDebito"]) + datetime.timedelta(minutes=transbordar["tiempo"]) > datetime.datetime.now():
                                self.bitacora.escribir("Existe un transbordo")
                                return { "tarifa": transbordar["tarifa"], "evento": 3 }
                        
                    except Exception as e:
                        self.bitacora.escribir("No hay transbordo con el idRuta: " + str(e))
                    
                    try:
                        if producto["servicio"]["idEntidadUltimoDebito"] != self.informacionLocal["entidad"] and producto["servicio"]["idRutaEstacionUltimoDebito"] != self.informacionLocal["idRuta_Estacion"]:    
                            transbordar = self.matrizTransbordos[producto["servicio"]["idEntidadUltimoDebito"]]["0"]
                            self.bitacora.escribir("Pero si existe un transbordo general")
                            if producto["servicio"]["fechaUltimoDebito"] != "1990-00-00 00:00:00" : 
                                if datetime.datetime.fromisoformat(producto["servicio"]["fechaUltimoDebito"]) + datetime.timedelta(seconds=transbordar["tiempo"]) > datetime.datetime.now():
                                    self.bitacora.escribir("Existe un transbordo")
                                    return { "tarifa": transbordar["tarifa"], "evento": 3 }
                    except Exception as e:
                        self.bitacora.escribir("No hay transbordo con : " + str(e))
                
                if perfil["codigoPerfil"] == 0:
                    return { "tarifa": perfil["productos"][producto["idProducto"]]["tarifa"], "evento": 1}
                else:
                    return { "tarifa": perfil["productos"][producto["idProducto"]]["tarifa"], "evento": 2}
            else:
                self.bitacora.escribir("Monto Maximo Excedido")
                return {"error": "MONTO_MAXIMO_EXCEDIDO"}
            
            return { "error": "PASSBACK"}
        except Exception as e:
            self.bitacora.escribir("Error validando reglas: " + str(e))
            return {"error": "ERROR_VALIDANDO_REGLAS"}

    def realizarCobro(self, punteros, perfil):
        """
        Funcion para evaluar un producto y realizar la validacion
        Retorna un diccionario con la informacion recopilada codigo de error o exito
        punteros: dict Diccionario del prodcuto
        perfil: dict Diccionario del perfil con el que sera evaluado el producto
        """
        try:
            resultado = {
                "eventos": [],
                "codigo": "ERROR_LEER_TARJETA",
                "leyenda":"General"
                }
            #self.bitacora.escribir(perfil["productos"][i["idProducto"]]["productoNombre"])
            
            #Si el producto es credito, no usarlo
            self.bitacora.escribir("Iniciando Uso del producto")
            producto = self.obtenerArchivosProducto(punteros)

            #Validamos listas sobre productos
          #  if self.verificarAccionesLAP_R(producto):
                
                #Obteniendo de nuevo los productos solo si se aplicaron cambios
                #producto = self.obtenerArchivosProducto(punteros)
            
            #Validando el producto 
            self.bitacora.escribir("Validando el producto")
            if self.validarProducto(producto):
                
                reglas = self.validarReglas(producto,perfil)
                
                if "error" in reglas.keys():
                    resultado["codigo"] = reglas["error"] 
                    return  resultado
                
                if self.productos[producto["idProducto"]]["productoNombre"] != "CREDITO":

                    #Verificando que el producto cuente con suficiente saldo
                    if producto["valor"] >= reglas["tarifa"] :
                        #Guardamos los datos que necesitamos para el registro de venta
                        retornar = {
                            "uid" : "".join(self.card["id"]),
                            "idProducto" : producto["idProducto"],
                            "saldoInicial" : producto["valor"],
                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                            "monto": reglas["tarifa"],
                            "perfil": perfil["codigoPerfil"],
                            "idDispositivo": self.informacionLocal["idDispositivo"],
                            "idSAM" : self.idSam,
                            "idTransporte": self.informacionLocal["idTransporte"],
                            "idDebito": reglas["evento"]
                        }
                        #Realizando la compra
                        self.bitacora.escribir("Compra Simple")
                        total = self.debitar(reglas["tarifa"], producto, self.productos[producto["idProducto"]]["validacion"]["llavePICC"], self.productos[producto["idProducto"]]["validacion"]["llaveSAM"])
                        if total != None:
                            self.bitacora.escribir("Debitado con Exito")
                            self.obtenerConsecutivo(self.productos[producto["idProducto"]]["validacion"]["llaveSAM"])
                            retornar["consecutivoSAM"] = self.consecutivoSam
                            
                            #Escribir Archivos
                            self.bitacora.escribir("Escribiendo archivos")
                            self.escribirEventos(producto,self.informacionLocal, reglas["tarifa"], 4)
                            self.bitacora.escribir("Escribir Servicio")
                            self.escribirServicio(producto,self.informacionLocal)
                            self.incrementarEstadoAplicacion(self.card)
                            
                            #Commit a la tarjeta
                            self.bitacora.escribir("Haciendo Commit")
                            if self.commit():
                                self.bitacora.escribir("Realizado con exito")
                                retornar["saldoFinal"] = self.estructura.readValue(self.coupler.readValueData(bytes.fromhex(producto["refValor"])))
                                retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"]
                                #self.bitacora.escribir("Seis")
                                                     
                                
                                resultado["eventos"].append(retornar)
                                
                                #resultado["leyenda"] = "General"  
                                
                                
                                if(retornar["idDebito"] == 3):
                                    resultado["leyenda"] = "Transbordo"
                                
                                else:
                                    resultado["leyenda"] = perfil["usuarioTipo"]
                               
                                
                                resultado["codigo"] = "COMPRA_SIMPLE"

                                
                                return resultado
                    else:
                        resultado["codigo"] = "SALDO_INSUFICIENTE"
                        self.bitacora.escribir("Saldo Insuficiente")
                        return resultado
                        
            
                resultado["codigo"] = "ERROR_VALIDANDO_TARJETA"
                return resultado
            resultado["codigo"] = "ERROR_VALIDANDO_PRODUCTO"
            self.bitacora.escribir("ERROR_VALIDANDO_PRODUCTO")
            #resultado["codigo"] = "ERROR_LEER_TARJETA"
            return resultado
        except Exception as e:
            self.bitacora.escribir("Error al realizar la compra: " + str(e))
            resultado["codigo"] = "ERROR_LEER_TARJETA"
            return resultado


    def realizarCobroMixto(self, monedero, credito, perfil):
        """
        Funcion para realizar un cobro pero utilizando el producto monedero y el producto credito a la vez
        Retorna un diccionario con el resultado de la operacion
        monedero: dict Diccionario del producto Monedero
        credito: dict Diccionaro del producto Credito
        perfil: dict Diccionario del perfil del usuario de la tarjeta
        """
        try:
            resultado = {
                "eventos": [],
                "codigo": "ERROR_LEER_TARJETA",
                "leyenda":"Cobro Mixto"
                }
            
            #Si el producto es credito, no usarlo
            self.bitacora.escribir("Iniciando Uso del producto")
            productoMonedero = self.obtenerArchivosProducto(monedero)
            productoCredito = self.obtenerArchivosProducto(credito)


            self.bitacora.escribir(productoMonedero)
            self.bitacora.escribir(productoCredito)
            
            if  productoCredito["valor"] != 0 :
                if productoMonedero["valor"] != 0:                 
                    resultado["codigo"] = "ERROR_SALDO_CREDITO"
                    self.bitacora.escribir("Error Tarjeta: Credito y Monedero")
                    return  resultado
                else:
                    self.bitacora.escribir("Saldo Credito")
                    
            
            
            elif isinstance(productoCredito["valor"], bool) or isinstance(productoMonedero["valor"], bool):
                resultado["codigo"] = "ERROR_LEER_TARJETA"
                return  resultado 
            
            
            #Validamos listas sobre productos
            #if self.verificarAccionesLAP_R(productoMonedero):
            #    resultado["codigo"] = "NO_TARJETA"
            #    return  resultado  
                
             
                
                
                
                
                #Obteniendo de nuevo los productos solo si se aplicaron cambios
                #productoMonedero = self.obtenerArchivosProducto(monedero)
                #productoCredito = self.obtenerArchivosProducto(credito)
                
                #retornar["consecutivoSAM"] = self.consecutivoSam
                #retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"]                                         
                #resultado["eventos"].append(retornar)
               # resultado["codigo"] = "COMPRA_MIXTA"
            
            #Validando que tenga saldo suficiente basandonos en que por lo menos le alcanza para pagar con credito
            if productoCredito["valor"] != 0:
                resultado["codigo"] = "SALDO_INSUFICIENTE"
                return  resultado


            #Validando el producto 
            self.bitacora.escribir("Validando los productos")
            self.bitacora.escribir("Validacion Mixta")
            if self.validarProducto(productoMonedero):
                #{ "tarifa": 00, "evento": 00 } | { "error": ""}
                reglas = self.validarReglas(productoMonedero,perfil)
                print(reglas)
                if "error" in reglas.keys():
                    resultado["codigo"] = reglas["error"] 
                    return  resultado
                
                if reglas["tarifa"] > productoMonedero["valor"]:
                    if self.validarProducto(productoCredito):
                        restante = reglas["tarifa"] - productoMonedero["valor"]
                        if productoMonedero["valor"] > 0:
                            retornar = {
                            "uid" : "".join(self.card["id"]),
                            "idProducto" : productoMonedero["idProducto"],
                            "saldoInicial" : productoMonedero["valor"],
                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                            "monto": productoMonedero["valor"],
                            "perfil": perfil["codigoPerfil"],
                            "idDispositivo": self.informacionLocal["idDispositivo"],
                            "idSAM" : self.idSam,
                            "idTransporte": self.informacionLocal["idTransporte"],
                            "idDebito": reglas["evento"],
                            "saldoFinal": 0
                            }

                            self.bitacora.escribir("Compra Mixta")
                            if self.debitar(productoMonedero["valor"], productoMonedero, self.productos[productoMonedero["idProducto"]]["validacion"]["llavePICC"], self.productos[productoMonedero["idProducto"]]["validacion"]["llaveSAM"]) != None:
                                self.bitacora.escribir("Debitado con Exito")
                                self.obtenerConsecutivo(self.productos[productoMonedero["idProducto"]]["validacion"]["llaveSAM"])
                                retornar["consecutivoSAM"] = self.consecutivoSam
                                
                                #Escribir Archivos
                                self.bitacora.escribir("Escribiendo archivos")
                                self.escribirEventos(productoMonedero,self.informacionLocal, productoMonedero["valor"], 4)
                                self.escribirServicio(productoMonedero,self.informacionLocal)
                                self.incrementarEstadoAplicacion(self.card)
                                
                                #Commit a la tarjeta
                                self.bitacora.escribir("Haciendo Commit")
                                if self.commit():
                                    self.bitacora.escribir("Realizado con exito")
                                    retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"]
                                    #self.bitacora.escribir("Seis")
                                    resultado["eventos"].append(retornar)

                                    retornar = {
                                        "uid" : "".join(self.card["id"]),
                                        "idProducto" : productoCredito["idProducto"],
                                        "saldoInicial" : productoCredito["valor"],
                                        "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                        "monto": restante,
                                        "perfil": perfil["codigoPerfil"],
                                        "idDispositivo": self.informacionLocal["idDispositivo"],
                                        "idSAM" : self.idSam,
                                        "idTransporte": self.informacionLocal["idTransporte"],
                                        "idDebito": reglas["evento"],
                                        "saldoFinal": -restante
                                    }

                                    if self.debitar(restante, productoCredito, self.productos[productoCredito["idProducto"]]["validacion"]["llavePICC"], self.productos[productoCredito["idProducto"]]["validacion"]["llaveSAM"]) != None:
                                        self.bitacora.escribir("Debitado con Exito")
                                        self.obtenerConsecutivo(self.productos[productoCredito["idProducto"]]["validacion"]["llaveSAM"])
                                        retornar["consecutivoSAM"] = self.consecutivoSam
                                        
                                        #Escribir Archivos
                                        self.bitacora.escribir("Escribiendo archivos")
                                        self.escribirEventos(productoCredito,self.informacionLocal, productoCredito["valor"], 4)
                                        self.escribirServicio(productoCredito,self.informacionLocal)
                                        self.incrementarEstadoAplicacion(self.card)
                                        
                                        #Commit a la tarjeta
                                        self.bitacora.escribir("Haciendo Commit")
                                        if self.commit():
                                            self.bitacora.escribir("Realizado con exito")
                                            retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"]
                                            #self.bitacora.escribir("Seis")
                                            resultado["eventos"].append(retornar)
                                            resultado["codigo"] = "COMPRA_MIXTA"
                                            
                                            
                                             
                                            if(retornar["idDebito"] == 3):
                                               resultado["leyenda"] = "Transbordo"
                                
                                            else:
                                               resultado["leyenda"] = perfil["usuarioTipo"]

                                            return resultado
                        else:
                            retornar = {
                                "uid" : "".join(self.card["id"]),
                                "idProducto" : productoCredito["idProducto"],
                                "saldoInicial" : productoCredito["valor"],
                                "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                "monto": restante,
                                "perfil": perfil["codigoPerfil"],
                                "idDispositivo": self.informacionLocal["idDispositivo"],
                                "idSAM" : self.idSam,
                                "idTransporte": self.informacionLocal["idTransporte"],
                                "idDebito": reglas["evento"],
                                "saldoFinal": -restante
                            }

                            self.bitacora.escribir("Compra Credito")
                            if self.debitar(restante, productoCredito, self.productos[productoCredito["idProducto"]]["validacion"]["llavePICC"], self.productos[productoCredito["idProducto"]]["validacion"]["llaveSAM"]) != None:
                                self.bitacora.escribir("Debitado con Exito")
                                self.obtenerConsecutivo(self.productos[productoCredito["idProducto"]]["validacion"]["llaveSAM"])
                                retornar["consecutivoSAM"] = self.consecutivoSam
                                
                                #Escribir Archivos
                                self.bitacora.escribir("Escribiendo archivos")
                                self.escribirEventos(productoCredito,self.informacionLocal, productoCredito["valor"], 4)
                                self.escribirServicio(productoCredito,self.informacionLocal)
                                self.incrementarEstadoAplicacion(self.card)
                                
                                #Commit a la tarjeta
                                self.bitacora.escribir("Haciendo Commit")
                                if self.commit():
                                    self.bitacora.escribir("Realizado con exito")
                                    retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"]
                                    #self.bitacora.escribir("Seis")
                                    resultado["eventos"].append(retornar)
                                    resultado["codigo"] = "COMPRA_CREDITO"
                                    
                                    if reglas["evento"] == 3:
                                       resultado["leyenda"] = "Transbordo"
                                
                                    else:
                                       resultado["leyenda"] = perfil["usuarioTipo"]                                    
                                    

                                    return resultado
                else:
                    retornar = {
                        "uid" : "".join(self.card["id"]),
                        "idProducto" : productoMonedero["idProducto"],
                        "saldoInicial" : productoMonedero["valor"],
                        "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                        "monto": reglas["tarifa"],
                        "perfil": perfil["codigoPerfil"],
                        "idDispositivo": self.informacionLocal["idDispositivo"],
                        "idSAM" : self.idSam,
                        "idTransporte": self.informacionLocal["idTransporte"],
                        "idDebito": reglas["evento"],
                        "saldoFinal": productoMonedero["valor"] - reglas["tarifa"]
                    }

                    self.bitacora.escribir("Compra Simple")
                    if self.debitar(reglas["tarifa"], productoMonedero, self.productos[productoMonedero["idProducto"]]["validacion"]["llavePICC"], self.productos[productoMonedero["idProducto"]]["validacion"]["llaveSAM"]) != None:
                        self.bitacora.escribir("Debitado con Exito")
                        self.obtenerConsecutivo(self.productos[productoMonedero["idProducto"]]["validacion"]["llaveSAM"])
                        retornar["consecutivoSAM"] = self.consecutivoSam
                        
                        #Escribir Archivos
                        self.bitacora.escribir("Escribiendo archivos")
                        self.escribirEventos(productoMonedero,self.informacionLocal, productoMonedero["valor"], 4)
                        self.escribirServicio(productoMonedero,self.informacionLocal)
                        self.incrementarEstadoAplicacion(self.card)
                        
                        #Commit a la tarjeta
                        self.bitacora.escribir("Haciendo Commit")
                        if self.commit():
                            self.bitacora.escribir("Realizado con exito")
                            retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"]
                            #self.bitacora.escribir("Seis")
                            resultado["eventos"].append(retornar)
                            
                            if reglas["evento"] == 3:
                                resultado["leyenda"] = "Transbordo"
                                
                            else:
                                resultado["leyenda"] = perfil["usuarioTipo"]
                                       
                            resultado["codigo"] = "COMPRA_SIMPLE"
                            return resultado
                
                resultado["codigo"] = "ERROR_VALIDANDO_PRODUCTO"
                return  resultado
            return resultado
        except Exception as e:
            self.bitacora.escribir("Error haciendo el cobro Mixto: " + str(e))
            return resultado


    
    def escribirArchivosRecarga(self, producto, monto):
        """
        Funcion para escribir los archivos con autenticacion y commit
        Retorna True si los archivos se escribieron con exito o False en caso contrario
        producto: dict Un diccionario del producto con el que se hizo la recarga
        monto: int El monto que se recargo a este producto
        """
        self.bitacora.escribir("Autenticando")
        if self.authenticar(self.productos[producto["idProducto"]]["validacion"]["llavePICC"], self.productos[producto["idProducto"]]["validacion"]["llaveSAM"],700):
            self.bitacora.escribir("Escribriendo Evento")
            self.escribirEventos(producto,self.informacionLocal, monto,6, 500)
            self.bitacora.escribir("Incrementando estado")
            self.incrementarEstadoAplicacion(self.card, 400)
            self.bitacora.escribir("Haciendo Commit")
            if self.commit(800):
                return True
        return False


    def autenticarMF(self, bloque):
        """
        Funcion para autenticar un bloque de una tarjeta MiFare
        Retorna True si la operacion fue exitosa, False en caso contrario
        bloque: string Numero de bloque que se desea autenticar en hexadecimal
        """
        self.coupler.loadKey(self.mifareKey)
        if self.coupler.authenticateMF("0B",bloque,"FF"):
            return True
        return False

    

    def obtenerDatos(self):
        """
        Funcion para obtener datos de una tarjeta MiFare
        """

        def convertirChar(valores):
            nueva = []
            for i in valores:
                if len(nueva) == 0:
                    nueva.append(i)
                elif nueva[-1] == '00' and i == '00':
                    break
                else:
                    nueva.append(i)
            nueva = nueva[:-1]
            cadena = "".join(nueva)
            return bytearray.fromhex(cadena).decode('latin1')

        self.autenticarMF("01")
        lectura = self.coupler.readBlockMF("04")
        retornar = {
            "codigo": "ERROR_LEER_TARJETA",
        }
        if lectura:
            lectura2 = self.coupler.readBlockMF("05")
            retornar["rol"] = int(lectura[2], 16)
            retornar["id"] = int(lectura2[0], 16)
            retornar["codigo"] = "TARJETA_OK"
            return retornar
        return False
            

    ##########################################################################################
    ######Funciones de escritura y autenticacion ############ Fin  ###########################
    ##########################################################################################

    ##########################################################################################
    ###### Procesos Principales  ############### Inicio ######################################
    ##########################################################################################
    def checklimit_recarga(self, amount_coins,cardinfo):
      
       """ if amount_coins + monedero["valor"]  <= perfil["productos"][monedero["idProducto"]]["maximoValor"]:
            self.bitacora.escribir("Debado del limite de recarga")
            return  0
        else:
            self.bitacora.escribir("Maximo Ingresado : se regresan monedas")
            ##resultado["codigo"] = "Error al recargar"
            ##resultado["cambio"] = amount_coins
            ##return resultado
            return 0
        """
       self.bitacora.escribir(cardinfo["saldo"])
       self.bitacora.escribir("Amount")
       self.bitacora.escribir(amount_coins)
       self.bitacora.escribir("Max amount")
       ##self.bitacora.escribir(perfil["productos"][monedero["idProducto"]]["maximoValor"])
       self.bitacora.escribir("Debado del limite de recarga")
       return 0
       
        


    def recargar(self, montoRecarga):
        """
        Funcion para recargar una tarjeta (Solo se aplica a Monedero y Credito)
        Retorna un diccionario con el resultado de la operacion
        montoRecarga: string Monto que sera acreditado en los monederos
        """
        try:
            #Verificar que exista una tarjeta y que sea valida
            if montoRecarga:
                self.card["id"] = self.hayTarjeta()
                validado = self.validacion(400)
                if validado == "OK":

                    resultado = {
                        "eventos": [],
                        "codigo": "NO_TARJETA"
                    }
                    #Validamos al perfil del usuario
                    self.bitacora.escribir("Leyendo perfil")
                    self.card["perfil"] = self.leerUsuario(400)
                    
                    #Si es funcionario, aplicar las funciones especiales
                    self.bitacora.escribir("Validando perfil")
                    if self.card["perfil"]["codigoPerfil"] == 9:
                        self.funcionario()
                    else:
                        #Se autentica solo para tarjetas de prueba, las de produccion no es necesario
                        
                        #Obteniendo Productos
                        self.bitacora.escribir("Leyendo productos")
                        self.card["productos"] = self.obtenerProductos()
                        self.bitacora.escribir("Productos leidos")
                        

                        
                        
                        
                        #Definimos una variable bandera que nos dara la pauta para recargar diferentes productos    
                        totalRecargar = montoRecarga
                        
                        #Recorremos los productos para intentar recargarlos, asumiendo que los de prioridad menor se recargan primero y solo se llega hasta el Monedero
                        credito = False
                        monedero = False
                        perfil = list(x for x  in self.perfiles if x["codigoPerfil"] == self.card["perfil"]["codigoPerfil"])[0]
                        
                        
                        #Verificar acciones pendientes LAPV
                        
                            
                        
                        
                        
                        
                        
                        #Verificar acciones pendientes LAPR
                        
                        lapr, productoLapr = self.verificarAccionesLAP_R(self.card["productos"])
                        
                        self.bitacora.escribir(lapr)
     
                        
                        
                        if lapr:
                            
                             if montoRecarga + productoLapr["valor"]  <= perfil["productos"][productoLapr["idProducto"]]["maximoValor"]:
                                        retornar = {
                                            "uid" : "".join(self.card["id"]),
                                            "idProducto" : productoLapr["idProducto"],
                                            "saldoInicial" : productoLapr["valor"],
                                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                            "monto": montoRecarga,
                                            "perfil": perfil["codigoPerfil"],
                                            "idSAM" : self.idSam,
                                            "tipoRecarga": 1,
                                            "saldoFinal": productoLapr["valor"] + montoRecarga
                                        }
                                        
                             if self.acreditar(montoRecarga, productoLapr, self.productos[productoLapr["idProducto"]]["recarga"]["llavePICC"], self.productos[productoLapr["idProducto"]]["recarga"]["llaveSAM"]) != None :                           
                                 if self.commit(800):                                        
                                     self.bitacora.escribir("Obtener consecutivo BPD2")
                                     self.obtenerConsecutivo(self.productos[productoLapr["idProducto"]]["recarga"]["llaveSAM"])
                                     retornar["consecutivoSAM"] = self.consecutivoSam
                                     retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"] + 1
                                     resultado["eventos"].append(retornar)
                                     self.escribirArchivosRecarga(productoLapr, montoRecarga)
                                     
                                     
                                     
                                     numeroAccion = f"{lapr[1]:04x}"
                                     self.bitacora.escribir(numeroAccion)
                                     self.bitacora.escribir(self.productos[productoLapr["idProducto"]]["recarga"]["llavePICC"])
                                     self.bitacora.escribir(self.productos[productoLapr["idProducto"]]["recarga"]["llaveSAM"])
                                     
                                     if self.authenticar(self.productos[productoLapr["idProducto"]]["contrato"]["llavePICC"], self.productos[productoLapr["idProducto"]]["contrato"]["llaveSAM"]):
                                         if self.escribirArchivo(productoLapr["refContrato"], "0018", numeroAccion, 300):
                                             self.commit()
                                             self.bitacora.escribir("Guardando en base de datos")
                                             self.database.confirmarLAP_R(lapr[0], self.idSam)
                                             self.bitacora.escribir("Guardado")
                                             self.card = {}
                                             resultado["codigo"] = "RECARGA_MONEDERO"
                                             return resultado 

                                     """
                                     if self.escribirArchivo(productoLapr["refContrato"], "0018", numeroAccion, 300):
                                         self.commit()
                                         self.bitacora.escribir("Guardando en base de datos")
                                         self.database.confirmarLAP_R(lapr[0], self.idSam)
                                         self.bitacora.escribir("Guardado")
                                     """
                                                                          
                                     #if self.escribirArchivo(productoLapr["refServicio"], "0000", numeroAccion):
                                                                                            
                                    
                                         
                            
                        
                        
                        for i in self.card["productos"]:
                                                     
                            if self.productos[i["idProducto"]]["productoNombre"] == "CREDITO" and perfil["credito"] :
                                credito = self.obtenerArchivosProducto(i,400)
                            elif self.productos[i["idProducto"]]["productoNombre"] == "MONEDERO" :
                                monedero = self.obtenerArchivosProducto(i,400)
                           # elif self.productos[i["idProducto"]] == action_recarga[3]:
                            #    self.bitacora.escribir("BPD Recarga")
                        
                        print(credito)
                        print(monedero)
                        #Verificamos listas sobre productos
                        
                        if perfil["credito"] and not isinstance(credito["valor"],bool):
                            if credito and monedero:
                                
                                #if self.verificarAccionesLAP_R(monedero):    
                                    #Obtenemos los productos de nuevo si se aplicaron cambios por las listas
                                #    credito = self.obtenerArchivosProducto(credito)
                                #    monedero = self.obtenerArchivosProducto(monedero)

                                
                                self.bitacora.escribir("Validados correctamente")
                        
                                #Verficando que los productos esten en orden, que no exista saldo en el monedero si el credito es diferente de 0
                                if credito["valor"] < 0  and monedero["valor"]== 0 :
                                    self.bitacora.escribir("Realizando recarga")
                                    
                                    #Calcular si se recargara el siguiente producto
                                    restante = credito["valor"] + montoRecarga
                                    
                                    self.bitacora.escribir("Restante")
                                    self.bitacora.escribir(restante)

                                    #Si el restante es mayor a 0 acreditar la substraccion del montoRecarga - restante
                                    if restante > 0 :
                                        self.bitacora.escribir("Recargando Credito")
                                        retornar = {
                                            "uid" : "".join(self.card["id"]),
                                            "idProducto" : credito["idProducto"],
                                            "saldoInicial" : credito["valor"],
                                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                            "monto": montoRecarga-restante,
                                            "perfil": perfil["codigoPerfil"],
                                            "idSAM" : self.idSam,
                                            "tipoRecarga": 1,
                                            "saldoFinal": 0
                                        }
                                        if self.acreditar((montoRecarga - restante), credito, self.productos[credito["idProducto"]]["recarga"]["llavePICC"], self.productos[credito["idProducto"]]["recarga"]["llaveSAM"]) != None :
                                            self.bitacora.escribir("Haciendo Commit")
                                            if self.commit(800):
                                                self.bitacora.escribir("Escribiendo Archivos")
                                                #Escribir archivos
                                                self.obtenerConsecutivo(self.productos[credito["idProducto"]]["recarga"]["llaveSAM"])
                                                retornar["consecutivoSAM"] = self.consecutivoSam
                                                retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"] + 1
                                                resultado["eventos"].append(retornar)
                                                self.bitacora.escribir("Escribiendo evento")
                                                self.escribirArchivosRecarga(credito, montoRecarga - restante)
                                                sleep(2)
                                                recargaMonedero = restante
                                                cambio = 0
                                                if restante <= perfil["productos"][monedero["idProducto"]]["maximoValor"]:
                                                    self.bitacora.escribir("Recargando Monedero")
                                                    retornar = {
                                                        "uid" : "".join(self.card["id"]),
                                                        "idProducto" : monedero["idProducto"],
                                                        "saldoInicial" : monedero["valor"],
                                                        "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                                        "monto": restante,
                                                        "perfil": perfil["codigoPerfil"],
                                                        "idSAM" : self.idSam,
                                                        "tipoRecarga": 1,
                                                        "saldoFinal": restante
                                                    }
                                                else:
                                                    cambio = restante + monedero["valor"] - perfil["productos"][monedero["idProducto"]]["maximoValor"]
                                                    recargaMonedero = perfil["productos"][monedero["idProducto"]]["maximoValor"] -  monedero["valor"]
                                                    self.bitacora.escribir("Recargando Monedero")
                                                    retornar = {
                                                        "uid" : "".join(self.card["id"]),
                                                        "idProducto" : monedero["idProducto"],
                                                        "saldoInicial" : monedero["valor"],
                                                        "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                                        "monto": recargaMonedero,
                                                        "perfil": perfil["codigoPerfil"],
                                                        "idSAM" : self.idSam,
                                                        "tipoRecarga": 1,
                                                        "saldoFinal": 0 + recargaMonedero
                                                    }

                                                if self.acreditar(recargaMonedero, monedero, self.productos[monedero["idProducto"]]["recarga"]["llavePICC"], self.productos[monedero["idProducto"]]["recarga"]["llaveSAM"]) != None :
                                                    if self.commit(800):
                                                        self.obtenerConsecutivo(self.productos[monedero["idProducto"]]["recarga"]["llaveSAM"])
                                                        retornar["consecutivoSAM"] = self.consecutivoSam
                                                        retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"] + 1
                                                        resultado["eventos"].append(retornar)
                                                        self.escribirArchivosRecarga(monedero, recargaMonedero)
                                                        self.card = {}
                                                        resultado["codigo"] = "RECARGA_MIXTA"
                                                        resultado["cambio"] = cambio
                                                        return resultado
                                                #Una vez que ya se hizo la recarga del credito se retorna como una recarga incompleta
                                            resultado["codigo"] = "RECARGA_CREDITO"
                                            resultado["cambio"] = restante
                                            self.card = {}
                                            return resultado
                                    
                                    #Si el restante no es mayor a cero, entonces recargar el monto total de la recarga al credito
                                    else:
                                        self.bitacora.escribir("Recargando Credito")
                                        retornar = {
                                            "uid" : "".join(self.card["id"]),
                                            "idProducto" : credito["idProducto"],
                                            "saldoInicial" : credito["valor"],
                                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                            "monto": montoRecarga,
                                            "perfil": perfil["codigoPerfil"],
                                            "idSAM" : self.idSam,
                                            "tipoRecarga": 1,
                                            "saldoFinal": credito["valor"] + montoRecarga
                                        }
                                        if self.acreditar(montoRecarga, credito, self.productos[credito["idProducto"]]["recarga"]["llavePICC"], self.productos[credito["idProducto"]]["recarga"]["llaveSAM"]) != None :                                    
                                            #Escrbiri archivos
                                            self.bitacora.escribir("Haciendo commit")
                                            if self.coupler.commit(800):
                                                self.obtenerConsecutivo(self.productos[credito["idProducto"]]["recarga"]["llaveSAM"])
                                                retornar["consecutivoSAM"] = self.consecutivoSam
                                                retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"] + 1
                                                resultado["eventos"].append(retornar)
                                                self.escribirArchivosRecarga(credito, montoRecarga)
                                                self.card= {}
                                                resultado["codigo"] = "RECARGA_CREDITO"
                                                return resultado
                                elif credito["valor"] == 0  and monedero["valor"]>= 0:
                                    self.bitacora.escribir("Recargando Monedero")
                                    cambio = 0
                                    recargaMonedero = montoRecarga
                                    if montoRecarga + monedero["valor"]  <= perfil["productos"][monedero["idProducto"]]["maximoValor"]:
                                        retornar = {
                                            "uid" : "".join(self.card["id"]),
                                            "idProducto" : monedero["idProducto"],
                                            "saldoInicial" : monedero["valor"],
                                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                            "monto": montoRecarga,
                                            "perfil": perfil["codigoPerfil"],
                                            "idSAM" : self.idSam,
                                            "tipoRecarga": 1,
                                            "saldoFinal": monedero["valor"] + montoRecarga
                                        }
                                    else:
                                        """
                                        cambio = montoRecarga + monedero["valor"] - perfil["productos"][monedero["idProducto"]]["maximoValor"]
                                        recargaMonedero = montoRecarga - cambio
                                        retornar = {
                                            "uid" : "".join(self.card["id"]),
                                            "idProducto" : monedero["idProducto"],
                                            "saldoInicial" : monedero["valor"],
                                            "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                            "monto": recargaMonedero,
                                            "perfil": perfil["codigoPerfil"],
                                            "idSAM" : self.idSam,
                                            "tipoRecarga": 1,
                                            "saldoFinal": monedero["valor"] + recargaMonedero
                                        }
                                        """
                                        resultado["codigo"] = "Error al recargar"
                                        resultado["cambio"] = montoRecarga
                                        return resultado
                                    
                                    if self.acreditar(recargaMonedero, monedero, self.productos[monedero["idProducto"]]["recarga"]["llavePICC"], self.productos[monedero["idProducto"]]["recarga"]["llaveSAM"]) != None :
                                        if self.commit(800):
                                            self.obtenerConsecutivo(self.productos[monedero["idProducto"]]["recarga"]["llaveSAM"])
                                            retornar["consecutivoSAM"] = self.consecutivoSam
                                            retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"] + 1
                                            resultado["eventos"].append(retornar)
                                            self.escribirArchivosRecarga(monedero, recargaMonedero)
                                            self.card = {}
                                            resultado["codigo"] = "RECARGA_MONEDERO"
                                            resultado["cambio"] = cambio
                                            return resultado                
                                        
                        elif not perfil["credito"] and monedero:
                            self.bitacora.escribir("Recargando Monedero")
                            cambio = 0
                            recargaMonedero = montoRecarga
                            if montoRecarga + monedero["valor"]  <= perfil["productos"][monedero["idProducto"]]["maximoValor"]:
                                retornar = {
                                    "uid" : "".join(self.card["id"]),
                                    "idProducto" : monedero["idProducto"],
                                    "saldoInicial" : monedero["valor"],
                                    "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                    "monto": montoRecarga,
                                    "perfil": perfil["codigoPerfil"],
                                    "idSAM" : self.idSam,
                                    "tipoRecarga": 1,
                                    "saldoFinal": monedero["valor"] + montoRecarga
                                }
                            else:
                                cambio = montoRecarga + monedero["valor"] - perfil["productos"][monedero["idProducto"]]["maximoValor"]
                                recargaMonedero = montoRecarga - cambio
                                retornar = {
                                    "uid" : "".join(self.card["id"]),
                                    "idProducto" : monedero["idProducto"],
                                    "saldoInicial" : monedero["valor"],
                                    "fechaHora": datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S"),
                                    "monto": recargaMonedero,
                                    "perfil": perfil["codigoPerfil"],
                                    "idSAM" : self.idSam,
                                    "tipoRecarga": 1,
                                    "saldoFinal": monedero["valor"] + recargaMonedero
                                }
                                
                            if recargaMonedero > 0:
                                
                                if self.acreditar(recargaMonedero, monedero, self.productos[monedero["idProducto"]]["recarga"]["llavePICC"], self.productos[monedero["idProducto"]]["recarga"]["llaveSAM"]) != None :
                                    
                                    if self.commit(800):
                                        self.obtenerConsecutivo(self.productos[monedero["idProducto"]]["recarga"]["llaveSAM"])
                                        retornar["consecutivoSAM"] = self.consecutivoSam
                                        retornar["consecutivoAplicacion"] = self.card["estado"]["consecutivoAplicacion"] + 1
                                        resultado["eventos"].append(retornar)
                                        self.escribirArchivosRecarga(monedero, recargaMonedero)
                                        self.card = {}
                                        resultado["codigo"] = "RECARGA_MONEDERO"
                                        resultado["cambio"] = cambio
                                        return resultado
                            else:
                                self.bitacora.escribir("Tarjeta con limite de saldo")
                                return { "eventos": [], "codigo": "Error al recargar"}
                else:
                    return { "eventos": [], "codigo": validado}
            return { "eventos": [], "codigo": "ERROR_LEER_TARJETA"}
        except Exception as e:
            self.bitacora.escribir("Error al realizar la recarga: " + str(e))
            self.card= {}
            return   { "eventos": [], "codigo": "ERROR_LEER_TARJETA"}


    def compra(self):
        
      
        
        """
        Funcion para realizar validaciones de tarjeta
        Retorna un diccionario con el resultado de la operacion
        """
        try:
            
            #Se verifica que haya tarjeta y se valida
            self.card["id"] = self.hayTarjeta() 
            validado = self.validacion(400)
            if validado == "OK":
                
                self.bitacora.escribir("Leyendo Perfil")
                #Validando perfil del  Usuario
                self.card["perfil"] = self.leerUsuario()
                
                #Si es funcionario entonces hacer las opciones de funcionario
                self.bitacora.escribir("Validando Perfil")
                if  self.card["perfil"]:
                    
                    perfilesExist = list(True for x in self.perfiles if x["codigoPerfil"] == self.card["perfil"]["codigoPerfil"])
                    self.bitacora.escribir(perfilesExist)

                    
                    if not perfilesExist:
                        self.bitacora.escribir("Perfil Invalido")
                        self.coupler.endCommunication()
                        self.card= {}
                        respuesta = {"eventos": [], "codigo": "ERROR_VALIDANDO_TARJETA"}
                        return respuesta
                        
                    
                    if self.card["perfil"]["codigoPerfil"] == 9:
                        self.funcionario()
                    else:
                        #self.bitacora.escribir(self.card["productos"])
                        #Obteniendo las caracteristicas del perfil
                        perfil = list(x for x  in self.perfiles if x["codigoPerfil"] == self.card["perfil"]["codigoPerfil"])[0]
                        
                        #Obtenemos los productos
                        self.bitacora.escribir("Obteniendo Productos")
                        self.card["productos"] = self.obtenerProductos(0)
                        self.bitacora.escribir("Productos Leidos")
                        self.bitacora.escribir(self.card["productos"])
                        
                        #Revisar si hay acciones pendientes
                        if self.verificarAccionesLAP_V(self.card["productos"]):
                             respuesta = {"eventos": [], "codigo": "CUSTOM_ERROR", "leyenda": "Producto suspendido"}
                             return respuesta
                            
                        
                        #Revisar si hay acciones pendientes
                        lapr,productoLapr = self.verificarAccionesLAP_R(self.card["productos"])
                        self.bitacora.escribir(lapr)   
                        
                        if lapr:
                            
                                self.bitacora.escribir("LAPR detected!")
                                
                                with open(self.montoLapr, 'w+') as amount:
                                    json.dump(lapr[2], amount)
                                    
                                #if os.path.exists(self.monto):
                                json.dump("REALIZANDO_RECARGA", open(self.documentoPantalla, 'w+'))
                                return {"codigo": "NO_TARJETA"}
                        
                        
    

                        #Si no existe una ventana de transbordo entonces entrar en el ciclo de uso de producto
                        respuesta = {"eventos": [], "codigo": "NO_TARJETA"}
                        self.bitacora.escribir("Entrando al bucle de productos")
                        
                        
                        
                        
                        for i in self.card["productos"]:
                            
                             
                            
                            #Validamos listas sobre productos
                             
                            
                            
                             
                             if self.productos[i["idProducto"]]["productoNombre"] not in ["MONEDERO","CREDITO","BPD1","BPD2"]:
                                self.bitacora.escribir("Producto Invalido en tarjeta")
                                self.coupler.endCommunication()
                                self.card= {}
                                respuesta = {"eventos": [], "codigo": "ERROR_VALIDANDO_TARJETA"}
                                return respuesta
                            
                             else:
                                 
                                 if self.productos[i["idProducto"]]["productoNombre"] == "MONEDERO":
                                     if perfil["credito"]:
                                         if self.card["productos"].index(i) < (len(self.card["productos"])-1):
                                             respuesta = self.realizarCobroMixto(i, self.card["productos"][-1], perfil)
                                        
                                         else:
                                             respuesta = self.realizarCobro(i, perfil)
                                    
                                     else:
                                         respuesta = self.realizarCobro(i, perfil)
                                         
                                     self.coupler.endCommunication()
                                     self.card= {}
                                     self.bitacora.escribir("Retornando respuesta")
                                     return respuesta
                                
                                 else:
                                     respuesta = self.realizarCobro(i, perfil)
                                   
                                     self.bitacora.escribir(str(respuesta))
                                     if  not respuesta["codigo"] in ["SALDO_INSUFICIENTE", "NO_TARJETA", "ERROR_VALIDANDO_PRODUCTO"]:
                                         self.coupler.endCommunication()
                                         self.card= {}
                                         self.bitacora.escribir("Retornando respuesta")
                                         return respuesta
                         
                                    
                        
                        return respuesta
            else:
               
                return {"eventos": [], "codigo": validado}
        except NameError:
            raise NameError("Acoplador no responde")
        except Exception as e:
            self.bitacora.escribir("Error realizando el uso de producto: " + str(e))
            self.coupler.endCommunication()
            self.card= {}
            return {"codigo": "NO_TARJETA"}
                            

    def buscarTarjeta(self):
        """
        Funcion para buscar una tarjeta y obtener sus caracteristicas
        Retorna un diccionario con las propiedades de la tarjeta
        """
        try:
            self.card["id"] = self.hayTarjeta()
            validado = self.validacion()
            if validado == "OK":
                
                self.bitacora.escribir("Leyendo Perfil")
                #Validando perfil del  Usuario
                self.card["perfil"] = self.leerUsuario()
                
                #Si es funcionario entonces hacer las opciones de funcionario
                self.bitacora.escribir("Validando Perfil")
                if  self.card["perfil"]:
                    if self.card["perfil"]["codigoPerfil"] == 9:
                        self.funcionario()
                    else:
                        perfil = list(x for x  in self.perfiles if x["codigoPerfil"] == self.card["perfil"]["codigoPerfil"])[0]
                        #Obtenemos los productos
                        self.bitacora.escribir("Obteniendo Productos")
                        self.card["productos"] = self.obtenerProductos(0)
                        self.bitacora.escribir("Productos Leidos")
                        
                        monedero = False
                        credito = False
                        print(self.card["perfil"])
                        for i in self.card["productos"]:
                            #Si el producto es el credito
                            if self.productos[i["idProducto"]]["productoNombre"] == "CREDITO" and perfil["credito"] :
                                credito = self.obtenerArchivosProducto(i)
                            elif self.productos[i["idProducto"]]["productoNombre"] == "MONEDERO" :
                                monedero = self.obtenerArchivosProducto(i)

                        if monedero and credito and perfil["credito"] and not isinstance(credito["valor"],bool) and not isinstance(monedero["valor"],bool):
                            self.bitacora.escribir("Credito y Monedero validados")
                            if (monedero["valor"] == 0 and credito["valor"] <= 0) or (monedero["valor"] >= 0 and credito["valor"] == 0):
                                resultado = {"tarjeta": "".join(self.card["id"]), "saldo": monedero["valor"]+ credito["valor"], "codigo": "TARJETA_OK"}
                                self.coupler.endCommunication()
                                self.card= {}
                                return resultado
                            
                            resultado = {"tarjeta": "".join(self.card["id"]), "saldo": 0, "codigo": "TARJETA_ERROR"}
                            self.coupler.endCommunication()
                            self.card= {}
                            return resultado
                        elif monedero and not isinstance(monedero["valor"],bool) and not perfil["credito"]:
                            resultado = {"tarjeta": "".join(self.card["id"]), "saldo": monedero["valor"], "codigo": "TARJETA_OK"}
                            self.coupler.endCommunication()
                            self.card= {}    
                            return resultado
                
                resultado = {"tarjeta": "".join(self.card["id"]), "saldo":0, "codigo": "TARJETA_ERROR"}
                self.coupler.endCommunication()
                self.card= {}    
                return resultado
            else:
                return {"tarjeta": "", "codigo": validado}
        except Exception as e:
            self.bitacora.escribir("Error al leer la tarjeta: " + str(e))
            self.coupler.endCommunication()
            self.card= {}    
            return {"tarjeta": "", "codigo": "NO_TARJETA"}

    
    def leerAccesos(self):
        """
        Funcion para buscar y obtener informacion de una tarjeta MiFare
        Retorna un diccionario con los datos si encuentra una tarjeta, False en caso contrario
        """
        try:
            self.card["id"] = self.coupler.findMF()
            if self.card["id"]:
                if self.coupler.selectMF("".join(self.card["id"])):
                    datos = self.obtenerDatos()
                    if datos:
                        return datos
            return False
        except NameError :
            raise NameError("Acoplador no responde")
        except Exception as e:
            self.bitacora.escribir("Error al leer MIFare: " + str(e))
            return False

    def verEventos(self):
        """
        Funcion para visualizar los eventos de una tarjeta
        Retorna una lista con diccionario sobre los eventos
        """
        result = []
        self.card['id'] = self.hayTarjeta()
        validado = self.validacion()
        if validado == "OK":
            self.card["perfil"] = self.leerUsuario()
            if self.card["perfil"]["codigoPerfil"] == 9:
                return False
            else:
                self.card["eventos"] = self.leerTodosEventos(self.archivosInformativos["eventos"]['id'],64)
                #Se autentica solo para tarjetas de prueba, las de produccion no es necesario
                self.authenticar("02","03")
                self.card["productos"] = self.obtenerProductos(0)
                print("Saldos")
                for i in self.card["productos"]:
                    print(self.productos[i["idProducto"]]["productoNombre"])
                    print(i["valor"])

                print("Fecha y hora | Tipo de Evento | Monto de Evento | Dispositivo | Transporte")
                for i in self.card["eventos"]:
                    result.append((i["fechaHoraEvento"], " | ", self.tiposEvento[str(i["tipoEvento"])], " | ", float(i["montoEvento"]/100), " | ", self.dispositivos[str(i["idTipoDispositivo"])], " | ", self.transportes[str(i["tipoTransporte"])]))
                    print(i["fechaHoraEvento"], " | ", self.tiposEvento[str(i["tipoEvento"])], " | ", float(i["montoEvento"]/100), " | ", self.dispositivos[str(i["idTipoDispositivo"])], " | ", self.transportes[str(i["tipoTransporte"])] )
        self.card= {}
        return result
