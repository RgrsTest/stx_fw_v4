from datetime import datetime
from os import error
import sys
import serial
import crcmod
from time import sleep
from bitacora import Bitacora


class Coupler:
    """
    Clase que se comunica directamente con el acoplador
    """
    
    def __init__(self, path, serialPort="COM4", baudRate=115200, timeOut=4):
        """
        serialPort: string Puerto al que esta conectado el dispositivo, default COM7
        baudRate: int Velocidad de comunicacion en baudios, default es 115200
        timeout: int Tiempo de espera maxima del serial en segundos, default son 4 segundos
        """
        self.ser= serial.Serial(serialPort, baudRate, timeout=timeOut)
        self.escribir = Bitacora(path,"Validador")

        #Funciones auxiliares
        #############################

    def armarCmd(self,cmd):
        """
        Funcion para encapsular el comando
        Retorna una cadena de bytes 
        cmd: bytes El comando que sera empaquetado
        """
        tam = len(cmd)
        prefix = b"\x80" + bytes.fromhex(f"{tam:02x}")
        result = prefix + cmd +b"\x00"
        return result


    def calcular_crc(self,packet):
        """
        Funcion para calcular el crc de una cadena de bytes
        Retorna una cadena de bytes
        packet: bytes El comando empaquetado
        """
        crc16 = crcmod.mkCrcFun(0x11021, rev=True, initCrc=0x0000, xorOut=0xFFFF)

        convertido = crc16(packet)
        bites = convertido.to_bytes(2,'little')
        solo = packet + bites
        return solo

    ###########################################

    #Funciones de Comunicacion primitiva
    ##########################################
    def inicializar(self):
        """
        Funcion para inicializar el acoplador
        """
        if self.ser.isOpen():
            self.ser.write(b"\x00")
            self.escribir.escribir("Getting Software Version.....")
            response = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x01")))
            if len(response) == 0:
                self.escribir.escribir("Erro al iniciar acoplador")
                raise NameError("Acoplador no responde")    
            self.escribir.escribir("Inicializacion de Coupler exitosa!")
        else:
            raise NameError("Acoplador no responde")
        

    def sendCommand(self,cmd, espera=400):
        """
        Funcion para enviar el comando a traves del puerto serial
        Retorna una lista con la respuesta del serial en hexadecimales [00, 00, 00, ...]
        cmd: bytes Comando que sera enviado al serial
        espera: int Tiempo de espera para leer los datos en milisegundos, por default son 200
        """
        def completa(respuesta):
            tam = len(respuesta)
            if tam == 1:
                if respuesta[0] != 0x01:
                    return True
                return False
            elif tam > 1:
                if tam >= respuesta[1] + 5:
                    return True
            return False

        #self.escribir.escribir("Solicitando comando:")
        self.ser.flush()
        self.ser.write(cmd)
        tiempoEspera = 0
        respuestaCompleta = []
        response = False
        while tiempoEspera < espera and not response:
            parcial = self.ser.read_all()
            listaParcial = list(parcial)
            if len(listaParcial)> 0:
                respuestaCompleta += listaParcial
                response = completa(respuestaCompleta)
                tiempoEspera = 0
            else:
                sleep(0.005)
                tiempoEspera += 5
            

        respuesta = []
        if len(respuestaCompleta)>0:
            respuesta = ['{0:02x}'.format(i) for i in respuestaCompleta]
        
        self.escribir.escribir(str(respuesta))
        
        return respuesta

    def getCurrentSpeed(self):
        """
        Funcion para obtener la velocidad actual de la antena
        """
        datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x01\x01\x15")))
        self.escribir.escribir("Velocidad actual")
        self.escribir.escribir("".join(datos))
        
    ##############################################################

    #Funciones de tratamiento de datos recibidos
    ################################################################
    def enterHuntParams(self):
        """
        Funcion para configurar los parametros de Enter Hunt Phase
        """
        self.escribir.escribir("Enter Hunt Phase Params")
        self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x17\x01\x00\x00\x00\x01\x00\x00")))
        

    def thereIsCard(self):
        """
        Funcion para ejecutar el comando Enter Hunt Phase y determinar si existe una tarjeta
        Retorna una lista con el id de la tarjeta dividido en bytes o False en caso de que no haya tarjeta
        """
        try:
            self.escribir.escribir("Enter Hunt Phase")
            response = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x03\x00\x00\x00\x10\x00\x01\x01\x05")),300)
            if len(response)>0:
                if response[5] == "02":
                    return response[9:16]
            elif len(response) == 0 :
                raise NameError("Acoplador no responde")
        except NameError:
            self.escribir.escribir("No se recibio respuesta, reiniciemos todo")
            raise NameError("Acoplador no responde")
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False

    def selectApp(self, app):
        """
        Funcion para ejecutar el comando Select App
        Retorna True si la operacion fue exitosa o False en caso contrario
        app: bytes Direccion donde se encuentra la app (b'\x00')
        """
        try:
            #self.escribir.escribir("Select App")
            cmd = b"\x11\x03" + app
            response = self.sendCommand(self.calcular_crc(self.armarCmd(cmd)))
            if response[4] == "02":
                if response[5] == "91" and response[6] == "00":
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False

    def getFilesID(self):
        """
        Funcion para ejecutar el comando Get Files Id
        """
        response = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x1a\x16")))
        self.escribir.escribir(response)

    def getFileSettings(self, fileId):
        """
        Funcion para ejecutar el comando Get Files Settings
        Retorna una lista con la iformacion obtenida
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        """
        cmd =b"\x11\x1b"+fileId
        response = self.sendCommand(self.calcular_crc(self.armarCmd(cmd)))
        return response

    def readFileData(self,fileId, timeout=200):
        """
        Funcion para ejecutar el comando Read File Data
        Retorna una lista con la iformacion obtenida, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 200
        """
        try:
            self.escribir.escribir("Read File Data")
            cmd = b"\x11\x1e"+fileId+b"\x00\x00\x00\x00\x00"
            response = self.sendCommand(self.calcular_crc(self.armarCmd(cmd)), timeout)
            
            if response[6]== '91' and response[7] == '00':
                tam = int(response[8] + response[9], 16)
                res = list(response[10:tam+10])
                return res
            return False
        except Exception as e:
            self.escribir.escribir("Error enviando comando 11 1e: " + str(e))
            
            return False

    def readValueData(self,fileId, timeout=200):
        """
        Funcion para ejecutar el comando Get Value
        Retorna una lista con la iformacion obtenida, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 200
        """
        try:
            #self.escribir.escribir("Read Value Data")
            cmd = b"\x11\x1c" + fileId + b"\x00"
            response = self.sendCommand(self.calcular_crc(self.armarCmd(cmd)), timeout)
            if response[5]== '91' and response[6] == '00':
                res = response[7] + response[8] + response[9] + response[10]
                return res
            return False
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False
    
    def creditValue(self, fileId, monto, timeout=800):
        """
        Funcion para ejecutar el comando Credit
        Retorna True si la operacion fue exitosa, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        monto: bytes Monto en bytes que seran acreditados (b'\x00\')
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 500
        """
        try:
            #self.escribir.escribir("Credit Value")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x17" + fileId + b"\x01" + monto)), timeout)
            if datos[4] == "02":
                if datos[5] == "91" and datos[6] == "00":
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error enviando comando:" + str(e))
            return False
    
    def debitValue(self, fileId, monto):
        """
        Funcion para ejecutar el comando Debit
        Retorna True si la operacion fue exitosa, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        monto: bytes Monto en bytes que seran acreditados (b'\x00\')
        """
        try:
            #self.escribir.escribir("Debit Value")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x18" + fileId + b"\x01" + monto)),200)
            if datos[4] == "02":
                if datos[5] == "91" and datos[6] == "00":
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False

    def readRecordData(self, fileId, fromRecord, recordsToRead, tam):
        """
        Funcion para ejecutar el comando Read Record
        Retorna una lista con la informacion obtenida, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        fromRecord: bytes Byte desde el cual se iniciara a leer los registros  (b'\x00\')
        recordsToRead: bytes Cantidad de registros a leer en bytes (b'\x00\')
        tam: bytes Longitud de cada uno de los registros
        """
        try:
            #self.escribir.escribir("Read Record Data")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x1F" + fileId + b"\x00" + fromRecord + recordsToRead + tam)))
            lon = int(datos[4] + datos[5],16)
            if datos[6] == "91" and datos[7] == "00":
                return datos[8:(8+lon-1)]
            return False
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False

    def writeData(self, fileId, fromByte, data, timeout=300):
        """
        Funcion para ejecutar el comando Write Data
        Retorna True si la operacion fue exitosa, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        fromByte: bytes Byte desde el cual se iniciara a leer los datos  (b'\x00\')
        data: bytes Datos que seran escritos (b'\x00\')
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 300
        """
        try:
            self.escribir.escribir("Write Data")
            tam = len(data)
            tam = bytes.fromhex(f"{tam:04x}")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x20" + fileId + b"\x01" + fromByte + tam + data)), timeout)
            if datos[4] == "02":
                if datos[5] == "91" and datos[6] == "00":
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error escribiendo archivo"+ str(fileId)  +":"  + str(e))
            return False

    def writeRecord(self, fileId, fromRecord, bytesToWrite, data, timeout=300):
        """
        Funcion para ejecutar el comando Read Record
        Retorna True si la operacion fue exitosa, False en caso de error
        fileId: bytes Direccion de memoria del archivo que se quiere consultar (b'\x00\')
        fromRecord: bytes Byte desde el cual se iniciara a escribir los registros  (b'\x00\')
        bytesToWrite: bytes Cantidad de bytes que seran escritos (b'\x00\')
        data: bytes Datos que seran escritos (b'\x00\')
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 300
        """
        try:
            #self.escribir.escribir("Write Record")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x21" + fileId + b"\x01" + fromRecord + bytesToWrite + data)),timeout)
            if datos[4] == "02":
                if datos[5] == "91" and datos[6] == "00":
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error escrbiendo evento comando:" + str(e))
            return False
    

    def commit(self, timeout=500):
        """
        Funcion para ejecutar el comando Commit
        Retorna True si la operacion fue exitosa, False en caso de error
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 500
        """
        try:
            #self.escribir.escribir("Commit")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x09")),timeout)
            if datos[4] == "02":
                if datos[5] == "91" and (datos[6] == "00" or datos[6] == "0c"):
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error haciendo commit:" + str(e))
            return False

    def switchOffAntenna(self):
        """
        Funcion para apagar la antena
        """
        datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x0e\x00\x00\x01\x00")))

    def switchOnAntenna(self):
        """
        Funcion para Encender la antena
        """
        datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x0e\x00\x01\x01\x00")))

    def endCommunication(self):
        """
        Funcion para terminar la comunicacion
        """
        self.escribir.escribir("Terminando la comunicacion")
        datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x04\x01")))


    ####################################################################################################################
    #Funciones para SAM ######################################## Inicio ################################################
    ####################################################################################################################

    def selectSAM(self, slot):
        """
        Funcion para seleccionar un SAM que sera usado en la aplicacion
        slot: bytes Numero de SAM seleccionada 
        """
        self.escribir.escribir("Select SAM")
        datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x19"+ slot + b"\x02")))
        if datos:
            self.escribir.escribir(datos)

    #Esta funcion solo se usa cuando se enciende el modulo
    def resetSAM(self,slot = b"\x00"):
        """
        Funcion para resetear e inicializar un SAM que sera usado en la aplicacion
        Retorna True si la operacion fue exitosa, False en caso contrario
        slot: bytes Numero de SAM seleccionada 
        """
        try:
            self.escribir.escribir("Reset SAM")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x13"+ slot + b"\x00\x01")),500)
            if datos[4] != "FF":
                return True
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False
        
    
    def SAMGetKucEntry(self,  referenceKey, samSlot=b"\x00",):
        """
        Funcion para obtener el contador de la sam
        Retorna un string representando el numero actual del contador en bytes, False en caso de error
        referenceKey: bytes Numero de contador a solicitar (b"\x00")
        samSlot: bytes Numero de sam a consultar, por defecto es 00 (b"\x00")
        """
        try:
            #self.escribir.escribir("SAM Get Kuc Entry")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x14" + samSlot  + b"\x06\x80\x6C" + referenceKey + b"\x00\x00\x03")))
            if datos[4] == "00":
                if datos[5] == "0d":
                    result = datos[12:16]

                    #Los datos vienen al reves, se deben de reversear
                    result.reverse()
                    return "".join(result)
            return False
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False
    
    
    def authenticateEV1(self, card, keyPicc, keySam, div, timeout=500):
        """
        Funcion para autenticar EV1
        Retorna True si la operacion fue exitosa, False en caso contrario
        card: bytes Id de la tarjeta para la diversificacion 
        keyPicc: bytes Numero de llave usada para autenticar en la tarjeta
        KeySam: bytes Numero de llave usada para autenticar en el SAM
        div: bytes Padding de diversificacion
        timeout: int Tiempo de espera para recibir la peticion, expresado en milisegundos, por default es 200
        """
        try:
            self.escribir.escribir("Authenticate EV1")
            diver = card +div
            tam = bytes.fromhex(f"{len(diver):02x}")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x30"+ keyPicc + b"\x11" + keySam + b"\x00\x02"+tam + diver)),timeout)
            if datos[4] == "02":
                if datos[5] == "90" and datos[6] == "00":
                    return True
            return False
        except Exception as e:
            self.escribir.escribir("Error enviando comando 11 30 :" + str(e))
            return False

    def SAMSetPPS(self):
        datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x27\x01\x18")))
        self.escribir.escribir("PPS seteado")
        self.escribir.escribir("".join(datos))

    def SAMGetVersion(self):
        """
        Funcion para obtener informacion acerca del SAM
        Retorna un string con los datos si la operacion fue exitosa, False en caso contrario
        """
        try:
            self.escribir.escribir("SAM Get Version")
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x11\x22")),500)
            if datos[5] == "90" and datos[6] == "00":
                return "".join(datos[22:29])
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False

    
    #################################################################################################################
    ################### Funciones para leer MIFARE Classic ##########################################################
    #################################################################################################################


    def findMF(self):
        """
        Funcion para buscar tarjetas MiFare Classic
        Retorna una lista con el uid de la tarjeta si encontro una, en caso contrario envia False
        """
        try:
            self.escribir.escribir("Enter Hunt Phase")
            response = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x01\x03\x00\x00\x00\x10\x00\x01\x01\x05")),100)
            self.escribir.escribir(str(response))
            if len(response)>0:
                if response[5] == "08" :
                    return response[9:13]
                return False
            elif len(response) == 0 :
                raise NameError("Acoplador no responde")
        except NameError:
            raise 
        except Exception as e:
            self.escribir.escribir("Error enviand comando:" + str(e))
            return False

    def selectMF(self, uid):
        """
        Funcion para seleccionar el tag MiFare Classic con el que se va a trabajar
        Retorna True si la operacion fue exitosa o False en caso contrario
        uid: string UID de la tarjeta que se quiere seleccionar
        """
        try:
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x02" + bytes.fromhex(uid))))
            if datos[4] == "06" and datos[5] == "00":
                return True
            return False
        except Exception as e:
            self.escribir.escribir("Error al enviar el comando: " + str(e))
            return False

    def authenticateMF(self, keyPICC, sector, keyPCD):
        """
        Funcion para autenticar un tag MiFare
        Retorna True si la operacion fue exitosa, False en caso contrario
        keyPICC: string Tipo de la llave a usar en la tarjeta, puede ser 0A o 0B
        sector: string Sector de la tarjeta a leer
        keyPCD: string Index de la llave que se usara para autenticar
        """
        try:
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x05\03" + bytes.fromhex(keyPICC + sector + keyPCD))))
            self.escribir.escribir(str(datos))
            if datos[5]=="00" and datos[6]=="08":
                return True
            return False
        except Exception as e:
            self.escribir.escribir("Error al enviar el comando: " + str(e))
            return False
    
    def readBlockMF(self, block):
        """
        Funcion para leer un bloque de MiFare
        Retorna una lista con los datos del bloque si la operacion fue exitosa, False en caso contrario
        block: string Bloque que se quiere leer
        """
        try:
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x06\x01" + bytes.fromhex(block))))
            if datos[5] == "00":
                return datos[6:22]
            return False
        except Exception as e:
            self.escribir.escribir("Error al enviar el comando: " + str(e))
            return False

    def loadKey(self, key):
        """
        Funcion para cargar la llave en el buffer del lector
        key: string Valor de la llave que se desea cargar
        """
        try:
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x01\x07\x0B" + bytes.fromhex(key))))
            self.escribir.escribir(str(datos))
        except Exception as e:
            self.escribir.escribir("Error al enviar el comando: " + str(e))
    
    def setKey(self, index, key):
        """
        Funcion para agregar una llave al lector
        index: string Index al que se le asignara una llave
        key: string El valor de la llave
        """
        try:
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x01\x08\x06" +  bytes.fromhex( index + key))))
            self.escribir.escribir(str(datos))
        except Exception as e:
            self.escribir.escribir("Error al enviar el comando: " + str(e))

    def loadKeyFromStorage(self, index):
        """
        Funcion para cargar una llave desde la memoria al buffer del lector
        index: string Index de la llave guardada en la memoria
        """
        try:
            datos = self.sendCommand(self.calcular_crc(self.armarCmd(b"\x10\x01\x02\x04" +  bytes.fromhex( index))))
            print(datos)
        except Exception as e:
            self.escribir.escribir("Error al enviar el comando: " + str(e))
