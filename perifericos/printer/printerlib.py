from datetime import datetime
import serial
import json
from time import sleep
import puerto
from bitacora import Bitacora

class Printer:
	"""
	Clase para controlar la impresora
	"""
	def __init__(self, path):
		"""
		path: string Ruta del directorio raiz
		"""
		self.bitacora = Bitacora(path,"Printer")
		self.identidad = path + '/shared/config/identidad.json'
		self.operacion = path + '/shared/config/operacion.json'

	def start(self, port, baudRate):
		'''
		Funcion para iniciar la configuracion de la impresora
		port: string Puerto al que esta conectada la impresora
		baudRate: int Velocidad de transmision en baudio
		'''
		try:
			self.serial = serial.Serial(puerto.obtenerPuerto(port), baudrate= baudRate, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1)
			print(self.serial)
			sleep(1)
			self.serial.setDTR(True)
			sleep(1)
			self.serial.setDTR(False)
			self.setMinimun()
			sleep(1)
			return True
		except Exception as e:
			self.bitacora.escribir("Error abriendo el puerto: " + str(e))
			return False

	def setMinimun(self):
		"""
		Setear el tamaño minimo del papel
		"""
		self.serial.write(b'\x1F\x03\x0A\x04')

	def fullCorte(self):
		"""
		Funcion para cortar completamente
		"""
		self.serial.write(b'\x19')
	
	def partialCorte(self):
		"""
		Funcion para cortar parcial
		"""
		self.serial.write(b'\x1A')

	def feedPaper(self):
		"""
		Funcion para alimentar la impresora una linea
		"""
		self.serial.write(b'\x0A')
	
	def feedLines(self):
		"""
		Funcion para alimentar la impresora varias lineas
		"""
		self.serial.write(b'\x14\x04')

	def sendData(self, texto):
		"""
		Funcion para enviar datos a impresion
		texto: string Cadena de texto que sera impresa
		"""
		self.serial.write(texto)
	
	def enphized(self):
		"""
		Funcion para activar negritas en el texto
		"""
		self.serial.write(b'\x1B\x21\x08')

	def encabezado(self):
		"""
		Funcion para escribir el encabezado del ticket
		"""
		valores = json.load(open(self.identidad, 'r'))
		unidad = "UNIDAD: " + valores["idUnidad"]
		ruta = "RUTA: " + valores["ruta"]
		self.sendData(valores["empresa"].encode('utf-8'))
		self.feedPaper()
		self.sendData(unidad.encode('utf-8'))
		self.feedPaper()
		self.sendData(ruta.encode('utf-8'))
		self.feedPaper()

	def imprimirPagare(self, datos):
		"""
		Funcion para imprimir el pagare de efectivo
		datos: int|string Valor del monto que se debe de pagar
		"""
		try:
			self.enphized()
			self.encabezado()
			fecha = "FECHA: " + datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
			
			monto = str(datos)
			
			
			monto = "MONTO $" + str(monto/100)
			
			self.sendData("PAGARE DE EFECTIVO".encode('utf-8'))
			self.feedPaper()
			self.sendData(fecha.encode("utf-8"))
			self.feedPaper()
			self.sendData(monto.encode("utf-8"))
			self.feedPaper()
			self.sendData("COBRAR EN CAJA".encode('utf-8'))
			self.feedPaper()
			self.fullCorte()
		except Exception as e:
			self.bitacora.escribir("Error al tratar de imprimir: " + str(e))	

	def imprimirRecarga(self, datos):
		"""
		Funcion para imprimir ticket de recarga
		datos: dict Diccionario con los datos de la recarga
		"""
		try:
			self.enphized()
			self.encabezado()
			fecha = "FECHA: " + datos["eventos"][-1]["fechaHora"]
			
			monto= 0
			for i in datos["eventos"]:
				monto += i["monto"]
			
			
			monto = "MONTO $" + str(monto/100)
			
			codigo = "ID: " + str(datos["eventos"][-1]["uid"])
			saldoInicial = "SALDO INICIAL: $" + str(datos["eventos"][0]["saldoInicial"]/100)
			saldoFinal = "SALDO FINAL: $" + str(datos["eventos"][-1]["saldoFinal"]/100)
			
			self.sendData("RECARGA DE SALDO".encode('utf-8'))
			self.feedPaper()
			self.sendData(codigo.encode('utf-8'))			
			self.feedPaper()
			self.sendData(fecha.encode("utf-8"))
			self.feedPaper()
			self.sendData(saldoInicial.encode("utf-8"))
			self.feedPaper()
			self.sendData(monto.encode("utf-8"))
			self.feedPaper()
			self.sendData(saldoFinal.encode("utf-8"))
			self.feedPaper()
			self.sendData("COMPROBANTE DE RECARGA".encode('utf-8'))
			self.feedPaper()
			self.fullCorte()
		except Exception as e:
			self.bitacora.escribir("Error al tratar de imprimir: " + str(e))	

	def imprimirValidacion(self, datos):
		"""
		Funcion para imprimir el ticket de validacion con tarjeta
		datos: dict Diccionario con los datos de la validacion
		"""
		try:
			self.enphized()
			self.encabezado()
			fecha = "FECHA: " + datos["eventos"][-1]["fechaHora"]
			operacion = json.load(open(self.operacion, 'r'))

			monto= 0
			for i in datos["eventos"]:
				monto += i["monto"]
			
			if "BPD" in operacion["productos"][datos["eventos"][-1]["idProducto"]]["productoNombre"]:
				monto = "TARIFA: " + str(monto) + "PASAJE"
			else:
				monto = "TARIFA: $" + str(monto/100)
			
			codigo = "ID: " + str(datos["eventos"][-1]["uid"])
			saldoInicial = "SALDO INICIAL: $" + str(datos["eventos"][0]["saldoInicial"]/100)
			saldoFinal = "SALDO FINAL: $" + str(datos["eventos"][-1]["saldoFinal"]/100)
			
			self.sendData("PAGO CON TARJETA".encode('utf-8'))
			self.feedPaper()
			self.sendData(codigo.encode('utf-8'))			
			self.feedPaper()
			self.sendData(fecha.encode("utf-8"))
			self.feedPaper()
			self.sendData(saldoInicial.encode("utf-8"))
			self.feedPaper()
			self.sendData(monto.encode("utf-8"))
			self.feedPaper()
			self.sendData(saldoFinal.encode("utf-8"))
			self.feedPaper()
			self.sendData("GUARDE SU BOLETO".encode('utf-8'))
			self.feedPaper()
			self.fullCorte()
		except Exception as e:
			self.bitacora.escribir("Error al tratar de imprimir: " + str(e))

	
	def imprimirVenta(self, datos):
		"""
		Funcion para imprimir ticket de venta en efectivo
		datos: dict Diccionario con los datos de la venta
		"""
		try:
			self.enphized()
			self.encabezado()
			fecha = "FECHA: " + datos["fechaHora"]
			self.sendData(fecha.encode("utf-8"))
			self.feedPaper()
			self.sendData(f"TARIFA: ${(datos['monto']/100):.2f}".encode('utf-8'))
			self.feedPaper()
			self.sendData("PAGO EN EFECTIVO".encode("utf-8"))
			self.feedPaper()
			self.sendData("GUARDE SU BOLETO".encode("utf-8"))
			self.feedPaper()
			self.fullCorte()
		except Exception as e:
			self.bitacora.escribir("Error al tratar de imprimir: " + str(e))\

	def paperInformation(self):
		"""
		Funcion para obtener el estado del papel
		Retorna True si hay papel, False en caso contrario
		"""
		lectura = self.serial.write(b"\x1B\x76")
		sleep(0.1)
		leer = self.serial.read_all()
		if leer == b"\x00":
			return True
		return False