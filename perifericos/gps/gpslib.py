import json
import serial
import puerto
from bitacora import Bitacora
from time import sleep
import os

# El GPS abre 4 puertos ttyUSB y para abrir el gps se tiene que abrir el segundo o el tercero

class GPS:
    """
    Clase que controla al GPS
    """
    #Constructor
    def __init__(self, path):
        """
        path: string Ruta del directorio raiz
        """
        self.port = None
        self.estado = False
        self.bitacora = Bitacora(path, "Gps")
        
    #Funcion para verificar el estado
    def checkStatus(self):
        """
        Funcion para verificar el estado
        Retorna True si la conexion fisica esta establecida, False en caso contrario
        """
        return self.estado

    #Funcion para iniciar el proceso desde afuera
    def start(self, path):
        """
        Funcion para iniciar el proceso desde afuera
        path: string Ruta del archivo de configuracion
        Retorna True si fue exitoso, False en caso contrario
        """
        self.estado = self.inicializar(path)
        return self.estado

    #Funcion para inicializar la comunicacion con el periferico
    def inicializar(self, path):
        """
        Funcion para inicializar la comunicacion con el periferico
        path: string Ruta del archivo de configuracion
        Retorna True si el puerto se ha abierto, False en caso contrario
        """
        try:
            
            if os.path.exists(path):
                archivo = json.load(open(path, 'r'))
                port = puerto.obtenerPuerto(archivo["port"])
                self.serial = serial.Serial(port, 115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=1)
                self.bitacora.escribir("Puerto Abierto con exito")
                return True
            else:
                self.bitacora.escribir("Error al abrir el puerto: " + str(e))
                return False
            
            
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            return False
    
    
    #Funcion para encender la funcion de gps en el periferico
    def powerOn(self):
        """
        Funcion para encender la funcion de gps en el periferico
        Retorna True si la funcion fue encendida con exito, False en caso contrario
        """
        self.bitacora.escribir("Encendiendo GPS")
        try:
            #Escribimos el comnado en el serial para encender
            self.serial.write(b"AT+CGPS=1,1\r")
            sleep(1)

            #Escribimos el comando para consultar el estado
            self.serial.write(b"AT+CGPS?\r")
            sleep(1)
            
            #Leemos el serial y decodificamos
            response = self.serial.readline()
            response = response.decode("latin-1")
            
            #Si en la respuesta viene un 1,1 entonces inidica que si esta encendido
            if "1,1" in response:
                return True
            return False
        except Exception as e:
            return False
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            
            

    #Funcion para verificar que el gps esta encendido en el periferico
    def isOn(self):
        """
        Funcion para verificar que el gps esta encendido en el periferico
        Retorna True si esta encendido, False en caso contrario
        """
        try:
            self.serial.write(b"AT+CGPS?\r")
            sleep(1)
            while self.serial.in_waiting > 0:
                response = self.serial.readline()
                print(response)
                response = response.decode("latin-1")
                print(response)
                if "1,1" in response:
                    return True
            return False
        except Exception as e:
            self.bitacora.escribir("Error al abrir el puerto: " + str(e))
            

    #Funcion para obtener las coordenadas
    def getCoord(self):
        """
        Funcion para obtener las coordenadas
        Retorna una lista de la forma [longitud, latitud] si encuentra ubicacion, de lo contrario una lista con ceros [0,0]
        """
        #print("Getting Location...")
        try:
            self.serial.write(b"AT+CGPSINFO\r")
            sleep(1)
            response =b""
            while self.serial.in_waiting >0:
                response+= self.serial.readline()
            response = response.decode("latin-1")
            self.bitacora.escribir(response)
            inform = response[response.find(":")+2:]
            if "OK" in inform :
                coordenadas = inform[:inform.find('\r')]
                coordenadas = coordenadas.split(',')
                valores ={
                    "lat" : self.convertir(coordenadas[0], coordenadas[1]),
                    "long": self.convertir(coordenadas[2], coordenadas[3]),
                    "date": coordenadas[4],
                    "time": coordenadas[5],
                    "alt": coordenadas[6],
                    "speed": coordenadas[7],
                    "course": coordenadas[8]
                }
                return valores
            else:
                return False
        except Exception as e:
            self.bitacora.escribir("Error al obtener coordenadas: " + str(e))
            return False
    
    #Funcion para convertir la informacion en coordenadas del tipo, lat, lon
    def convertir(self, cantidad, cuadrante):
        """
        Funcion para convertir la informacion en coordenadas del tipo, lat, lon
        Retorna un valor numerico que indica la medida
        cantidad: float|string El numero flotante que indica la coordenada
        cuadrante: string Indica cada uno de los puntos cardinales N,S,E,W
        """
        cadena = str(cantidad)
        self.bitacora.escribir("cantidad" + str(cantidad))
        grados = cadena[:cadena.find('.')-2]
        convertido = int((float(cantidad[cantidad.find('.')-2:])*1000000)/60)
        print(grados)
        total = grados+"." + str(convertido)    
        if cuadrante == "N" or cuadrante == "E":
            return total
        else:
            return float("-"+total)

    #Funcion para cerrar la comunicacion serial
    def serialClose(self):
        """ Funcion para cerrar la comunicacion serial """
        self.serial.close()
